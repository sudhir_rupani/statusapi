<?php 
  $page_title="Manage Categories";
  include("includes/header.php");
	include("includes/connection.php");
	
	require("includes/function.php");
	require("language/language.php");

  if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != ""){
    $url = $_SERVER['HTTP_REFERER'];
  }else{
    $url = "manage_category.php";
  }


  if(isset($_POST['data_search']))
  {
      $keyword=htmlentities(trim($_POST['search_value']));
      $qry="SELECT * FROM tbl_category                   
            WHERE tbl_category.`category_name` LIKE '%$keyword%'
            ORDER BY tbl_category.`category_name`";

      $result=mysqli_query($mysqli,$qry); 

  }
  else
  {

      //Get all Category 
      $tableName="tbl_category";   
      $targetpage = "manage_category.php"; 
      $limit = 12; 

      $query = "SELECT COUNT(*) as num FROM $tableName";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];

      $stages = 3;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
      $start = ($page - 1) * $limit; 
      }else{
      $start = 0; 
      } 

      $qry="SELECT * FROM tbl_category
      ORDER BY tbl_category.`cid` DESC LIMIT $start, $limit";

      $result=mysqli_query($mysqli,$qry); 

  } 

  if(isset($_GET['cat_id']))
  { 

      $cat_res=mysqli_query($mysqli,'SELECT * FROM tbl_category WHERE cid='.$_GET['cat_id']);
      $cat_res_row=mysqli_fetch_assoc($cat_res);

      if($cat_res_row['category_image']!="")
      {
          unlink('images/'.$cat_res_row['category_image']);
          unlink('images/thumbs/'.$cat_res_row['category_image']);
      }

      Delete('tbl_category','cid='.$_GET['cat_id']);

      $_SESSION['msg']="12";
      header("Location: ".$url);
      exit;
  }

	 
?>
                
    <div class="row">
      <div class="col-xs-12">
        <div class="card mrg_bottom">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title"><?=$page_title?></div>
            </div>
            <div class="col-md-7 col-xs-12">
              <div class="search_list">
                <div class="search_block">
                  <form  method="post" action="">
                    <input class="form-control input-sm" placeholder="Search category..." type="search" name="search_value" value="<?=(isset($_POST['search_value'])) ? $keyword : ''?>" required>
                    <button type="submit" name="data_search" class="btn-search"><i class="fa fa-search"></i></button>
                  </form>  
                </div>
                <div class="add_btn_primary"> <a href="add_category.php?add=yes">Add Category</a> </div>
              </div>
            </div>
          </div>
           <div class="clearfix"></div>
          <div class="col-md-12 mrg-top">
            <div class="row">
              <?php 
                $i=0;
                while($row=mysqli_fetch_array($result))
                {         
              ?>
              <div class="col-lg-3 col-sm-4 col-xs-12">
                <div class="block_wallpaper add_wall_category" style="background-image: linear-gradient(to right, #<?=$row['start_color']?> , #<?=$row['end_color']?>);">           
                  <div class="wall_image_title" style="word-break: break-all;">
                    <h2><a href="javascript:void(0)"><?php echo $row['category_name'];?></a></h2>
                   
                    <ul> 

                      <?php if($row['show_on_home']!='0'){?>
                        <li>
                          <a href="javascript:void(0)" class="toggle_btn" data-id="<?php echo $row['cid'];?>" data-action="deactive" data-column="show_on_home" data-toggle="tooltip" data-tooltip="Show Home">
                              <i class="fa fa-home" style="color: green"></i>
                            </a>
                        </li>
                      <?php }else{?>
                        <li>
                          <a href="javascript:void(0)" class="toggle_btn" data-id="<?php echo $row['cid'];?>" data-action="active" data-column="show_on_home" data-toggle="tooltip" data-tooltip="Set On Home"><i class="fa fa-home"></i>
                            </a>
                        </li>
                      <?php }?>

                      <li><a href="add_category.php?cat_id=<?php echo $row['cid'];?>" data-toggle="tooltip" data-tooltip="Edit"><i class="fa fa-edit"></i></a></li>               
                      <li><a href="?cat_id=<?php echo $row['cid'];?>" data-toggle="tooltip" data-tooltip="Delete" onclick="return confirm('Are you sure you want to delete this category?');"><i class="fa fa-trash"></i></a></li>
                      
                      <?php if($row['status']!="0"){?>
                      <li><div class="row toggle_btn"><a href="javascript:void(0)" data-id="<?php echo $row['cid'];?>" data-action="deactive" data-column="status" data-toggle="tooltip" data-tooltip="ENABLE"><img src="assets/images/btn_enabled.png" alt="wallpaper_1" /></a></div></li>

                      <?php }else{?>
                      
                      <li><div class="row toggle_btn"><a href="javascript:void(0)" data-id="<?=$row['cid']?>" data-action="active" data-column="status" data-toggle="tooltip" data-tooltip="DISABLE"><img src="assets/images/btn_disabled.png" alt="wallpaper_1" /></a></div></li>
                  
                      <?php }?>

                    </ul>
                  </div>
                  <span><img src="images/<?php echo $row['category_image'];?>" style="padding-top:20px;padding-bottom:60px" /></span>
                </div>
              </div>
          <?php
            
            $i++;
              }
        ?>     
               
      </div>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="pagination_item_block">
              <nav>
                <?php if(!isset($_POST["data_search"])){ include("pagination.php");}?>
              </nav>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>     

<script type="text/javascript">

  $(".toggle_btn a, a.toggle_btn").on("click",function(e){
    e.preventDefault();

    var _for=$(this).data("action");

    
    var _id=$(this).data("id");
    var _column=$(this).data("column");
    var _table='tbl_category';

    $.ajax({
      type:'post',
      url:'processData.php',
      dataType:'json',
      data:{id:_id,for_action:_for,column:_column,table:_table,'action':'toggle_status','tbl_id':'cid'},
      success:function(res){
          console.log(res);
          if(res.status=='1'){
            location.reload();
          }
        }
    });

  });
</script>
<?php if(isset($_SESSION['msg'])){?>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-12 col-sm-12">
          <script type="text/javascript">
            $('.notifyjs-corner').empty();
            $.notify(
              '<?php echo $client_lang[$_SESSION['msg']] ; ?>',
              { position:"top center",className: 'success'}
            );
          </script>
      </div>
    </div>
  </div>
<?php unset($_SESSION['msg']);}?> 
