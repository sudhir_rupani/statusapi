<?php 
  
  $page_title=(isset($_GET['language_id'])) ? 'Edit Language' : 'Add Language';
  include("includes/header.php");

  require("includes/function.php");
  require("language/language.php");

  require_once("thumbnail_images.class.php");

  if(isset($_GET['language_id']))
  {
       
      $qry="SELECT * FROM tbl_language where id='".$_GET['language_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

  }

  if(isset($_POST['submit']) and isset($_GET['add']))
  {
  
       $language_image=rand(0,99999)."_".$_FILES['language_image']['name'];
       
       //Main Image
       $tpath1='images/'.$language_image;        
       $pic1=compress_image($_FILES["language_image"]["tmp_name"], $tpath1, 80);
   
       //Thumb Image 
       $thumbpath='images/thumbs/'.$language_image;   
       $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'400','200');  


	    $data = array(
	         'language_name'=>addslashes(trim($_POST['language_name'])),
	          'language_image'  =>  $language_image
	    );  

	    $qry = Insert('tbl_language',$data); 

	    $_SESSION['msg']="10";
	    header( "Location:manage_language.php");
	    exit; 
  }

  if(isset($_POST['submit']) and isset($_POST['language_id']))
  {
     
     if($_FILES['language_image']['name']!="")
     {    


          $img_res=mysqli_query($mysqli,'SELECT * FROM tbl_language WHERE id='.$_GET['language_id'].'');
          $img_res_row=mysqli_fetch_assoc($img_res);
      
          if($img_res_row['language_image']!="")
            {
              unlink('images/thumbs/'.$img_res_row['language_image']);
              unlink('images/'.$img_res_row['language_image']);
           }

           $language_image=rand(0,99999)."_".$_FILES['language_image']['name'];
       
             //Main Image
             $tpath1='images/'.$language_image;        
             $pic1=compress_image($_FILES["language_image"]["tmp_name"], $tpath1, 80);
         
          //Thumb Image 
           $thumbpath='images/thumbs/'.$language_image;   
           $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'400','200');

            $data = array(
              'language_name'  =>  $_POST['language_name'],
              'language_image'  =>  $language_image
            );

          $category_edit=Update('tbl_language', $data, "WHERE id = '".$_POST['language_id']."'");

     }
     else
     {

           $data = array(
                'language_name'  =>  $_POST['language_name']
            );  
 
               $category_edit=Update('tbl_language', $data, "WHERE id = '".$_POST['language_id']."'");

     }
 
    
    $_SESSION['msg']="11"; 
    header( "Location:add_language.php?language_id=".$_POST['language_id']);
    exit;
 
  }



?>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title"><?=$page_title?></div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
                 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?> 
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="addeditlanguage" method="post" class="form form-horizontal" enctype="multipart/form-data">
              <input  type="hidden" name="language_id" value="<?php echo $_GET['language_id'];?>" />

              <div class="section">
                <div class="section-body">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Language Title :-
                    
                    </label>
                    <div class="col-md-6">
                      <input type="text" name="language_name" placeholder="Enter language title" id="language_name" value="<?php  echo $row['language_name'];?>" class="form-control" required>
                    </div>
                  </div>
                 <div class="form-group">
                    <label class="col-md-3 control-label">Select Image :-
                      <p class="control-label-help">(Recommended resolution: W:400*H:200)</p>
                    </label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="language_image" value="fileupload" id="fileupload" accept=".png, .jpg, .jpeg" onchange="fileValidation()">
                          <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                      </div>
                      <div id="uploadPreview">
                        <?php if(isset($_GET['language_id']) and $row['language_image']!="") {?>
                          <img width="100%" src="images/<?php echo $row['language_image']?>" style="margin-bottom: 20px;border:1px solid #000"/>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>

<script type="text/javascript">
    function fileValidation(){
      var fileInput = document.getElementById('fileupload');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/i;
      if(!allowedExtensions.exec(filePath)){
          alert('Please upload file having extension .png, .jpg, .jpeg .PNG, .JPG, .JPEG only.');
          fileInput.value = '';
          return false;
      }else{
          //image preview
          if (fileInput.files && fileInput.files[0]) {

              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('uploadPreview').innerHTML = '<img src="'+e.target.result+'" style="width:100%;height:100%;margin-bottom: 20px;border:1px solid #000"/>';
              };
              reader.readAsDataURL(fileInput.files[0]);
          }
      }
    }
</script>