<?php 
  
  $page_title="Manage Languages";

  include("includes/header.php");
	require("includes/function.php");
	require("language/language.php");

  if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != ""){
    $url = $_SERVER['HTTP_REFERER'];
  }else{
    $url = "manage_users.php";
  }

  if(isset($_POST['data_search']))
  {

      $keyword=htmlentities(trim($_POST['search_value']));
      $qry="SELECT * FROM tbl_language                   
            WHERE tbl_language.`language_name` like '%$keyword%'
            ORDER BY tbl_language.`language_name`";

      $result=mysqli_query($mysqli,$qry); 

  }
  else
  { 

      $tableName="tbl_language";   
      $targetpage = "manage_language.php"; 
      $limit = 12; 

      $query = "SELECT COUNT(*) as num FROM $tableName";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];

      $stages = 3;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
      $start = ($page - 1) * $limit; 
      }else{
      $start = 0; 
      } 

      $qry="SELECT * FROM tbl_language
      ORDER BY tbl_language.id DESC LIMIT $start, $limit";

      $result=mysqli_query($mysqli,$qry); 

  } 

	if(isset($_GET['language_id']))
	{ 
  
    $id=$_GET['language_id'];
		$sql=mysqli_query($mysqli,'SELECT * FROM tbl_language WHERE id=$id');
		$row=mysqli_fetch_assoc($sql);

		if($row['language_image']!="")
    {
    	 unlink('images/'.$row['language_image']);
		   unlink('images/thumbs/'.$row['language_image']);

	  }
 
		Delete('tbl_language','id='.$id);

		$_SESSION['msg']="12";
    header("Location: ".$url);
		exit;
			
	}  
	 
?>
                
    <div class="row">
      <div class="col-xs-12">
        <div class="card mrg_bottom">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title"><?=$page_title?></div>
            </div>
            <div class="col-md-7 col-xs-12">
              <div class="search_list">
                <div class="search_block">
                  <form  method="post" action="">
                  <input class="form-control input-sm" placeholder="Search language..." type="search" name="search_value" value="<?=(isset($_POST['search_value'])) ? $keyword : ''?>" required>
                        <button type="submit" name="data_search" class="btn-search"><i class="fa fa-search"></i></button>
                  </form>  
                </div>
                <div class="add_btn_primary"> <a href="add_language.php?add=yes">Add Language</a> </div>
                
              </div>
            </div>
          </div>
           <div class="clearfix"></div>
          <div class="col-md-12 mrg-top">
            <div class="row">
              <?php 
              $i=0;
              while($row=mysqli_fetch_array($result))
              {         
              ?>
              <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="block_wallpaper add_wall_category" style="border-radius: 10px;box-shadow: 0px 2px 5px #999">           
                  <div class="wall_image_title">
                    <h2><a href="javascript:void(0)"><?php echo $row['language_name'];?></a></h2>
                    <ul> 

                      <li><a href="add_language.php?language_id=<?php echo $row['id'];?>" data-toggle="tooltip" data-tooltip="Edit"><i class="fa fa-edit"></i></a></li>        
                             
                      <li><a href="?language_id=<?php echo $row['id']; if(isset($_GET['page'])){ echo '&page='.$_GET['page'];}?>" data-toggle="tooltip" data-tooltip="Delete" onclick="return confirm('Are you sure you want to delete this language?');"><i class="fa fa-trash"></i></a></li>

                      <?php if($row['status']!="0"){?>
                        <li><div class="row toggle_btn"><a href="javascript:void(0)" data-id="<?php echo $row['id'];?>" data-action="deactive" data-column="status" data-toggle="tooltip" data-tooltip="ENABLE"><img src="assets/images/btn_enabled.png" alt="wallpaper_1" /></a></div></li>

                      <?php }else{?>
                      
                        <li><div class="row toggle_btn"><a href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="active" data-column="status" data-toggle="tooltip" data-tooltip="DISABLE"><img src="assets/images/btn_disabled.png" alt="wallpaper_1" /></a></div></li>
                  
                      <?php }?>


                    </ul>
                  </div>
                  <span><img src="images/<?php echo $row['language_image'];?>" /></span>
                </div>
              </div>
          <?php
            
            $i++;
              }
        ?>     
               
      </div>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="pagination_item_block">
              <nav>
                <?php if(!isset($_POST["data_search"])){ include("pagination.php");}?>
              </nav>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>  

<script type="text/javascript">

  $(".toggle_btn a").on("click",function(e){
    e.preventDefault();

    var _for=$(this).data("action");

    
    var _id=$(this).data("id");
    var _column=$(this).data("column");
    var _table='tbl_language';

    $.ajax({
      type:'post',
      url:'processData.php',
      dataType:'json',
      data:{id:_id,for_action:_for,column:_column,table:_table,'action':'toggle_status','tbl_id':'id'},
      success:function(res){
          console.log(res);
          if(res.status=='1'){
            location.reload();
          }
        }
    });

  });
</script>
<?php if(isset($_SESSION['msg'])){?>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-12 col-sm-12">
          <script type="text/javascript">
            $('.notifyjs-corner').empty();
            $.notify(
              '<?php echo $client_lang[$_SESSION['msg']] ; ?>',
              { position:"top center",className: 'success'}
            );
          </script>
      </div>
    </div>
  </div>
<?php unset($_SESSION['msg']);}?>      
