-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2020 at 12:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nilkanth_status_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`, `image`) VALUES
(1, 'admin', 'admin', 'nilkanthtech@gmail.com', 'profile.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `start_color` varchar(30) NOT NULL DEFAULT 'FF493B',
  `end_color` varchar(30) NOT NULL DEFAULT 'FFE245',
  `show_on_home` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`, `start_color`, `end_color`, `show_on_home`, `status`) VALUES
(8, 'Funny', '83030.png', 'EB8B60', 'C56185', 1, 1),
(10, 'Inspiration', '73539.png', 'EC6757', 'CB165D', 1, 1),
(12, 'Dancing', '54600.png', '3FBEA1', '1C5C9D', 1, 1),
(13, 'Romance', '65618.png', 'F9503C', 'E42C6A', 1, 1),
(14, 'Friendship', '97351.png', 'FF3E2E', 'FFA95C', 1, 1),
(20, 'Sad', '94954.png', '5E21FF', '987DFF', 0, 1),
(21, 'Love', '73774.png', 'C23156', '7F3581', 1, 1),
(26, 'Birthday', '69184.png', '32B2C2', '2A89DE', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comments`
--

CREATE TABLE `tbl_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment_text` text NOT NULL,
  `comment_on` varchar(150) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'video',
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_list`
--

CREATE TABLE `tbl_contact_list` (
  `id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` int(5) NOT NULL,
  `contact_msg` text NOT NULL,
  `created_at` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_sub`
--

CREATE TABLE `tbl_contact_sub` (
  `id` int(5) NOT NULL,
  `title` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_sub`
--

INSERT INTO `tbl_contact_sub` (`id`, `title`, `status`) VALUES
(2, 'Suspend', 1),
(3, 'Other', 1),
(4, 'Transaction', 1),
(5, 'Verification issue', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deleted_users`
--

CREATE TABLE `tbl_deleted_users` (
  `id` int(10) NOT NULL,
  `user_code` varchar(150) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `device_id` varchar(225) NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `total_video` int(10) NOT NULL DEFAULT 0,
  `total_image` int(10) NOT NULL DEFAULT 0,
  `total_gif` int(10) NOT NULL DEFAULT 0,
  `total_quote` int(10) NOT NULL DEFAULT 0,
  `total_point` int(10) NOT NULL DEFAULT 0,
  `pending_points` int(10) NOT NULL DEFAULT 0,
  `paid_points` int(10) NOT NULL DEFAULT 0,
  `total_followers` int(10) NOT NULL DEFAULT 0,
  `total_following` int(10) NOT NULL DEFAULT 0,
  `verify_status` int(3) NOT NULL,
  `registration_on` text NOT NULL,
  `auth_id` varchar(225) NOT NULL DEFAULT '0',
  `deleted_on` text NOT NULL,
  `deleted_by` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favourite`
--

CREATE TABLE `tbl_favourite` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_follows`
--

CREATE TABLE `tbl_follows` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `created_at` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_img_status`
--

CREATE TABLE `tbl_img_status` (
  `id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT 0,
  `cat_id` int(10) NOT NULL,
  `lang_ids` text NOT NULL,
  `image_title` varchar(150) NOT NULL,
  `image_tags` text NOT NULL,
  `image_layout` varchar(40) NOT NULL,
  `image_file` text NOT NULL,
  `total_download` int(10) NOT NULL DEFAULT 0,
  `total_likes` int(10) NOT NULL DEFAULT 0,
  `total_views` int(10) NOT NULL DEFAULT 0,
  `featured` int(2) NOT NULL DEFAULT 0,
  `status_type` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_img_status`
--

INSERT INTO `tbl_img_status` (`id`, `user_id`, `cat_id`, `lang_ids`, `image_title`, `image_tags`, `image_layout`, `image_file`, `total_download`, `total_likes`, `total_views`, `featured`, `status_type`, `status`) VALUES
(1, 0, 11, '6', 'Love Birds are Here !', 'Love birds, Kiss,Cute', 'Landscape', '94528_img_status.jpg', 0, 0, 0, 0, 'image', 1),
(3, 0, 21, '6', 'Valentine day Image 1', 'valentine day,valentine', 'Landscape', '19628_gif_status.gif', 0, 0, 0, 0, 'gif', 1),
(4, 0, 21, '6', 'Valentine day Image 2', 'Valentine ,Valentine Day', 'Landscape', '99463_gif_status.gif', 0, 0, 0, 0, 'gif', 1),
(5, 0, 21, '6', 'Hindi Valentine day Image', 'Hindi ,Valentine,Hindi Valentine', 'Portrait', '73971_gif_status.gif', 0, 0, 0, 0, 'gif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_image` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`id`, `language_name`, `language_image`, `status`) VALUES
(3, 'Arabic', '53477_unnamed.png', 1),
(4, 'Tamil', '22010_YZpHOsdtbsrfdoE-800x450-noPad.jpg', 1),
(5, 'Hindi', '45650_hindi-is-the-3rd-most-spoken-language-of-the-world.jpg', 1),
(6, 'English', '12146_800px-English_language.svg.png', 1),
(7, 'Gujarati', '86551_gujrati-icon.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_like`
--

CREATE TABLE `tbl_like` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `unlike` int(11) NOT NULL DEFAULT 0,
  `like_type` varchar(10) NOT NULL DEFAULT 'video'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_mode`
--

CREATE TABLE `tbl_payment_mode` (
  `id` int(5) NOT NULL,
  `mode_title` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_payment_mode`
--

INSERT INTO `tbl_payment_mode` (`id`, `mode_title`, `status`) VALUES
(1, 'Paypal', 1),
(2, 'PayTM', 1),
(3, 'Bank Detail', 1),
(5, 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quotes`
--

CREATE TABLE `tbl_quotes` (
  `id` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT 0,
  `lang_ids` text COLLATE utf8mb4_bin NOT NULL,
  `quote` longtext COLLATE utf8mb4_bin NOT NULL,
  `quote_font` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `quote_tags` text COLLATE utf8mb4_bin NOT NULL,
  `quote_bg` text COLLATE utf8mb4_bin NOT NULL,
  `total_likes` int(10) NOT NULL DEFAULT 0,
  `total_views` int(10) NOT NULL DEFAULT 0,
  `featured` int(2) NOT NULL DEFAULT 0,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `tbl_quotes`
--

INSERT INTO `tbl_quotes` (`id`, `cat_id`, `user_id`, `lang_ids`, `quote`, `quote_font`, `quote_tags`, `quote_bg`, `total_likes`, `total_views`, `featured`, `status`) VALUES
(1, 21, 0, '6', 'I love you. \r\nI knew it the minute I met you. \r\nI\\\'m sorry it took so long for me to catch up. \r\nI just got stuck\r\n❤️❤️❤️', 'Pacifico.ttf', 'I love you,VALENTINE\'S DAY', 'FF426D', 0, 0, 1, 1),
(4, 11, 0, '6', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'Roboto.ttf', '', 'ff9745', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reports`
--

CREATE TABLE `tbl_reports` (
  `id` int(11) NOT NULL,
  `post_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `report` text NOT NULL,
  `report_type` varchar(30) NOT NULL DEFAULT 'video',
  `report_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(5) NOT NULL,
  `envato_buyer_name` varchar(200) NOT NULL,
  `envato_purchase_code` text NOT NULL,
  `envato_buyer_email` varchar(100) NOT NULL,
  `envato_purchased_status` int(1) NOT NULL DEFAULT 0,
  `package_name` varchar(150) NOT NULL,
  `email_from` varchar(150) NOT NULL,
  `redeem_points` int(11) NOT NULL,
  `redeem_money` float(11,2) NOT NULL,
  `redeem_currency` varchar(100) NOT NULL,
  `minimum_redeem_points` int(11) NOT NULL,
  `onesignal_app_id` text NOT NULL,
  `onesignal_rest_key` text NOT NULL,
  `app_name` text NOT NULL,
  `app_logo` text NOT NULL,
  `app_email` varchar(150) NOT NULL,
  `app_version` varchar(60) NOT NULL,
  `app_author` varchar(150) NOT NULL,
  `app_contact` varchar(60) NOT NULL,
  `app_website` varchar(100) NOT NULL,
  `app_description` text NOT NULL,
  `app_developed_by` varchar(100) NOT NULL,
  `app_privacy_policy` text NOT NULL,
  `api_page_limit` int(11) NOT NULL,
  `api_all_order_by` varchar(30) NOT NULL,
  `api_latest_limit` int(3) NOT NULL,
  `api_cat_order_by` varchar(30) NOT NULL,
  `api_cat_post_order_by` varchar(30) NOT NULL,
  `publisher_id` text NOT NULL,
  `interstital_ad` text NOT NULL,
  `interstital_ad_type` varchar(30) NOT NULL,
  `registration_reward` int(50) NOT NULL,
  `app_refer_reward` int(50) NOT NULL,
  `video_views` int(5) NOT NULL,
  `video_add` int(11) NOT NULL,
  `like_video_points` int(11) NOT NULL,
  `download_video_points` int(11) NOT NULL,
  `registration_reward_status` varchar(20) NOT NULL DEFAULT 'true',
  `app_refer_reward_status` varchar(20) NOT NULL DEFAULT 'true',
  `video_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `video_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_video_points_status` varchar(20) NOT NULL DEFAULT 'false',
  `download_video_points_status` varchar(20) NOT NULL DEFAULT 'false',
  `other_user_video_status` varchar(10) NOT NULL,
  `other_user_video_point` varchar(10) NOT NULL,
  `image_add` int(5) NOT NULL DEFAULT 0,
  `image_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `image_views` int(5) NOT NULL DEFAULT 0,
  `image_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_image_point` int(5) NOT NULL DEFAULT 0,
  `other_user_image_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_image_points` int(5) NOT NULL DEFAULT 0,
  `like_image_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `download_image_points` int(5) NOT NULL DEFAULT 0,
  `download_image_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `gif_add` int(5) NOT NULL DEFAULT 0,
  `gif_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `gif_views` int(5) NOT NULL DEFAULT 0,
  `gif_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_gif_point` int(5) NOT NULL DEFAULT 0,
  `other_user_gif_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_gif_points` int(5) NOT NULL DEFAULT 0,
  `like_gif_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `download_gif_points` int(5) NOT NULL DEFAULT 0,
  `download_gif_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `quotes_add` int(5) NOT NULL DEFAULT 0,
  `quotes_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `quotes_views` int(5) NOT NULL DEFAULT 0,
  `quotes_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_quotes_point` int(5) NOT NULL DEFAULT 0,
  `other_user_quotes_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_quotes_points` int(5) NOT NULL DEFAULT 0,
  `like_quotes_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `interstital_ad_id` text NOT NULL,
  `interstital_ad_click` varchar(10) NOT NULL,
  `banner_ad` text NOT NULL,
  `banner_ad_type` varchar(30) NOT NULL,
  `banner_ad_id` text NOT NULL,
  `facebook_interstital_ad_id` text NOT NULL,
  `facebook_banner_ad_id` text NOT NULL,
  `rewarded_video_ads` varchar(20) NOT NULL,
  `rewarded_video_ads_id` text NOT NULL,
  `rewarded_video_click` int(3) NOT NULL DEFAULT 5,
  `app_faq` text NOT NULL,
  `otp_status` varchar(10) NOT NULL DEFAULT 'true',
  `watermark_on_off` varchar(20) NOT NULL DEFAULT 'false',
  `watermark_image` text DEFAULT NULL,
  `spinner_opt` varchar(10) NOT NULL DEFAULT 'Enable',
  `spinner_limit` int(10) NOT NULL DEFAULT 1,
  `default_youtube_url` text NOT NULL,
  `default_instagram_url` text NOT NULL,
  `auto_approve` varchar(10) NOT NULL,
  `auto_approve_img` varchar(10) NOT NULL DEFAULT 'off',
  `auto_approve_gif` varchar(10) NOT NULL DEFAULT 'off',
  `auto_approve_quote` varchar(10) NOT NULL DEFAULT 'off',
  `user_video_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_image_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_gif_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_quotes_upload_limit` int(10) NOT NULL DEFAULT 5,
  `video_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `image_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `gif_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `quotes_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `video_file_size` int(5) NOT NULL DEFAULT 1,
  `video_file_duration` int(10) NOT NULL DEFAULT 1,
  `image_file_size` int(5) NOT NULL DEFAULT 1,
  `gif_file_size` int(5) NOT NULL DEFAULT 1,
  `cat_show_home_limit` int(10) NOT NULL DEFAULT 1,
  `delete_note` text NOT NULL,
  `ad_on_spin` varchar(30) NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `envato_buyer_name`, `envato_purchase_code`, `envato_buyer_email`, `envato_purchased_status`, `package_name`, `email_from`, `redeem_points`, `redeem_money`, `redeem_currency`, `minimum_redeem_points`, `onesignal_app_id`, `onesignal_rest_key`, `app_name`, `app_logo`, `app_email`, `app_version`, `app_author`, `app_contact`, `app_website`, `app_description`, `app_developed_by`, `app_privacy_policy`, `api_page_limit`, `api_all_order_by`, `api_latest_limit`, `api_cat_order_by`, `api_cat_post_order_by`, `publisher_id`, `interstital_ad`, `interstital_ad_type`, `registration_reward`, `app_refer_reward`, `video_views`, `video_add`, `like_video_points`, `download_video_points`, `registration_reward_status`, `app_refer_reward_status`, `video_views_status`, `video_add_status`, `like_video_points_status`, `download_video_points_status`, `other_user_video_status`, `other_user_video_point`, `image_add`, `image_add_status`, `image_views`, `image_views_status`, `other_user_image_point`, `other_user_image_status`, `like_image_points`, `like_image_points_status`, `download_image_points`, `download_image_points_status`, `gif_add`, `gif_add_status`, `gif_views`, `gif_views_status`, `other_user_gif_point`, `other_user_gif_status`, `like_gif_points`, `like_gif_points_status`, `download_gif_points`, `download_gif_points_status`, `quotes_add`, `quotes_add_status`, `quotes_views`, `quotes_views_status`, `other_user_quotes_point`, `other_user_quotes_status`, `like_quotes_points`, `like_quotes_points_status`, `interstital_ad_id`, `interstital_ad_click`, `banner_ad`, `banner_ad_type`, `banner_ad_id`, `facebook_interstital_ad_id`, `facebook_banner_ad_id`, `rewarded_video_ads`, `rewarded_video_ads_id`, `rewarded_video_click`, `app_faq`, `otp_status`, `watermark_on_off`, `watermark_image`, `spinner_opt`, `spinner_limit`, `default_youtube_url`, `default_instagram_url`, `auto_approve`, `auto_approve_img`, `auto_approve_gif`, `auto_approve_quote`, `user_video_upload_limit`, `user_image_upload_limit`, `user_gif_upload_limit`, `user_quotes_upload_limit`, `video_upload_opt`, `image_upload_opt`, `gif_upload_opt`, `quotes_upload_opt`, `video_file_size`, `video_file_duration`, `image_file_size`, `gif_file_size`, `cat_show_home_limit`, `delete_note`, `ad_on_spin`) VALUES
(1, '', '', '', 0, 'com.example.status', '', 100, 1.00, 'USD', 10, '8ae91bea-8990-4b45-83e8-cc4e46478fcb', 'OWNjMmMxNjUtMWUzYy00ZTMzLWE0ODQtOTdjY2YxN2VmM2Fh', 'Status App', 'ic_launcher.png', 'nilkanthtech@gmail.com', '1.0.0', 'Viavi Webtech', '+91 9227777522', 'www.nilkanth.com', '<p>As Viavi Webtech is finest offshore IT company which has expertise in the below mentioned all technologies and our professional, dedicated approach towards our work has always satisfied our clients as well as users. We have reached to this level because of the dedication and hard work of our 10+ years experienced team as well as new ideas of freshers, they always provide the best solutions. Here are the promising services served by Viavi Webtech.</p>\r\n\r\n<p>Contact on Skype &amp; Email for more information.</p>\r\n\r\n<p><strong>Skype ID:</strong> support.nilkanth <strong>OR</strong> nilkanthtech<br />\r\n<strong>Email:</strong> info@nilkanth.com <strong>OR</strong> nilkanthtech@gmail.com<br />\r\n<strong>Website:</strong> <a href=\"http://www.nilkanth.com\">http://www.nilkanth.com</a><br />\r\n<br />\r\nOur Products : <em><strong><a href=\"https://codecanyon.net/user/nilkanthtech/portfolio?ref=nilkanthtech\">CODECANYON</a></strong></em></p>\r\n', 'Viavi Webtech', '<p><strong>We are committed to protecting your privacy</strong></p>\n\n<p>We collect the minimum amount of information about you that is commensurate with providing you with a satisfactory service. This policy indicates the type of processes that may result in data being collected about you. Your use of this website gives us the right to collect that information.&nbsp;</p>\n\n<p><strong>Information Collected</strong></p>\n\n<p>We may collect any or all of the information that you give us depending on the type of transaction you enter into, including your name, address, telephone number, and email address, together with data about your use of the website. Other information that may be needed from time to time to process a request may also be collected as indicated on the website.</p>\n\n<p><strong>Information Use</strong></p>\n\n<p>We use the information collected primarily to process the task for which you visited the website. Data collected in the UK is held in accordance with the Data Protection Act. All reasonable precautions are taken to prevent unauthorised access to this information. This safeguard may require you to provide additional forms of identity should you wish to obtain information about your account details.</p>\n\n<p><strong>Cookies</strong></p>\n\n<p>Your Internet browser has the in-built facility for storing small files - &quot;cookies&quot; - that hold information which allows a website to recognise your account. Our website takes advantage of this facility to enhance your experience. You have the ability to prevent your computer from accepting cookies but, if you do, certain functionality on the website may be impaired.</p>\n\n<p><strong>Disclosing Information</strong></p>\n\n<p>We do not disclose any personal information obtained about you from this website to third parties unless you permit us to do so by ticking the relevant boxes in registration or competition forms. We may also use the information to keep in contact with you and inform you of developments associated with us. You will be given the opportunity to remove yourself from any mailing list or similar device. If at any time in the future we should wish to disclose information collected on this website to any third party, it would only be with your knowledge and consent.&nbsp;</p>\n\n<p>We may from time to time provide information of a general nature to third parties - for example, the number of individuals visiting our website or completing a registration form, but we will not use any information that could identify those individuals.&nbsp;</p>\n\n<p>In addition Dummy may work with third parties for the purpose of delivering targeted behavioural advertising to the Dummy website. Through the use of cookies, anonymous information about your use of our websites and other websites will be used to provide more relevant adverts about goods and services of interest to you. For more information on online behavioural advertising and about how to turn this feature off, please visit youronlinechoices.com/opt-out.</p>\n\n<p><strong>Changes to this Policy</strong></p>\n\n<p>Any changes to our Privacy Policy will be placed here and will supersede this version of our policy. We will take reasonable steps to draw your attention to any changes in our policy. However, to be on the safe side, we suggest that you read this document each time you use the website to ensure that it still meets with your approval.</p>\n\n<p><strong>Contacting Us</strong></p>\n\n<p>If you have any questions about our Privacy Policy, or if you want to know what information we have collected about you, please email us at hd@dummy.com. You can also correct any factual errors in that information or require us to remove your details form any list under our control.</p>\n', 5, 'DESC', 0, 'category_name', 'DESC', 'pub-3940256099942544', 'true', 'admob', 5, 5, 1, 5, 2, 1, 'true', 'true', 'true', 'true', 'true', 'true', 'true', '1', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 1, 'true', 'ca-app-pub-3940256099942544/1033173712', '5', 'true', 'admob', 'ca-app-pub-3940256099942544/6300978111', '1554573358028971_1556332951186345', '1554573358028971_1556313221188318', 'true', 'ca-app-pub-3940256099942544/5224354917', 5, '<p><strong>How to earn points in video status app?</strong></p>\r\n\r\n<p>- When user views,like,download video or upload video then they will get reward points.</p>\r\n\r\n<p>- Share your reference code to others and get reward points for every user registered with your reference code.</p>\r\n\r\n<p>- When user registers in application they will get reward points</p>\r\n\r\n<p><strong>Note:-</strong> When user will upload video and when admin approves user video, after that user will get reward points.</p>\r\n\r\n<p><strong>Video Upload Guidance :-</strong></p>\r\n\r\n<p>- Please check that uploading video file name is in english and there is no space in video file name.</p>\r\n\r\n<p>- Please follow the instruction of video file size, duration and format.</p>\r\n\r\n<p><strong>How to claim reward points and earn money?</strong></p>\r\n\r\n<p>- User need to acquire minimum points to claim money from reward points.</p>\r\n\r\n<p>- To claim money from reward points user have to fill the form and if there is any mistake in form you submitted, then you have to fill Contact Us form and admin will contact user ASAP</p>\r\n\r\n<p>- When admin approves user&#39;s claim for money after that user will get money</p>\r\n\r\n<p><strong>Note :-</strong></p>\r\n\r\n<p>- When you share video status to any social media app, the length of the video will be depended on the app you are sharing.</p>\r\n\r\n<p>- Any misbehavior or any type of sexual and unnecessary video upload will make admin block your account.</p>\r\n\r\n<p>- Allow the read and write file permission then you will be able to use download, upload and share the video feature otherwise you will not able to use.</p>\r\n\r\n<p>- Share video works only on supported social media applications.</p>\r\n\r\n<p>- User can select payment method when filling form to claim money.&nbsp;</p>\r\n\r\n<p>- User can upload video only in mp4 format</p>\r\n\r\n<p>- Spamming report video feature may lead to account ban.</p>\r\n', 'true', 'true', 'watermark_1.png', 'true', 3, 'https://www.youtube.com/user/nilkanthtech', 'https://www.instagram.com/nilkanthtech/', 'on', 'on', 'on', 'on', 5, 5, 5, 5, 'true', 'true', 'true', 'true', 5, 30, 5, 5, 6, 'Your all status, earn points, pending points data will be deleted after click on Delete button.', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL DEFAULT 0,
  `slider_type` varchar(30) DEFAULT NULL,
  `slider_title` varchar(150) DEFAULT NULL,
  `external_url` text DEFAULT NULL,
  `external_image` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `post_id`, `slider_type`, `slider_title`, `external_url`, `external_image`, `status`) VALUES
(2, 5, 'gif', '', '', '', 1),
(3, 0, 'external', 'WhatsApp Video Status Saver application for android', 'https://codecanyon.net/user/nilkanthtech/portfolio', '87233_slider.png', 1),
(5, 1, 'quote', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_smtp_settings`
--

CREATE TABLE `tbl_smtp_settings` (
  `id` int(5) NOT NULL,
  `smtp_host` varchar(150) NOT NULL,
  `smtp_email` varchar(150) NOT NULL,
  `smtp_password` text NOT NULL,
  `smtp_secure` varchar(20) NOT NULL,
  `port_no` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_smtp_settings`
--

INSERT INTO `tbl_smtp_settings` (`id`, `smtp_host`, `smtp_email`, `smtp_password`, `smtp_secure`, `port_no`) VALUES
(1, '', '', '', 'tls', '587');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_spinner`
--

CREATE TABLE `tbl_spinner` (
  `block_id` int(5) NOT NULL,
  `block_points` varchar(5) NOT NULL,
  `block_bg` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_spinner`
--

INSERT INTO `tbl_spinner` (`block_id`, `block_points`, `block_bg`) VALUES
(1, '0', 'BE4EBA'),
(2, '1', '0DABE9'),
(3, '2', 'E0E92D'),
(4, '3', 'E94C1E'),
(5, '4', 'E91E06'),
(6, '5', '0B8945'),
(7, '6', '86E93B');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suspend_account`
--

CREATE TABLE `tbl_suspend_account` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `suspended_on` varchar(255) NOT NULL,
  `activated_on` int(11) DEFAULT NULL,
  `suspension_reason` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_image` varchar(500) DEFAULT NULL,
  `total_followers` int(11) NOT NULL DEFAULT 0,
  `total_following` int(11) NOT NULL DEFAULT 0,
  `user_youtube` varchar(500) DEFAULT NULL,
  `user_instagram` varchar(500) DEFAULT NULL,
  `confirm_code` varchar(255) DEFAULT NULL,
  `total_point` int(11) NOT NULL DEFAULT 0,
  `is_verified` int(1) NOT NULL DEFAULT 0,
  `player_id` text DEFAULT NULL,
  `is_duplicate` int(1) NOT NULL DEFAULT 0,
  `registration_on` varchar(255) NOT NULL DEFAULT '0',
  `auth_id` varchar(255) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_code`, `user_type`, `device_id`, `name`, `email`, `password`, `phone`, `user_image`, `total_followers`, `total_following`, `user_youtube`, `user_instagram`, `confirm_code`, `total_point`, `is_verified`, `player_id`, `is_duplicate`, `registration_on`, `auth_id`, `status`) VALUES
(0, 'adminsi5', 'Admin', '', 'Admin', 'admin@gmail.com', '123456', NULL, 'profile.png', 0, 0, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 105, 1, 'b7889592-0dc6-4685-8925-5e3227f9ebc1', 0, '0', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_redeem`
--

CREATE TABLE `tbl_users_redeem` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_points` int(11) NOT NULL,
  `redeem_price` float(11,2) NOT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `bank_details` text NOT NULL,
  `request_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cust_message` longtext DEFAULT NULL,
  `receipt_img` text DEFAULT NULL,
  `responce_date` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_rewards_activity`
--

CREATE TABLE `tbl_users_rewards_activity` (
  `id` int(10) NOT NULL,
  `user_id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `activity_type` varchar(255) NOT NULL,
  `points` int(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `redeem_id` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users_rewards_activity`
--

INSERT INTO `tbl_users_rewards_activity` (`id`, `user_id`, `post_id`, `activity_type`, `points`, `date`, `redeem_id`, `status`) VALUES
(3, 0, 32, 'Add Video', 5, '2020-02-19 10:53:39', 0, 1),
(4, 0, 31, 'Add Video', 5, '2020-02-19 10:53:39', 0, 1),
(5, 0, 30, 'Add Video', 5, '2020-02-19 10:53:40', 0, 1),
(6, 0, 29, 'Add Video', 5, '2020-02-19 10:53:40', 0, 1),
(7, 0, 28, 'Add Video', 5, '2020-02-19 10:53:41', 0, 1),
(8, 0, 27, 'Add Video', 5, '2020-02-19 10:53:41', 0, 1),
(9, 0, 26, 'Add Video', 5, '2020-02-19 10:53:42', 0, 1),
(10, 0, 25, 'Add Video', 5, '2020-02-19 10:53:42', 0, 1),
(11, 0, 24, 'Add Video', 5, '2020-02-19 10:53:43', 0, 1),
(12, 0, 23, 'Add Video', 5, '2020-02-19 10:53:43', 0, 1),
(13, 0, 22, 'Add Video', 5, '2020-02-19 10:53:44', 0, 1),
(14, 0, 21, 'Add Video', 5, '2020-02-19 10:53:44', 0, 1),
(16, 0, 1, 'Your quote is viewed', 1, '2020-02-19 10:54:20', 0, 1),
(18, 0, 4, 'Your quote is viewed', 1, '2020-02-19 10:54:26', 0, 1),
(20, 0, 4, 'Your gif is viewed', 1, '2020-02-19 11:16:47', 0, 1),
(22, 0, 3, 'Your gif is viewed', 1, '2020-02-19 11:21:22', 0, 1),
(34, 0, 2, 'Your gif is viewed', 1, '2020-02-20 09:07:47', 0, 1),
(43, 0, 1, 'Your image is viewed', 1, '2020-02-22 06:19:40', 0, 1),
(54, 0, 0, 'Add Image', 5, '2020-02-24 04:46:08', 0, 1),
(58, 0, 14, 'Add Video', 5, '2020-02-24 10:54:50', 0, 1),
(64, 0, 30, 'Your video is viewed', 1, '2020-02-24 12:17:30', 0, 1),
(79, 0, 5, 'Your gif is viewed', 1, '2020-02-26 09:43:41', 0, 1),
(83, 0, 0, 'Register Rewards', 5, '2020-02-26 10:29:29', 0, 1),
(84, 0, 0, 'Register Rewards', 5, '2020-02-26 10:30:08', 0, 1),
(85, 0, 0, 'Register Rewards', 5, '2020-02-26 10:30:50', 0, 1),
(87, 0, 0, 'Register Rewards', 5, '2020-02-26 10:32:08', 0, 1),
(106, 0, 29, 'Video Like', 2, '2020-02-26 12:17:43', 0, 1),
(143, 0, 0, 'Add GIF', 5, '2020-02-27 10:58:50', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verify_user`
--

CREATE TABLE `tbl_verify_user` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `document` text NOT NULL,
  `created_at` varchar(150) NOT NULL,
  `verify_at` varchar(150) NOT NULL DEFAULT '0',
  `reject_reason` text DEFAULT NULL,
  `is_opened` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

CREATE TABLE `tbl_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `cat_id` int(11) NOT NULL,
  `lang_ids` text NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_url` text NOT NULL,
  `video_id` varchar(255) NOT NULL,
  `video_tags` text NOT NULL,
  `video_layout` varchar(255) NOT NULL DEFAULT 'Landscape',
  `video_thumbnail` text NOT NULL,
  `video_duration` varchar(255) DEFAULT NULL,
  `total_likes` int(11) NOT NULL DEFAULT 0,
  `totel_viewer` int(11) NOT NULL DEFAULT 0,
  `total_download` int(10) NOT NULL DEFAULT 0,
  `featured` int(1) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_video`
--

INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`) VALUES
(27, 0, 13, '', 'server_url', 'ਤਾਮਿਲ ਸੰਗੀਤ ਵੀਡੀਓ', 'http://www.nilkanth.in/envato/cc/demo_video/S_portrait_3.mp4', '', '', 'Portrait', '26698_1.png', NULL, 0, 0, 0, 0, 1),
(28, 0, 13, '', 'server_url', 'ਸੈਲੀ ਵਾਕਰ (ਸਰਕਾਰੀ ਸੰਗੀਤ ਵੀਡੀਓ)', 'http://www.nilkanth.in/envato/cc/demo_video/S_portrait_01.mp4', '', '', 'Portrait', '99900_2.png', NULL, 0, 0, 0, 0, 1),
(29, 0, 12, '6,7', 'server_url', 'ગુજરાતી ગીતો 2019', 'http://www.nilkanth.in/envato/cc/demo_video/S_landscape3.mp4', '-', '', 'Landscape', '99007_1.png', NULL, 1, 0, 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_list`
--
ALTER TABLE `tbl_contact_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_sub`
--
ALTER TABLE `tbl_contact_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_deleted_users`
--
ALTER TABLE `tbl_deleted_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_favourite`
--
ALTER TABLE `tbl_favourite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_follows`
--
ALTER TABLE `tbl_follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_img_status`
--
ALTER TABLE `tbl_img_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_language`
--
ALTER TABLE `tbl_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_like`
--
ALTER TABLE `tbl_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment_mode`
--
ALTER TABLE `tbl_payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quotes`
--
ALTER TABLE `tbl_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_smtp_settings`
--
ALTER TABLE `tbl_smtp_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_spinner`
--
ALTER TABLE `tbl_spinner`
  ADD PRIMARY KEY (`block_id`);

--
-- Indexes for table `tbl_suspend_account`
--
ALTER TABLE `tbl_suspend_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_redeem`
--
ALTER TABLE `tbl_users_redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_rewards_activity`
--
ALTER TABLE `tbl_users_rewards_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_verify_user`
--
ALTER TABLE `tbl_verify_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_contact_list`
--
ALTER TABLE `tbl_contact_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_contact_sub`
--
ALTER TABLE `tbl_contact_sub`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_deleted_users`
--
ALTER TABLE `tbl_deleted_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_favourite`
--
ALTER TABLE `tbl_favourite`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_follows`
--
ALTER TABLE `tbl_follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_img_status`
--
ALTER TABLE `tbl_img_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_language`
--
ALTER TABLE `tbl_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_like`
--
ALTER TABLE `tbl_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_payment_mode`
--
ALTER TABLE `tbl_payment_mode`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_quotes`
--
ALTER TABLE `tbl_quotes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_smtp_settings`
--
ALTER TABLE `tbl_smtp_settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_spinner`
--
ALTER TABLE `tbl_spinner`
  MODIFY `block_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_suspend_account`
--
ALTER TABLE `tbl_suspend_account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users_redeem`
--
ALTER TABLE `tbl_users_redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users_rewards_activity`
--
ALTER TABLE `tbl_users_rewards_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `tbl_verify_user`
--
ALTER TABLE `tbl_verify_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
