<?php 

  $page_title="Edit Video";
  $active_page="status";
  
  include("includes/header.php");
	include("includes/connection.php");

	require("includes/function.php");
	require("language/language.php");

  $qry="SELECT * FROM tbl_video where id='".$_GET['video_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);

  $lang_ids=explode(',', $row['lang_ids']);

  $video_file=$row['video_url'];

  if($row['video_type']=='local'){
      $video_file=$file_path.'uploads/'.basename($row['video_url']);
  }
	
	if(isset($_POST['submit']))
	{
      $lang_ids=implode(',', $_POST['lang_id']);

      $video_id='-';

      $video_tags=implode(',', $_POST['video_tags']);
  
      if ($_POST['video_type']=='server_url')
      {
        $video_url=addslashes(trim($_POST['video_url']));  
      } 
      else if ($_POST['video_type']=='local')
      {
        if (!empty($_FILES['video_local']['name'])) {

            $path = "uploads/"; //set your folder path

            $file_size=round($_FILES['video_local']['size'] / 1024 / 1024, 2);

            if($file_size > $settings_details['video_file_size']) { 
                $_SESSION['class']='alert-danger';
                $_SESSION['msg']="Video file size must be less or equal to ".$settings_details['video_file_size']." MB";
                header( "Location:add_video.php");
                exit;
            }

            unlink('uploads/'.basename($row['video_url']));

            $video_local=rand(0,99999)."_".str_replace(" ", "-", $_FILES['video_local']['name']);

            $tmp = $_FILES['video_local']['tmp_name'];
            
            if (move_uploaded_file($tmp, $path.$video_local)) 
            {
                $video_url=$video_local;
            } else {
                echo "Error in uploading video file !!";
                exit;
            }
        }
        else{
          $video_url=$row['video_url'];
        }
      } 

      if (!empty($_FILES['video_thumbnail']['name'])) {
          $ext = pathinfo($_FILES['video_thumbnail']['name'], PATHINFO_EXTENSION);

          $video_thumbnail=rand(0,99999)."_video_thumb.".$ext;

          //Main Image
          $tpath1='images/'.$video_thumbnail;   

          if($ext!='png')  {
            $pic1=compress_image($_FILES["video_thumbnail"]["tmp_name"], $tpath1, 80);
          }
          else{
            $tmp = $_FILES['video_thumbnail']['tmp_name'];
            move_uploaded_file($tmp, $tpath1);
          }
      }
      else{
          $video_thumbnail=$row['video_thumbnail'];
      }
        
      $data = array( 
        'cat_id'  =>  $_POST['cat_id'],
        'lang_ids'  =>  $lang_ids,
        'video_tags'  =>  $video_tags,
        'video_type'  =>  $_POST['video_type'],
        'video_title'  =>  addslashes($_POST['video_title']),
        'video_url'  =>  $video_url,
        'video_id'  =>  $video_id,
        'video_layout'  =>  $_POST['video_layout'],
        'video_thumbnail'  =>  $video_thumbnail
      );
  		 		 
      $qry=Update('tbl_video', $data, "WHERE id = '".$_POST['video_id']."'");
    
      $_SESSION['class']='alert-success';
  		$_SESSION['msg']="11"; 
  		header( "Location:edit_video.php?video_id=".$_POST['video_id']);
  		exit;	
	}
	
	  
?>
<!-- For Bootstrap Tags -->
  <link rel="stylesheet" type="text/css" href="assets/bootstrap-tag/bootstrap-tagsinput.css">
<!-- End -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(e) {
      $("#video_type").change(function(){
        var type=$("#video_type").val();

        if(type=="server_url")
        {
          $("#video_url_display").show();
          $("#thumbnail").show();
          $("#video_local_display").hide();
        }
        else
        {   

          $("#video_url_display").hide();
          $("#video_local_display").show();
          $("#thumbnail").show();
        }    

      });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(event){
      $('#video_local').change(function(e){
        var file_size=parseFloat(((this.files[0].size) / (1024 * 1024)).toFixed(2));
        var required_file_size=parseFloat('<?=$settings_details['video_file_size']?>');

        if(file_size <= required_file_size)
        {
          if(isVideo($(this).val())){
            $('.video-preview').attr('src', URL.createObjectURL(this.files[0]));
            $('#uploadPreview').show();
          }
          else
          {
            $('#video_local').val('');
            $('#uploadPreview').hide();
            if($(this).val()!='')
              alert("Only video files are allowed to upload.")
          }
        }
        else{
          $('#video_local').val('');
          alert("Video file size must be less or equal to "+required_file_size+" MB");
        }
      });
  });
  // If user tries to upload videos other than these extension , it will throw error.
  function isVideo(filename) {
      var ext = getExtension(filename);
      switch (ext.toLowerCase()) {
      case 'm4v':
      case 'avi':
      case 'mp4':
      case 'mov':
      case 'mpg':
      case 'mpeg':
          // etc
          return true;
      }
      return false;
  }

  function getExtension(filename) {
      var parts = filename.split('.');
      return parts[parts.length - 1];
  }

</script>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Edit Video</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
                 <div class="alert <?=$_SESSION['class']?> alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <?php if(!empty($client_lang[$_SESSION['msg']])){ echo $client_lang[$_SESSION['msg']]; }else{ echo $_SESSION['msg']; } ?></div>
                <?php unset($_SESSION['msg'], $_SESSION['class']);}?> 
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="edit_form" method="post" class="form form-horizontal" enctype="multipart/form-data">
              <input  type="hidden" name="video_id" value="<?php echo $_GET['video_id'];?>" />
              <div class="section">
                <div class="section-body">
                   <div class="form-group">
                    <label class="col-md-3 control-label">Category :-</label>
                    <div class="col-md-6">
                      <select name="cat_id" id="cat_id" class="select2">
                        <option value="">--Select Category--</option>
          							<?php
                            //Get Category
                            $cat_qry="SELECT * FROM tbl_category ORDER BY category_name";
                            $cat_result=mysqli_query($mysqli,$cat_qry);
          									while($cat_row=mysqli_fetch_array($cat_result))
          									{
          							?>          						 
          							<option value="<?php echo $cat_row['cid'];?>" <?php if($cat_row['cid']==$row['cat_id']){?>selected<?php }?>><?php echo $cat_row['category_name'];?></option>	          							 
          							<?php
          								}
          							?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label" for="lang_id">Languages:-</label>
                    <div class="col-md-6">

                      <select name="lang_id[]" id="lang_id" class="select2" multiple="" required>
                        <?php
                            $sql="SELECT * FROM tbl_language WHERE `status`='1' ORDER BY `language_name`";
                            $res=mysqli_query($mysqli,$sql);
                            while($row_data=mysqli_fetch_assoc($res))
                            {
                        ?>                       
                          <option value="<?php echo $row_data['id'];?>" <?=(in_array($row_data['id'], $lang_ids)) ? 'selected' : ''; ?>><?php echo ucfirst(strtolower($row_data['language_name']));?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Title :-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_title" id="video_title" value="<?php echo stripslashes($row['video_title']);?>" class="form-control" required>
                    </div>
                  </div>   
                  <div class="form-group">
                    <label class="col-md-3 control-label">Tags(Optional):-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_tags[]" id="video_tags" value="<?php echo $row['video_tags'];?>" data-role="tagsinput" class="form-control">
                    </div>
                  </div>                 
                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Upload Option :-</label>
                    <div class="col-md-6">                       
                      <select name="video_type" id="video_type" style="width:280px; height:25px;" class="select2" required>
                            <option value="">--Select Type--</option>
                             
                            <option value="server_url" <?php if($row['video_type']=='server_url'){?>selected<?php }?>>Server URL</option>
                            <option value="local" <?php if($row['video_type']=='local'){?>selected<?php }?>>Browse From Computer</option>
                      </select>
                    </div>
                  </div>
                  <div id="video_url_display" class="form-group" <?php if($row['video_type']=='local'){?>style="display:none;"<?php }else{?>style="display:block;"<?php }?>>
                    <label class="col-md-3 control-label">Video URL :-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_url" id="video_url" value="<?php echo $row['video_url']?>" class="form-control">
                    </div>
                  </div>
                  <div id="video_local_display" class="form-group" <?php if($row['video_type']=='local'){?>style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                    <label class="col-md-3 control-label">Video Upload :-
                      <p class="control-label-help">(Recommended : Maximum <strong><?=$settings_details['video_file_size']?>MB</strong> file size)</p>
                    </label>
                    <div class="col-md-6">
                      <input type="file" name="video_local" id="video_local" value="" class="form-control">
                      <div id="uploadPreview" style="background: #eee;text-align: center;">
                        <video height="400" width="100%" class="video-preview" src="<?php echo $video_file?>" controls="controls"/>
                      </div>
                    </div>
                  </div><br>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Layout :-</label>
                    <div class="col-md-6">                       
                      <select name="video_layout" id="video_layout" style="width:280px; height:25px;" class="select2" required>
                            <option value="Landscape" <?php if($row['video_layout']=='Landscape'){?>selected<?php }?>>Landscape</option>
                            <option value="Portrait" <?php if($row['video_layout']=='Portrait'){?>selected<?php }?>>Portrait</option>
                      </select>
                    </div>
                  </div>
                  <div id="thumbnail" class="form-group">
                    <label class="col-md-3 control-label">Thumbnail Image:-
                      <p class="control-label-help">(Recommended resolution: Landscape: 800x500,650x450  Portrait: 720X1280, 640X1136, 350x800)</p>
                    </label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="video_thumbnail" value="" id="fileupload">
                       <?php if(isset($_GET['video_id']) and $row['video_thumbnail']!="") {?>
                       <input type="hidden" name="video_thumbnail_name" id="video_thumbnail_name" value="<?php echo $row['video_thumbnail'];?>" class="form-control">
                             
                          <?php }?>
                          <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                       </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp; </label>
                    <div class="col-md-6">
                      <?php if(isset($_GET['video_id']) and $row['video_thumbnail']!="") {?>
                            <div class="block_wallpaper">
                      <?php   
                         if (preg_match_all('#\b(https|http)\b#', $row['video_thumbnail'], $matches)) { ?>
                      <img src="<?php echo $row['video_thumbnail']; ?>" />
                      <?php } else { ?>
                        <img src="images/<?php echo $row['video_thumbnail']; ?>" />
                      <?php }?>
                            <!-- <img src="images/<?php //echo $row['video_thumbnail'];?>" alt="category image" /> -->
                            </div>
                          <?php } ?>
                    </div>
                  </div><br> 
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>  

<script type="text/javascript" src="assets/bootstrap-tag/bootstrap-tagsinput.js"></script>

<script type="text/javascript">
  $('#video_tags').tagsinput();
</script>     
