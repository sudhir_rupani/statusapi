-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 13, 2021 at 06:05 PM
-- Server version: 10.3.27-MariaDB-log-cll-lve
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prayosha_status`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`, `image`) VALUES
(1, 'prayoshasoftech', 'prayoshasoftech@125', 'prayosha@prayoshasoftech.tech', 'profile.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `start_color` varchar(30) NOT NULL DEFAULT 'FF493B',
  `end_color` varchar(30) NOT NULL DEFAULT 'FFE245',
  `show_on_home` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`, `start_color`, `end_color`, `show_on_home`, `status`) VALUES
(12, 'Dancing', '15025.png', '3FBEA1', '1C5C9D', 0, 1),
(13, 'Romance', '51930.png', '251C7A', '320A17', 0, 1),
(26, 'Birthday', '20587.png', '32B2C2', '2A89DE', 0, 1),
(71, 'Lyrical', '26401.png', 'FF5F5C', 'FA0AA5', 1, 1),
(72, 'Shivratri Special', '9744.png', 'FFC5B5', '171716', 1, 1),
(73, 'Love', '17392.png', '98A13D', '2C2434', 1, 1),
(74, 'Sad', '66943.png', 'BF0B67', '3D4396', 0, 1),
(75, 'Breakup', '84802.png', 'B31811', 'FFF0BA', 0, 1),
(76, 'Hayat & Murat', '85930.png', '170302', '3B4191', 0, 1),
(77, 'Marathi', '', 'FF2319', '6772FF', 0, 0),
(78, 'Punjabi', '84625.png', '783050', 'D17143', 0, 1),
(79, 'Tamil', '', 'FF2319', '6772FF', 0, 0),
(80, 'Gujarati', '30596.png', '82120D', '303678', 0, 1),
(81, 'Aagri Koli', '51991.png', '769E47', '1F224D', 0, 1),
(82, 'Shivaji Maharaj', '82495.png', '4E0B08', 'FF80B9', 0, 1),
(83, 'Tv Serial', '90992.png', '690E0A', '2E3373', 0, 1),
(84, 'Festivals', '86212.png', '695473', '3C4294', 0, 1),
(85, 'Funny', '92763.png', 'BD406B', '69649E', 0, 1),
(86, 'God', '15509.png', '488622', '773858', 1, 1),
(87, 'Ganesh Chaturthi', '26280.png', '580C09', '272B60', 0, 1),
(88, 'Dialogue', '84910.png', 'F9FFB8', '3B242A', 0, 1),
(89, 'Animated', '54476.png', 'FFC5B8', '1C243B', 0, 1),
(90, 'Unplugged', '68192.png', 'AD2694', '2D3270', 0, 1),
(91, 'Old Songs', '23771.png', '38775F', '10201C', 0, 1),
(92, 'Korean Mix', '71754.png', '4B2069', '8CB8B5', 0, 1),
(93, 'Happy New Year', '24776.png', '8C817B', '92ADA3', 0, 1),
(94, 'Independence Day', '74284.png', '5A3475', 'BDF9FF', 0, 1),
(95, 'Valentine Special', '75166.png', 'FF2319', '6772FF', 0, 1),
(96, 'Hug Day', '25547.png', 'C4DBFF', '25295C', 0, 1),
(97, 'Promise Day', '24173.png', 'B0B553', '913744', 0, 1),
(98, 'Teddy Day', '46343.png', 'FF967E', 'C3FFC0', 0, 1),
(99, 'Chocolate Day', '10674.png', '2C3847', '996E6A', 0, 1),
(100, 'Propose Day', '31590.png', 'EDB5FF', '782519', 0, 1),
(101, 'Rose Day', '76298.png', 'A3FFD0', '383E8A', 0, 1),
(102, 'Friendship Day', '96634.png', 'FF7669', '6EB5AB', 0, 1),
(103, 'Emotional', '78323.png', 'AF0D50', 'FFC0ED', 0, 1),
(104, 'Rainy Special', '62913.png', 'FFE2DA', 'C40A69', 0, 1),
(105, 'Makar Sankranti', '86117.png', '1C72FF', '565F69', 0, 1),
(106, 'Ekadashi Special', '19192.png', 'B41912', '2B306A', 0, 1),
(107, 'Guru Purnima', '86960.png', 'C21B13', '4B53BA', 0, 1),
(108, 'Attitude', '82322.png', '630E0A', 'ABE1FF', 0, 1),
(109, 'Motivational', '655.png', 'FF2319', '6772FF', 0, 1),
(110, 'Darshan Raval', '96184.png', '737D74', '5C5161', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comments`
--

CREATE TABLE `tbl_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment_text` text NOT NULL,
  `comment_on` varchar(150) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'video',
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_list`
--

CREATE TABLE `tbl_contact_list` (
  `id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` int(5) NOT NULL,
  `contact_msg` text NOT NULL,
  `created_at` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_contact_list`
--

INSERT INTO `tbl_contact_list` (`id`, `contact_name`, `contact_email`, `contact_subject`, `contact_msg`, `created_at`) VALUES
(2, 'sudhir', 'helpingstudent125@gmail.com', 5, 'sskbx', '1611864995'),
(3, 'jffd', 'kanani.manthan@rediffmail.com', 3, 'ft :\'(', '1615045131');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_sub`
--

CREATE TABLE `tbl_contact_sub` (
  `id` int(5) NOT NULL,
  `title` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_sub`
--

INSERT INTO `tbl_contact_sub` (`id`, `title`, `status`) VALUES
(2, 'Suspend', 1),
(3, 'Other', 1),
(4, 'Transaction', 1),
(5, 'Verification issue', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deleted_users`
--

CREATE TABLE `tbl_deleted_users` (
  `id` int(10) NOT NULL,
  `user_code` varchar(150) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `device_id` varchar(225) NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `total_video` int(10) NOT NULL DEFAULT 0,
  `total_image` int(10) NOT NULL DEFAULT 0,
  `total_gif` int(10) NOT NULL DEFAULT 0,
  `total_quote` int(10) NOT NULL DEFAULT 0,
  `total_point` int(10) NOT NULL DEFAULT 0,
  `pending_points` int(10) NOT NULL DEFAULT 0,
  `paid_points` int(10) NOT NULL DEFAULT 0,
  `total_followers` int(10) NOT NULL DEFAULT 0,
  `total_following` int(10) NOT NULL DEFAULT 0,
  `verify_status` int(3) NOT NULL,
  `registration_on` text NOT NULL,
  `auth_id` varchar(225) NOT NULL DEFAULT '0',
  `deleted_on` text NOT NULL,
  `deleted_by` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_deleted_users`
--

INSERT INTO `tbl_deleted_users` (`id`, `user_code`, `user_type`, `device_id`, `name`, `email`, `phone`, `total_video`, `total_image`, `total_gif`, `total_quote`, `total_point`, `pending_points`, `paid_points`, `total_followers`, `total_following`, `verify_status`, `registration_on`, `auth_id`, `deleted_on`, `deleted_by`) VALUES
(8, '0tvssp6y', 'Facebook', '09d6799c693f233d', 'Sudhir Rupani', 'sudhirrupani1993@gmail.com', '', 0, 0, 0, 0, 20, 0, 0, 0, 1, 0, '1615035420', '2971759816432730', '1615035917', 1),
(9, 'rinkal', 'Normal', '3351a2a655f0e153', 'rinkal', 'test3@yopmail.com', '9773174104', 0, 0, 0, 0, 12, 0, 0, 0, 1, 0, '1611687660', '0', '1615035929', 1),
(10, 'jwigre83', 'Normal', '3351a2a655f0e153', 'rinkal', 'test2@yopmail.com', '9773174104', 0, 0, 0, 0, 5, 0, 0, 0, 1, 0, '1611684900', '0', '1615035933', 1),
(11, '6w7aa7oo', 'Google', 'dcaff1b5799f8b10', 'Nuage Laboratoire', '7BC44QNZDIPU67WE3ZCBEIDVGY-00@cloudtestlabaccounts.com', '', 0, 0, 0, 0, 21, 0, 0, 0, 1, 0, '1615276740', '101906926378421333693', '1615289735', 1),
(12, 'bxmzw2ew', 'Google', 'b958f02718a35712', 'Milanjitsinh Sindha', 'milanjitsinh@gmail.com', '', 0, 0, 0, 0, 20, 0, 0, 0, 1, 0, '1615393800', '104449551941960272470', '1615394008', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favourite`
--

CREATE TABLE `tbl_favourite` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_favourite`
--

INSERT INTO `tbl_favourite` (`id`, `post_id`, `user_id`, `type`, `created_at`) VALUES
(19, 1123, 15, 'video', '1615190702'),
(20, 1121, 15, 'video', '1615193148'),
(25, 1158, 27, 'video', '1615299565'),
(26, 510, 27, 'video', '1615299652'),
(27, 1164, 25, 'video', '1615307694'),
(28, 1164, 29, 'video', '1615307785'),
(29, 1165, 25, 'video', '1615311942'),
(30, 1173, 15, 'video', '1615347703'),
(31, 1, 34, 'quote', '1615394427'),
(32, 1210, 34, 'video', '1615436364'),
(33, 1197, 34, 'video', '1615436384'),
(34, 1186, 39, 'video', '1615459691'),
(35, 1123, 19, 'video', '1615486408'),
(36, 1219, 34, 'video', '1615560522'),
(37, 6, 34, 'image', '1615561219'),
(38, 2, 34, 'quote', '1615561224'),
(39, 7, 34, 'image', '1615568012'),
(40, 22, 34, 'video', '1615603216'),
(41, 21, 34, 'video', '1615603226');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_follows`
--

CREATE TABLE `tbl_follows` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `created_at` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_follows`
--

INSERT INTO `tbl_follows` (`id`, `user_id`, `follower_id`, `created_at`) VALUES
(1, 0, 0, '12-01-2021 02:33:22 AM'),
(2, 0, 0, '12-01-2021 02:35:36 AM'),
(3, 0, 1, NULL),
(4, 0, 0, '12-01-2021 02:36:50 PM'),
(5, 0, 0, '13-01-2021 01:22:47 AM'),
(6, 0, 0, '13-01-2021 01:43:01 AM'),
(7, 0, 0, '13-01-2021 02:28:09 AM'),
(8, 0, 0, '13-01-2021 02:28:19 AM'),
(9, 0, 0, '13-01-2021 02:29:21 AM'),
(10, 0, 2, '13-01-2021 02:31:25 AM'),
(14, 0, 6, '27-01-2021 12:45:09 AM'),
(15, 0, 7, '28-01-2021 01:01:36 AM'),
(18, 0, 10, '06-03-2021 12:56:58 AM'),
(23, 0, 15, '06-03-2021 06:29:07 PM'),
(24, 0, 16, '06-03-2021 06:32:38 PM'),
(25, 0, 17, '06-03-2021 06:35:51 PM'),
(26, 0, 18, '06-03-2021 07:29:25 PM'),
(27, 0, 19, '06-03-2021 07:30:19 PM'),
(28, 0, 20, '07-03-2021 07:53:16 PM'),
(29, 17, 15, '08-03-2021 02:16:24 PM'),
(30, 0, 21, '08-03-2021 04:40:04 PM'),
(31, 0, 22, '09-03-2021 12:34:55 PM'),
(32, 0, 23, '09-03-2021 12:51:42 PM'),
(34, 0, 25, '09-03-2021 02:05:23 PM'),
(35, 0, 26, '09-03-2021 02:07:09 PM'),
(36, 0, 27, '09-03-2021 07:49:06 PM'),
(37, 0, 28, '09-03-2021 08:01:54 PM'),
(38, 0, 29, '09-03-2021 09:44:05 PM'),
(40, 29, 25, '09-03-2021 10:06:07 PM'),
(41, 0, 30, '09-03-2021 10:25:08 PM'),
(42, 29, 15, '09-03-2021 10:30:47 PM'),
(43, 0, 31, '10-03-2021 07:54:52 AM'),
(44, 25, 17, '10-03-2021 10:02:35 AM'),
(45, 26, 17, '10-03-2021 10:02:48 AM'),
(46, 28, 17, '10-03-2021 10:03:04 AM'),
(47, 6, 17, '10-03-2021 10:04:52 AM'),
(48, 0, 32, '10-03-2021 12:05:22 PM'),
(49, 0, 33, '10-03-2021 09:47:59 PM'),
(50, 0, 34, '10-03-2021 09:58:59 PM'),
(52, 0, 36, '10-03-2021 10:03:44 PM'),
(53, 0, 37, '10-03-2021 10:55:54 PM'),
(54, 0, 38, '10-03-2021 11:29:55 PM'),
(55, 0, 39, '11-03-2021 10:09:53 AM'),
(56, 15, 39, '11-03-2021 04:16:53 PM'),
(57, 0, 40, '12-03-2021 07:11:15 AM'),
(58, 0, 41, '12-03-2021 08:33:46 AM'),
(59, 0, 42, '12-03-2021 02:13:59 PM'),
(60, 0, 43, '12-03-2021 08:24:32 PM'),
(61, 0, 44, '13-03-2021 12:16:42 PM'),
(62, 0, 45, '13-03-2021 04:12:40 PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_img_status`
--

CREATE TABLE `tbl_img_status` (
  `id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT 0,
  `cat_id` int(10) NOT NULL,
  `lang_ids` text NOT NULL,
  `image_title` varchar(150) NOT NULL,
  `image_tags` text NOT NULL,
  `image_layout` varchar(40) NOT NULL,
  `image_file` text NOT NULL,
  `total_download` int(10) NOT NULL DEFAULT 0,
  `total_likes` int(10) NOT NULL DEFAULT 0,
  `total_views` int(10) NOT NULL DEFAULT 0,
  `featured` int(2) NOT NULL DEFAULT 0,
  `status_type` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_img_status`
--

INSERT INTO `tbl_img_status` (`id`, `user_id`, `cat_id`, `lang_ids`, `image_title`, `image_tags`, `image_layout`, `image_file`, `total_download`, `total_likes`, `total_views`, `featured`, `status_type`, `status`) VALUES
(1, 0, 11, '6', 'Love Birds are Here !', 'Love birds, Kiss,Cute', 'Landscape', '94528_img_status.jpg', 0, 0, 0, 0, 'image', 1),
(3, 0, 21, '6', 'Valentine day Image 1', 'valentine day,valentine', 'Landscape', '19628_gif_status.gif', 0, 1, 11, 0, 'gif', 1),
(4, 0, 21, '6', 'Valentine day Image 2', 'Valentine ,Valentine Day', 'Landscape', '99463_gif_status.gif', 0, 0, 7, 0, 'gif', 1),
(5, 0, 21, '6', 'Hindi Valentine day Image', 'Hindi ,Valentine,Hindi Valentine', 'Portrait', '73971_gif_status.gif', 0, 0, 27, 0, 'gif', 1),
(6, 34, 109, '6,7,5', 'businessman quotes', 'motivational,quotes,business,businessman,businessman quotes,motivational speaker,entrepreneurs,day quotes,morning quotes', 'Landscape', '57895_image.jpg', 0, 0, 0, 0, 'image', 0),
(7, 34, 85, '6,7,5', 'k l Rahul effort', 'cricket,never give up,k l Rahul,match,catches', 'Landscape', '18514_image.jpg', 0, 0, 0, 0, 'image', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_image` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`id`, `language_name`, `language_image`, `status`) VALUES
(3, 'Arabic', '53477_unnamed.png', 0),
(4, 'Tamil', '22010_YZpHOsdtbsrfdoE-800x450-noPad.jpg', 0),
(5, 'Hindi', '45650_hindi-is-the-3rd-most-spoken-language-of-the-world.jpg', 1),
(6, 'English', '12146_800px-English_language.svg.png', 1),
(7, 'Gujarati', '86551_gujrati-icon.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_like`
--

CREATE TABLE `tbl_like` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `unlike` int(11) NOT NULL DEFAULT 0,
  `like_type` varchar(10) NOT NULL DEFAULT 'video'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_like`
--

INSERT INTO `tbl_like` (`id`, `post_id`, `device_id`, `likes`, `unlike`, `like_type`) VALUES
(5, 22, '10', 1, 0, 'video'),
(6, 1124, '15', 1, 0, 'video'),
(7, 1131, '15', 1, 0, 'video'),
(8, 1158, '22', 1, 0, 'video'),
(9, 510, '27', 1, 0, 'video'),
(10, 1124, '25', 1, 0, 'video'),
(11, 1164, '25', 1, 0, 'video'),
(12, 1164, '15', 1, 0, 'video'),
(13, 1210, '34', 1, 0, 'video'),
(14, 1197, '34', 1, 0, 'video'),
(15, 1061, '28', 1, 0, 'video'),
(16, 1219, '34', 1, 0, 'video'),
(17, 22, '34', 1, 0, 'video'),
(18, 21, '34', 1, 0, 'video'),
(19, 20, '34', 1, 0, 'video'),
(20, 19, '34', 1, 0, 'video'),
(21, 18, '34', 1, 0, 'video'),
(22, 17, '34', 1, 0, 'video'),
(23, 16, '34', 1, 0, 'video'),
(24, 15, '34', 1, 0, 'video'),
(25, 14, '34', 1, 0, 'video'),
(26, 12, '34', 1, 0, 'video'),
(27, 11, '34', 1, 0, 'video'),
(28, 10, '34', 1, 0, 'video'),
(29, 9, '34', 1, 0, 'video'),
(30, 8, '34', 1, 0, 'video'),
(31, 7, '34', 1, 0, 'video'),
(32, 6, '34', 1, 0, 'video'),
(33, 5, '34', 1, 0, 'video'),
(34, 4, '34', 1, 0, 'video'),
(35, 3, '34', 1, 0, 'video'),
(36, 2, '34', 1, 0, 'video'),
(37, 178, '45', 1, 0, 'video');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_mode`
--

CREATE TABLE `tbl_payment_mode` (
  `id` int(5) NOT NULL,
  `mode_title` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_payment_mode`
--

INSERT INTO `tbl_payment_mode` (`id`, `mode_title`, `status`) VALUES
(1, 'Paypal', 0),
(2, 'PayTM', 1),
(3, 'Bank Detail', 0),
(5, 'Others', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quotes`
--

CREATE TABLE `tbl_quotes` (
  `id` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT 0,
  `lang_ids` text COLLATE utf8mb4_bin NOT NULL,
  `quote` longtext COLLATE utf8mb4_bin NOT NULL,
  `quote_font` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `quote_tags` text COLLATE utf8mb4_bin NOT NULL,
  `quote_bg` text COLLATE utf8mb4_bin NOT NULL,
  `total_likes` int(10) NOT NULL DEFAULT 0,
  `total_views` int(10) NOT NULL DEFAULT 0,
  `featured` int(2) NOT NULL DEFAULT 0,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `tbl_quotes`
--

INSERT INTO `tbl_quotes` (`id`, `cat_id`, `user_id`, `lang_ids`, `quote`, `quote_font`, `quote_tags`, `quote_bg`, `total_likes`, `total_views`, `featured`, `status`) VALUES
(1, 109, 34, '6', 'BE PATIEN \n EMPIRES AREN\'T\n BUILT IN A DAY.', 'Lemonada.ttf', 'motivational,business,entrepreneurs,quest,motivationquest', '933925', 0, 0, 0, 0),
(2, 109, 34, '6', 'DO SOMETHING TODAY THAT WILL MAKE TOMORROW BETTER', 'Cinzel.ttf', 'motivational,quotes,buisness,entrepreneurs', '933d15', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reports`
--

CREATE TABLE `tbl_reports` (
  `id` int(11) NOT NULL,
  `post_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `report` text NOT NULL,
  `report_type` varchar(30) NOT NULL DEFAULT 'video',
  `report_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(5) NOT NULL,
  `envato_buyer_name` varchar(200) NOT NULL,
  `envato_purchase_code` text NOT NULL,
  `envato_buyer_email` varchar(100) NOT NULL,
  `envato_purchased_status` int(1) NOT NULL DEFAULT 0,
  `package_name` varchar(150) NOT NULL,
  `email_from` varchar(150) NOT NULL,
  `redeem_points` int(11) NOT NULL,
  `redeem_money` float(11,2) NOT NULL,
  `redeem_currency` varchar(100) NOT NULL,
  `minimum_redeem_points` int(11) NOT NULL,
  `onesignal_app_id` text NOT NULL,
  `onesignal_rest_key` text NOT NULL,
  `app_name` text NOT NULL,
  `app_logo` text NOT NULL,
  `app_email` varchar(150) NOT NULL,
  `app_version` varchar(60) NOT NULL,
  `app_author` varchar(150) NOT NULL,
  `app_contact` varchar(60) NOT NULL,
  `app_website` varchar(100) NOT NULL,
  `app_description` text NOT NULL,
  `app_developed_by` varchar(100) NOT NULL,
  `app_privacy_policy` text NOT NULL,
  `api_page_limit` int(11) NOT NULL,
  `api_all_order_by` varchar(30) NOT NULL,
  `api_latest_limit` int(3) NOT NULL,
  `api_cat_order_by` varchar(30) NOT NULL,
  `api_cat_post_order_by` varchar(30) NOT NULL,
  `publisher_id` text NOT NULL,
  `interstital_ad` text NOT NULL,
  `interstital_ad_type` varchar(30) NOT NULL,
  `registration_reward` int(50) NOT NULL,
  `app_refer_reward` int(50) NOT NULL,
  `video_views` int(5) NOT NULL,
  `video_add` int(11) NOT NULL,
  `like_video_points` int(11) NOT NULL,
  `download_video_points` int(11) NOT NULL,
  `registration_reward_status` varchar(20) NOT NULL DEFAULT 'true',
  `app_refer_reward_status` varchar(20) NOT NULL DEFAULT 'true',
  `video_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `video_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_video_points_status` varchar(20) NOT NULL DEFAULT 'false',
  `download_video_points_status` varchar(20) NOT NULL DEFAULT 'false',
  `other_user_video_status` varchar(10) NOT NULL,
  `other_user_video_point` varchar(10) NOT NULL,
  `image_add` int(5) NOT NULL DEFAULT 0,
  `image_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `image_views` int(5) NOT NULL DEFAULT 0,
  `image_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_image_point` int(5) NOT NULL DEFAULT 0,
  `other_user_image_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_image_points` int(5) NOT NULL DEFAULT 0,
  `like_image_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `download_image_points` int(5) NOT NULL DEFAULT 0,
  `download_image_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `gif_add` int(5) NOT NULL DEFAULT 0,
  `gif_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `gif_views` int(5) NOT NULL DEFAULT 0,
  `gif_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_gif_point` int(5) NOT NULL DEFAULT 0,
  `other_user_gif_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_gif_points` int(5) NOT NULL DEFAULT 0,
  `like_gif_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `download_gif_points` int(5) NOT NULL DEFAULT 0,
  `download_gif_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `quotes_add` int(5) NOT NULL DEFAULT 0,
  `quotes_add_status` varchar(20) NOT NULL DEFAULT 'true',
  `quotes_views` int(5) NOT NULL DEFAULT 0,
  `quotes_views_status` varchar(20) NOT NULL DEFAULT 'true',
  `other_user_quotes_point` int(5) NOT NULL DEFAULT 0,
  `other_user_quotes_status` varchar(20) NOT NULL DEFAULT 'true',
  `like_quotes_points` int(5) NOT NULL DEFAULT 0,
  `like_quotes_points_status` varchar(20) NOT NULL DEFAULT 'true',
  `interstital_ad_id` text NOT NULL,
  `interstital_ad_click` varchar(10) NOT NULL,
  `banner_ad` text NOT NULL,
  `banner_ad_type` varchar(30) NOT NULL,
  `banner_ad_id` text NOT NULL,
  `facebook_interstital_ad_id` text NOT NULL,
  `facebook_banner_ad_id` text NOT NULL,
  `rewarded_video_ads` varchar(20) NOT NULL,
  `rewarded_video_ads_id` text NOT NULL,
  `rewarded_video_click` int(3) NOT NULL DEFAULT 5,
  `app_faq` text NOT NULL,
  `otp_status` varchar(10) NOT NULL DEFAULT 'true',
  `watermark_on_off` varchar(20) NOT NULL DEFAULT 'false',
  `watermark_image` text DEFAULT NULL,
  `spinner_opt` varchar(10) NOT NULL DEFAULT 'Enable',
  `spinner_limit` int(10) NOT NULL DEFAULT 1,
  `default_youtube_url` text NOT NULL,
  `default_instagram_url` text NOT NULL,
  `auto_approve` varchar(10) NOT NULL,
  `auto_approve_img` varchar(10) NOT NULL DEFAULT 'off',
  `auto_approve_gif` varchar(10) NOT NULL DEFAULT 'off',
  `auto_approve_quote` varchar(10) NOT NULL DEFAULT 'off',
  `user_video_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_image_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_gif_upload_limit` int(10) NOT NULL DEFAULT 5,
  `user_quotes_upload_limit` int(10) NOT NULL DEFAULT 5,
  `video_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `image_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `gif_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `quotes_upload_opt` varchar(10) NOT NULL DEFAULT 'true',
  `video_file_size` int(5) NOT NULL DEFAULT 1,
  `video_file_duration` int(10) NOT NULL DEFAULT 1,
  `image_file_size` int(5) NOT NULL DEFAULT 1,
  `gif_file_size` int(5) NOT NULL DEFAULT 1,
  `cat_show_home_limit` int(10) NOT NULL DEFAULT 1,
  `delete_note` text NOT NULL,
  `ad_on_spin` varchar(30) NOT NULL DEFAULT 'false',
  `sms_otp_status` tinyint(1) NOT NULL DEFAULT 0,
  `extraSettingObject` text DEFAULT NULL,
  `app_update_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `envato_buyer_name`, `envato_purchase_code`, `envato_buyer_email`, `envato_purchased_status`, `package_name`, `email_from`, `redeem_points`, `redeem_money`, `redeem_currency`, `minimum_redeem_points`, `onesignal_app_id`, `onesignal_rest_key`, `app_name`, `app_logo`, `app_email`, `app_version`, `app_author`, `app_contact`, `app_website`, `app_description`, `app_developed_by`, `app_privacy_policy`, `api_page_limit`, `api_all_order_by`, `api_latest_limit`, `api_cat_order_by`, `api_cat_post_order_by`, `publisher_id`, `interstital_ad`, `interstital_ad_type`, `registration_reward`, `app_refer_reward`, `video_views`, `video_add`, `like_video_points`, `download_video_points`, `registration_reward_status`, `app_refer_reward_status`, `video_views_status`, `video_add_status`, `like_video_points_status`, `download_video_points_status`, `other_user_video_status`, `other_user_video_point`, `image_add`, `image_add_status`, `image_views`, `image_views_status`, `other_user_image_point`, `other_user_image_status`, `like_image_points`, `like_image_points_status`, `download_image_points`, `download_image_points_status`, `gif_add`, `gif_add_status`, `gif_views`, `gif_views_status`, `other_user_gif_point`, `other_user_gif_status`, `like_gif_points`, `like_gif_points_status`, `download_gif_points`, `download_gif_points_status`, `quotes_add`, `quotes_add_status`, `quotes_views`, `quotes_views_status`, `other_user_quotes_point`, `other_user_quotes_status`, `like_quotes_points`, `like_quotes_points_status`, `interstital_ad_id`, `interstital_ad_click`, `banner_ad`, `banner_ad_type`, `banner_ad_id`, `facebook_interstital_ad_id`, `facebook_banner_ad_id`, `rewarded_video_ads`, `rewarded_video_ads_id`, `rewarded_video_click`, `app_faq`, `otp_status`, `watermark_on_off`, `watermark_image`, `spinner_opt`, `spinner_limit`, `default_youtube_url`, `default_instagram_url`, `auto_approve`, `auto_approve_img`, `auto_approve_gif`, `auto_approve_quote`, `user_video_upload_limit`, `user_image_upload_limit`, `user_gif_upload_limit`, `user_quotes_upload_limit`, `video_upload_opt`, `image_upload_opt`, `gif_upload_opt`, `quotes_upload_opt`, `video_file_size`, `video_file_duration`, `image_file_size`, `gif_file_size`, `cat_show_home_limit`, `delete_note`, `ad_on_spin`, `sms_otp_status`, `extraSettingObject`, `app_update_type`) VALUES
(1, 'aw', 'qw', '-', 1, 'com.prayosha.shareyourstatus', '', 100, 1.00, 'RS', 500, '7885561d-ec61-4764-992a-2ad0c6ab70ad', 'MWYxNWFhNjMtYTZmZC00OTE5LTgyNTMtYzE0MGE2MWFmMTU0', 'Jakaas  - Share Status Get Money', 'ic_launcher.png', 'support@prayoshasoftech.tech', '1.0.0', 'Prayoshasoftech', 'support@prayoshasoftech.tech', 'https://prayoshasoftech.tech/', '', 'prayosha', '<p><strong>We are committed to protecting your privacy</strong></p>\r\n\r\n<p>We collect the minimum amount of information about you that is commensurate with providing you with a satisfactory service. This policy indicates the type of processes that may result in data being collected about you. Your use of this website gives us the right to collect that information.&nbsp;</p>\r\n\r\n<p><strong>Information Collected</strong></p>\r\n\r\n<p>We may collect any or all of the information that you give us depending on the type of transaction you enter into, including your name, address, telephone number, and email address, together with data about your use of the website. Other information that may be needed from time to time to process a request may also be collected as indicated on the website.</p>\r\n\r\n<p><strong>Information Use</strong></p>\r\n\r\n<p>We use the information collected primarily to process the task for which you visited the website. Data collected in the UK is held in accordance with the Data Protection Act. All reasonable precautions are taken to prevent unauthorized access to this information. This safeguard may require you to provide additional forms of identity should you wish to obtain information about your account details.</p>\r\n\r\n<p><strong>Cookies</strong></p>\r\n\r\n<p>Your Internet browser has the in-built facility for storing small files - &quot;cookies&quot; - that hold information which allows a website to recognise your account. Our website takes advantage of this facility to enhance your experience. You have the ability to prevent your computer from accepting cookies but, if you do, certain functionality on the website may be impaired.</p>\r\n\r\n<p><strong>Disclosing Information</strong></p>\r\n\r\n<p>We do not disclose any personal information obtained about you from this website to third parties unless you permit us to do so by ticking the relevant boxes in registration or competition forms. We may also use the information to keep in contact with you and inform you of developments associated with us. You will be given the opportunity to remove yourself from any mailing list or similar device. If at any time in the future we should wish to disclose information collected on this website to any third party, it would only be with your knowledge and consent.&nbsp;</p>\r\n\r\n<p>We may from time to time provide information of a general nature to third parties - for example, the number of individuals visiting our website or completing a registration form, but we will not use any information that could identify those individuals.&nbsp;</p>\r\n\r\n<p>In addition Dummy may work with third parties for the purpose of delivering targeted behavioural advertising to the Dummy website. Through the use of cookies, anonymous information about your use of our websites and other websites will be used to provide more relevant adverts about goods and services of interest to you. For more information on online behavioural advertising and about how to turn this feature off, please visit youronlinechoices.com/opt-out.</p>\r\n\r\n<p><strong>Changes to this Policy</strong></p>\r\n\r\n<p>Any changes to our Privacy Policy will be placed here and will supersede this version of our policy. We will take reasonable steps to draw your attention to any changes in our policy. However, to be on the safe side, we suggest that you read this document each time you use the website to ensure that it still meets with your approval.</p>\r\n\r\n<p><strong>Contacting Us</strong></p>\r\n\r\n<p>If you have any questions about our Privacy Policy, or if you want to know what information we have collected about you, please email us at hd@dummy.com. You can also correct any factual errors in that information or require us to remove your details form any list under our control.</p>\r\n', 20, 'DESC', 0, 'category_name', 'DESC', 'ca-app-pub-6077970083918284~9537954806', 'false', 'admob', 20, 20, 1, 5, 1, 1, 'true', 'true', 'true', 'true', 'true', 'false', 'false', '1', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 1, 'false', 'ca-app-pub-6077970083918284/4794277478', '10', 'false', 'admob', 'ca-app-pub-6077970083918284/6367213592', '', '', 'false', 'ca-app-pub-6077970083918284/3329986714', 10, '<p><strong>How to earn points in the video status app?</strong></p>\r\n\r\n<p>- When user views, like, download video or upload a video then they will get reward points.</p>\r\n\r\n<p>- <strong>Share your referral code to others and get reward points for every user registered with your reference code.</strong></p>\r\n\r\n<p>- When a user registers in the application they will get reward points too</p>\r\n\r\n<p><strong>Note:-</strong> When the user will upload the video and when the admin approves user video, after that user will get reward points.</p>\r\n\r\n<p><strong>Video Upload Guidance:-</strong></p>\r\n\r\n<p>- Please check that the uploading video file name is in English and there is no space in the video file name.</p>\r\n\r\n<p>- Please follow the instruction of video file size, duration, and format.</p>\r\n\r\n<p><strong>How to claim reward points and earn money?</strong></p>\r\n\r\n<p>- User need to acquire minimum points to claim money from reward points.</p>\r\n\r\n<p>- To claim money from reward points user have to fill the form and if there is any mistake in form you submitted, then you have to fill Contact Us form and admin will contact user ASAP</p>\r\n\r\n<p>- When admin approves user&#39;s claim for money after that user will get money</p>\r\n\r\n<p><strong>Note :-</strong></p>\r\n\r\n<p>- When you share video status to any social media app, the length of the video will be depended on the app you are sharing.</p>\r\n\r\n<p>- Any misbehavior or any type of sexual and unnecessary video upload will make admin block your account.</p>\r\n\r\n<p>- Allow the read and write file permission then you will be able to use download, upload and share the video feature otherwise you will not able to use.</p>\r\n\r\n<p>- Share video works only on supported social media applications.</p>\r\n\r\n<p>- User <strong>can select a payment method when filling the form to claim the money.&nbsp;</strong></p>\r\n\r\n<p>- User can upload video only in mp4 format</p>\r\n\r\n<p>- Spamming report video feature may lead to an account ban.</p>\r\n', 'true', 'true', 'New Project (4).png', 'true', 1, 'https://www.youtube.com/', 'https://www.instagram.com/', 'on', 'off', 'off', 'off', 20, 5, 5, 5, 'true', 'true', 'false', 'true', 10, 35, 5, 5, 20, 'Your all status, earn points, pending points data will be deleted after a click on the Delete button.', 'true', 0, 's:0:\"\";', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL DEFAULT 0,
  `slider_type` varchar(30) DEFAULT NULL,
  `slider_title` varchar(150) DEFAULT NULL,
  `external_url` text DEFAULT NULL,
  `external_image` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `post_id`, `slider_type`, `slider_title`, `external_url`, `external_image`, `status`) VALUES
(2, 3, 'video', '', '', '', 0),
(5, 1, 'quote', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_smtp_settings`
--

CREATE TABLE `tbl_smtp_settings` (
  `id` int(5) NOT NULL,
  `smtp_host` varchar(150) NOT NULL,
  `smtp_email` varchar(150) NOT NULL,
  `smtp_password` text NOT NULL,
  `smtp_secure` varchar(20) NOT NULL,
  `port_no` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_smtp_settings`
--

INSERT INTO `tbl_smtp_settings` (`id`, `smtp_host`, `smtp_email`, `smtp_password`, `smtp_secure`, `port_no`) VALUES
(1, 'mail.prayoshasoftech.tech ', 'support@prayoshasoftech.tech', 'LO*Sl1m7@xG@', 'tls', '587');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_spinner`
--

CREATE TABLE `tbl_spinner` (
  `block_id` int(5) NOT NULL,
  `block_points` varchar(5) NOT NULL,
  `block_bg` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_spinner`
--

INSERT INTO `tbl_spinner` (`block_id`, `block_points`, `block_bg`) VALUES
(2, '1', '0DABE9'),
(3, '2', 'E0E92D'),
(4, '3', 'E94C1E'),
(5, '4', 'E91E06'),
(6, '5', '0B8945'),
(7, '6', '86E93B'),
(8, '8', 'A580E9'),
(9, '9', 'E9B3CC'),
(10, '10', 'E91E63');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suspend_account`
--

CREATE TABLE `tbl_suspend_account` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `suspended_on` varchar(255) NOT NULL,
  `activated_on` int(11) DEFAULT NULL,
  `suspension_reason` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_image` varchar(500) DEFAULT NULL,
  `total_followers` int(11) NOT NULL DEFAULT 0,
  `total_following` int(11) NOT NULL DEFAULT 0,
  `user_youtube` varchar(500) DEFAULT NULL,
  `user_instagram` varchar(500) DEFAULT NULL,
  `confirm_code` varchar(255) DEFAULT NULL,
  `total_point` int(11) NOT NULL DEFAULT 0,
  `is_verified` int(1) NOT NULL DEFAULT 0,
  `player_id` text DEFAULT NULL,
  `is_duplicate` int(1) NOT NULL DEFAULT 0,
  `registration_on` varchar(255) NOT NULL DEFAULT '0',
  `auth_id` varchar(255) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT '1',
  `is_premium` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_code`, `user_type`, `device_id`, `name`, `email`, `password`, `phone`, `user_image`, `total_followers`, `total_following`, `user_youtube`, `user_instagram`, `confirm_code`, `total_point`, `is_verified`, `player_id`, `is_duplicate`, `registration_on`, `auth_id`, `status`, `is_premium`) VALUES
(0, 'sudhir', 'Normal', '0', 'Sudhir Rupani', 'sudhirrupani191993@gmail.com', 'sudhir@125', '9696969696', '', 37, 5, '', '', NULL, 50, 0, '11e01b65-f4e7-401c-9c9d-369a23ce0126', 0, '1610399700', '0', '1', 1),
(6, '8vgdyyee', 'Normal', '5c6ea5bcb42f560e', 'sudhir rupani', 'common@prayoshasoftech.tech', 'sudhir125', '9696969696', NULL, 1, 1, NULL, NULL, NULL, 5, 0, NULL, 1, '1611688500', '0', '1', NULL),
(10, 'hi6qjww0', 'Normal', '610c5d01f4f1e672', 'Radha', 'radha19932013@gmail.com', 'Tula*1230', '9081223344', '', 0, 1, '', '', NULL, 7, 0, 'd384d71b-c7aa-403f-b6ed-38e495ed90d0', 0, '1614972360', '0', '1', 1),
(15, 'vbsvomu2', 'Google', 'e29bd3f2ff0c2f17', 'Rahul patel', 'rpatel191993@gmail.com', '', '96969696 6', '09032021085041_91646.png', 1, 3, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 318, 0, 'b21692d0-afbe-43c9-8fa2-7c7e1c64889d', 0, '1615035540', '115950721166660993522', '1', 1),
(16, '08hv8fhd', 'Google', '8e5c307145714f21', 'Sonal Rupani', 'sonalrupani1995@gmail.com', '', '', '', 0, 1, '', '', NULL, 39, 0, '8ea7b2d6-5ed5-42e9-87f7-c5518ce03709', 0, '1615035720', '107334838946619691858', '1', 1),
(17, 'ajvpmbms', 'Google', '09d6799c693f233d', 'Creative Gujju', 'rvgadhiya95@gmail.com', '', '', '', 1, 5, '', '', NULL, 340, 0, 'b2b9bb30-bab1-40b4-a495-032c0eac50c6', 0, '1615035900', '115171828367183762840', '1', 1),
(18, 'kkkczydy', 'Google', '24d28daffd6af7a9', 'Mit Bhikadiya', 'mitbhikadiya72@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'cd083c19-0d1b-4117-98b4-9ebdeeaa3e16', 0, '1615039140', '110215649223746106147', '1', NULL),
(19, 'i7gg2r7j', 'Google', '24d28daffd6af7a9', 'Meet Patel', 'meet91957@gmail.com', '', '', '', 0, 1, '', '', NULL, 25, 0, 'cd083c19-0d1b-4117-98b4-9ebdeeaa3e16', 1, '1615039200', '102116458843636954918', '1', 1),
(20, 'jtjnmjr6', 'Google', '31ff22a9ad489fd6', 'unknown unknown', 'lovevibes87@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 22, 0, 'deb61210-ec1d-4fbd-bebe-a23e775cbe59', 0, '1615126980', '116884992982400168484', '1', NULL),
(21, 'kdpexh46', 'Normal', 'a6beca05ced8792', 'Bado Dezider ', 'badodezider1@Gmail.com', 'Nikolas6', '07539845706', NULL, 0, 1, NULL, NULL, NULL, 20, 0, '16be0ca3-a0ce-4eab-8e88-2bf93a3f89fd', 0, '1615201800', '0', '1', NULL),
(22, '6sdzoqqs', 'Normal', '3bd1c52bbc5e0f81', 'sagar variya', 'sagar.variya@yahoo.com', '123654', '8155077469', NULL, 0, 1, NULL, NULL, NULL, 41, 0, '49936cee-fd9a-4cd9-961a-c3807e076945', 0, '1615273440', '0', '1', NULL),
(23, 'c6rqijix', 'Normal', 'df8145f0b6a8ca20', 'nirali ', 'variyasagar512@gmail.com', '123654', '8141906953', NULL, 0, 1, NULL, NULL, NULL, 20, 0, '7ab09aaf-032d-4f27-ac90-701cca813c9b', 0, '1615274460', '0', '1', NULL),
(25, 'qpzk8x2w', 'Google', 'c28e7ed9302f1d70', 'Jeel Kuvadiya', 'kuvadiyajeel@gmail.com', '', '6355767276', '09032021100725_85744.jpg', 1, 2, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 63, 0, 'c56ac4e7-e658-42af-b8dc-09e836d5b577', 1, '1615278900', '102026408590635161415', '1', NULL),
(26, '3s4bhuww', 'Google', 'c28e7ed9302f1d70', 'Jeel Kuvadiya', 'jeelkuvadiya2001@gmail.com', '', '', '', 1, 1, '', '', NULL, 20, 0, 'c56ac4e7-e658-42af-b8dc-09e836d5b577', 1, '1615279020', '105967077320494583706', '1', 1),
(27, 'yv2cpfqf', 'Google', 'd21703ec236d5775', 'Desani jas', 'jasdesani@gmail.com', '', '917359813774', '09032021075524_94827.jpg', 0, 1, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 21, 0, 'f7333693-9aaa-4c08-98f8-877cdbfd32fd', 0, '1615299540', '114484142534523717052', '1', NULL),
(28, 'jp6n78nf', 'Google', '378b3fcbc5ae28b2', 'Alpesh Gadhiya', 'gadhiyaa797@gmail.com', '', '', NULL, 1, 1, NULL, NULL, NULL, 21, 0, 'd9f62c65-182a-4391-8dd3-d904ad97a3ff', 0, '1615300260', '105485924877968259297', '1', NULL),
(29, 'zf8p7ri2', 'Google', '4ff745bad2d70bb5', 'Dharmik Moradiya', 'dharmikmoradiya1182003@gmail.com', '', '8347798826', '09032021100554_52631.png', 2, 1, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 30, 0, '3ce9e253-f4e8-4480-8a3c-1220ebfe239c', 0, '1615306440', '117832668152813135770', '1', NULL),
(30, 's6ruw0d6', 'Google', 'c53a4a23490522fc', 'punita Gabani', 'gabani1994@gmail.com', '', '6352607511', '10032021030619_20122.jpg', 0, 1, 'https://www.youtube.com/', 'https://www.instagram.com/', NULL, 20, 0, '49af1fc3-3dce-43fa-9f1e-75ec41a80867', 0, '1615308900', '116153188268231701612', '1', NULL),
(31, 'p3aaoqge', 'Google', 'afd427ac0db1b7c3', 'Yatish Narigara', 'yatishnarigara7@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'ac94dbc3-f713-45b0-963b-0d6a26abfc47', 0, '1615343040', '112000377378803331747', '1', NULL),
(32, 'kkwzmhgd', 'Google', '1adbc243ea057f26', 'Parth Hingrajiya', 'parthhingrajiya@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 31, 0, '81e73a18-4562-46d1-8c0b-2dd393d22096', 0, '1615358100', '104297429646190117042', '1', NULL),
(33, 'gb26zeyh', 'Google', 'b5bc9f89fcbc002d', 'Sandip Gabani', 'sandipgabani25@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'e3f73203-ab34-49b9-955c-835ef38a1297', 0, '1615393020', '107329588143347369334', '1', NULL),
(34, '5vjtra6a', 'Google', '85e897dafdf030cc', 'Parth Patel', 'parthpatel.pn@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 88, 0, 'c93b2087-ed78-470a-9695-01c4613d3b27', 0, '1615393680', '112722642161714848853', '1', NULL),
(36, 'kv5djm87', 'Google', 'b958f02718a35712', 'Milanjitsinh Sindha', 'milanjitsinh@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'cfd8c79c-b597-40d7-bcd8-ab9e1c28ba14', 0, '1615393980', '104449551941960272470', '1', NULL),
(37, 'wde3wycn', 'Google', '7d784f6f5fd5d2af', 'bhavesh savani', 'savanibhavesh311@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 21, 0, '618a913a-14e6-4194-a8ac-1ed09d796f94', 0, '1615397100', '110517397577756409808', '1', NULL),
(38, '7qemcgde', 'Google', 'cb933b204a767978', 'Jeel Patel', 'jeel73874@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'd3a59b41-cdd3-4f62-95e6-c0a609af9357', 0, '1615399140', '110760582004492740157', '1', NULL),
(39, '0w84us4g', 'Google', 'b001d1d334816ce1', 'Kishan Metadiya', 'kishanmetadiya5577@gmail.com', '', '', NULL, 0, 2, NULL, NULL, NULL, 25, 0, '0e678551-4d62-435c-aebf-61d8092dffdd', 0, '1615437540', '113442009694070283504', '1', NULL),
(40, 'mybf6dkf', 'Google', 'd6ddc99124ed6b39', 'Atin Gupta', 'guptaatin.123@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'e9e461dd-058a-49a1-adb1-d6fceb071e27', 0, '1615513260', '111360753766000983161', '1', NULL),
(41, 'r25dtrqe', 'Google', '49b2cff0473e32e1', 'Vishal Gohil', 'vg3057611@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, '88e8a41b-0416-4530-8d62-89b650b00337', 0, '1615518180', '103277231738064910464', '1', NULL),
(42, 'rntek6k2', 'Google', 'dbf9a04fbc2640c5', 'Kgmadam Ahir', 'kgmadamahir477@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 32, 0, '3aa8d02a-46e8-4073-ae7b-36ea6ba1b5ea', 0, '1615538580', '107850773549527819131', '1', NULL),
(43, 'wf4y3tqb', 'Google', '0a0bb11669884e34', 'Jay Patel', 'jay290497@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 20, 0, 'decde14b-1f31-45cb-8340-fcbf1252d3b1', 0, '1615560840', '102549667515479484199', '1', NULL),
(44, 'ebv8yifa', 'Google', 'd81d3367cfb5610a', 'Hanil Patel', 'hanilpatel2396@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 21, 0, '747e3bad-c2d4-49f5-82ca-fb997c966a45', 0, '1615617960', '103400535511797437726', '1', NULL),
(45, 'us4gjqhg', 'Google', '150086a43a1b377e', 'DIVYA JAIN', 'dj4394015@gmail.com', '', '', NULL, 0, 1, NULL, NULL, NULL, 21, 0, 'ee2125d0-2b6a-4d0c-92bb-e862e313eff5', 0, '1615632120', '105514714927560173090', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_redeem`
--

CREATE TABLE `tbl_users_redeem` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_points` int(11) NOT NULL,
  `redeem_price` float(11,2) NOT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `bank_details` text NOT NULL,
  `request_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cust_message` longtext DEFAULT NULL,
  `receipt_img` text DEFAULT NULL,
  `responce_date` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_rewards_activity`
--

CREATE TABLE `tbl_users_rewards_activity` (
  `id` int(10) NOT NULL,
  `user_id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `activity_type` varchar(255) NOT NULL,
  `points` int(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `redeem_id` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users_rewards_activity`
--

INSERT INTO `tbl_users_rewards_activity` (`id`, `user_id`, `post_id`, `activity_type`, `points`, `date`, `redeem_id`, `status`) VALUES
(1, 0, 0, 'Register Rewards', 5, '2021-01-11 21:03:22', 0, 1),
(2, 0, 0, 'Register Rewards', 5, '2021-01-11 21:05:36', 0, 1),
(3, 1, 5, 'GIF View', 1, '2021-01-12 08:58:34', 0, 1),
(4, 1, 0, 'Lucky Spin', 5, '2021-01-12 08:59:37', 0, 1),
(5, 1, 0, 'Lucky Spin', 6, '2021-01-12 08:59:43', 0, 1),
(6, 0, 0, 'Register Rewards', 5, '2021-01-12 09:06:50', 0, 1),
(7, 0, 0, 'Register Rewards', 5, '2021-01-12 19:52:47', 0, 1),
(8, 0, 0, 'Register Rewards', 5, '2021-01-12 20:13:01', 0, 1),
(9, 0, 0, 'Register Rewards', 5, '2021-01-12 20:58:09', 0, 1),
(10, 0, 0, 'Register Rewards', 5, '2021-01-12 20:58:19', 0, 1),
(11, 0, 0, 'Register Rewards', 5, '2021-01-12 20:59:21', 0, 1),
(12, 2, 0, 'Register Rewards', 5, '2021-01-12 21:01:25', 0, 1),
(14, 0, 0, 'User Refer Rewards - Used by sudhir', 5, '2021-01-23 16:33:32', 0, 1),
(18, 6, 0, 'Register Rewards', 5, '2021-01-26 19:15:09', 0, 1),
(20, 7, 0, 'Register Rewards', 5, '2021-01-27 19:31:36', 0, 1),
(21, 0, 22, 'Add Video', 5, '2021-02-18 19:46:48', 0, 1),
(32, 10, 0, 'Register Rewards', 5, '2021-03-05 19:26:58', 0, 1),
(33, 10, 22, 'Video Like', 2, '2021-03-05 19:47:29', 0, 1),
(39, 15, 0, 'Register Rewards', 20, '2021-03-06 12:59:07', 0, 1),
(40, 16, 0, 'Register Rewards', 20, '2021-03-06 13:02:38', 0, 1),
(41, 15, 0, 'User Refer Rewards - Used by Sonal Rupani', 20, '2021-03-06 13:03:21', 0, 1),
(42, 17, 0, 'Register Rewards', 20, '2021-03-06 13:05:51', 0, 1),
(43, 16, 0, 'Lucky Spin', 10, '2021-03-06 13:06:26', 0, 1),
(44, 15, 0, 'Lucky Spin', 4, '2021-03-06 13:06:44', 0, 1),
(45, 17, 1120, 'Add Video', 5, '2021-03-06 13:49:30', 0, 1),
(46, 17, 1121, 'Add Video', 5, '2021-03-06 13:59:25', 0, 1),
(47, 18, 0, 'Register Rewards', 20, '2021-03-06 13:59:25', 0, 1),
(48, 19, 0, 'Register Rewards', 20, '2021-03-06 14:00:19', 0, 1),
(49, 15, 1116, 'Video View', 1, '2021-03-06 14:01:12', 0, 1),
(50, 15, 0, 'User Refer Rewards - Used by Meet Patel', 20, '2021-03-06 14:02:16', 0, 1),
(51, 17, 1122, 'Add Video', 5, '2021-03-06 14:03:16', 0, 1),
(52, 16, 1121, 'Video View', 1, '2021-03-06 14:05:34', 0, 1),
(53, 16, 1120, 'Video View', 1, '2021-03-06 14:05:59', 0, 1),
(54, 16, 1116, 'Video View', 1, '2021-03-06 14:06:34', 0, 1),
(55, 19, 1123, 'Add Video', 5, '2021-03-06 14:55:34', 0, 1),
(56, 17, 1124, 'Add Video', 5, '2021-03-07 06:09:00', 0, 1),
(57, 16, 1125, 'Add Video', 5, '2021-03-07 06:14:53', 0, 1),
(58, 17, 1125, 'Video View', 1, '2021-03-07 06:15:32', 0, 1),
(59, 15, 0, 'Lucky Spin', 4, '2021-03-07 09:46:58', 0, 1),
(60, 20, 0, 'Register Rewards', 20, '2021-03-07 14:23:16', 0, 1),
(61, 15, 1123, 'Video Download', 1, '2021-03-08 08:05:13', 0, 1),
(62, 15, 0, 'Lucky Spin', 5, '2021-03-08 08:07:06', 0, 1),
(63, 15, 1124, 'Video Like', 1, '2021-03-08 08:46:12', 0, 1),
(64, 20, 1108, 'Video View', 1, '2021-03-08 10:02:22', 0, 1),
(65, 20, 1094, 'Video Download', 1, '2021-03-08 10:03:39', 0, 1),
(66, 21, 0, 'Register Rewards', 20, '2021-03-08 11:10:04', 0, 1),
(67, 15, 1124, 'Video View', 1, '2021-03-08 13:41:29', 0, 1),
(68, 15, 1128, 'Add Video', 5, '2021-03-08 13:44:11', 0, 1),
(69, 15, 1129, 'Add Video', 5, '2021-03-08 13:45:44', 0, 1),
(70, 15, 1130, 'Add Video', 5, '2021-03-08 13:48:34', 0, 1),
(71, 17, 1131, 'Add Video', 5, '2021-03-09 04:50:18', 0, 1),
(72, 17, 1132, 'Add Video', 5, '2021-03-09 04:51:23', 0, 1),
(73, 17, 1133, 'Add Video', 5, '2021-03-09 04:52:09', 0, 1),
(74, 15, 1134, 'Add Video', 5, '2021-03-09 04:55:55', 0, 1),
(75, 15, 1135, 'Add Video', 5, '2021-03-09 04:57:23', 0, 1),
(76, 15, 1136, 'Add Video', 5, '2021-03-09 04:58:57', 0, 1),
(77, 15, 1137, 'Add Video', 5, '2021-03-09 05:00:36', 0, 1),
(78, 15, 1138, 'Add Video', 5, '2021-03-09 05:02:12', 0, 1),
(79, 15, 1139, 'Add Video', 5, '2021-03-09 05:03:48', 0, 1),
(80, 15, 1140, 'Add Video', 5, '2021-03-09 05:04:57', 0, 1),
(81, 15, 1141, 'Add Video', 5, '2021-03-09 05:10:52', 0, 1),
(82, 15, 1142, 'Add Video', 5, '2021-03-09 05:12:52', 0, 1),
(83, 15, 1143, 'Add Video', 5, '2021-03-09 05:14:42', 0, 1),
(84, 15, 1144, 'Add Video', 5, '2021-03-09 05:16:39', 0, 1),
(85, 15, 1145, 'Add Video', 5, '2021-03-09 05:19:03', 0, 1),
(86, 15, 1146, 'Add Video', 5, '2021-03-09 05:20:02', 0, 1),
(87, 15, 1147, 'Add Video', 5, '2021-03-09 05:21:41', 0, 1),
(88, 15, 1148, 'Add Video', 5, '2021-03-09 05:22:56', 0, 1),
(89, 15, 1149, 'Add Video', 5, '2021-03-09 05:25:10', 0, 1),
(90, 15, 1150, 'Add Video', 5, '2021-03-09 05:26:51', 0, 1),
(91, 15, 1151, 'Add Video', 5, '2021-03-09 05:28:21', 0, 1),
(92, 15, 1152, 'Add Video', 5, '2021-03-09 05:29:50', 0, 1),
(93, 15, 1153, 'Add Video', 5, '2021-03-09 05:30:49', 0, 1),
(94, 15, 1154, 'Add Video', 5, '2021-03-09 05:32:09', 0, 1),
(95, 15, 1155, 'Add Video', 5, '2021-03-09 05:34:07', 0, 1),
(96, 15, 1156, 'Add Video', 5, '2021-03-09 05:35:33', 0, 1),
(97, 15, 1157, 'Add Video', 5, '2021-03-09 05:37:05', 0, 1),
(98, 15, 1158, 'Add Video', 5, '2021-03-09 05:38:27', 0, 1),
(99, 15, 1159, 'Add Video', 5, '2021-03-09 05:39:50', 0, 1),
(100, 15, 1160, 'Add Video', 5, '2021-03-09 05:42:13', 0, 1),
(101, 15, 1161, 'Add Video', 5, '2021-03-09 05:43:32', 0, 1),
(102, 15, 1162, 'Add Video', 5, '2021-03-09 05:45:11', 0, 1),
(103, 15, 1131, 'Video Like', 1, '2021-03-09 06:45:53', 0, 1),
(104, 15, 1131, 'Video View', 1, '2021-03-09 06:46:27', 0, 1),
(105, 22, 0, 'Register Rewards', 20, '2021-03-09 07:04:55', 0, 1),
(106, 23, 0, 'Register Rewards', 20, '2021-03-09 07:21:42', 0, 1),
(107, 22, 0, 'User Refer Rewards - Used by nirali ', 20, '2021-03-09 07:21:42', 0, 1),
(108, 22, 1158, 'Video Like', 1, '2021-03-09 07:23:05', 0, 1),
(111, 25, 0, 'Register Rewards', 20, '2021-03-09 08:35:23', 0, 1),
(112, 26, 0, 'Register Rewards', 20, '2021-03-09 08:37:09', 0, 1),
(113, 27, 0, 'Register Rewards', 20, '2021-03-09 14:19:06', 0, 1),
(114, 27, 510, 'Video Like', 1, '2021-03-09 14:20:51', 0, 1),
(115, 28, 0, 'Register Rewards', 20, '2021-03-09 14:31:54', 0, 1),
(116, 17, 1160, 'Video View', 1, '2021-03-09 14:36:34', 0, 1),
(117, 15, 0, 'Lucky Spin', 5, '2021-03-09 15:22:36', 0, 1),
(118, 25, 1139, 'Video View', 1, '2021-03-09 15:38:03', 0, 1),
(119, 25, 1124, 'Video View', 1, '2021-03-09 15:43:40', 0, 1),
(120, 25, 1124, 'Video Like', 1, '2021-03-09 15:44:56', 0, 1),
(121, 29, 0, 'Register Rewards', 20, '2021-03-09 16:14:05', 0, 1),
(122, 25, 0, 'User Refer Rewards - Used by 41 F Y MORADIYA DHARMIK P.', 20, '2021-03-09 16:14:27', 0, 1),
(123, 25, 1131, 'Video View', 1, '2021-03-09 16:33:47', 0, 1),
(124, 29, 1164, 'Add Video', 5, '2021-03-09 16:33:58', 0, 1),
(125, 29, 1163, 'Add Video', 5, '2021-03-09 16:34:03', 0, 1),
(126, 25, 1164, 'Video View', 1, '2021-03-09 16:34:30', 0, 1),
(127, 25, 1164, 'Video Like', 1, '2021-03-09 16:34:52', 0, 1),
(128, 25, 1164, 'Video Download', 1, '2021-03-09 16:34:55', 0, 1),
(129, 25, 1131, 'Video Download', 1, '2021-03-09 16:42:59', 0, 1),
(130, 30, 0, 'Register Rewards', 20, '2021-03-09 16:55:08', 0, 1),
(131, 17, 1164, 'Video View', 1, '2021-03-09 16:58:22', 0, 1),
(132, 15, 1164, 'Video Like', 1, '2021-03-09 17:00:51', 0, 1),
(133, 15, 145, 'Video View', 1, '2021-03-09 17:27:52', 0, 1),
(134, 15, 1164, 'Video Download', 1, '2021-03-09 17:37:50', 0, 1),
(135, 17, 1166, 'Add Video', 5, '2021-03-09 17:58:54', 0, 1),
(136, 17, 1167, 'Add Video', 5, '2021-03-09 18:00:47', 0, 1),
(137, 17, 1168, 'Add Video', 5, '2021-03-09 18:02:05', 0, 1),
(138, 17, 1169, 'Add Video', 5, '2021-03-09 18:04:04', 0, 1),
(139, 17, 1170, 'Add Video', 5, '2021-03-09 18:05:37', 0, 1),
(140, 17, 1171, 'Add Video', 5, '2021-03-09 18:07:08', 0, 1),
(141, 17, 1172, 'Add Video', 5, '2021-03-09 18:09:18', 0, 1),
(142, 17, 1173, 'Add Video', 5, '2021-03-09 18:10:31', 0, 1),
(143, 31, 0, 'Register Rewards', 20, '2021-03-10 02:24:52', 0, 1),
(144, 17, 1174, 'Add Video', 5, '2021-03-10 04:38:46', 0, 1),
(145, 32, 0, 'Register Rewards', 20, '2021-03-10 06:35:22', 0, 1),
(146, 32, 1174, 'Video Download', 1, '2021-03-10 06:35:57', 0, 1),
(147, 32, 0, 'Lucky Spin', 10, '2021-03-10 06:37:00', 0, 1),
(148, 15, 1169, 'Video Download', 1, '2021-03-10 06:39:08', 0, 1),
(149, 17, 1175, 'Add Video', 5, '2021-03-10 06:41:33', 0, 1),
(150, 17, 1176, 'Add Video', 5, '2021-03-10 06:42:40', 0, 1),
(151, 17, 1177, 'Add Video', 5, '2021-03-10 06:48:28', 0, 1),
(152, 17, 1178, 'Add Video', 5, '2021-03-10 06:54:32', 0, 1),
(153, 25, 1104, 'Video View', 1, '2021-03-10 15:31:40', 0, 1),
(154, 15, 615, 'Video View', 1, '2021-03-10 15:32:03', 0, 1),
(155, 25, 0, 'Lucky Spin', 3, '2021-03-10 15:35:21', 0, 1),
(156, 15, 0, 'Lucky Spin', 4, '2021-03-10 15:36:14', 0, 1),
(157, 33, 0, 'Register Rewards', 20, '2021-03-10 16:17:59', 0, 1),
(158, 34, 0, 'Register Rewards', 20, '2021-03-10 16:28:59', 0, 1),
(160, 15, 0, 'User Refer Rewards - Used by Parth Patel', 20, '2021-03-10 16:30:59', 0, 1),
(161, 34, 0, 'Lucky Spin', 10, '2021-03-10 16:32:55', 0, 1),
(162, 36, 0, 'Register Rewards', 20, '2021-03-10 16:33:44', 0, 1),
(163, 15, 0, 'User Refer Rewards - Used by Milanjitsinh Sindha', 20, '2021-03-10 16:34:12', 0, 1),
(164, 25, 1177, 'Video View', 1, '2021-03-10 16:36:27', 0, 1),
(165, 25, 1175, 'Video View', 1, '2021-03-10 16:38:07', 0, 1),
(166, 37, 0, 'Register Rewards', 20, '2021-03-10 17:25:54', 0, 1),
(167, 37, 1178, 'Video View', 1, '2021-03-10 17:27:10', 0, 1),
(168, 25, 1155, 'Video View', 1, '2021-03-10 17:29:20', 0, 1),
(169, 17, 1181, 'Add Video', 5, '2021-03-10 17:40:21', 0, 1),
(170, 17, 1182, 'Add Video', 5, '2021-03-10 17:41:52', 0, 1),
(171, 17, 1183, 'Add Video', 5, '2021-03-10 17:44:43', 0, 1),
(172, 17, 1184, 'Add Video', 5, '2021-03-10 17:46:21', 0, 1),
(173, 17, 1185, 'Add Video', 5, '2021-03-10 17:48:10', 0, 1),
(174, 17, 1186, 'Add Video', 5, '2021-03-10 17:49:37', 0, 1),
(175, 17, 1187, 'Add Video', 5, '2021-03-10 17:50:39', 0, 1),
(176, 17, 1188, 'Add Video', 5, '2021-03-10 17:52:47', 0, 1),
(177, 17, 1189, 'Add Video', 5, '2021-03-10 17:58:29', 0, 1),
(178, 17, 1190, 'Add Video', 5, '2021-03-10 17:59:47', 0, 1),
(179, 38, 0, 'Register Rewards', 20, '2021-03-10 17:59:55', 0, 1),
(180, 15, 1191, 'Add Video', 5, '2021-03-10 18:00:50', 0, 1),
(181, 17, 1192, 'Add Video', 5, '2021-03-10 18:01:35', 0, 1),
(182, 17, 1193, 'Add Video', 5, '2021-03-10 18:03:24', 0, 1),
(183, 17, 1194, 'Add Video', 5, '2021-03-10 18:04:29', 0, 1),
(184, 17, 1195, 'Add Video', 5, '2021-03-10 18:05:43', 0, 1),
(185, 17, 1196, 'Add Video', 5, '2021-03-10 18:39:27', 0, 1),
(186, 17, 1197, 'Add Video', 5, '2021-03-10 18:40:21', 0, 1),
(187, 17, 1198, 'Add Video', 5, '2021-03-10 18:41:36', 0, 1),
(188, 17, 1199, 'Add Video', 5, '2021-03-10 18:44:02', 0, 1),
(189, 17, 1200, 'Add Video', 5, '2021-03-10 18:44:55', 0, 1),
(190, 17, 1201, 'Add Video', 5, '2021-03-10 18:45:46', 0, 1),
(191, 17, 1202, 'Add Video', 5, '2021-03-10 18:46:41', 0, 1),
(192, 17, 1203, 'Add Video', 5, '2021-03-10 18:49:12', 0, 1),
(193, 17, 1204, 'Add Video', 5, '2021-03-10 18:50:08', 0, 1),
(194, 17, 1205, 'Add Video', 5, '2021-03-10 18:50:55', 0, 1),
(195, 17, 1206, 'Add Video', 5, '2021-03-10 18:51:50', 0, 1),
(196, 17, 1207, 'Add Video', 5, '2021-03-10 18:52:37', 0, 1),
(197, 17, 1208, 'Add Video', 5, '2021-03-10 18:53:38', 0, 1),
(198, 17, 1209, 'Add Video', 5, '2021-03-10 18:54:43', 0, 1),
(199, 17, 1210, 'Add Video', 5, '2021-03-10 18:55:31', 0, 1),
(200, 17, 1211, 'Add Video', 5, '2021-03-10 18:56:43', 0, 1),
(201, 17, 1212, 'Add Video', 5, '2021-03-10 18:57:36', 0, 1),
(202, 17, 1213, 'Add Video', 5, '2021-03-10 18:58:44', 0, 1),
(203, 17, 365, 'Video View', 1, '2021-03-10 18:59:38', 0, 1),
(204, 15, 1207, 'Video View', 1, '2021-03-11 03:35:41', 0, 1),
(205, 15, 1210, 'Video View', 1, '2021-03-11 03:35:59', 0, 1),
(206, 34, 1210, 'Video Like', 1, '2021-03-11 04:19:23', 0, 1),
(207, 34, 1197, 'Video Like', 1, '2021-03-11 04:19:44', 0, 1),
(208, 39, 0, 'Register Rewards', 20, '2021-03-11 04:39:53', 0, 1),
(209, 16, 1210, 'Video View', 1, '2021-03-11 05:17:51', 0, 1),
(210, 17, 1214, 'Add Video', 5, '2021-03-11 05:21:15', 0, 1),
(211, 15, 1215, 'Add Video', 5, '2021-03-11 06:02:02', 0, 1),
(212, 15, 1216, 'Add Video', 5, '2021-03-11 06:03:28', 0, 1),
(213, 17, 1217, 'Add Video', 5, '2021-03-11 08:44:51', 0, 1),
(214, 17, 1218, 'Add Video', 5, '2021-03-11 08:46:07', 0, 1),
(215, 17, 1219, 'Add Video', 5, '2021-03-11 08:47:19', 0, 1),
(216, 15, 0, 'Lucky Spin', 3, '2021-03-11 10:16:32', 0, 1),
(217, 17, 0, 'Lucky Spin', 6, '2021-03-11 10:17:08', 0, 1),
(218, 39, 1154, 'Video View', 1, '2021-03-11 10:47:02', 0, 1),
(219, 39, 1186, 'Video View', 1, '2021-03-11 10:48:14', 0, 1),
(220, 39, 0, 'Lucky Spin', 3, '2021-03-11 10:50:50', 0, 1),
(221, 25, 0, 'Lucky Spin', 6, '2021-03-11 16:30:26', 0, 1),
(222, 40, 0, 'Register Rewards', 20, '2021-03-12 01:41:15', 0, 1),
(223, 25, 1153, 'Video View', 1, '2021-03-12 03:01:20', 0, 1),
(224, 41, 0, 'Register Rewards', 20, '2021-03-12 03:03:46', 0, 1),
(225, 42, 0, 'Register Rewards', 20, '2021-03-12 08:43:59', 0, 1),
(226, 42, 0, 'Lucky Spin', 6, '2021-03-12 08:44:49', 0, 1),
(227, 17, 1223, 'Add Video', 5, '2021-03-12 11:41:54', 0, 1),
(228, 17, 1224, 'Add Video', 5, '2021-03-12 11:44:00', 0, 1),
(229, 17, 1225, 'Add Video', 5, '2021-03-12 11:45:55', 0, 1),
(230, 17, 1226, 'Add Video', 5, '2021-03-12 11:48:16', 0, 1),
(231, 17, 1227, 'Add Video', 5, '2021-03-12 11:50:40', 0, 1),
(232, 17, 1228, 'Add Video', 5, '2021-03-12 11:53:08', 0, 1),
(233, 15, 1227, 'Video View', 1, '2021-03-12 14:20:12', 0, 1),
(234, 15, 0, 'Lucky Spin', 4, '2021-03-12 14:21:37', 0, 1),
(235, 28, 1061, 'Video Like', 1, '2021-03-12 14:24:24', 0, 1),
(236, 34, 1219, 'Video Like', 1, '2021-03-12 14:48:46', 0, 1),
(237, 34, 0, 'Lucky Spin', 6, '2021-03-12 14:52:23', 0, 1),
(238, 43, 0, 'Register Rewards', 20, '2021-03-12 14:54:32', 0, 1),
(239, 34, 0, 'User Refer Rewards - Used by Jay Patel', 20, '2021-03-12 14:54:52', 0, 1),
(240, 42, 0, 'Lucky Spin', 6, '2021-03-13 02:07:15', 0, 1),
(241, 34, 0, 'Lucky Spin', 9, '2021-03-13 02:40:00', 0, 1),
(242, 34, 22, 'Video Like', 1, '2021-03-13 02:40:58', 0, 1),
(243, 34, 21, 'Video Like', 1, '2021-03-13 02:41:12', 0, 1),
(244, 34, 20, 'Video Like', 1, '2021-03-13 02:41:18', 0, 1),
(245, 34, 19, 'Video Like', 1, '2021-03-13 02:41:23', 0, 1),
(246, 34, 18, 'Video Like', 1, '2021-03-13 02:41:27', 0, 1),
(247, 34, 17, 'Video Like', 1, '2021-03-13 02:41:35', 0, 1),
(248, 34, 16, 'Video Like', 1, '2021-03-13 02:41:38', 0, 1),
(249, 34, 15, 'Video Like', 1, '2021-03-13 02:41:42', 0, 1),
(250, 34, 14, 'Video Like', 1, '2021-03-13 02:41:45', 0, 1),
(251, 34, 12, 'Video Like', 1, '2021-03-13 02:41:55', 0, 1),
(252, 34, 11, 'Video Like', 1, '2021-03-13 02:42:01', 0, 1),
(253, 34, 10, 'Video Like', 1, '2021-03-13 02:42:05', 0, 1),
(254, 34, 9, 'Video Like', 1, '2021-03-13 02:42:09', 0, 1),
(255, 34, 8, 'Video Like', 1, '2021-03-13 02:42:13', 0, 1),
(256, 34, 7, 'Video Like', 1, '2021-03-13 02:42:18', 0, 1),
(257, 34, 6, 'Video Like', 1, '2021-03-13 02:42:22', 0, 1),
(258, 34, 5, 'Video Like', 1, '2021-03-13 02:42:26', 0, 1),
(259, 34, 4, 'Video Like', 1, '2021-03-13 02:42:30', 0, 1),
(260, 34, 3, 'Video Like', 1, '2021-03-13 02:42:33', 0, 1),
(261, 34, 2, 'Video Like', 1, '2021-03-13 02:42:38', 0, 1),
(262, 25, 1161, 'Video View', 1, '2021-03-13 02:58:06', 0, 1),
(263, 44, 0, 'Register Rewards', 20, '2021-03-13 06:46:42', 0, 1),
(264, 44, 1228, 'Video View', 1, '2021-03-13 06:48:10', 0, 1),
(265, 45, 0, 'Register Rewards', 20, '2021-03-13 10:42:40', 0, 1),
(266, 45, 178, 'Video Like', 1, '2021-03-13 10:45:01', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verify_user`
--

CREATE TABLE `tbl_verify_user` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `document` text NOT NULL,
  `created_at` varchar(150) NOT NULL,
  `verify_at` varchar(150) NOT NULL DEFAULT '0',
  `reject_reason` text DEFAULT NULL,
  `is_opened` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

CREATE TABLE `tbl_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `cat_id` int(11) NOT NULL,
  `lang_ids` text NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_url` text NOT NULL,
  `video_id` varchar(255) NOT NULL,
  `video_tags` text NOT NULL,
  `video_layout` varchar(255) NOT NULL DEFAULT 'Landscape',
  `video_thumbnail` text NOT NULL,
  `video_duration` varchar(255) DEFAULT NULL,
  `total_likes` int(11) NOT NULL DEFAULT 0,
  `totel_viewer` int(11) NOT NULL DEFAULT 0,
  `total_download` int(10) NOT NULL DEFAULT 0,
  `featured` int(1) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1,
  `isUploaded` tinyint(1) NOT NULL DEFAULT 0,
  `external_id` int(11) DEFAULT NULL,
  `cloud_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_video`
--

INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(1, 0, 71, '5', 'server_url', 'New Love Mashup', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073611/s71xlonquldbw64fbirr.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073614/tmpxz0upfu4yuzhb5vpr.jpg', NULL, 0, 1, 0, 0, 1, 1, 0, 0),
(2, 0, 71, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073630/qt7rpqgdl070p5nlqjmg.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073632/vsxamxhckwuf9ykmnybe.jpg', NULL, 1, 1, 0, 0, 1, 1, 0, 0),
(3, 0, 71, '5', 'server_url', 'Main Yeh Haath Jo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073671/xw5yerwarazot6mdlyla.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073673/veq1ajrqbytooje9bi3i.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(4, 0, 71, '5', 'server_url', 'Pal Pal Dil Ke Paas', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073694/laurt0h7qcgr2owczknw.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073697/wktsav27llqydu6rapu5.jpg', NULL, 1, 1, 0, 0, 1, 1, 0, 0),
(5, 0, 71, '5', 'server_url', 'Phir Chala', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073721/e5kbfx7geaakpoji2e12.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073723/ffy6kahvveiuctqw0m8w.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(6, 0, 71, '5', 'server_url', 'Dhanteras Wishes', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073746/zr52aavilish34fi4gyl.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073748/cm3guzr5eawclsiknzdc.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(7, 0, 71, '5', 'server_url', 'Aabaad Barbaad', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073764/qfxjypl6lvoazuyvmn9g.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073767/aen5cqjreg8lsuxe5fbv.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(8, 0, 71, '5', 'server_url', 'Teri Aankhon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073786/f7mgepb45ug5ziaa8yxz.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073788/xhseocw0lehulmjrcsp8.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(9, 0, 71, '5', 'server_url', 'Phir Chala', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073823/tg441vh4laeqxi0fwo9o.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073825/zrxbhbedqqxdgrmtawii.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(10, 0, 71, '5', 'server_url', 'Tum Hi Aana - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073851/yoryzjxg0fpcksehvlkc.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073854/aazcwipwamjuzgezek3p.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(11, 0, 71, '5', 'server_url', 'Raanjhana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073884/uf3gdcvbiptfpw32rues.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073886/uhfl8upfhv0yfdkp6neu.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(12, 0, 71, '5', 'server_url', 'Sanam Teri Kasam', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073908/umnylrpfh8zbufiimvhq.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073910/avm2cxq2tfpuadnoycfi.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 0, 71, '5', 'server_url', 'Tu Hi Re', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073915/dxvlarv6rdspqgot14an.mp4', '-', 'Lyrical', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073917/tgnioabb0343ozo4ini0.jpg', NULL, 0, 0, 0, 0, 1, 1, 0, 0),
(14, 0, 71, '5', 'server_url', 'New Love Mashup', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073950/nqko2w2nji0jjjezg1tb.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073952/qe1xhdmianlltld5syhc.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(15, 0, 71, '5', 'server_url', 'Dil Chahte Ho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609073980/mfim03qxd64r4nvxndau.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609073982/lafujlt76e4tbqcrcfgs.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(16, 0, 71, '5', 'server_url', 'Itni Si Baat Hain', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074017/g2ie9chlebxzjmedpeqt.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074019/wvy9bk8wvcmpwxbdg7ah.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(17, 0, 71, '5', 'server_url', 'Itni Si Baat Hain', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074041/a581cwzou4nmt5njotfb.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074043/aj5i73pfkek2ca4dqtaa.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(18, 0, 71, '5', 'server_url', 'Teri Khushboo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074061/fxmz6ovyiost82v9neap.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074063/gifo9jjapjiih4aqxmb6.jpg', NULL, 1, 1, 0, 0, 1, 1, 0, 0),
(19, 0, 71, '5', 'server_url', 'Heartbeat', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074090/w1lpd9o84fihzilrwnt3.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074093/ulerw1hgeguqomsyei8f.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(20, 0, 71, '5', 'server_url', 'Taaron Ke Shehar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074113/yhpktfy9wdnryjycz0fa.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074116/eeq5b04h0njrbfikej1t.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(21, 0, 71, '5', 'server_url', 'Tu Hi Hai Aashiqui', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074130/odrbr79311fvykla0ti8.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074132/fzdtgsezvw0zwllmuk8k.jpg', NULL, 1, 0, 0, 0, 1, 1, 0, 0),
(22, 0, 71, '5', 'server_url', 'Khulne Do', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609074186/zrov4ateq466qlz87kb3.mp4', '-', 'Lyrical', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609074188/zvvslvs4ucyig2ozvqyk.jpg', NULL, 2, 0, 0, 0, 1, 1, 0, 0),
(24, 1, 72, '5', 'server_url', 'Tu Hai', 'http://www.kids2win.com/api/videosFile/P4ei7_tu-hai-f-maha-h.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7YeJq_tu-hai-f-maha-h.jpg?h=270&drp=1', NULL, 0, 28850, 0, 0, 1, 0, 8611, 0),
(25, 1, 72, '5', 'server_url', 'Shambhu Nath Re', 'http://www.kids2win.com/api/videosFile/EZ8at_shambhu-nath-re-h-maha.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4UCTP_shambhu-nath-re-h-maha.jpg?h=270&drp=1', NULL, 0, 47966, 0, 0, 1, 0, 8405, 0),
(26, 1, 72, '5', 'server_url', 'Shiv Shakti', 'http://www.kids2win.com/api/videosFile/MAhTE_shiv-shakti-mah-h-sq.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/eHcyr_shiv-shakti-mah-h-sq.jpg?h=270&drp=1', NULL, 0, 50656, 0, 0, 1, 0, 8372, 0),
(27, 1, 72, '5', 'server_url', 'Bholenath', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316600/oyhet0gfq9zkza08xuix.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/1zPRm_bholenath-f-mahadev-h.jpg?h=270&drp=1', NULL, 169, 531, 84, 0, 1, 1, 8218, 0),
(28, 1, 72, '5', 'server_url', 'Mahadev', 'http://www.kids2win.com/api/videosFile/Sc6uE_mahadev-h-sq.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pA0uz_mahadev-h-sq.jpg?h=270&drp=1', NULL, 0, 49687, 0, 0, 1, 0, 8214, 0),
(29, 1, 72, '5', 'server_url', 'Mann Mein Shiva', 'http://www.kids2win.com/api/videosFile/EY3OQ_mann-mein-shiva-mah-h-15.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mnK4V_mann-mein-shiva-mah-h-15.jpg?h=270&drp=1', NULL, 0, 86845, 0, 0, 1, 0, 7660, 0),
(30, 1, 72, '5', 'server_url', 'Jai Mahakal', 'http://www.kids2win.com/api/videosFile/k7f8D_jai-mahakal-mah-h-f-15.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DLSxh_jai-mahakal-mah-h-f-15.jpg?h=270&drp=1', NULL, 0, 79695, 0, 0, 1, 0, 7526, 0),
(31, 1, 72, '5', 'server_url', 'Bholenath Shankara', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402737/diftlxdilojvm7wp2jll.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/URPyX_bholenath-shankara-mahad-h-sq.jpg?h=270&drp=1', NULL, 122, 644, 11, 0, 1, 1, 7493, 0),
(32, 1, 72, '5', 'server_url', 'Har Har Mahadev', 'http://www.kids2win.com/api/videosFile/Wb6KQ_har-har-mahadev-h.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8tsCW_har-har-mahadev-h.jpg?h=270&drp=1', NULL, 0, 47442, 0, 0, 1, 0, 7489, 0),
(33, 1, 72, '5', 'server_url', 'Jay Mahakal', 'http://www.kids2win.com/api/videosFile/wfZCc_jay-mahakal-f-h-mahadev.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/l6KLs_jay-mahakal-f-h-mahadev.jpg?h=270&drp=1', NULL, 0, 51105, 0, 0, 1, 0, 7332, 0),
(34, 1, 72, '5', 'server_url', 'Namo Namo', 'http://www.kids2win.com/api/videosFile/5Xnb9_namo-namo-f-h-mahadev.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3MfKY_namo-namo-f-h-mahadev.jpg?h=270&drp=1', NULL, 0, 61355, 0, 0, 1, 0, 7331, 0),
(35, 1, 72, '5', 'server_url', 'Har Har Mahadev', 'http://www.kids2win.com/api/videosFile/eq8JH_har-har-mahadev-h.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YSar1_har-har-mahadev-h.jpg?h=270&drp=1', NULL, 0, 42804, 0, 0, 1, 0, 7330, 0),
(36, 1, 72, '5', 'server_url', 'Auliya', 'http://www.kids2win.com/api/videosFile/3MmRb_auliya-h-mahadev.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OxwHt_auliya-h-mahadev.jpg?h=270&drp=1', NULL, 0, 44648, 0, 0, 1, 0, 7329, 0),
(37, 1, 72, '5', 'server_url', 'Namo Namo', 'http://www.kids2win.com/api/videosFile/KsGpy_namo-namo-mahadev-h-f.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/n5FKq_namo-namo-mahadev-h-f.jpg?h=270&drp=1', NULL, 0, 40155, 0, 0, 1, 0, 7309, 0),
(38, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://www.kids2win.com/api/videosFile/BrTo0_mahadev-status-h-f.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BvA5c_mahadev-status-h-f.jpg?h=270&drp=1', NULL, 0, 51998, 0, 0, 1, 0, 7296, 0),
(39, 1, 72, '5', 'server_url', 'Mann Mein Shiva', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313142/lmjakkuxzniihj7xd5dv.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313145/kpd3k7nuwypxdwa12y9j.jpg', NULL, 120, 714, 100, 0, 1, 1, 7272, 0),
(40, 1, 72, '5', 'server_url', 'Shravan Special', 'http://www.kids2win.com/api/videosFile/9iIAf_shravan-special-hindi-mahadev-god.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sLFdH_shravan-special-hindi-mahadev-god.jpg?h=270&drp=1', NULL, 0, 102972, 0, 0, 1, 0, 2606, 0),
(41, 1, 72, '5', 'server_url', 'Lord Shiva', 'http://www.kids2win.com/api/videosFile/8LHGR_lord-shiva-h-ly-mhadev.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iFPzL_lord-shiva-h-ly-mhadev.jpg?h=270&drp=1', NULL, 0, 112937, 0, 0, 1, 0, 6955, 0),
(42, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338863/cer9myzwxsfggsunokj8.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/fXBmH_mahadev-status-h.jpg?h=270&drp=1', NULL, 178, 751, 71, 0, 1, 1, 6434, 0),
(43, 1, 72, '5', 'server_url', 'Bhole Se Bhole Baba', 'http://www.kids2win.com/api/videosFile/bCD5d_bhole-se-bhole-baba-mahadev-hindi-square.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0VAgK_bhole-se-bhole-baba-mahadev-hindi-square.jpg?h=270&drp=1', NULL, 0, 150555, 0, 0, 1, 0, 5548, 0),
(44, 1, 72, '5', 'server_url', 'Ravan Ravan Hoon Main', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309423/hoefeamydd0erhto3dcs.mp4', '-', 'Mahadev', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309425/d35fy0e6ti5ue3gk2z93.jpg', NULL, 182, 727, 92, 0, 1, 1, 5264, 0),
(45, 1, 72, '5', 'server_url', 'Mera Bhola Hai Bhandari - Unplugged', 'http://www.kids2win.com/api/videosFile/nKc5z_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen-unplugged.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7K3iz_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen-unplugged.jpg?h=270&drp=1', NULL, 0, 127270, 0, 0, 1, 0, 4702, 0),
(46, 1, 72, '5', 'server_url', 'Mahashivratri Special - Mahadev', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271437/rxevpyp4noxsliji6ugq.mp4', '-', 'Mahadev', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271439/yfmv6d2b5livrrkewydc.jpg', NULL, 1, 734, 85, 0, 1, 1, 0, 0),
(47, 1, 72, '5', 'server_url', 'Mahashivratri Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314868/sbzylhcvwybxyjcx9hdo.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/WAJmM_mahashivratri-special-hindi-god-fullscreen-festival.jpg?h=270&drp=1', NULL, 197, 825, 80, 0, 1, 1, 4691, 0),
(48, 1, 72, '5', 'server_url', 'Mahadev Status - Mahashivratri Special', 'http://www.kids2win.com/api/videosFile/rwmGI_mahadev-status-mahashivratri-special-fullscreen-hindi-mahadeve.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7Rcpj_mahadev-status-mahashivratri-special-fullscreen-hindi-mahadeve.jpg?h=270&drp=1', NULL, 0, 71081, 0, 0, 1, 0, 2065, 0),
(49, 1, 72, '5', 'server_url', 'Mahashivratri Special - Mahadeve', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352900/honsncgsisyxqo0ox5yw.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352902/l70uu6an9e464wvyvvep.jpg', NULL, 146, 756, 51, 0, 1, 1, 2064, 0),
(50, 1, 72, '5', 'server_url', 'Happy Mahashivratri Status', 'http://www.kids2win.com/api/videosFile/WXV9G_happy-mahashivratri-status-hindi-mahadeve.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3wZNF_happy-mahashivratri-status-hindi-mahadeve.jpg?h=270&drp=1', NULL, 0, 37224, 0, 0, 1, 0, 2063, 0),
(51, 1, 72, '5', 'server_url', 'Mera Bhola Hai Bhandari', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353199/tbsnvqojxoidnwuffca3.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609353200/wigckra5zhzckrrk8mef.jpg', NULL, 166, 706, 71, 0, 1, 1, 3763, 0),
(52, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609341280/dev16eebek0fwfjmn2pf.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609341283/gdjy5ake8v4u92iqbunr.jpg', NULL, 163, 804, 72, 0, 1, 1, 3458, 0),
(53, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337013/atlxufs7723r8grwqzba.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337016/obulgfnmx1gsw5yzdwyy.jpg', NULL, 129, 732, 74, 0, 1, 1, 3353, 0),
(54, 1, 72, '5', 'server_url', 'Mahakal - Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401263/iltu2yxi81mrlmrkpyzv.mp4', '-', 'Mahadev', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401265/a3wq5xphkyiy3db5mkfp.jpg', NULL, 139, 451, 27, 0, 1, 1, 3245, 0),
(55, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://www.kids2win.com/api/videosFile/mYbSa_mahadev-status-hindi.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0dPWx_mahadev-status-hindi.jpg?h=270&drp=1', NULL, 0, 40870, 0, 0, 1, 0, 3160, 0),
(56, 1, 72, '5', 'server_url', 'Mahakal Dialogue', 'http://www.kids2win.com/api/videosFile/dDWN1_mahakal-dialogue-hindi-mahadev.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/S9Dl4_mahakal-dialogue-hindi-mahadev.jpg?h=270&drp=1', NULL, 0, 69574, 0, 0, 1, 0, 3082, 0),
(57, 1, 72, '5', 'server_url', 'Mahadev Status', 'http://www.kids2win.com/api/videosFile/yS7mL_mahadev-status-hindi-god.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/THzSO_mahadev-status-hindi-god.jpg?h=270&drp=1', NULL, 0, 35776, 0, 0, 1, 0, 3071, 0),
(58, 1, 72, '5', 'server_url', 'Shiv Shiv Mahadev', 'http://www.kids2win.com/api/videosFile/fKFqv_shiv-shiv-mahadev-god-hindi.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/il15o_shiv-shiv-mahadev-god-hindi.jpg?h=270&drp=1', NULL, 0, 49114, 0, 0, 1, 0, 2600, 0),
(59, 1, 72, '5', 'server_url', 'Somnath Mahadev', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317548/esbmzyfjjxby533nf0ek.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/foPJr_somnath-mahadev-hindi-god.jpg?h=270&drp=1', NULL, 160, 885, 57, 0, 1, 1, 2599, 0),
(60, 1, 72, '5', 'server_url', 'Mera Bhola Hai - Mahadev', 'http://www.kids2win.com/api/videosFile/e2UbP_mera-bhola-hai-bhandari-mahadev-god-hindi.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/q7o6z_mera-bhola-hai-bhandari-mahadev-god-hindi.jpg?h=270&drp=1', NULL, 0, 72921, 0, 0, 1, 0, 2506, 0),
(61, 1, 72, '5', 'server_url', 'Mera Bhola Hai Bhandari - Mahadev', 'http://www.kids2win.com/api/videosFile/VHBKl_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EkgmL_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 40774, 0, 0, 1, 0, 2072, 0),
(62, 1, 72, '5', 'server_url', 'Mann Laga Hai Bhole Mein - Mahadev', 'http://www.kids2win.com/api/videosFile/2Oxr0_mann-laga-hai-bhole-mein-fullscreen-hindi-mahadeve-animated.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yblHe_mann-laga-hai-bhole-mein-fullscreen-hindi-mahadeve-animated.jpg?h=270&drp=1', NULL, 0, 50414, 0, 0, 1, 0, 2066, 0),
(63, 1, 72, '5', 'server_url', 'Bum Bhole Bum Bhole - Mahadev', 'http://www.kids2win.com/api/videosFile/ukdsU_bum-bhole-bum-bhole-mahadev-fullscreen-hindi.mp4', '-', 'Mahadev', 'Landscape', 'http://www.kids2win.com/api/thumbImage/xfHLe_bum-bhole-bum-bhole-mahadev-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 59596, 0, 0, 1, 0, 2062, 0),
(64, 1, 72, '5', 'server_url', 'Om Namaha Shivaya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339110/mmf0ldup2cztew8czkug.mp4', '-', 'Mahadev', 'Portrait', 'http://www.kids2win.com/api/thumbImage/dEJOD_om-namaha-shivaya-hindi-fullscreen.jpg?h=270&drp=1', NULL, 103, 811, 85, 0, 1, 1, 1760, 0),
(65, 1, 21, '5', 'server_url', 'New Love Mashup', 'http://www.kids2win.com/api/videosFile/6wn4G_new-love-mashup-ly-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/R9VwL_new-love-mashup-ly-h.jpg?h=270&drp=1', NULL, 0, 66424, 0, 0, 1, 0, 8649, 0),
(66, 1, 21, '5', 'server_url', 'Relax', 'http://www.kids2win.com/api/videosFile/jstd0_relax-h-l.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/B2f7x_relax-h-l.jpg?h=270&drp=1', NULL, 0, 51091, 0, 0, 1, 0, 8610, 0),
(67, 1, 21, '5', 'server_url', 'Ae Man Karda Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340338/hdbju4w8rp8stigurq3e.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609340340/ktjkzu3xilb51cbfqenv.jpg', NULL, 115, 728, 41, 0, 1, 1, 8606, 0),
(68, 1, 21, '5', 'server_url', 'Tumse Milne Ko', 'http://www.kids2win.com/api/videosFile/M1fZ0_tumse-milne-ko-h-l-sq-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vPLVD_tumse-milne-ko-h-l-sq-15.jpg?h=270&drp=1', NULL, 0, 68410, 0, 0, 1, 0, 8589, 0),
(69, 1, 21, '5', 'server_url', 'Nayan', 'http://www.kids2win.com/api/videosFile/ze7Uf_nayan-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ulEzR_nayan-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 119739, 0, 0, 1, 0, 8532, 0),
(70, 1, 21, '5', 'server_url', 'Mirchi - Divine', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314015/z4zefkavlpsrncuvbjdr.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/ipPLd_mirchi-divine-f-h-l.png?h=270&drp=1', NULL, 196, 678, 86, 0, 1, 1, 8495, 0),
(71, 1, 21, '5', 'server_url', 'Akhiyan', 'http://www.kids2win.com/api/videosFile/daZDp_akhiyan-f-l-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SQDy4_akhiyan-f-l-h-15.jpg?h=270&drp=1', NULL, 0, 63703, 0, 0, 1, 0, 8485, 0),
(72, 1, 21, '5', 'server_url', 'Ishq Tera', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336137/fvxeurewecbz9ltsut7b.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/RaeJm_ishq-tera-f-l-h.jpg?h=270&drp=1', NULL, 193, 465, 25, 0, 1, 1, 8481, 0),
(73, 1, 21, '5', 'server_url', 'Dil Jaaniye', 'http://www.kids2win.com/api/videosFile/WkhSv_dil-jaaniye-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Vn8yk_dil-jaaniye-l-f-h.jpg?h=270&drp=1', NULL, 0, 74663, 0, 0, 1, 0, 8480, 0),
(74, 1, 21, '5', 'server_url', 'Jab Se Tumko Dekha Maine', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310678/p4zyk77pjv7189mc1sag.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310681/xmc7djq7ktjhqajwbrpk.jpg', NULL, 145, 688, 27, 0, 1, 1, 8471, 0),
(75, 1, 21, '5', 'server_url', 'Dil Na Jaaneya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309486/qs0jxgzblqvbljv8zy2d.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309488/gyplz4i0jzbavdboommd.jpg', NULL, 180, 677, 49, 0, 1, 1, 8470, 0),
(76, 1, 21, '5', 'server_url', 'Black Love', 'http://www.kids2win.com/api/videosFile/fjtmL_black-love-f-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/H50S4_black-love-f-h-15.jpg?h=270&drp=1', NULL, 0, 74709, 0, 0, 1, 0, 8466, 0),
(77, 1, 21, '5', 'server_url', 'Titliaan', 'http://www.kids2win.com/api/videosFile/eShmE_titliaan-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bZ0iX_titliaan-l-h.jpg?h=270&drp=1', NULL, 0, 66207, 0, 0, 1, 0, 8457, 0),
(78, 1, 21, '5', 'server_url', 'Liggi - Ritviz', 'http://www.kids2win.com/api/videosFile/DVYW0_liggi-ritviz-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Dpq3w_liggi-ritviz-l-f-h.jpg?h=270&drp=1', NULL, 0, 26261, 0, 0, 1, 0, 8454, 0),
(79, 1, 21, '5', 'server_url', 'Tere Naina', 'http://www.kids2win.com/api/videosFile/IRsME_tere-naina-l-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/05f8P_tere-naina-l-h-15.jpg?h=270&drp=1', NULL, 0, 55169, 0, 0, 1, 0, 8453, 0),
(80, 1, 21, '5', 'server_url', 'Baras Baras - B Praak', 'http://www.kids2win.com/api/videosFile/lIU6s_baras-baras-b-praak-f-l-h-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZxuBL_baras-baras-b-praak-f-l-h-n.jpg?h=270&drp=1', NULL, 0, 40288, 0, 0, 1, 0, 8450, 0),
(81, 1, 21, '5', 'server_url', 'Dil Mera Dekho', 'http://www.kids2win.com/api/videosFile/fQKUT_dil-mera-dekho-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZlxE9_dil-mera-dekho-l-h.jpg?h=270&drp=1', NULL, 0, 56115, 0, 0, 1, 0, 8433, 0),
(82, 1, 21, '5', 'server_url', 'Tum Aaoge Mujhe Milne', 'http://www.kids2win.com/api/videosFile/CKAXB_tum-aaoge-mujhe-milne-h-l.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/45Inf_tum-aaoge-mujhe-milne-h-l.jpg?h=270&drp=1', NULL, 0, 85946, 0, 0, 1, 0, 8425, 0),
(83, 1, 21, '5', 'server_url', 'Ek Dil Ek Jaan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352630/vqvv02jwl01njcsjizal.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352632/rg6tox3rykbjua3dskzn.jpg', NULL, 113, 669, 25, 0, 1, 1, 8421, 0),
(84, 1, 21, '5', 'server_url', 'Duniyaa', 'http://www.kids2win.com/api/videosFile/vm8Fj_duniyaa-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EnjtN_duniyaa-l-f-h.jpg?h=270&drp=1', NULL, 0, 48854, 0, 0, 1, 0, 8420, 0),
(85, 1, 21, '5', 'server_url', 'Ab Tum Ko Kese', 'http://www.kids2win.com/api/videosFile/gLC10_ab-tum-ko-kese-l-h-f-social.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gmqIn_ab-tum-ko-kese-l-h-f-social.jpg?h=270&drp=1', NULL, 0, 49757, 0, 0, 1, 0, 8418, 0),
(86, 1, 21, '5', 'server_url', 'Meri Dehleez Se Hokar', 'http://www.kids2win.com/api/videosFile/BlC5L_meri-dehleez-se-hokar-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aRNb8_meri-dehleez-se-hokar-l-h-sq.jpg?h=270&drp=1', NULL, 0, 41828, 0, 0, 1, 0, 8412, 0),
(87, 1, 21, '5', 'server_url', 'Main Yeh Haath Jo', 'http://www.kids2win.com/api/videosFile/XDOqc_main-yeh-haath-jo-l-h-ly.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gRAyd_main-yeh-haath-jo-l-h-ly.jpg?h=270&drp=1', NULL, 0, 113145, 0, 0, 1, 0, 8409, 0),
(88, 1, 21, '5', 'server_url', 'Pal Pal Dil Ke Paas', 'http://www.kids2win.com/api/videosFile/R9X86_pal-pal-dil-ke-paas-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OAF6s_pal-pal-dil-ke-paas-ly-l-h.jpg?h=270&drp=1', NULL, 0, 85482, 0, 0, 1, 0, 8404, 0),
(89, 1, 21, '5', 'server_url', 'Soch Na Sake - Female', 'http://www.kids2win.com/api/videosFile/0YUJM_soch-na-sake-l-h-f-fem.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CYhnO_soch-na-sake-l-h-f-fem.jpg?h=270&drp=1', NULL, 0, 66764, 0, 0, 1, 0, 8392, 0),
(90, 1, 21, '5', 'server_url', 'Zehnaseeb', 'http://www.kids2win.com/api/videosFile/yExDH_zehnaseeb-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FpCJM_zehnaseeb-l-h.jpg?h=270&drp=1', NULL, 0, 28612, 0, 0, 1, 0, 8391, 0),
(91, 1, 21, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/QtzjK_bewafa-tera-masoom-chehra-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/G8h5D_bewafa-tera-masoom-chehra-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 61919, 0, 0, 1, 0, 8380, 0),
(92, 1, 21, '5', 'server_url', 'Dil Na Jaaneya', 'http://www.kids2win.com/api/videosFile/0D5yr_dil-na-jaaneya-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/klfnj_dil-na-jaaneya-l-h.jpg?h=270&drp=1', NULL, 0, 32362, 0, 0, 1, 0, 8371, 0),
(93, 1, 21, '5', 'server_url', 'O Saathi', 'http://www.kids2win.com/api/videosFile/lyxV2_o-saathi-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QnX6T_o-saathi-l-h.jpg?h=270&drp=1', NULL, 0, 44699, 0, 0, 1, 0, 8370, 0),
(94, 1, 21, '5', 'server_url', 'Tera Hua', 'http://www.kids2win.com/api/videosFile/Cksgh_tera-hua-l-h-f.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UHrSm_tera-hua-l-h-f.jpg?h=270&drp=1', NULL, 0, 50430, 0, 0, 1, 0, 8367, 0),
(95, 1, 21, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/t9awf_teri-aankhon-mein-rain-l-h-.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AuYWM_teri-aankhon-mein-rain-l-h-.jpg?h=270&drp=1', NULL, 0, 62109, 0, 0, 1, 0, 8316, 0),
(96, 1, 21, '5', 'server_url', 'Tere Khayalon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316365/d1cvtpo6joyp1lgb0yzm.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316368/eeznkunlvwmgyt88yqoo.jpg', NULL, 122, 613, 62, 0, 1, 1, 8310, 0),
(97, 1, 21, '5', 'server_url', 'Teri Aankhon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337733/vupyu6mi1pzjdwz4zzd9.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/KgJn9_teri-aankhon-mein-l-h-rain.jpg?h=270&drp=1', NULL, 131, 806, 54, 0, 1, 1, 8309, 0),
(98, 1, 21, '5', 'server_url', 'Tere Bina', 'http://www.kids2win.com/api/videosFile/Gd0N5_tere-bina-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RAwGW_tere-bina-l-h.jpg?h=270&drp=1', NULL, 0, 65000, 0, 0, 1, 0, 8308, 0),
(99, 1, 21, '5', 'server_url', 'Dilkash', 'http://www.kids2win.com/api/videosFile/SsckM_dilkash-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iqh8H_dilkash-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 87334, 0, 0, 1, 0, 8307, 0),
(100, 1, 21, '5', 'server_url', 'Meri Dahlig Se Hokar', 'http://www.kids2win.com/api/videosFile/AzqNa_meri-dahlig-se-hokar-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/znjBs_meri-dahlig-se-hokar-l-h.jpg?h=270&drp=1', NULL, 0, 26692, 0, 0, 1, 0, 8302, 0),
(101, 1, 21, '5', 'server_url', 'Raabta', 'http://www.kids2win.com/api/videosFile/3AUre_raabta-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lzJc9_raabta-l-h.jpg?h=270&drp=1', NULL, 0, 33336, 0, 0, 1, 0, 8300, 0),
(102, 1, 21, '5', 'server_url', 'Aabaad Barbaad', 'http://www.kids2win.com/api/videosFile/HkxXj_aabaad-barbaad-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nKRBl_aabaad-barbaad-l-h-sq.jpg?h=270&drp=1', NULL, 0, 72650, 0, 0, 1, 0, 8269, 0),
(103, 1, 21, '5', 'server_url', 'Meri Rahe Tere Tak Hai', 'http://www.kids2win.com/api/videosFile/DEG1V_meri-rahe-tere-tak-hai-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8tTaH_meri-rahe-tere-tak-hai-l-h-sq.jpg?h=270&drp=1', NULL, 0, 105199, 0, 0, 1, 0, 8220, 0),
(104, 1, 21, '5', 'server_url', 'Aabaad Barbaad', 'http://www.kids2win.com/api/videosFile/41xvu_aabaad-barbaad-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DQk2q_aabaad-barbaad-ly-l-h.jpg?h=270&drp=1', NULL, 0, 127552, 0, 0, 1, 0, 8188, 0),
(105, 1, 21, '5', 'server_url', 'Love - Remix', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310642/fhlasbxmgsfsngip1vb7.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/lOKQi_love-h-old-new.jpg?h=270&drp=1', NULL, 126, 459, 85, 0, 1, 1, 8187, 0),
(106, 1, 21, '5', 'server_url', 'Burjkhalifa', 'http://www.kids2win.com/api/videosFile/b6RF1_burjkhalifa-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Xb7vK_burjkhalifa-l-f-h.jpg?h=270&drp=1', NULL, 0, 63668, 0, 0, 1, 0, 8186, 0),
(107, 1, 21, '5', 'server_url', 'Aabaad Barbaad', 'http://www.kids2win.com/api/videosFile/Y0jvh_aabaad-barbaad-f-n-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/L19CI_aabaad-barbaad-f-n-l-h.jpg?h=270&drp=1', NULL, 0, 42584, 0, 0, 1, 0, 8182, 0),
(108, 1, 21, '5', 'server_url', 'Dil Diyan Gallan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340019/gdvzih84mwo4ioblnhpa.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/hbft5_dil-diyan-gallan-l-h-sq.jpg?h=270&drp=1', NULL, 112, 479, 11, 0, 1, 1, 8174, 0),
(109, 1, 21, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/nX0c5_teri-aankhon-mein-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/i1IJK_teri-aankhon-mein-ly-l-h.jpg?h=270&drp=1', NULL, 0, 120458, 0, 0, 1, 0, 8172, 0),
(110, 1, 21, '5', 'server_url', 'Deewano Si Halat Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352678/xwmhvgiaxbn8jeta8tc5.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/nQXRb_deewano-si-halat-hai-l-h-tv-seri-f.jpg?h=270&drp=1', NULL, 178, 404, 69, 0, 1, 1, 8169, 0),
(111, 1, 21, '5', 'server_url', 'Tum Hi Aana - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335968/zy9ylfeau806ornissam.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335971/cmgu4flnky8glivqszeo.jpg', NULL, 132, 428, 92, 0, 1, 1, 8168, 0),
(112, 1, 21, '5', 'server_url', 'Jab Koi Baat', 'http://www.kids2win.com/api/videosFile/5yJ8I_jab-koi-baat-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FQrV7_jab-koi-baat-l-h.jpg?h=270&drp=1', NULL, 0, 46013, 0, 0, 1, 0, 8166, 0),
(113, 1, 21, '5', 'server_url', 'Yuhi Nahi Tujhpe', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337396/ymltfutuy7spo5fyuyaz.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/EIjqe_yuhi-nahi-tujhpe-sq-l-h-tv-seri.jpg?h=270&drp=1', NULL, 133, 438, 76, 0, 1, 1, 8164, 0),
(114, 1, 21, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/JGYri_teri-aankhon-mein-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wXLGf_teri-aankhon-mein-l-h.jpg?h=270&drp=1', NULL, 0, 23366, 0, 0, 1, 0, 8163, 0),
(115, 1, 21, '5', 'server_url', 'Ishqdaari', 'http://www.kids2win.com/api/videosFile/IHalb_ishqdaari-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tDku0_ishqdaari-l-h.jpg?h=270&drp=1', NULL, 0, 16557, 0, 0, 1, 0, 8162, 0),
(116, 1, 21, '5', 'server_url', 'Raanjhana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313373/ziwauzmozhyclkaoen8q.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313375/k7fszhbfkulg4jgul04o.jpg', NULL, 170, 573, 56, 0, 1, 1, 8161, 0),
(117, 1, 21, '5', 'server_url', 'Jeene Laga Hoon', 'http://www.kids2win.com/api/videosFile/q2KAl_jeene-laga-hoon-f-h-l-ani.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QRT0a_jeene-laga-hoon-f-h-l-ani.jpg?h=270&drp=1', NULL, 0, 54090, 0, 0, 1, 0, 8160, 0),
(118, 1, 21, '5', 'server_url', 'Aawara Shaam Hai', 'http://www.kids2win.com/api/videosFile/6l7CY_aawara-shaam-hai-ani-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nTqfN_aawara-shaam-hai-ani-l-h.jpg?h=270&drp=1', NULL, 0, 45836, 0, 0, 1, 0, 8159, 0),
(119, 1, 21, '5', 'server_url', 'Sanam Teri Kasam', 'http://www.kids2win.com/api/videosFile/5RcQD_sanam-teri-kasam-l-h-ly.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KBHwW_sanam-teri-kasam-l-h-ly.jpg?h=270&drp=1', NULL, 0, 117670, 0, 0, 1, 0, 8103, 0),
(120, 1, 21, '5', 'server_url', 'Sunn Zara', 'http://www.kids2win.com/api/videosFile/37ir0_sunn-zara-f-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rjb0a_sunn-zara-f-l-h.jpg?h=270&drp=1', NULL, 0, 80859, 0, 0, 1, 0, 8102, 0),
(121, 1, 21, '5', 'server_url', 'Leh Le Meri Jaan', 'http://www.kids2win.com/api/videosFile/DNz3X_leh-le-meri-jaan-l-h-f.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/674fv_leh-le-meri-jaan-l-h-f.jpg?h=270&drp=1', NULL, 0, 55028, 0, 0, 1, 0, 8101, 0),
(122, 1, 21, '5', 'server_url', 'Tu Hi Re', 'http://www.kids2win.com/api/videosFile/0Bk5w_tu-hi-re-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2GkKY_tu-hi-re-ly-l-h.jpg?h=270&drp=1', NULL, 0, 55569, 0, 0, 1, 0, 8100, 0),
(123, 1, 21, '5', 'server_url', 'Dil Hai Ke Manta Nahin', 'http://www.kids2win.com/api/videosFile/jgrI9_dil-hai-ke-manta-nahin-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fWnMp_dil-hai-ke-manta-nahin-l-h.jpg?h=270&drp=1', NULL, 0, 66667, 0, 0, 1, 0, 8080, 0),
(124, 1, 21, '5', 'server_url', 'Betabi Kya Hoti Hai', 'http://www.kids2win.com/api/videosFile/aRnAo_betabi-kya-hoti-hai-l-h-old.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/E2nuH_betabi-kya-hoti-hai-l-h-old.jpg?h=270&drp=1', NULL, 0, 46250, 0, 0, 1, 0, 8079, 0),
(125, 1, 73, '5', 'server_url', 'New Love Mashup', 'http://www.kids2win.com/api/videosFile/6wn4G_new-love-mashup-ly-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/R9VwL_new-love-mashup-ly-h.jpg?h=270&drp=1', NULL, 0, 66424, 0, 0, 1, 0, 8649, 0),
(126, 1, 73, '5', 'server_url', 'Relax', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352594/itkd3agoomgynbghlk24.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352595/erisxzyo6pz1ewpfbpn3.jpg', NULL, 138, 894, 46, 0, 1, 1, 8610, 0),
(127, 1, 73, '5', 'server_url', 'Ae Man Karda Hai', 'http://www.kids2win.com/api/videosFile/vK5M2_ae-man-karda-hai-l-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7idmY_ae-man-karda-hai-l-h-15.jpg?h=270&drp=1', NULL, 0, 76195, 0, 0, 1, 0, 8606, 0),
(128, 1, 73, '5', 'server_url', 'Tumse Milne Ko', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314904/rlump1khm3gnkndu4eih.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609314906/desi7aikx7mj0atse009.jpg', NULL, 113, 413, 19, 0, 1, 1, 8589, 0),
(129, 1, 73, '5', 'server_url', 'Nayan', 'http://www.kids2win.com/api/videosFile/ze7Uf_nayan-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ulEzR_nayan-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 119739, 0, 0, 1, 0, 8532, 0),
(130, 1, 73, '5', 'server_url', 'Mirchi - Divine', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337780/hjkgvbe7uv0rgibmwnzn.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/ipPLd_mirchi-divine-f-h-l.png?h=270&drp=1', NULL, 189, 892, 80, 0, 1, 1, 8495, 0),
(131, 1, 73, '5', 'server_url', 'Akhiyan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336440/jc2ksdorswcdtgjz6h9c.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/SQDy4_akhiyan-f-l-h-15.jpg?h=270&drp=1', NULL, 170, 402, 80, 0, 1, 1, 8485, 0),
(132, 1, 73, '5', 'server_url', 'Ishq Tera', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312439/ecom4ybfjtwvm0x68kcr.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/RaeJm_ishq-tera-f-l-h.jpg?h=270&drp=1', NULL, 154, 817, 67, 0, 1, 1, 8481, 0),
(133, 1, 73, '5', 'server_url', 'Dil Jaaniye', 'http://www.kids2win.com/api/videosFile/WkhSv_dil-jaaniye-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Vn8yk_dil-jaaniye-l-f-h.jpg?h=270&drp=1', NULL, 0, 74663, 0, 0, 1, 0, 8480, 0),
(134, 1, 73, '5', 'server_url', 'Jab Se Tumko Dekha Maine', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320574/rjtxcwqqg48w2l04ukn8.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320576/rjp5vjvazgjawr4frelf.jpg', NULL, 179, 662, 23, 0, 1, 1, 8471, 0),
(135, 1, 73, '5', 'server_url', 'Dil Na Jaaneya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312812/srxa3umthlrfuzfbvmaa.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609312815/nds6w1szejdadmlnje3w.jpg', NULL, 169, 898, 17, 0, 1, 1, 8470, 0),
(136, 1, 73, '5', 'server_url', 'Black Love', 'http://www.kids2win.com/api/videosFile/fjtmL_black-love-f-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/H50S4_black-love-f-h-15.jpg?h=270&drp=1', NULL, 0, 74709, 0, 0, 1, 0, 8466, 0),
(138, 1, 73, '5', 'server_url', 'Liggi - Ritviz', 'http://www.kids2win.com/api/videosFile/DVYW0_liggi-ritviz-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Dpq3w_liggi-ritviz-l-f-h.jpg?h=270&drp=1', NULL, 0, 26261, 0, 0, 1, 0, 8454, 0),
(139, 1, 73, '5', 'server_url', 'Tere Naina', 'http://www.kids2win.com/api/videosFile/IRsME_tere-naina-l-h-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/05f8P_tere-naina-l-h-15.jpg?h=270&drp=1', NULL, 0, 55169, 0, 0, 1, 0, 8453, 0),
(140, 1, 73, '5', 'server_url', 'Baras Baras - B Praak', 'http://www.kids2win.com/api/videosFile/lIU6s_baras-baras-b-praak-f-l-h-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZxuBL_baras-baras-b-praak-f-l-h-n.jpg?h=270&drp=1', NULL, 0, 40288, 0, 0, 1, 0, 8450, 0),
(141, 1, 73, '5', 'server_url', 'Dil Mera Dekho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609321233/w4dcmnqglgtjq5udz5wd.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/ZlxE9_dil-mera-dekho-l-h.jpg?h=270&drp=1', NULL, 117, 758, 55, 0, 1, 1, 8433, 0),
(142, 1, 73, '5', 'server_url', 'Tum Aaoge Mujhe Milne', 'http://www.kids2win.com/api/videosFile/CKAXB_tum-aaoge-mujhe-milne-h-l.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/45Inf_tum-aaoge-mujhe-milne-h-l.jpg?h=270&drp=1', NULL, 0, 85946, 0, 0, 1, 0, 8425, 0),
(143, 1, 73, '5', 'server_url', 'Ek Dil Ek Jaan', 'http://www.kids2win.com/api/videosFile/NkoQb_ek-dil-ek-jaan-l-h-f.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/e3fYz_ek-dil-ek-jaan-l-h-f.jpg?h=270&drp=1', NULL, 0, 39216, 0, 0, 1, 0, 8421, 0),
(144, 1, 73, '5', 'server_url', 'Duniyaa', 'http://www.kids2win.com/api/videosFile/vm8Fj_duniyaa-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EnjtN_duniyaa-l-f-h.jpg?h=270&drp=1', NULL, 0, 48854, 0, 0, 1, 0, 8420, 0),
(145, 1, 73, '5', 'server_url', 'Ab Tum Ko Kese', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335526/e5zkttwp7k0okkdmek8v.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/gmqIn_ab-tum-ko-kese-l-h-f-social.jpg?h=270&drp=1', NULL, 156, 468, 84, 0, 1, 1, 8418, 0),
(146, 1, 73, '5', 'server_url', 'Meri Dehleez Se Hokar', 'http://www.kids2win.com/api/videosFile/BlC5L_meri-dehleez-se-hokar-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aRNb8_meri-dehleez-se-hokar-l-h-sq.jpg?h=270&drp=1', NULL, 0, 41828, 0, 0, 1, 0, 8412, 0),
(147, 1, 73, '5', 'server_url', 'Main Yeh Haath Jo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403035/mwxajehvwz9uqtk16kle.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403037/emkdtfld0uszqunz0d62.jpg', NULL, 138, 862, 47, 0, 1, 1, 8409, 0),
(148, 1, 73, '5', 'server_url', 'Pal Pal Dil Ke Paas', 'http://www.kids2win.com/api/videosFile/R9X86_pal-pal-dil-ke-paas-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OAF6s_pal-pal-dil-ke-paas-ly-l-h.jpg?h=270&drp=1', NULL, 0, 85482, 0, 0, 1, 0, 8404, 0),
(149, 1, 73, '5', 'server_url', 'Soch Na Sake - Female', 'http://www.kids2win.com/api/videosFile/0YUJM_soch-na-sake-l-h-f-fem.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CYhnO_soch-na-sake-l-h-f-fem.jpg?h=270&drp=1', NULL, 0, 66764, 0, 0, 1, 0, 8392, 0),
(150, 1, 73, '5', 'server_url', 'Zehnaseeb', 'http://www.kids2win.com/api/videosFile/yExDH_zehnaseeb-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FpCJM_zehnaseeb-l-h.jpg?h=270&drp=1', NULL, 0, 28612, 0, 0, 1, 0, 8391, 0),
(151, 1, 73, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/QtzjK_bewafa-tera-masoom-chehra-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/G8h5D_bewafa-tera-masoom-chehra-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 61919, 0, 0, 1, 0, 8380, 0),
(152, 1, 73, '5', 'server_url', 'Dil Na Jaaneya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309144/hkmbifyhxftuhh6tuue4.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309146/idz4osxdgmluvrpjo7qb.jpg', NULL, 142, 552, 99, 0, 1, 1, 8371, 0),
(153, 1, 73, '5', 'server_url', 'O Saathi', 'http://www.kids2win.com/api/videosFile/lyxV2_o-saathi-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QnX6T_o-saathi-l-h.jpg?h=270&drp=1', NULL, 0, 44699, 0, 0, 1, 0, 8370, 0),
(154, 1, 73, '5', 'server_url', 'Tera Hua', 'http://www.kids2win.com/api/videosFile/Cksgh_tera-hua-l-h-f.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UHrSm_tera-hua-l-h-f.jpg?h=270&drp=1', NULL, 0, 50430, 0, 0, 1, 0, 8367, 0),
(155, 1, 73, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/t9awf_teri-aankhon-mein-rain-l-h-.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AuYWM_teri-aankhon-mein-rain-l-h-.jpg?h=270&drp=1', NULL, 0, 62109, 0, 0, 1, 0, 8316, 0),
(156, 1, 73, '5', 'server_url', 'Tere Khayalon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352066/a5iy3nvumewxthiyyrf2.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352068/mtz6rkxpy6fwch8i670f.jpg', NULL, 197, 771, 86, 0, 1, 1, 8310, 0),
(157, 1, 73, '5', 'server_url', 'Teri Aankhon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340362/x3ktca5qhrgxq4im3z9b.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/KgJn9_teri-aankhon-mein-l-h-rain.jpg?h=270&drp=1', NULL, 131, 708, 72, 0, 1, 1, 8309, 0),
(158, 1, 73, '5', 'server_url', 'Tere Bina', 'http://www.kids2win.com/api/videosFile/Gd0N5_tere-bina-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RAwGW_tere-bina-l-h.jpg?h=270&drp=1', NULL, 0, 65000, 0, 0, 1, 0, 8308, 0),
(159, 1, 73, '5', 'server_url', 'Dilkash', 'http://www.kids2win.com/api/videosFile/SsckM_dilkash-l-h-f-n.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iqh8H_dilkash-l-h-f-n.jpg?h=270&drp=1', NULL, 0, 87334, 0, 0, 1, 0, 8307, 0),
(160, 1, 73, '5', 'server_url', 'Meri Dahlig Se Hokar', 'http://www.kids2win.com/api/videosFile/AzqNa_meri-dahlig-se-hokar-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/znjBs_meri-dahlig-se-hokar-l-h.jpg?h=270&drp=1', NULL, 0, 26692, 0, 0, 1, 0, 8302, 0),
(161, 1, 73, '5', 'server_url', 'Raabta', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312829/wguo0rpfdri73dixdrpf.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609312831/okiusctmrryz9pc9n7qe.jpg', NULL, 188, 694, 80, 0, 1, 1, 8300, 0),
(162, 1, 73, '5', 'server_url', 'Aabaad Barbaad', 'http://www.kids2win.com/api/videosFile/HkxXj_aabaad-barbaad-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nKRBl_aabaad-barbaad-l-h-sq.jpg?h=270&drp=1', NULL, 0, 72650, 0, 0, 1, 0, 8269, 0),
(163, 1, 73, '5', 'server_url', 'Meri Rahe Tere Tak Hai', 'http://www.kids2win.com/api/videosFile/DEG1V_meri-rahe-tere-tak-hai-l-h-sq.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8tTaH_meri-rahe-tere-tak-hai-l-h-sq.jpg?h=270&drp=1', NULL, 0, 105199, 0, 0, 1, 0, 8220, 0),
(164, 1, 73, '5', 'server_url', 'Aabaad Barbaad', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320817/gaiqtiinsln14odj2iks.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320820/xxtbr7tebjrcw84jo7d5.jpg', NULL, 110, 605, 53, 0, 1, 1, 8188, 0),
(165, 1, 73, '5', 'server_url', 'Love - Remix', 'http://www.kids2win.com/api/videosFile/ionWR_love-h-old-new.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lOKQi_love-h-old-new.jpg?h=270&drp=1', NULL, 0, 70820, 0, 0, 1, 0, 8187, 0),
(166, 1, 73, '5', 'server_url', 'Burjkhalifa', 'http://www.kids2win.com/api/videosFile/b6RF1_burjkhalifa-l-f-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Xb7vK_burjkhalifa-l-f-h.jpg?h=270&drp=1', NULL, 0, 63668, 0, 0, 1, 0, 8186, 0),
(167, 1, 73, '5', 'server_url', 'Aabaad Barbaad', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316308/pri3uqclcrntkcddxfee.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/L19CI_aabaad-barbaad-f-n-l-h.jpg?h=270&drp=1', NULL, 167, 709, 20, 0, 1, 1, 8182, 0),
(168, 1, 73, '5', 'server_url', 'Dil Diyan Gallan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313029/re0yeodli5b1g4niwpq2.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/hbft5_dil-diyan-gallan-l-h-sq.jpg?h=270&drp=1', NULL, 161, 602, 43, 0, 1, 1, 8174, 0),
(169, 1, 73, '5', 'server_url', 'Teri Aankhon Mein', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609341332/nzywcb2iwju94famcxip.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609341334/hh8015hm55zymm8wmbuh.jpg', NULL, 114, 592, 31, 0, 1, 1, 8172, 0),
(170, 1, 73, '5', 'server_url', 'Deewano Si Halat Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310413/s58tkygk5eap8e7litrk.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/nQXRb_deewano-si-halat-hai-l-h-tv-seri-f.jpg?h=270&drp=1', NULL, 153, 409, 92, 0, 1, 1, 8169, 0),
(171, 1, 73, '5', 'server_url', 'Tum Hi Aana - Unplugged', 'http://www.kids2win.com/api/videosFile/75YgJ_tum-hi-aana-ly-h-unp.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Hx7PF_tum-hi-aana-ly-h-unp.jpg?h=270&drp=1', NULL, 0, 89703, 0, 0, 1, 0, 8168, 0),
(172, 1, 73, '5', 'server_url', 'Jab Koi Baat', 'http://www.kids2win.com/api/videosFile/5yJ8I_jab-koi-baat-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FQrV7_jab-koi-baat-l-h.jpg?h=270&drp=1', NULL, 0, 46013, 0, 0, 1, 0, 8166, 0),
(173, 1, 73, '5', 'server_url', 'Yuhi Nahi Tujhpe', 'http://www.kids2win.com/api/videosFile/BmylV_yuhi-nahi-tujhpe-sq-l-h-tv-seri.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EIjqe_yuhi-nahi-tujhpe-sq-l-h-tv-seri.jpg?h=270&drp=1', NULL, 0, 54707, 0, 0, 1, 0, 8164, 0),
(174, 1, 73, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/JGYri_teri-aankhon-mein-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wXLGf_teri-aankhon-mein-l-h.jpg?h=270&drp=1', NULL, 0, 23366, 0, 0, 1, 0, 8163, 0),
(175, 1, 73, '5', 'server_url', 'Ishqdaari', 'http://www.kids2win.com/api/videosFile/IHalb_ishqdaari-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tDku0_ishqdaari-l-h.jpg?h=270&drp=1', NULL, 0, 16557, 0, 0, 1, 0, 8162, 0),
(176, 1, 73, '5', 'server_url', 'Raanjhana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312741/xcop5nszaq9t3bsgzqdk.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609312743/eg3umqn8geejgypezqyp.jpg', NULL, 117, 628, 86, 0, 1, 1, 8161, 0),
(177, 1, 73, '5', 'server_url', 'Jeene Laga Hoon', 'http://www.kids2win.com/api/videosFile/q2KAl_jeene-laga-hoon-f-h-l-ani.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QRT0a_jeene-laga-hoon-f-h-l-ani.jpg?h=270&drp=1', NULL, 0, 54090, 0, 0, 1, 0, 8160, 0),
(178, 1, 73, '5', 'server_url', 'Aawara Shaam Hai', 'http://www.kids2win.com/api/videosFile/6l7CY_aawara-shaam-hai-ani-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nTqfN_aawara-shaam-hai-ani-l-h.jpg?h=270&drp=1', NULL, 1, 45836, 0, 0, 1, 0, 8159, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(179, 1, 73, '5', 'server_url', 'Sanam Teri Kasam', 'http://www.kids2win.com/api/videosFile/5RcQD_sanam-teri-kasam-l-h-ly.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KBHwW_sanam-teri-kasam-l-h-ly.jpg?h=270&drp=1', NULL, 0, 117670, 0, 0, 1, 0, 8103, 0),
(180, 1, 73, '5', 'server_url', 'Sunn Zara', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312796/sc7meop5shfrhj8vjbia.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/rjb0a_sunn-zara-f-l-h.jpg?h=270&drp=1', NULL, 198, 605, 91, 0, 1, 1, 8102, 0),
(181, 1, 73, '5', 'server_url', 'Leh Le Meri Jaan', 'http://www.kids2win.com/api/videosFile/DNz3X_leh-le-meri-jaan-l-h-f.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/674fv_leh-le-meri-jaan-l-h-f.jpg?h=270&drp=1', NULL, 0, 55028, 0, 0, 1, 0, 8101, 0),
(182, 1, 73, '5', 'server_url', 'Tu Hi Re', 'http://www.kids2win.com/api/videosFile/0Bk5w_tu-hi-re-ly-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2GkKY_tu-hi-re-ly-l-h.jpg?h=270&drp=1', NULL, 0, 55569, 0, 0, 1, 0, 8100, 0),
(183, 1, 73, '5', 'server_url', 'Dil Hai Ke Manta Nahin', 'http://www.kids2win.com/api/videosFile/jgrI9_dil-hai-ke-manta-nahin-l-h.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fWnMp_dil-hai-ke-manta-nahin-l-h.jpg?h=270&drp=1', NULL, 0, 66667, 0, 0, 1, 0, 8080, 0),
(184, 1, 73, '5', 'server_url', 'Betabi Kya Hoti Hai', 'http://www.kids2win.com/api/videosFile/aRnAo_betabi-kya-hoti-hai-l-h-old.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/E2nuH_betabi-kya-hoti-hai-l-h-old.jpg?h=270&drp=1', NULL, 0, 46250, 0, 0, 1, 0, 8079, 0),
(185, 1, 20, '5', 'server_url', 'Aankhe Meri Har Jagah', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336476/uqabjnvzodqptvh7qxq9.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/QHRGu_aankhe-meri-har-jagah-s-h-f.jpg?h=270&drp=1', NULL, 157, 896, 31, 0, 1, 1, 8631, 0),
(186, 1, 20, '5', 'server_url', 'Rah Wo Jispe Main Chal Raha Tha', 'http://www.kids2win.com/api/videosFile/B3Dhl_rah-wo-jispe-main-chal-raha-tha-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4kOgM_rah-wo-jispe-main-chal-raha-tha-s-h-f.jpg?h=270&drp=1', NULL, 0, 73836, 0, 0, 1, 0, 8630, 0),
(187, 1, 20, '5', 'server_url', 'Bewafa Tera Masum Chehra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319416/pauyu4fcescjgb7wy1jh.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319419/sq2fubxlrbkctu0hjktu.jpg', NULL, 187, 704, 87, 0, 1, 1, 8612, 0),
(188, 1, 20, '5', 'server_url', 'Katal Bazar Mai Ho Chuka Hu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313960/svs0uisd95aefjy86auu.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313962/usyx4z073k602cbuoher.jpg', NULL, 100, 579, 87, 0, 1, 1, 8607, 0),
(189, 1, 20, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313724/adhioajrcfki7yeikc4l.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313726/nzf9a2aoawxot0cyb8uo.jpg', NULL, 123, 803, 22, 0, 1, 1, 8592, 0),
(190, 1, 20, '5', 'server_url', 'O Khuda', 'http://www.kids2win.com/api/videosFile/sduSb_o-khuda-s-f-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9Od1F_o-khuda-s-f-h.jpg?h=270&drp=1', NULL, 0, 39588, 0, 0, 1, 0, 8590, 0),
(191, 1, 20, '5', 'server_url', 'Veham', 'http://www.kids2win.com/api/videosFile/6niYW_veham-s-h-f-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NuRcP_veham-s-h-f-n.jpg?h=270&drp=1', NULL, 0, 52469, 0, 0, 1, 0, 8581, 0),
(192, 1, 20, '5', 'server_url', 'Besharam Bewaffa', 'http://www.kids2win.com/api/videosFile/xvA5T_besharam-bewaffa-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sTX41_besharam-bewaffa-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 101475, 0, 0, 1, 0, 8559, 0),
(193, 1, 20, '5', 'server_url', 'Ek Tarfa Pyar', 'http://www.kids2win.com/api/videosFile/qdz2F_ek-tarfa-pyar-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CAhkJ_ek-tarfa-pyar-s-h-f.jpg?h=270&drp=1', NULL, 0, 80161, 0, 0, 1, 0, 8558, 0),
(194, 1, 20, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/W3Id7_bewafa-tera-masoom-chehra-s-f-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4EdXn_bewafa-tera-masoom-chehra-s-f-h.jpg?h=270&drp=1', NULL, 0, 61913, 0, 0, 1, 0, 8555, 0),
(195, 1, 20, '5', 'server_url', 'Jinke Liye', 'http://www.kids2win.com/api/videosFile/Z4nJk_jinke-liye-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vBuJ7_jinke-liye-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 61606, 0, 0, 1, 0, 8554, 0),
(196, 1, 20, '5', 'server_url', 'Ek Tarfa Pyar', 'http://www.kids2win.com/api/videosFile/U5wiu_ek-tarfa-pyar-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dVTqB_ek-tarfa-pyar-f-s-h.jpg?h=270&drp=1', NULL, 0, 60568, 0, 0, 1, 0, 8530, 0),
(197, 1, 20, '5', 'server_url', 'Karam Ka Hisab Mat Puchho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353498/bsojvq1ttaef7icqakf8.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/GNUVP_karam-ka-hisab-mat-puchho-h.jpg?h=270&drp=1', NULL, 150, 892, 40, 0, 1, 1, 8527, 0),
(198, 1, 20, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/x9ro4_bewafa-tera-masoom-chehra-s-ly-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gf2PC_bewafa-tera-masoom-chehra-s-ly-h.jpg?h=270&drp=1', NULL, 0, 83555, 0, 0, 1, 0, 8523, 0),
(199, 1, 20, '5', 'server_url', 'Besharam Bewafa', 'http://www.kids2win.com/api/videosFile/gFTp2_besharam-bewafa-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bT3se_besharam-bewafa-s-h-f.jpg?h=270&drp=1', NULL, 0, 61685, 0, 0, 1, 0, 8472, 0),
(200, 1, 20, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/eCYZr_phir-chala-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5HZUl_phir-chala-f-s-h.jpg?h=270&drp=1', NULL, 0, 41356, 0, 0, 1, 0, 8465, 0),
(201, 1, 20, '5', 'server_url', 'Aadat - Ninja', 'http://www.kids2win.com/api/videosFile/O8RLG_aadat-ninja-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wDCez_aadat-ninja-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 73768, 0, 0, 1, 0, 8464, 0),
(202, 1, 20, '5', 'server_url', 'Besharam Bewaffa', 'http://www.kids2win.com/api/videosFile/V1kEm_besharam-bewaffa-f-s-h-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/e8ODF_besharam-bewaffa-f-s-h-n.jpg?h=270&drp=1', NULL, 0, 45840, 0, 0, 1, 0, 8451, 0),
(203, 1, 20, '5', 'server_url', 'Woh Chaand Kahan Se Laogi', 'http://www.kids2win.com/api/videosFile/A9Xq0_woh-chaand-kahan-se-laogi-f-s-h-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rAE6T_woh-chaand-kahan-se-laogi-f-s-h-n.jpg?h=270&drp=1', NULL, 0, 60430, 0, 0, 1, 0, 8434, 0),
(204, 1, 20, '5', 'server_url', 'Agar Tum Saath Ho', 'http://www.kids2win.com/api/videosFile/zUHsA_agar-tum-saath-ho-s-h-sq.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9LnoE_agar-tum-saath-ho-s-h-sq.jpg?h=270&drp=1', NULL, 0, 72068, 0, 0, 1, 0, 8415, 0),
(205, 1, 20, '5', 'server_url', 'Galti', 'http://www.kids2win.com/api/videosFile/s6Heq_galti-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UbmF1_galti-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 73892, 0, 0, 1, 0, 8411, 0),
(206, 1, 20, '5', 'server_url', 'I Don\'t Need Everybody', 'http://www.kids2win.com/api/videosFile/wRr95_i-dont-need-everybody-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oiCXA_i-dont-need-everybody-s-h.jpg?h=270&drp=1', NULL, 0, 26109, 0, 0, 1, 0, 8408, 0),
(207, 1, 20, '5', 'server_url', 'True Fact Must Watch', 'http://www.kids2win.com/api/videosFile/MvKt4_true-fact-must-watch-h-sq-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/glVCX_true-fact-must-watch-h-sq-s.jpg?h=270&drp=1', NULL, 0, 27723, 0, 0, 1, 0, 8406, 0),
(208, 1, 20, '5', 'server_url', 'Mujhe Peene Do', 'http://www.kids2win.com/api/videosFile/fW3Qv_mujhe-peene-do-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Gqndy_mujhe-peene-do-s-h.jpg?h=270&drp=1', NULL, 0, 47069, 0, 0, 1, 0, 8393, 0),
(209, 1, 20, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/302iG_phir-chala-ly-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GReS6_phir-chala-ly-s-h.jpg?h=270&drp=1', NULL, 0, 100242, 0, 0, 1, 0, 8374, 0),
(210, 1, 20, '5', 'server_url', 'Log Humse Jalte Hain', 'http://www.kids2win.com/api/videosFile/yO5tf_log-humse-jalte-hain-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rf0IL_log-humse-jalte-hain-s-h.jpg?h=270&drp=1', NULL, 0, 37674, 0, 0, 1, 0, 8368, 0),
(211, 1, 20, '5', 'server_url', 'Milne Hai Mujhse Aayi', 'http://www.kids2win.com/api/videosFile/zdL8J_milne-hai-mujhse-aayi-s-h-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/etAbz_milne-hai-mujhse-aayi-s-h-15.jpg?h=270&drp=1', NULL, 0, 62667, 0, 0, 1, 0, 8360, 0),
(212, 1, 20, '5', 'server_url', 'Sajda Tera Kar Na Sakoon', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311860/en4afvi6ncbjpq3yfsgd.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609311862/ijopwsx2by1mbuo6v9fw.jpg', NULL, 187, 563, 62, 0, 1, 1, 8306, 0),
(213, 1, 20, '5', 'server_url', 'Vo Jab Kehte The', 'http://www.kids2win.com/api/videosFile/ualA3_vo-jab-kehte-the-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Pn2S9_vo-jab-kehte-the-s-h.jpg?h=270&drp=1', NULL, 0, 47537, 0, 0, 1, 0, 8305, 0),
(214, 1, 20, '5', 'server_url', 'Afsos Karoge', 'http://www.kids2win.com/api/videosFile/bOtn2_afsos-karoge-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4aKql_afsos-karoge-s-h.jpg?h=270&drp=1', NULL, 0, 38407, 0, 0, 1, 0, 8304, 0),
(215, 1, 20, '5', 'server_url', 'Tanhaai - Tulsi Kumar', 'http://www.kids2win.com/api/videosFile/gvxVD_tanhaai-tulsi-kumar-s-h-f-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PbTtx_tanhaai-tulsi-kumar-s-h-f-n.jpg?h=270&drp=1', NULL, 0, 108873, 0, 0, 1, 0, 8245, 0),
(216, 1, 20, '5', 'server_url', 'Ae Mere Dil', 'http://www.kids2win.com/api/videosFile/VTtcz_ae-mere-dil-s-h-ani.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wP5iH_ae-mere-dil-s-h-ani.jpg?h=270&drp=1', NULL, 0, 126009, 0, 0, 1, 0, 8244, 0),
(217, 1, 20, '5', 'server_url', 'Tune To Mera Dil Kuch', 'http://www.kids2win.com/api/videosFile/jl0dM_tune-to-mera-dil-kuch-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Pq3RU_tune-to-mera-dil-kuch-f-s-h.jpg?h=270&drp=1', NULL, 0, 106275, 0, 0, 1, 0, 8221, 0),
(218, 1, 20, '5', 'server_url', 'Ae Mere Dil', 'http://www.kids2win.com/api/videosFile/ZTPQE_ae-mere-dil-f-s-h-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UW95v_ae-mere-dil-f-s-h-n.jpg?h=270&drp=1', NULL, 0, 71522, 0, 0, 1, 0, 8222, 0),
(219, 1, 20, '5', 'server_url', 'Mere Pass Nahi Hai', 'http://www.kids2win.com/api/videosFile/8REik_mere-pass-nahi-hai-s-sq-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bLOo9_mere-pass-nahi-hai-s-sq-h.jpg?h=270&drp=1', NULL, 0, 84976, 0, 0, 1, 0, 8194, 0),
(220, 1, 20, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/flm9e_phir-chala-ly-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7Q4cH_phir-chala-ly-s-h.jpg?h=270&drp=1', NULL, 0, 89891, 0, 0, 1, 0, 8171, 0),
(221, 1, 20, '5', 'server_url', 'Phir Na Milen Kabhi', 'http://www.kids2win.com/api/videosFile/5GJD9_phir-na-milen-kabhi-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/T5HUZ_phir-na-milen-kabhi-s-h.jpg?h=270&drp=1', NULL, 0, 39328, 0, 0, 1, 0, 8165, 0),
(222, 1, 20, '5', 'server_url', 'Bula Le Apne Paas Mujhe', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320498/i7cfjbri4gw2p4iegwyv.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320500/h7bcndbdkzbeof0ccral.jpg', NULL, 175, 832, 63, 0, 1, 1, 8082, 0),
(223, 1, 20, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/0Nvw3_phir-chala-s-h-f-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WmLr7_phir-chala-s-h-f-n.jpg?h=270&drp=1', NULL, 0, 73358, 0, 0, 1, 0, 8056, 0),
(224, 1, 20, '5', 'server_url', 'Jeena Nahi Chahte', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312749/id9uvghihlqnqn7njjxt.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609312752/ipoycg8xjpmxhkvpmmcm.jpg', NULL, 190, 761, 61, 0, 1, 1, 8037, 0),
(225, 1, 20, '5', 'server_url', 'Dil Ki Purani Sadak', 'http://www.kids2win.com/api/videosFile/dSyuC_dil-ki-purani-sadak-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/U9tOT_dil-ki-purani-sadak-s-h.jpg?h=270&drp=1', NULL, 0, 49632, 0, 0, 1, 0, 8036, 0),
(226, 1, 20, '5', 'server_url', 'Chidiya', 'http://www.kids2win.com/api/videosFile/JoTtQ_chidiya-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cW0um_chidiya-f-s-h.jpg?h=270&drp=1', NULL, 0, 52413, 0, 0, 1, 0, 8010, 0),
(227, 1, 20, '5', 'server_url', 'Teri Khushboo', 'http://www.kids2win.com/api/videosFile/LfZyo_teri-khushboo-s-h-ly.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9Daxm_teri-khushboo-s-h-ly.jpg?h=270&drp=1', NULL, 0, 84318, 0, 0, 1, 0, 7979, 0),
(228, 1, 20, '5', 'server_url', 'Shukriya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334938/imo0mfgthnblutrtvhlf.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334941/i9djbsgslexwuxurkciu.jpg', NULL, 154, 503, 72, 0, 1, 1, 7940, 0),
(229, 1, 20, '5', 'server_url', 'Dil Chahte Ho', 'http://www.kids2win.com/api/videosFile/GHDlz_dil-chahte-ho-s-h-f-ani.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IyJ62_dil-chahte-ho-s-h-f-ani.jpg?h=270&drp=1', NULL, 0, 87461, 0, 0, 1, 0, 7934, 0),
(230, 1, 20, '5', 'server_url', 'Ek-hi-zindagi-mili-thi-s-h-f-dai-15', 'http://www.kids2win.com/api/videosFile/tPSLx_ek-hi-zindagi-mili-thi-s-h-f-dai-.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vpQFu_ek-hi-zindagi-mili-thi-s-h-f-dai-.jpg?h=270&drp=1', NULL, 0, 82731, 0, 0, 1, 0, 7933, 0),
(231, 1, 20, '5', 'server_url', 'Deewane Hum Nahi Hote', 'http://www.kids2win.com/api/videosFile/p4SKa_deewane-hum-nahi-hote-s-h-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/zubNi_deewane-hum-nahi-hote-s-h-15.jpg?h=270&drp=1', NULL, 0, 64957, 0, 0, 1, 0, 7909, 0),
(232, 1, 20, '5', 'server_url', 'Sad Dialogue', 'http://www.kids2win.com/api/videosFile/cD4Zj_sad-dialogue-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/B4hP0_sad-dialogue-h-f.jpg?h=270&drp=1', NULL, 0, 71247, 0, 0, 1, 0, 7860, 0),
(233, 1, 20, '5', 'server_url', 'Dil Chahte Ho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339134/rdeykticx2ancm7i1hv5.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609339136/ldici1hi70bcc8jaisei.jpg', NULL, 200, 712, 40, 0, 1, 1, 7827, 0),
(234, 1, 20, '5', 'server_url', 'Bheege Mann', 'http://www.kids2win.com/api/videosFile/DMiaj_bheege-mann-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YswLi_bheege-mann-s-h-f.jpg?h=270&drp=1', NULL, 0, 54106, 0, 0, 1, 0, 7826, 0),
(235, 1, 20, '5', 'server_url', 'Afsos Karoge', 'http://www.kids2win.com/api/videosFile/alSq0_afsos-karoge-s-h-ly.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/uCE6n_afsos-karoge-s-h-ly.jpg?h=270&drp=1', NULL, 0, 107505, 0, 0, 1, 0, 7823, 0),
(236, 1, 20, '5', 'server_url', 'Shukriya', 'http://www.kids2win.com/api/videosFile/rab60_shukriya-ly-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/P7fpW_shukriya-ly-s-h.jpg?h=270&drp=1', NULL, 0, 45882, 0, 0, 1, 0, 7816, 0),
(237, 1, 20, '5', 'server_url', 'Heart Broken', 'http://www.kids2win.com/api/videosFile/ywkn9_heart-broken-ly-h-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RXeq4_heart-broken-ly-h-s.jpg?h=270&drp=1', NULL, 0, 65683, 0, 0, 1, 0, 7802, 0),
(238, 1, 20, '5', 'server_url', 'Sad Status', 'http://www.kids2win.com/api/videosFile/XJ0Rn_sad-status-h-sq-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mrM0N_sad-status-h-sq-s.jpg?h=270&drp=1', NULL, 0, 76550, 0, 0, 1, 0, 7801, 0),
(239, 1, 20, '5', 'server_url', 'Hum Bure Log Hain', 'http://www.kids2win.com/api/videosFile/lpTDZ_hum-bure-log-hain-s-h-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/grdRh_hum-bure-log-hain-s-h-15.jpg?h=270&drp=1', NULL, 0, 91871, 0, 0, 1, 0, 7776, 0),
(240, 1, 20, '5', 'server_url', 'Sad', 'http://www.kids2win.com/api/videosFile/E5Ncj_sad-h-sq.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4BnEc_sad-h-sq.jpg?h=270&drp=1', NULL, 0, 87483, 0, 0, 1, 0, 7760, 0),
(241, 1, 20, '5', 'server_url', 'Dil Tod Ke - B Praak', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319302/lsm2o1hbpapgvrfriucw.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319304/cuvnnpxfoaiksthupoow.jpg', NULL, 102, 630, 45, 0, 1, 1, 7759, 0),
(242, 1, 20, '5', 'server_url', 'Wo Cheez Nhi Hai Hum', 'http://www.kids2win.com/api/videosFile/B0ujh_wo-cheez-nhi-hai-hum-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DeWf8_wo-cheez-nhi-hai-hum-s-h.jpg?h=270&drp=1', NULL, 0, 63393, 0, 0, 1, 0, 7756, 0),
(243, 1, 20, '5', 'server_url', 'Mood Off', 'http://www.kids2win.com/api/videosFile/esPgF_mood-off-s-h-sq.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rf5z1_mood-off-s-h-sq.jpg?h=270&drp=1', NULL, 0, 62397, 0, 0, 1, 0, 7755, 0),
(244, 1, 20, '5', 'server_url', 'Diwane Hum Nahi Hote', 'http://www.kids2win.com/api/videosFile/2ClxO_diwane-hum-nahi-hote-h-s-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/MmLvx_diwane-hum-nahi-hote-h-s-15.jpg?h=270&drp=1', NULL, 0, 53462, 0, 0, 1, 0, 7754, 0),
(245, 1, 74, '5', 'server_url', 'Aankhe Meri Har Jagah', 'http://www.kids2win.com/api/videosFile/DYpei_aankhe-meri-har-jagah-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QHRGu_aankhe-meri-har-jagah-s-h-f.jpg?h=270&drp=1', NULL, 0, 85246, 0, 0, 1, 0, 8631, 0),
(246, 1, 74, '5', 'server_url', 'Rah Wo Jispe Main Chal Raha Tha', 'http://www.kids2win.com/api/videosFile/B3Dhl_rah-wo-jispe-main-chal-raha-tha-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4kOgM_rah-wo-jispe-main-chal-raha-tha-s-h-f.jpg?h=270&drp=1', NULL, 0, 73836, 0, 0, 1, 0, 8630, 0),
(247, 1, 74, '5', 'server_url', 'Bewafa Tera Masum Chehra', 'http://www.kids2win.com/api/videosFile/dfLG1_bewafa-tera-masum-chehra-s-f-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tLDal_bewafa-tera-masum-chehra-s-f-h.jpg?h=270&drp=1', NULL, 0, 55156, 0, 0, 1, 0, 8612, 0),
(248, 1, 74, '5', 'server_url', 'Katal Bazar Mai Ho Chuka Hu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335913/nruz00mws591qbgkwp46.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335915/tdev2pwwaos0qz1ww1jx.jpg', NULL, 155, 877, 85, 0, 1, 1, 8607, 0),
(249, 1, 74, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/CRiLd_bewafa-tera-masoom-chehra-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Efd2o_bewafa-tera-masoom-chehra-f-s-h.jpg?h=270&drp=1', NULL, 0, 23781, 0, 0, 1, 0, 8592, 0),
(250, 1, 74, '5', 'server_url', 'O Khuda', 'http://www.kids2win.com/api/videosFile/sduSb_o-khuda-s-f-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9Od1F_o-khuda-s-f-h.jpg?h=270&drp=1', NULL, 0, 39588, 0, 0, 1, 0, 8590, 0),
(251, 1, 74, '5', 'server_url', 'Veham', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315888/zczwb29qpo7hlgxmjwyn.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/NuRcP_veham-s-h-f-n.jpg?h=270&drp=1', NULL, 165, 508, 47, 0, 1, 1, 8581, 0),
(252, 1, 74, '5', 'server_url', 'Besharam Bewaffa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338838/uhyxorbdarq9leqsoymx.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338839/wyedarrkidxuqkxbmeo0.jpg', NULL, 114, 557, 77, 0, 1, 1, 8559, 0),
(253, 1, 74, '5', 'server_url', 'Ek Tarfa Pyar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311274/iyf3hncwvvf1vw4kxtqh.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/CAhkJ_ek-tarfa-pyar-s-h-f.jpg?h=270&drp=1', NULL, 108, 809, 37, 0, 1, 1, 8558, 0),
(254, 1, 74, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/W3Id7_bewafa-tera-masoom-chehra-s-f-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4EdXn_bewafa-tera-masoom-chehra-s-f-h.jpg?h=270&drp=1', NULL, 0, 61913, 0, 0, 1, 0, 8555, 0),
(255, 1, 74, '5', 'server_url', 'Jinke Liye', 'http://www.kids2win.com/api/videosFile/Z4nJk_jinke-liye-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vBuJ7_jinke-liye-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 61606, 0, 0, 1, 0, 8554, 0),
(256, 1, 74, '5', 'server_url', 'Ek Tarfa Pyar', 'http://www.kids2win.com/api/videosFile/U5wiu_ek-tarfa-pyar-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dVTqB_ek-tarfa-pyar-f-s-h.jpg?h=270&drp=1', NULL, 0, 60568, 0, 0, 1, 0, 8530, 0),
(257, 1, 74, '5', 'server_url', 'Karam Ka Hisab Mat Puchho', 'http://www.kids2win.com/api/videosFile/QFEwq_karam-ka-hisab-mat-puchho-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GNUVP_karam-ka-hisab-mat-puchho-h.jpg?h=270&drp=1', NULL, 0, 38665, 0, 0, 1, 0, 8527, 0),
(258, 1, 74, '5', 'server_url', 'Bewafa Tera Masoom Chehra', 'http://www.kids2win.com/api/videosFile/x9ro4_bewafa-tera-masoom-chehra-s-ly-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gf2PC_bewafa-tera-masoom-chehra-s-ly-h.jpg?h=270&drp=1', NULL, 0, 83555, 0, 0, 1, 0, 8523, 0),
(259, 1, 74, '5', 'server_url', 'Besharam Bewafa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338234/vaal62tn9aqs9dqejneu.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/bT3se_besharam-bewafa-s-h-f.jpg?h=270&drp=1', NULL, 105, 653, 65, 0, 1, 1, 8472, 0),
(260, 1, 74, '5', 'server_url', 'Phir Chala', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317865/zutqiu7yhradatnemrsd.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/5HZUl_phir-chala-f-s-h.jpg?h=270&drp=1', NULL, 110, 597, 99, 0, 1, 1, 8465, 0),
(261, 1, 74, '5', 'server_url', 'Aadat - Ninja', 'http://www.kids2win.com/api/videosFile/O8RLG_aadat-ninja-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wDCez_aadat-ninja-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 73768, 0, 0, 1, 0, 8464, 0),
(262, 1, 74, '5', 'server_url', 'Besharam Bewaffa', 'http://www.kids2win.com/api/videosFile/V1kEm_besharam-bewaffa-f-s-h-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/e8ODF_besharam-bewaffa-f-s-h-n.jpg?h=270&drp=1', NULL, 0, 45840, 0, 0, 1, 0, 8451, 0),
(263, 1, 74, '5', 'server_url', 'Woh Chaand Kahan Se Laogi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314945/w0ug4yquua62elyyjcjp.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/rAE6T_woh-chaand-kahan-se-laogi-f-s-h-n.jpg?h=270&drp=1', NULL, 170, 897, 24, 0, 1, 1, 8434, 0),
(264, 1, 74, '5', 'server_url', 'Agar Tum Saath Ho', 'http://www.kids2win.com/api/videosFile/zUHsA_agar-tum-saath-ho-s-h-sq.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9LnoE_agar-tum-saath-ho-s-h-sq.jpg?h=270&drp=1', NULL, 0, 72068, 0, 0, 1, 0, 8415, 0),
(266, 1, 74, '5', 'server_url', 'I Don\'t Need Everybody', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320628/a6lamvm3jdboxqhl66zv.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320630/znrbi6wocpzqhfaaztru.jpg', NULL, 166, 579, 13, 0, 1, 1, 8408, 0),
(267, 1, 74, '5', 'server_url', 'True Fact Must Watch', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334029/qkcyfb5dh6pir2blhtzz.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334032/n6cst8v04gnfozwjfrts.jpg', NULL, 165, 628, 30, 0, 1, 1, 8406, 0),
(268, 1, 74, '5', 'server_url', 'Mujhe Peene Do', 'http://www.kids2win.com/api/videosFile/fW3Qv_mujhe-peene-do-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Gqndy_mujhe-peene-do-s-h.jpg?h=270&drp=1', NULL, 0, 47069, 0, 0, 1, 0, 8393, 0),
(269, 1, 74, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/302iG_phir-chala-ly-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GReS6_phir-chala-ly-s-h.jpg?h=270&drp=1', NULL, 0, 100242, 0, 0, 1, 0, 8374, 0),
(270, 1, 74, '5', 'server_url', 'Log Humse Jalte Hain', 'http://www.kids2win.com/api/videosFile/yO5tf_log-humse-jalte-hain-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rf0IL_log-humse-jalte-hain-s-h.jpg?h=270&drp=1', NULL, 0, 37674, 0, 0, 1, 0, 8368, 0),
(271, 1, 74, '5', 'server_url', 'Milne Hai Mujhse Aayi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334950/pk9ceez7pl7fohufuk6x.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334953/qh58afglit4gmuktyjyg.jpg', NULL, 145, 819, 64, 0, 1, 1, 8360, 0),
(272, 1, 74, '5', 'server_url', 'Sajda Tera Kar Na Sakoon', 'http://www.kids2win.com/api/videosFile/sP74r_sajda-tera-kar-na-sakoon-s-h-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PthpN_sajda-tera-kar-na-sakoon-s-h-15.jpg?h=270&drp=1', NULL, 0, 106015, 0, 0, 1, 0, 8306, 0),
(273, 1, 74, '5', 'server_url', 'Vo Jab Kehte The', 'http://www.kids2win.com/api/videosFile/ualA3_vo-jab-kehte-the-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Pn2S9_vo-jab-kehte-the-s-h.jpg?h=270&drp=1', NULL, 0, 47537, 0, 0, 1, 0, 8305, 0),
(274, 1, 74, '5', 'server_url', 'Afsos Karoge', 'http://www.kids2win.com/api/videosFile/bOtn2_afsos-karoge-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4aKql_afsos-karoge-s-h.jpg?h=270&drp=1', NULL, 0, 38408, 0, 0, 1, 0, 8304, 0),
(275, 1, 74, '5', 'server_url', 'Tanhaai - Tulsi Kumar', 'http://www.kids2win.com/api/videosFile/gvxVD_tanhaai-tulsi-kumar-s-h-f-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PbTtx_tanhaai-tulsi-kumar-s-h-f-n.jpg?h=270&drp=1', NULL, 0, 108873, 0, 0, 1, 0, 8245, 0),
(276, 1, 74, '5', 'server_url', 'Ae Mere Dil', 'http://www.kids2win.com/api/videosFile/VTtcz_ae-mere-dil-s-h-ani.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wP5iH_ae-mere-dil-s-h-ani.jpg?h=270&drp=1', NULL, 0, 126009, 0, 0, 1, 0, 8244, 0),
(277, 1, 74, '5', 'server_url', 'Tune To Mera Dil Kuch', 'http://www.kids2win.com/api/videosFile/jl0dM_tune-to-mera-dil-kuch-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Pq3RU_tune-to-mera-dil-kuch-f-s-h.jpg?h=270&drp=1', NULL, 0, 106275, 0, 0, 1, 0, 8221, 0),
(278, 1, 74, '5', 'server_url', 'Ae Mere Dil', 'http://www.kids2win.com/api/videosFile/ZTPQE_ae-mere-dil-f-s-h-n.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UW95v_ae-mere-dil-f-s-h-n.jpg?h=270&drp=1', NULL, 0, 71522, 0, 0, 1, 0, 8222, 0),
(279, 1, 74, '5', 'server_url', 'Mere Pass Nahi Hai', 'http://www.kids2win.com/api/videosFile/8REik_mere-pass-nahi-hai-s-sq-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bLOo9_mere-pass-nahi-hai-s-sq-h.jpg?h=270&drp=1', NULL, 0, 84976, 0, 0, 1, 0, 8194, 0),
(280, 1, 74, '5', 'server_url', 'Phir Chala', 'http://www.kids2win.com/api/videosFile/flm9e_phir-chala-ly-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7Q4cH_phir-chala-ly-s-h.jpg?h=270&drp=1', NULL, 0, 89891, 0, 0, 1, 0, 8171, 0),
(281, 1, 74, '5', 'server_url', 'Phir Na Milen Kabhi', 'http://www.kids2win.com/api/videosFile/5GJD9_phir-na-milen-kabhi-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/T5HUZ_phir-na-milen-kabhi-s-h.jpg?h=270&drp=1', NULL, 0, 39328, 0, 0, 1, 0, 8165, 0),
(282, 1, 74, '5', 'server_url', 'Bula Le Apne Paas Mujhe', 'http://www.kids2win.com/api/videosFile/sWKlg_bula-le-apne-paas-mujhe-s-h-f-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lZGLx_bula-le-apne-paas-mujhe-s-h-f-15.jpg?h=270&drp=1', NULL, 0, 111639, 0, 0, 1, 0, 8082, 0),
(283, 1, 74, '5', 'server_url', 'Phir Chala', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313099/ttiqbhoqlbi0zkh7ni7m.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/WmLr7_phir-chala-s-h-f-n.jpg?h=270&drp=1', NULL, 171, 826, 10, 0, 1, 1, 8056, 0),
(284, 1, 74, '5', 'server_url', 'Jeena Nahi Chahte', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317876/yvk43crxzwpihg6uuruy.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317878/tgeabcevg5kdt3vu9q48.jpg', NULL, 124, 674, 25, 0, 1, 1, 8037, 0),
(285, 1, 74, '5', 'server_url', 'Dil Ki Purani Sadak', 'http://www.kids2win.com/api/videosFile/dSyuC_dil-ki-purani-sadak-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/U9tOT_dil-ki-purani-sadak-s-h.jpg?h=270&drp=1', NULL, 0, 49632, 0, 0, 1, 0, 8036, 0),
(286, 1, 74, '5', 'server_url', 'Chidiya', 'http://www.kids2win.com/api/videosFile/JoTtQ_chidiya-f-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cW0um_chidiya-f-s-h.jpg?h=270&drp=1', NULL, 0, 52413, 0, 0, 1, 0, 8010, 0),
(287, 1, 74, '5', 'server_url', 'Teri Khushboo', 'http://www.kids2win.com/api/videosFile/LfZyo_teri-khushboo-s-h-ly.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9Daxm_teri-khushboo-s-h-ly.jpg?h=270&drp=1', NULL, 0, 84318, 0, 0, 1, 0, 7979, 0),
(288, 1, 74, '5', 'server_url', 'Shukriya', 'http://www.kids2win.com/api/videosFile/OBH5z_shukriya-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wunCZ_shukriya-s-h.jpg?h=270&drp=1', NULL, 0, 42509, 0, 0, 1, 0, 7940, 0),
(289, 1, 74, '5', 'server_url', 'Dil Chahte Ho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319395/lz6walfgtxfybas1cuxa.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319397/wm3shimobjn59ctxi3an.jpg', NULL, 131, 400, 12, 0, 1, 1, 7934, 0),
(290, 1, 74, '5', 'server_url', 'Ek-hi-zindagi-mili-thi-s-h-f-dai-15', 'http://www.kids2win.com/api/videosFile/tPSLx_ek-hi-zindagi-mili-thi-s-h-f-dai-.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vpQFu_ek-hi-zindagi-mili-thi-s-h-f-dai-.jpg?h=270&drp=1', NULL, 0, 82731, 0, 0, 1, 0, 7933, 0),
(291, 1, 74, '5', 'server_url', 'Deewane Hum Nahi Hote', 'http://www.kids2win.com/api/videosFile/p4SKa_deewane-hum-nahi-hote-s-h-15.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/zubNi_deewane-hum-nahi-hote-s-h-15.jpg?h=270&drp=1', NULL, 0, 64957, 0, 0, 1, 0, 7909, 0),
(292, 1, 74, '5', 'server_url', 'Sad Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313437/ofttt0ovuarphinf8pfq.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313440/eb55lvzsiym52lh1irm2.jpg', NULL, 172, 484, 24, 0, 1, 1, 7860, 0),
(293, 1, 74, '5', 'server_url', 'Dil Chahte Ho', 'http://www.kids2win.com/api/videosFile/OHZ9V_dil-chahte-ho-ly-l-h-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4esko_dil-chahte-ho-ly-l-h-s.jpg?h=270&drp=1', NULL, 0, 87513, 0, 0, 1, 0, 7827, 0),
(294, 1, 74, '5', 'server_url', 'Bheege Mann', 'http://www.kids2win.com/api/videosFile/DMiaj_bheege-mann-s-h-f.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YswLi_bheege-mann-s-h-f.jpg?h=270&drp=1', NULL, 0, 54106, 0, 0, 1, 0, 7826, 0),
(295, 1, 74, '5', 'server_url', 'Afsos Karoge', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310393/i8qbckjfbsl8uhwes7e0.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310395/zkw5y95pfeux7fwubge5.jpg', NULL, 125, 454, 89, 0, 1, 1, 7823, 0),
(296, 1, 74, '5', 'server_url', 'Shukriya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609270582/ygmrh1sub1fhw2mdtw10.mp4', '-', 'Sad', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609270584/mya2lobwb14hbtr92eyv.jpg', NULL, 135, 798, 29, 0, 1, 1, 0, 0),
(297, 1, 74, '5', 'server_url', 'Heart Broken', 'http://www.kids2win.com/api/videosFile/ywkn9_heart-broken-ly-h-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RXeq4_heart-broken-ly-h-s.jpg?h=270&drp=1', NULL, 0, 65683, 0, 0, 1, 0, 7802, 0),
(298, 1, 74, '5', 'server_url', 'Sad Status', 'http://www.kids2win.com/api/videosFile/XJ0Rn_sad-status-h-sq-s.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mrM0N_sad-status-h-sq-s.jpg?h=270&drp=1', NULL, 0, 76550, 0, 0, 1, 0, 7801, 0),
(299, 1, 74, '5', 'server_url', 'Hum Bure Log Hain', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336098/hpissekex1nmao8vvr2s.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/grdRh_hum-bure-log-hain-s-h-15.jpg?h=270&drp=1', NULL, 155, 591, 19, 0, 1, 1, 7776, 0),
(300, 1, 74, '5', 'server_url', 'Sad', 'http://www.kids2win.com/api/videosFile/E5Ncj_sad-h-sq.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4BnEc_sad-h-sq.jpg?h=270&drp=1', NULL, 0, 87483, 0, 0, 1, 0, 7760, 0),
(301, 1, 74, '5', 'server_url', 'Dil Tod Ke - B Praak', 'http://www.kids2win.com/api/videosFile/6D5oE_dil-tod-ke-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/s6bYS_dil-tod-ke-s-h.jpg?h=270&drp=1', NULL, 0, 60743, 0, 0, 1, 0, 7759, 0),
(302, 1, 74, '5', 'server_url', 'Wo Cheez Nhi Hai Hum', 'http://www.kids2win.com/api/videosFile/B0ujh_wo-cheez-nhi-hai-hum-s-h.mp4', '-', 'Sad', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DeWf8_wo-cheez-nhi-hai-hum-s-h.jpg?h=270&drp=1', NULL, 0, 63393, 0, 0, 1, 0, 7756, 0),
(303, 1, 74, '5', 'server_url', 'Mood Off', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402439/jiwuapxztanyqz4lhrat.mp4', '-', 'Sad', 'Portrait', 'http://www.kids2win.com/api/thumbImage/rf5z1_mood-off-s-h-sq.jpg?h=270&drp=1', NULL, 108, 899, 78, 0, 1, 1, 7755, 0),
(304, 1, 74, '5', 'server_url', 'Diwane Hum Nahi Hote', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309759/j3nlsyuxjsjyjz2yv0rz.mp4', '-', 'Sad', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309761/ogbpvvszvz4lgebhhdaw.jpg', NULL, 157, 490, 74, 0, 1, 1, 7754, 0),
(305, 1, 75, '5', 'server_url', 'Oh Itna Na Yaad Aaya Karo', 'http://www.kids2win.com/api/videosFile/U9657_oh-itna-na-yaad-aaya-karo-brek-h-s.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/F0cx8_oh-itna-na-yaad-aaya-karo-brek-h-s.jpg?h=270&drp=1', NULL, 0, 242795, 0, 0, 1, 0, 7491, 0),
(306, 1, 75, '5', 'server_url', 'Tum Wapas Mat Aana', 'http://www.kids2win.com/api/videosFile/bR3QY_tum-wapas-mat-aana-b-h.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7cnkP_tum-wapas-mat-aana-b-h.jpg?h=270&drp=1', NULL, 0, 380578, 0, 0, 1, 0, 6347, 0),
(307, 1, 75, '5', 'server_url', 'Duaa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313642/vfklo2zmcaytrhwm5wna.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313643/znvbfcofuvbh4bvhnkb5.jpg', NULL, 191, 546, 25, 0, 1, 1, 5410, 0),
(308, 1, 75, '5', 'server_url', 'Jinke Liye - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309471/hgtv3vv1fpj1ivejvzdw.mp4', '-', 'Breakup', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309474/c8axzjqhfsc0gtg3w3j1.jpg', NULL, 165, 634, 11, 0, 1, 1, 5365, 0),
(309, 1, 75, '5', 'server_url', 'Neendo Se Breakup - Fullscreen', 'http://www.kids2win.com/api/videosFile/mhcWP_neendo-se-breakup-sad-fullscreen-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZHzh2_neendo-se-breakup-sad-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 98171, 0, 0, 1, 0, 4864, 0),
(310, 1, 75, '5', 'server_url', 'Phir Na Milen Kabhi', 'http://www.kids2win.com/api/videosFile/oTD7U_phir-na-milen-kabhi-breakup-hindi-lyrical.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fsil2_phir-na-milen-kabhi-breakup-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 176025, 0, 0, 1, 0, 4462, 0),
(311, 1, 75, '5', 'server_url', 'Bekhayali', 'http://www.kids2win.com/api/videosFile/ojEzv_bekhayali-breakup-sad-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/q34Y1_bekhayali-breakup-sad-hindi.jpg?h=270&drp=1', NULL, 0, 152875, 0, 0, 1, 0, 3734, 0),
(312, 1, 75, '5', 'server_url', 'Breakup Dialogue', 'http://www.kids2win.com/api/videosFile/vmVgU_breakup-dialogue-hindi-status.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/E64QC_breakup-dialogue-hindi-status.jpg?h=270&drp=1', NULL, 0, 145087, 0, 0, 1, 0, 3630, 0),
(313, 1, 75, '5', 'server_url', 'Bichad To Gaye Ho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352691/yamdgaz7ftqqmfhqiufb.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352693/zhubrwpfq45anh8bmfsz.jpg', NULL, 153, 421, 60, 0, 1, 1, 3536, 0),
(314, 1, 75, '5', 'server_url', 'Killer Attitude Status For Boys', 'http://www.kids2win.com/api/videosFile/cwgzD_killer-attitude-status-for-boys-breakup-fullscreen-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TfC6N_killer-attitude-status-for-boys-breakup-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 111612, 0, 0, 1, 0, 3476, 0),
(315, 1, 75, '5', 'server_url', 'Ho Gayi Galti Mujse', 'http://www.kids2win.com/api/videosFile/89Q2L_ho-gayi-galti-mujse-break-up-hindi-fullscreen.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NdFq8_ho-gayi-galti-mujse-break-up-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 92546, 0, 0, 1, 0, 3180, 0),
(316, 1, 75, '5', 'server_url', 'Broken Heart Status - Sad', 'http://www.kids2win.com/api/videosFile/03ZrR_broken-heart-status-sad-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3LRk8_broken-heart-status-sad-hindi.jpg?h=270&drp=1', NULL, 0, 125253, 0, 0, 1, 0, 3128, 0),
(317, 1, 75, '5', 'server_url', 'Pachtaoge - Female Version', 'http://www.kids2win.com/api/videosFile/8BXEq_pachtaoge-hindi-breakup-sad-female-version.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8oF2H_pachtaoge-hindi-breakup-sad-female-version.jpg?h=270&drp=1', NULL, 0, 80296, 0, 0, 1, 0, 2853, 0),
(318, 1, 75, '5', 'server_url', 'Sab Kuch Bhula Diya - Sad', 'http://www.kids2win.com/api/videosFile/Vwlkn_sab-kuch-bhula-diya-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/528v3_sab-kuch-bhula-diya-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 74764, 0, 0, 1, 0, 2827, 0),
(319, 1, 75, '5', 'server_url', 'Bada Pachtaoge - Lyrical', 'http://www.kids2win.com/api/videosFile/aHvnI_bada-pachtaoge-lyrical-hindi-sad-all-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HBZLX_bada-pachtaoge-lyrical-hindi-sad-all-breakup.jpg?h=270&drp=1', NULL, 0, 38145, 0, 0, 1, 0, 2808, 0),
(320, 1, 75, '5', 'server_url', 'Bada Pachtaoge - Breakup', 'http://www.kids2win.com/api/videosFile/rjmG3_bada-pachtaoge-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PHzw5_bada-pachtaoge-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 42060, 0, 0, 1, 0, 2806, 0),
(321, 1, 75, '5', 'server_url', 'Bekhayali - Lyrical', 'http://www.kids2win.com/api/videosFile/MEmZI_bekhayali-lyrical-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/E4aXU_bekhayali-lyrical-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 68173, 0, 0, 1, 0, 2616, 0),
(322, 1, 75, '5', 'server_url', 'Tere Pyaar Me Khoya Hu Me - Everything', 'http://www.kids2win.com/api/videosFile/hjQeJ_tere-pyaar-me-khoya-hu-me-everything-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DANos_tere-pyaar-me-khoya-hu-me-everything-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 36995, 0, 0, 1, 0, 2566, 0),
(323, 1, 75, '5', 'server_url', 'Tujhe Kitna Maine Chaha - Everything', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315840/ujhnqyyograls6ghmkf8.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609315842/gyssjjpnxbg0kga17ws4.jpg', NULL, 104, 717, 21, 0, 1, 1, 2562, 0),
(324, 1, 75, '5', 'server_url', 'Phirta Rahoon Dar Badar - Sad', 'http://www.kids2win.com/api/videosFile/Ad91t_phirta-rahoon-dar-badar-sad-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NRnpi_phirta-rahoon-dar-badar-sad-hindi.jpg?h=270&drp=1', NULL, 0, 30835, 0, 0, 1, 0, 2553, 0),
(325, 1, 75, '5', 'server_url', 'Mushkil Se Main - Sad', 'http://www.kids2win.com/api/videosFile/jdAfq_mushkil-se-main-sad-hindi-breakup-lyrical.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/azlkO_mushkil-se-main-sad-hindi-breakup-lyrical.jpg?h=270&drp=1', NULL, 0, 83060, 0, 0, 1, 0, 2501, 0),
(326, 1, 75, '5', 'server_url', 'Sad Shayari', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334693/ixlmyamsip6z7iccsm7u.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334695/vchq5cpqzxaqcvmgd6pn.jpg', NULL, 175, 875, 10, 0, 1, 1, 2495, 0),
(327, 1, 75, '5', 'server_url', 'Dil Tod Ke Hasti Ho Mera', 'http://www.kids2win.com/api/videosFile/F61aS_dil-tod-ke-hasti-ho-mera-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8wCsL_dil-tod-ke-hasti-ho-mera-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 53257, 0, 0, 1, 0, 2321, 0),
(328, 1, 75, '5', 'server_url', 'Haan Tu Hain - Sad', 'http://www.kids2win.com/api/videosFile/RiOz4_haan-tu-hain-sad-breakup-hindi-lyrical.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1mhFs_haan-tu-hain-sad-breakup-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 63616, 0, 0, 1, 0, 2421, 0),
(329, 1, 75, '5', 'server_url', 'Broken Heart Shayari', 'http://www.kids2win.com/api/videosFile/WrI6B_broken-heart-shayari-sad-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bkiqK_broken-heart-shayari-sad-hindi.jpg?h=270&drp=1', NULL, 0, 60631, 0, 0, 1, 0, 2407, 0),
(330, 1, 75, '5', 'server_url', 'Tumko Dil Mein Basana Galat Tha', 'http://www.kids2win.com/api/videosFile/QdGK9_tumko-dil-mein-basana-galat-tha-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8szBc_tumko-dil-mein-basana-galat-tha-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 56371, 0, 0, 1, 0, 2380, 0),
(331, 1, 75, '5', 'server_url', 'Mere Mehboob Ki Tooti - Old Song', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353493/logmrknm55lcsnnydumi.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609353495/leaqj75cbkdag63whv1k.jpg', NULL, 173, 698, 24, 0, 1, 1, 2374, 0),
(332, 1, 75, '5', 'server_url', 'Yeh Jism Hai Toh Kya', 'http://www.kids2win.com/api/videosFile/NMuHp_yeh-jism-hai-toh-kya-sad-breakup-hindi.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cbiAG_yeh-jism-hai-toh-kya-sad-breakup-hindi.jpg?h=270&drp=1', NULL, 0, 54902, 0, 0, 1, 0, 2337, 0),
(333, 1, 75, '5', 'server_url', 'Jiye To Jiye Kaise - Fullscreen', 'http://www.kids2win.com/api/videosFile/p1mWL_jiye-to-jiye-kaise-sad-hindi-breakup-fullscreen.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/al5Rz_jiye-to-jiye-kaise-sad-hindi-breakup-fullscreen.jpg?h=270&drp=1', NULL, 0, 38465, 0, 0, 1, 0, 2310, 0),
(334, 1, 75, '5', 'server_url', 'Chaha Hai Tujhko', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609321249/fnycgx112tubydqwdave.mp4', '-', 'Breakup', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609321252/wmyltycmfx81u2cbvsx9.jpg', NULL, 184, 815, 98, 0, 1, 1, 2284, 0),
(335, 1, 75, '5', 'server_url', 'Na Kare Bewafai Koi Dil Se', 'http://www.kids2win.com/api/videosFile/JkGmi_na-kare-bewafai-koi-dil-se-kabhi-sad-hindi-breakup-fullscreen.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GNDko_na-kare-bewafai-koi-dil-se-kabhi-sad-hindi-breakup-fullscreen.jpg?h=270&drp=1', NULL, 0, 31635, 0, 0, 1, 0, 2238, 0),
(336, 1, 75, '5', 'server_url', 'Karo Na Karo Tum Humpe Bharosa - Sad', 'http://www.kids2win.com/api/videosFile/QSXTI_karo-na-karo-tum-humpe-bharosa-sad-hindi-breakup.mp4', '-', 'Breakup', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Sx6wP_karo-na-karo-tum-humpe-bharosa-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 40195, 0, 0, 1, 0, 2190, 0),
(337, 1, 76, '5', 'server_url', 'Jeene Bhi De Duniya Humein', 'http://www.kids2win.com/api/videosFile/yzwU6_jeene-bhi-de-duniya-hume-love-hindi-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WcBCu_jeene-bhi-de-duniya-hume-love-hindi-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 217592, 0, 0, 1, 0, 506, 0),
(338, 1, 76, '5', 'server_url', 'Dil Ibaadat - Hayat & Murat', 'http://www.kids2win.com/api/videosFile/Xubhj_dil-ibaadat-love-hindi-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SkTvz_dil-ibaadat-love-hindi-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 405766, 0, 0, 1, 0, 3740, 0),
(339, 1, 76, '5', 'server_url', 'Ek Baar - Hayat And Murat', 'http://www.kids2win.com/api/videosFile/B2FDC_ek-baar-hayat-and-murat-love-hindi.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jcXap_ek-baar-hayat-and-murat-love-hindi.jpg?h=270&drp=1', NULL, 0, 288945, 0, 0, 1, 0, 3248, 0),
(340, 1, 76, '5', 'server_url', 'Sun Sonio Sun Dildar', 'http://www.kids2win.com/api/videosFile/aXU6Y_sun-sonio-sun-dildar-hayat-and-murat-hindi-love.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NsM7u_sun-sonio-sun-dildar-hayat-and-murat-hindi-love.jpg?h=270&drp=1', NULL, 0, 165277, 0, 0, 1, 0, 3200, 0),
(341, 1, 76, '5', 'server_url', 'Saanson Ko Jeene Ka - Hayat And Murat', 'http://www.kids2win.com/api/videosFile/1EzYt_saanson-ko-jeene-ka-love-hindi-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/w3yBq_saanson-ko-jeene-ka-love-hindi-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 106829, 0, 0, 1, 0, 3024, 0),
(342, 1, 76, '5', 'server_url', 'Mar Jaayen - Hayat And Murat', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310702/ro1mb5ctze4sspmp097i.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310705/oeknsxbgegueuiqrw4g9.jpg', NULL, 187, 780, 24, 0, 1, 1, 2992, 0),
(343, 1, 76, '5', 'server_url', 'Pal Pal Dil Ke Paas - Fullscreen', 'http://www.kids2win.com/api/videosFile/rvE8d_pal-pal-dil-ke-paas-arijit-singh-hindi-fullscreen-love.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HIxk2_pal-pal-dil-ke-paas-arijit-singh-hindi-fullscreen-love.jpg?h=270&drp=1', NULL, 0, 59962, 0, 0, 1, 0, 2921, 0),
(344, 1, 76, '5', 'server_url', 'Dil Ibaadat - Hayat And Murat', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318729/aw3xqh0of2o49archtrj.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609318731/wrrhz2p3kqz8njsvnq6j.jpg', NULL, 117, 577, 19, 0, 1, 1, 2742, 0),
(345, 1, 76, '5', 'server_url', 'Kabir Singh - Love Mashup', 'http://www.kids2win.com/api/videosFile/n6WzS_kabir-singh-love-mashup-love-hindi-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oRCjn_kabir-singh-love-mashup-love-hindi-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 105380, 0, 0, 1, 0, 2719, 0),
(346, 1, 76, '5', 'server_url', 'Badhne do chaahtein ye thoda aur', 'http://www.kids2win.com/api/videosFile/avwZb_badhne-do-chaahtein-ye-thoda-aur-love-romantic-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WSwgL_badhne-do-chaahtein-ye-thoda-aur-love-romantic-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 96233, 0, 0, 1, 0, 167, 0),
(347, 1, 76, '5', 'server_url', 'Haan Tu Hain - Love', 'http://www.kids2win.com/api/videosFile/XyviI_haan-tu-hain-love-hindi-hayat-and-murat.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2pPnv_haan-tu-hain-love-hindi-hayat-and-murat.jpg?h=270&drp=1', NULL, 0, 68370, 0, 0, 1, 0, 2619, 0),
(348, 1, 76, '5', 'server_url', 'Kuch Toh Hai Tujhse Raabta', 'http://www.kids2win.com/api/videosFile/6wbso_kuch-toh-hai-tujhse-raabta-hayat-and-murat-love-hindi.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iRL5t_kuch-toh-hai-tujhse-raabta-hayat-and-murat-love-hindi.jpg?h=270&drp=1', NULL, 0, 71081, 0, 0, 1, 0, 2611, 0),
(349, 1, 76, '5', 'server_url', 'Mile ho tum humko - Hayat and murat', 'http://www.kids2win.com/api/videosFile/UIXYZ_mile-ho-tum-humko-hayat-and-murat-love-hindi.mp4', '-', 'Hayat & Murat', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8odn2_mile-ho-tum-humko-hayat-and-murat-love-hindi.jpg?h=270&drp=1', NULL, 0, 109325, 0, 0, 1, 0, 311, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(350, 1, 76, '5', 'server_url', 'Ishq Adhura - Hayat And Murat', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609351730/nppn44xbtfk6dih9hvk2.mp4', '-', 'Hayat & Murat', 'Portrait', 'http://www.kids2win.com/api/thumbImage/38JdN_ishq-adhura-sad-hindi-fullscreen-hayat-and-murat.jpg?h=270&drp=1', NULL, 161, 682, 88, 0, 1, 1, 2297, 0),
(351, 1, 78, '5', 'server_url', 'Love mashup', 'http://www.kids2win.com/api/videosFile/6JDEy_love-mashup-hindi-punjabi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/icNBO_love-mashup-hindi-punjabi.jpg?h=270&drp=1', NULL, 0, 42629, 0, 0, 1, 0, 3850, 0),
(352, 1, 78, '5', 'server_url', 'Meri Galti', 'http://www.kids2win.com/api/videosFile/FxNLX_meri-galti-sad-hindi-punjabi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4M3ur_meri-galti-sad-hindi-punjabi.jpg?h=270&drp=1', NULL, 0, 18031, 0, 0, 1, 0, 3553, 0),
(353, 1, 78, '5', 'server_url', 'Rog - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311328/vqcyctt9oavtgodo2hvs.mp4', '-', 'Punjabi', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609311330/dnhbxlio5t4itze6ibws.jpg', NULL, 188, 538, 71, 0, 1, 1, 3530, 0),
(354, 1, 78, '5', 'server_url', 'Main Teri Ho Gayi', 'http://www.kids2win.com/api/videosFile/AD2Pu_main-teri-ho-gayi-punjabi-love-hindi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4P8IX_main-teri-ho-gayi-punjabi-love-hindi.jpg?h=270&drp=1', NULL, 0, 11796, 0, 0, 1, 0, 1688, 0),
(355, 1, 78, '5', 'server_url', 'Teri Hoke Rehna Ae - Punjabi', 'http://www.kids2win.com/api/videosFile/4TCgX_teri-hoke-rehna-ae-punjabi-love-hindi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/uomIR_teri-hoke-rehna-ae-punjabi-love-hindi.jpg?h=270&drp=1', NULL, 0, 8371, 0, 0, 1, 0, 3061, 0),
(356, 1, 78, '5', 'server_url', 'Mile ho tum humko', 'http://www.kids2win.com/api/videosFile/t3WRV_mile-ho-tum-humko-hindi-love-panjabi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/12wQq_mile-ho-tum-humko-hindi-love-panjabi.jpg?h=270&drp=1', NULL, 0, 11552, 0, 0, 1, 0, 829, 0),
(357, 1, 78, '5', 'server_url', 'Daru badnam kardi', 'http://www.kids2win.com/api/videosFile/U50ZC_daru-badnam-kardi-panjabi-love-hindi.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8UHdA_daru-badnam-kardi-panjabi-love-hindi.jpg?h=270&drp=1', NULL, 0, 9868, 0, 0, 1, 0, 107, 0),
(358, 1, 78, '5', 'server_url', 'Bom diggy digg', 'http://www.kids2win.com/api/videosFile/ZFMOr_bom-diggy-digg-hindi-panjabi-love.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wgvXD_bom-diggy-digg-hindi-panjabi-love.jpg?h=270&drp=1', NULL, 0, 8314, 0, 0, 1, 0, 102, 0),
(359, 1, 78, '5', 'server_url', 'Dil kya kare jab kisi se', 'http://www.kids2win.com/api/videosFile/lqY2D_dil-kya-kare-jab-kisi-se-punjabi-love.mp4', '-', 'Punjabi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FOH5x_dil-kya-kare-jab-kisi-se-punjabi-love.jpg?h=270&drp=1', NULL, 0, 7094, 0, 0, 1, 0, 60, 0),
(360, 1, 78, '5', 'server_url', 'Ban ja tu meri rani', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403425/lbhhl36ccyaa8qwphb5c.mp4', '-', 'Punjabi', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403427/tor6xp71uvw86wlmerjf.jpg', NULL, 137, 726, 10, 0, 1, 1, 113, 0),
(361, 1, 78, '5', 'server_url', 'Paagal - Badshah', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336292/lvc1iwrnvetcn0l2hdch.mp4', '-', 'Punjabi', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609336295/fb9mip96xvbwaplje337.jpg', NULL, 139, 751, 66, 0, 1, 1, 2494, 0),
(362, 1, 82, '5', 'server_url', 'Ghamand Kar', 'http://www.kids2win.com/api/videosFile/sFblL_ghamand-kar-h-shi.mp4', '-', 'Shivaji Maharaj', 'Landscape', 'http://www.kids2win.com/api/thumbImage/I8rKo_ghamand-kar-h-shi.jpg?h=270&drp=1', NULL, 0, 11327, 0, 0, 1, 0, 8299, 0),
(363, 1, 82, '5', 'server_url', 'Mann Mein Shiva', 'http://www.kids2win.com/api/videosFile/4nMSw_mann-mein-shiva-h-sq-shi.mp4', '-', 'Shivaji Maharaj', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CwcZg_mann-mein-shiva-h-sq-shi.jpg?h=270&drp=1', NULL, 0, 16420, 0, 0, 1, 0, 8211, 0),
(364, 1, 82, '5', 'server_url', 'Shiv Jayanti', 'http://www.kids2win.com/api/videosFile/vqCMZ_shiv-jayanti-festival-shivaji-hindi.mp4', '-', 'Shivaji Maharaj', 'Landscape', 'http://www.kids2win.com/api/thumbImage/c8PyJ_shiv-jayanti-festival-shivaji-hindi.jpg?h=270&drp=1', NULL, 0, 8752, 0, 0, 1, 0, 4654, 0),
(365, 1, 82, '5', 'server_url', 'Shiv Jayanti Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309807/agdczdfg4bprfd9d4jdq.mp4', '-', 'Shivaji Maharaj', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309810/omqs06au4uecblxpyzh2.jpg', NULL, 180, 507, 56, 0, 1, 1, 4653, 0),
(366, 1, 82, '5', 'server_url', 'Maay Bhavani - Fullscreen', 'http://www.kids2win.com/api/videosFile/GU56g_maay-bhavani-fullscreen-hindi-shivaji.mp4', '-', 'Shivaji Maharaj', 'Landscape', 'http://www.kids2win.com/api/thumbImage/xIbXy_maay-bhavani-fullscreen-hindi-shivaji.jpg?h=270&drp=1', NULL, 0, 11716, 0, 0, 1, 0, 4349, 0),
(367, 1, 83, '5', 'server_url', 'Karwa Chauth', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337931/lzpjag3hktfiplz3ul77.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337934/dv4a9ahcw52qeqwghp0a.jpg', NULL, 185, 712, 91, 0, 1, 1, 8275, 0),
(368, 1, 83, '5', 'server_url', 'Deewano Si Halat Hai', 'http://www.kids2win.com/api/videosFile/ez0Tm_deewano-si-halat-hai-l-h-tv-seri-f.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nQXRb_deewano-si-halat-hai-l-h-tv-seri-f.jpg?h=270&drp=1', NULL, 0, 80803, 0, 0, 1, 0, 8169, 0),
(369, 1, 83, '5', 'server_url', 'Yuhi Nahi Tujhpe', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333462/thdioyxd2gnheyinl2k3.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/EIjqe_yuhi-nahi-tujhpe-sq-l-h-tv-seri.jpg?h=270&drp=1', NULL, 118, 805, 21, 0, 1, 1, 8164, 0),
(370, 1, 83, '5', 'server_url', 'Zinda Rehti Hain Mohabbatein', 'http://www.kids2win.com/api/videosFile/WAwRC_zinda-rehti-hain-mohabbatein-l-h-sq-tvs.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GARls_zinda-rehti-hain-mohabbatein-l-h-sq-tvs.jpg?h=270&drp=1', NULL, 0, 54920, 0, 0, 1, 0, 7988, 0),
(371, 1, 83, '5', 'server_url', 'Chand Taron Me Nazar Aaye', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401361/ex6fo5jqiittz6wgvb1k.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401363/i898z0tozcbnytmhxt26.jpg', NULL, 162, 686, 74, 0, 1, 1, 5977, 0),
(372, 1, 83, '5', 'server_url', 'Roke Na Ruke Naina', 'http://www.kids2win.com/api/videosFile/1vDnB_roke-na-ruke-naina-tv-serial-hindi-sad.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mEDO5_roke-na-ruke-naina-tv-serial-hindi-sad.jpg?h=270&drp=1', NULL, 0, 105195, 0, 0, 1, 0, 5939, 0),
(373, 1, 83, '5', 'server_url', 'Falak Tak Chal Sath Mere', 'http://www.kids2win.com/api/videosFile/MfT6l_falak-tak-chal-sath-mere-sad-hindi-tvserials-animated.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yCB0h_falak-tak-chal-sath-mere-sad-hindi-tvserials-animated.jpg?h=270&drp=1', NULL, 0, 152566, 0, 0, 1, 0, 1485, 0),
(374, 1, 83, '5', 'server_url', 'Holi Status - Tv Serials', 'http://www.kids2win.com/api/videosFile/8OJTh_holi-status-tv-serials-dailogs-hindi.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YGN3k_holi-status-tv-serials-dailogs-hindi.jpg?h=270&drp=1', NULL, 0, 59581, 0, 0, 1, 0, 2165, 0),
(375, 1, 83, '5', 'server_url', 'Radha Krishna - Holi Special', 'http://www.kids2win.com/api/videosFile/P8qrt_radha-krishna-holi-special-status-tvserial-hindi-dailogs.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4l8yk_radha-krishna-holi-special-status-tvserial-hindi-dailogs.jpg?h=270&drp=1', NULL, 0, 107385, 0, 0, 1, 0, 2170, 0),
(376, 1, 83, '5', 'server_url', 'Tere bina jeena saza ho gaya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399716/xspkdricvsrgahjx2nqr.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/Qpbsl_tere-bina-jeena-saza-ho-gaya-romantic-tvserial-hindi.jpg?h=270&drp=1', NULL, 177, 759, 94, 0, 1, 1, 1221, 0),
(377, 1, 83, '5', 'server_url', 'Hum Asie Karenge Pyar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334736/pyp4bzowlgd3bkqy4dfo.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334738/e5egrcifst8dij7j2wjh.jpg', NULL, 110, 823, 43, 0, 1, 1, 4307, 0),
(378, 1, 83, '5', 'server_url', 'Agar Tum Na Hote - Romantic', 'http://www.kids2win.com/api/videosFile/0KZlS_agar-tum-na-hote-hindi-love-romantic-tv-serial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FUE8H_agar-tum-na-hote-hindi-love-romantic-tv-serial.jpg?h=270&drp=1', NULL, 0, 133631, 0, 0, 1, 0, 4267, 0),
(379, 1, 83, '5', 'server_url', 'Kho Na Doon', 'http://www.kids2win.com/api/videosFile/uAK52_kho-na-doon-love-hindi-lyrical-tv-serial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TI5W1_kho-na-doon-love-hindi-lyrical-tv-serial.jpg?h=270&drp=1', NULL, 0, 59802, 0, 0, 1, 0, 4193, 0),
(380, 1, 83, '5', 'server_url', 'Jana Na Dil Se Door', 'http://www.kids2win.com/api/videosFile/T0gxB_jana-na-dil-se-door-love-lyrical-tv-serial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Umcdl_jana-na-dil-se-door-love-lyrical-tv-serial.jpg?h=270&drp=1', NULL, 0, 44233, 0, 0, 1, 0, 4162, 0),
(381, 1, 83, '5', 'server_url', 'Dil laga liya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316021/nqbmvuvukhfj9fndzjkc.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/r41t7_dil-laga-liya-love-fullscreen-tv-serial-hindi.jpg?h=270&drp=1', NULL, 169, 898, 30, 0, 1, 1, 4020, 0),
(382, 1, 83, '5', 'server_url', 'Aaye Ho Meri Zindagi Mein', 'http://www.kids2win.com/api/videosFile/NQRcD_aaye-ho-meri-zindagi-mein-unplugged-love-hindi-tv-sreial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8B4lK_aaye-ho-meri-zindagi-mein-unplugged-love-hindi-tv-sreial.jpg?h=270&drp=1', NULL, 0, 68464, 0, 0, 1, 0, 3944, 0),
(383, 1, 83, '5', 'server_url', 'Musafir - Tv Serial', 'http://www.kids2win.com/api/videosFile/7ipFH_musafir-love-hindi-chocolate-day-fullscreen-tv-serial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/C7fi3_musafir-love-hindi-chocolate-day-fullscreen-tv-serial.jpg?h=270&drp=1', NULL, 0, 57546, 0, 0, 1, 0, 3851, 0),
(384, 1, 83, '5', 'server_url', 'Khali Khali Dil Ko', 'http://www.kids2win.com/api/videosFile/zuo2w_khali-khali-dil-ko-love-hindi-fullscreen-tv-serial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Grq8E_khali-khali-dil-ko-love-hindi-fullscreen-tv-serial.jpg?h=270&drp=1', NULL, 0, 41139, 0, 0, 1, 0, 3809, 0),
(385, 1, 83, '5', 'server_url', 'Sawan Aaya Hai', 'http://www.kids2win.com/api/videosFile/abCnM_sawan-aaya-hai-tv-serial-fullscreen-hindi-love-rainy.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YSNFQ_sawan-aaya-hai-tv-serial-fullscreen-hindi-love-rainy.jpg?h=270&drp=1', NULL, 0, 79163, 0, 0, 1, 0, 3568, 0),
(386, 1, 83, '5', 'server_url', 'Meri Har Khushi Mein - Love', 'http://www.kids2win.com/api/videosFile/ZLdWi_meri-har-khushi-mein-love-hindi-tvserial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yFhWC_meri-har-khushi-mein-love-hindi-tvserial.jpg?h=270&drp=1', NULL, 0, 59809, 0, 0, 1, 0, 3490, 0),
(387, 1, 83, '5', 'server_url', 'Naina', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316052/oxdsple4fsoqwxt2ok7g.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316055/idyih8xhyej7munaoknh.jpg', NULL, 112, 604, 37, 0, 1, 1, 805, 0),
(388, 1, 83, '5', 'server_url', 'Chogada - Tv Serials', 'http://www.kids2win.com/api/videosFile/R5Mod_chogada-tv-serials-hindi-navratri.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ytamp_chogada-tv-serials-hindi-navratri.jpg?h=270&drp=1', NULL, 0, 37556, 0, 0, 1, 0, 3099, 0),
(389, 1, 83, '5', 'server_url', 'Radhe Radhe - Janmashtami', 'http://www.kids2win.com/api/videosFile/PH80K_radhe-radhe-festival-krishna-tv-serials-hindi-fullscreen-janmashtami.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2NeFt_radhe-radhe-festival-krishna-tv-serials-hindi-fullscreen-janmashtami.jpg?h=270&drp=1', NULL, 0, 32499, 0, 0, 1, 0, 2711, 0),
(390, 1, 83, '5', 'server_url', 'Sab Kuch Bhula Diya - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319032/aje1fp8zjigz6dvf4qtc.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/xcfAo_sab-kuch-bhula-diya-tv-serial-sad-hindi-fullscreen-female-version.jpg?h=270&drp=1', NULL, 190, 825, 55, 0, 1, 1, 2648, 0),
(391, 1, 83, '5', 'server_url', 'Tum Hi Ho - Tv Serial', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402497/e8fco1xqe2ckode2dpn9.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/wJuAW_Tum-Hi-Ho-tv-serial-rainy-hindi-love.jpg?h=270&drp=1', NULL, 149, 661, 79, 0, 1, 1, 2645, 0),
(392, 1, 83, '5', 'server_url', 'Love Dialogue', 'http://www.kids2win.com/api/videosFile/ufn3g_love-dailogs-hindi-tv-serial-square.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iPn2x_love-dailogs-hindi-tv-serial-square.jpg?h=270&drp=1', NULL, 0, 30480, 0, 0, 1, 0, 2641, 0),
(393, 1, 83, '5', 'server_url', 'Tumhe dekhen meri ankhen', 'http://www.kids2win.com/api/videosFile/9WzGy_tumhe-dekhe-meri-aankhen-love-oldsongs-hindi.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lipXd_tumhe-dekhe-meri-aankhen-love-oldsongs-hindi.jpg?h=270&drp=1', NULL, 0, 131845, 0, 0, 1, 0, 549, 0),
(394, 1, 83, '5', 'server_url', 'Radha Me Hai Shyam', 'http://www.kids2win.com/api/videosFile/87iHA_radha-me-hai-shyam-tvserial-hindi-god.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4oNE6_radha-me-hai-shyam-tvserial-hindi-god.jpg?h=270&drp=1', NULL, 0, 45655, 0, 0, 1, 0, 2545, 0),
(395, 1, 83, '5', 'server_url', 'Main Woh Chaand', 'http://www.kids2win.com/api/videosFile/14q3f_meri-duaon-mein-hai-sad-hindi-tvserial.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pF8eI_meri-duaon-mein-hai-sad-hindi-tvserial.jpg?h=270&drp=1', NULL, 0, 45945, 0, 0, 1, 0, 1833, 0),
(396, 1, 83, '5', 'server_url', 'Main Bhi Nachu Manavu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399159/nzllginoknluhbqdjnnw.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/3hGFS_main-bhi-nachu-manavu-hindi-love-tvserial.jpg?h=270&drp=1', NULL, 149, 509, 46, 0, 1, 1, 2452, 0),
(397, 1, 83, '5', 'server_url', 'Kaise Mujhe Tum Mil Gayi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314216/xagncoynrevn8nc2g8qr.mp4', '-', 'Tv Serial', 'Portrait', 'http://www.kids2win.com/api/thumbImage/GgY76_kaise-mujhe-tum-mil-gayi-love-hindi-tvserials-fullscreen.jpg?h=270&drp=1', NULL, 127, 833, 48, 0, 1, 1, 2290, 0),
(398, 1, 83, '5', 'server_url', 'Hamko Tumse Pyaar Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317897/gj0wymyarww9hx6udpxw.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317899/huexl4bbpelzgrrrfcnb.jpg', NULL, 193, 803, 81, 0, 1, 1, 2288, 0),
(399, 1, 83, '5', 'server_url', 'Bolna - Tv Serial', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401832/ps6j5103kpcu3i9501fi.mp4', '-', 'Tv Serial', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401835/miugbrgq7wqnneqmq257.jpg', NULL, 152, 573, 44, 0, 1, 1, 2285, 0),
(400, 1, 83, '5', 'server_url', 'Hum Mar Jayenge - Tv Serial', 'http://www.kids2win.com/api/videosFile/jm9yU_hum-mar-jayenge-tvserial-hindi-love-fullscreen.mp4', '-', 'Tv Serial', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lQ2mz_hum-mar-jayenge-tvserial-hindi-love-fullscreen.jpg?h=270&drp=1', NULL, 0, 36641, 0, 0, 1, 0, 2259, 0),
(401, 1, 84, '5', 'server_url', 'Dhanteras Wishes', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316984/tgb6vcf1lcx4uiyao4f5.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316986/jzsl2xaabq4jw4zkuooo.jpg', NULL, 101, 469, 29, 0, 1, 1, 8326, 0),
(402, 1, 84, '5', 'server_url', 'Karwa Chauth', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319351/cbqop7dy2pvsyfmz2cdc.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319354/hwcjju0rjzpfjy6xa88t.jpg', NULL, 126, 546, 49, 0, 1, 1, 8275, 0),
(403, 1, 84, '5', 'server_url', 'Eid Milad Un Nabi', 'http://www.kids2win.com/api/videosFile/xtza4_eid-milad-un-nabi-status-h-isl-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9zO4l_eid-milad-un-nabi-status-h-isl-fes.jpg?h=270&drp=1', NULL, 0, 6907, 0, 0, 1, 0, 8229, 0),
(404, 1, 84, '5', 'server_url', 'Wo Noor Eid E Milad', 'http://www.kids2win.com/api/videosFile/ArYPh_wo-noor-eid-e-milad-fes-isl-f-h.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/v7arW_wo-noor-eid-e-milad-fes-isl-f-h.jpg?h=270&drp=1', NULL, 0, 13837, 0, 0, 1, 0, 8228, 0),
(405, 1, 84, '5', 'server_url', 'Eid Milad Un Nabi', 'http://www.kids2win.com/api/videosFile/hV9cl_eid-milad-un-nabi-h-isl-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZO5qY_eid-milad-un-nabi-h-isl-fes.jpg?h=270&drp=1', NULL, 0, 3445, 0, 0, 1, 0, 8226, 0),
(406, 1, 84, '5', 'server_url', 'Navratri Special 2020 - Coming Soon', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308517/xvsst48ayqjc3gno2xue.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308520/hjjbfotcleq43t3jabcu.jpg', NULL, 185, 484, 23, 0, 1, 1, 8133, 0),
(407, 1, 84, '5', 'server_url', 'Navratri Special 2020', 'http://www.kids2win.com/api/videosFile/ZMlm3_navratri-special-2020-f-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9wdoZ_navratri-special-2020-f-fes.jpg?h=270&drp=1', NULL, 0, 36583, 0, 0, 1, 0, 8122, 0),
(408, 1, 84, '5', 'server_url', 'Coming Soon - Navratri Special', 'http://www.kids2win.com/api/videosFile/PAuVh_coming-soon-navratri-special-fes-h.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5IXTL_coming-soon-navratri-special-fes-h.jpg?h=270&drp=1', NULL, 0, 40214, 0, 0, 1, 0, 8121, 0),
(409, 1, 84, '5', 'server_url', 'Navratri Special', 'http://www.kids2win.com/api/videosFile/dx5QE_navratri-special-f-h-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tFSgK_navratri-special-f-h-fes.jpg?h=270&drp=1', NULL, 0, 27855, 0, 0, 1, 0, 8120, 0),
(410, 1, 84, '5', 'server_url', 'Agle Baras Aana Hai Aana Hi Hoga', 'http://www.kids2win.com/api/videosFile/nZTRl_agle-baras-aana-hai-aana-hi-hoga-ganpati-h-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/VDmt2_agle-baras-aana-hai-aana-hi-hoga-ganpati-h-fes.jpg?h=270&drp=1', NULL, 0, 22908, 0, 0, 1, 0, 7711, 0),
(411, 1, 84, '5', 'server_url', 'Bappa Wala Gana', 'http://www.kids2win.com/api/videosFile/yMuQt_bappa-wala-gana-ganp-f-h-fes-15-n.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AXJGO_bappa-wala-gana-ganp-f-h-fes-15-n.jpg?h=270&drp=1', NULL, 0, 20464, 0, 0, 1, 0, 7641, 0),
(412, 1, 84, '5', 'server_url', 'Radha Krishna Special', 'http://www.kids2win.com/api/videosFile/Tjyw9_radha-krishna-special-fes-kri-janm-h.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/o1geD_radha-krishna-special-fes-kri-janm-h.jpg?h=270&drp=1', NULL, 0, 49022, 0, 0, 1, 0, 7498, 0),
(413, 1, 84, '5', 'server_url', 'Go Go Govinda', 'http://www.kids2win.com/api/videosFile/A6jE1_go-go-govinda-h-f-janm-n-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dSNsZ_go-go-govinda-h-f-janm-n-fes.jpg?h=270&drp=1', NULL, 0, 26787, 0, 0, 1, 0, 7497, 0),
(414, 1, 84, '5', 'server_url', 'Happy Krishna Janmashtami', 'http://www.kids2win.com/api/videosFile/wQvyk_happy-krishna-janmashtami-f-fes-kri.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DLOkl_happy-krishna-janmashtami-f-fes-kri.jpg?h=270&drp=1', NULL, 0, 19260, 0, 0, 1, 0, 7495, 0),
(415, 1, 84, '5', 'server_url', 'Brother Sister Emotional', 'http://www.kids2win.com/api/videosFile/yUqZr_brother-sister-emotional-rakhi-h.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Wsh94_brother-sister-emotional-rakhi-h.jpg?h=270&drp=1', NULL, 0, 38787, 0, 0, 1, 0, 7410, 0),
(416, 1, 84, '5', 'server_url', 'Raksha Bandhan Special', 'http://www.kids2win.com/api/videosFile/Y4Wlp_raksha-bandhan-brother-sq-h-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jsWBe_raksha-bandhan-brother-sq-h-fes.jpg?h=270&drp=1', NULL, 0, 26274, 0, 0, 1, 0, 7406, 0),
(417, 1, 84, '5', 'server_url', 'Raksha Bandhan', 'http://www.kids2win.com/api/videosFile/D1PNn_raksha-bandhan-h-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UPSqD_raksha-bandhan-h-fes.jpg?h=270&drp=1', NULL, 0, 21901, 0, 0, 1, 0, 7405, 0),
(418, 1, 84, '5', 'server_url', 'Bakri Eid Mubarak', 'http://www.kids2win.com/api/videosFile/JNhbf_bakri-eid-mubarak-festival-eid-hindi-animated.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qzyIJ_bakri-eid-mubarak-festival-eid-hindi-animated.jpg?h=270&drp=1', NULL, 0, 14006, 0, 0, 1, 0, 2628, 0),
(419, 1, 84, '5', 'server_url', 'Guru Purnima Wishes', 'http://www.kids2win.com/api/videosFile/J3EjZ_guru-purnima-wishes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hcgDJ_guru-purnima-wishes.jpg?h=270&drp=1', NULL, 0, 18107, 0, 0, 1, 0, 6919, 0),
(420, 1, 84, '5', 'server_url', 'Happy Guru Purnima', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320601/wzixeb6of7p3t8uo3k0d.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320604/pp5gjfnisy5qabbqedqa.jpg', NULL, 192, 823, 36, 0, 1, 1, 6918, 0),
(421, 1, 84, '5', 'server_url', 'Eid Mubarak 2020', 'http://www.kids2win.com/api/videosFile/icsOb_eid-mubarak-2020-fes-lyrical-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pX5Y1_eid-mubarak-2020-fes-lyrical-hindi.jpg?h=270&drp=1', NULL, 0, 20425, 0, 0, 1, 0, 6081, 0),
(422, 1, 84, '5', 'server_url', 'Happy Eid Mubarak', 'http://www.kids2win.com/api/videosFile/Mmvz1_happy-eid-mubarak-ani-hindi-ramzan.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wtfXl_happy-eid-mubarak-ani-hindi-ramzan.jpg?h=270&drp=1', NULL, 0, 12865, 0, 0, 1, 0, 6080, 0),
(423, 1, 84, '5', 'server_url', 'Eid Aane Wali Hai', 'http://www.kids2win.com/api/videosFile/q0dD2_eid-aane-wali-hai-h-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qORQ1_eid-aane-wali-hai-h-fes.jpg?h=270&drp=1', NULL, 0, 34431, 0, 0, 1, 0, 6060, 0),
(424, 1, 84, '5', 'server_url', 'Ramzan Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334615/bdpka5f8ted4srjj1oql.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334617/nwmcv2h6nrwgifjke2t9.jpg', NULL, 108, 562, 98, 0, 1, 1, 5691, 0),
(425, 1, 84, '5', 'server_url', 'Ramzan Ka Mahina', 'http://www.kids2win.com/api/videosFile/m0LHq_ramzan-ka-mahina-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Qnjy7_ramzan-ka-mahina-hindi.jpg?h=270&drp=1', NULL, 0, 4867, 0, 0, 1, 0, 5688, 0),
(426, 1, 84, '5', 'server_url', 'Holi Haiiii', 'http://www.kids2win.com/api/videosFile/TecE0_holi-haiiii-hindi-holi-festivel.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DV8Yt_holi-haiiii-hindi-holi-festivel.jpg?h=270&drp=1', NULL, 0, 7434, 0, 0, 1, 0, 4932, 0),
(427, 1, 84, '5', 'server_url', 'Holi Special - Best Status Ever', 'http://www.kids2win.com/api/videosFile/GM8wQ_holi-special-best-status-ever-festivel-hindi-holi-heart-touching.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/D5NPF_holi-special-best-status-ever-festivel-hindi-holi-heart-touching.jpg?h=270&drp=1', NULL, 0, 23993, 0, 0, 1, 0, 2174, 0),
(428, 1, 84, '5', 'server_url', 'Mahashivratri Special - Mahadev', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334325/yokm9bunhirk0wfuzgdi.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/8H7Xa_mahashivratri-special-hindi-mahadev-festival.jpg?h=270&drp=1', NULL, 185, 734, 98, 0, 1, 1, 4692, 0),
(429, 1, 84, '5', 'server_url', 'Mahashivratri Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609321205/awnm9pdrv58xnyvs7h0v.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/WAJmM_mahashivratri-special-hindi-god-fullscreen-festival.jpg?h=270&drp=1', NULL, 183, 552, 70, 0, 1, 1, 4691, 0),
(430, 1, 84, '5', 'server_url', 'Shiv Jayanti', 'http://www.kids2win.com/api/videosFile/vqCMZ_shiv-jayanti-festival-shivaji-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/c8PyJ_shiv-jayanti-festival-shivaji-hindi.jpg?h=270&drp=1', NULL, 0, 8752, 0, 0, 1, 0, 4654, 0),
(431, 1, 84, '5', 'server_url', 'Shiv Jayanti Special', 'http://www.kids2win.com/api/videosFile/fXxTr_shiv-jayanti-special-fullscreen-hindi-festival.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1xP9B_shiv-jayanti-special-fullscreen-hindi-festival.jpg?h=270&drp=1', NULL, 0, 11214, 0, 0, 1, 0, 4653, 0),
(432, 1, 84, '5', 'server_url', 'Happy Makar Sankranti!', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334999/gxuvwyg0ptcekzmjhpqq.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335001/isyxvmtyb8nfjiyhnfy4.jpg', NULL, 103, 540, 37, 0, 1, 1, 4263, 0),
(433, 1, 84, '5', 'server_url', 'Happy Makar Sankranti - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335095/hzms9qecbrtgrlen7ozc.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335097/fhzbrxymqejdhbf4dpvm.jpg', NULL, 149, 546, 36, 0, 1, 1, 4262, 0),
(434, 1, 84, '5', 'server_url', 'Uttarayan Special - Makar Sankranti', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271453/lgwwapqkidmnkhsoiw4s.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271456/nbfpfyro3jcafzegbils.jpg', NULL, 117, 641, 82, 0, 1, 1, 0, 0),
(435, 1, 84, '5', 'server_url', 'Makar Sankranti - Uttarayan Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308579/olfbxa5azyukfh7rz2b6.mp4', '-', 'Festivals', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308585/s6useqo1uaxojxq0380z.jpg', NULL, 171, 606, 28, 0, 1, 1, 4211, 0),
(436, 1, 84, '5', 'server_url', 'Makar Sankranti - Festival', 'http://www.kids2win.com/api/videosFile/hOBai_makar-sankranti-hindi-festival.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NkR2i_makar-sankranti-hindi-festival.jpg?h=270&drp=1', NULL, 0, 6083, 0, 0, 1, 0, 4203, 0),
(437, 1, 84, '5', 'server_url', 'Happy diwali', 'http://www.kids2win.com/api/videosFile/A5whq_happy-diwali-2018.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8dHQf_happy-diwali-2018.jpg?h=270&drp=1', NULL, 0, 32458, 0, 0, 1, 0, 204, 0),
(438, 1, 84, '5', 'server_url', 'Mere tumhare sabke liye', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353204/rdeihruopdoqhlswln5n.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/UXtjp_mere-tumhare-sabke-liye-hindi-diwali-fullscreen.jpg?h=270&drp=1', NULL, 157, 517, 62, 0, 1, 1, 254, 0),
(439, 1, 84, '5', 'server_url', 'Mere Tumhare Sabke Liye Happy Diwali', 'http://www.kids2win.com/api/videosFile/2n5eT_mere-tumhare-sabke-liye-happy-diwali-festivel-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4NS5g_mere-tumhare-sabke-liye-happy-diwali-festivel-hindi.jpg?h=270&drp=1', NULL, 0, 13152, 0, 0, 1, 0, 3413, 0),
(440, 1, 84, '5', 'server_url', 'Happy Diwali - Festival', 'http://www.kids2win.com/api/videosFile/k0jdS_happy-diwali-hindi-festival.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fFn5w_happy-diwali-hindi-festival.jpg?h=270&drp=1', NULL, 0, 8551, 0, 0, 1, 0, 3434, 0),
(441, 1, 84, '5', 'server_url', 'Ab lag raha hai na diwali jaisa - Happy-Diwali', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314521/h0pzeagavkezfyrk1otv.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609314524/kojfxrps5ep1lc7dgowf.jpg', NULL, 121, 749, 70, 0, 1, 1, 199, 0),
(442, 1, 84, '5', 'server_url', 'Agar diwali aisi ho - Happy Diwali', 'http://www.kids2win.com/api/videosFile/HDleZ_agar-diwali-aisi-ho-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/edPnY_agar-diwali-aisi-ho-happy-diwali.jpg?h=270&drp=1', NULL, 0, 4277, 0, 0, 1, 0, 200, 0),
(443, 1, 84, '5', 'server_url', 'Mere tumhare sabke liye - Happy diwali', 'http://www.kids2win.com/api/videosFile/ySqPs_mere-tumhare-sabke-liye-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GWcSs_mere-tumhare-sabke-liye-happy-diwali.jpg?h=270&drp=1', NULL, 0, 8117, 0, 0, 1, 0, 201, 0),
(444, 1, 84, '5', 'server_url', 'Vo saj saj sajna - Happy diwali', 'http://www.kids2win.com/api/videosFile/nBA5Q_vo-saj-saj-sajna-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sJrfv_vo-saj-saj-sajna-happy-diwali.jpg?h=270&drp=1', NULL, 0, 3900, 0, 0, 1, 0, 206, 0),
(445, 1, 84, '5', 'server_url', 'Toffa aya - Happy diwali', 'http://www.kids2win.com/api/videosFile/aVrGg_toffa-aaya-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Cg9o3_toffa-aaya-happy-diwali.jpg?h=270&drp=1', NULL, 0, 1372, 0, 0, 1, 0, 208, 0),
(446, 1, 84, '5', 'server_url', 'Jam k diwali ho jaye - Happy diwali', 'http://www.kids2win.com/api/videosFile/MIDTR_jam-k-diwali-ho-jaye-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/XCvTO_jam-k-diwali-ho-jaye-happy-diwali.jpg?h=270&drp=1', NULL, 0, 2223, 0, 0, 1, 0, 209, 0),
(447, 1, 84, '5', 'server_url', 'Saman k saat sanman bhi dijiye - Happy diwali', 'http://www.kids2win.com/api/videosFile/83UNP_saman-k-saat-sanman-bhi-dijiye-happy-diwali.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wTJGV_saman-k-saat-sanman-bhi-dijiye-happy-diwali.jpg?h=270&drp=1', NULL, 0, 1980, 0, 0, 1, 0, 212, 0),
(448, 1, 84, '5', 'server_url', 'Usne babayi sabke liye happy diwali', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609398893/sbq4ei6vsg73hi4aqhzk.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/yoZlY_usne-babayi-sabke-liye-happy-diwali.jpg?h=270&drp=1', NULL, 135, 509, 64, 0, 1, 1, 237, 0),
(449, 1, 84, '5', 'server_url', 'Mere tumhare sab k liye - Happy diwali', 'http://www.kids2win.com/api/videosFile/kwSyc_mere-tumhare-sab-k-liye-happy-diwali-hindi-fullscreen.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0uJQF_mere-tumhare-sab-k-liye-happy-diwali-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 3098, 0, 0, 1, 0, 243, 0),
(450, 1, 84, '5', 'server_url', 'Mere tumhare sabke liye - Happy Diwali', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353537/gf1qcr6yzvi0ymxtylfz.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/6tZV5_mere-tumhare-sabke-liye-happy-diwali-fullscreen-himdi.jpg?h=270&drp=1', NULL, 106, 770, 74, 0, 1, 1, 244, 0),
(451, 1, 84, '5', 'server_url', 'Sab K Liye Happy Diwali', 'http://www.kids2win.com/api/videosFile/UKVzs_sab-k-liye-happy-diwali-hindi-animated.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/huqZ4_sab-k-liye-happy-diwali-hindi-animated.jpg?h=270&drp=1', NULL, 0, 3422, 0, 0, 1, 0, 3418, 0),
(452, 1, 84, '5', 'server_url', 'Sab K Liye Happy Diwali - Festival', 'http://www.kids2win.com/api/videosFile/05JbR_sab-k-liye-happy-diwali-festival-animated-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/f8KcV_sab-k-liye-happy-diwali-festival-animated-hindi.jpg?h=270&drp=1', NULL, 0, 2379, 0, 0, 1, 0, 3417, 0),
(453, 1, 84, '5', 'server_url', 'Dil Dil Se Happy Diwali', 'http://www.kids2win.com/api/videosFile/qe5Fn_dil-dil-se-happy-diwali-animated-hindi-festival.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/E4gAT_dil-dil-se-happy-diwali-animated-hindi-festival.jpg?h=270&drp=1', NULL, 0, 6673, 0, 0, 1, 0, 3404, 0),
(454, 1, 84, '5', 'server_url', 'Happy Diwali - Festival', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309750/kjnimq4hptv50zgye4zq.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309752/zfsybu1ztxyogmayezoj.jpg', NULL, 156, 710, 33, 0, 1, 1, 3401, 0),
(455, 1, 84, '5', 'server_url', 'Dussehra - Happy Vijaydashmi', 'http://www.kids2win.com/api/videosFile/OvVCs_dushehra-happy-vijaydashmi-festival-hindi-fullscreen.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DFkrR_dushehra-happy-vijaydashmi-festival-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 4291, 0, 0, 1, 0, 3215, 0),
(456, 1, 84, '5', 'server_url', 'Happy Dussehra - Vijayadashami', 'http://www.kids2win.com/api/videosFile/2Rg3a_happy-dussehra-vijayadashami-festival-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/htdEN_happy-dussehra-vijayadashami-festival-hindi.jpg?h=270&drp=1', NULL, 0, 4630, 0, 0, 1, 0, 3214, 0),
(457, 1, 84, '5', 'server_url', 'Dussehra Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401415/oehfecq3hsqii3qzdxwo.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401417/pvb6ulhkbf0yk3hyxgsw.jpg', NULL, 171, 540, 13, 0, 1, 1, 3211, 0),
(458, 1, 84, '5', 'server_url', 'Dussehra Status - Festival', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401647/emolz34tnwyiv3hswbw5.mp4', '-', 'Festivals', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401650/xizlex1dwapvkyqiyc7r.jpg', NULL, 113, 415, 20, 0, 1, 1, 3210, 0),
(459, 1, 84, '5', 'server_url', 'Choodiyan - Fullscreen', 'http://www.kids2win.com/api/videosFile/Vyfr8_choodiyan-navratri-festival-fullscreen-hindi.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DPzYA_choodiyan-navratri-festival-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 16238, 0, 0, 1, 0, 3102, 0),
(460, 1, 84, '5', 'server_url', 'Kamariya', 'http://www.kids2win.com/api/videosFile/LYDyJ_kamariya-navaratri-hindi-festival.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gjxOl_kamariya-navaratri-hindi-festival.jpg?h=270&drp=1', NULL, 0, 18454, 0, 0, 1, 0, 3096, 0),
(461, 1, 8, '5', 'server_url', 'Ye Kohli Chauka Mar Na', 'http://www.kids2win.com/api/videosFile/lqHvo_ye-kohli-chauka-mar-na-funny-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fKGwM_ye-kohli-chauka-mar-na-funny-hindi.jpg?h=270&drp=1', NULL, 0, 110532, 0, 0, 1, 0, 5502, 0),
(462, 1, 8, '5', 'server_url', 'Diya Jalav', 'http://www.kids2win.com/api/videosFile/BmIL8_diya-jalav-hindi-square-funny.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wagUZ_diya-jalav-hindi-square-funny.jpg?h=270&drp=1', NULL, 0, 52306, 0, 0, 1, 0, 5308, 0),
(463, 1, 8, '5', 'server_url', 'Bhai Kal Galti Se', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340409/uznyfvvh2pzw5e86vyaj.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/vBMCx_bhai-kal-galti-se-hindi-fullscreen-funny-social.jpg?h=270&drp=1', NULL, 179, 726, 99, 0, 1, 1, 5291, 0),
(464, 1, 8, '5', 'server_url', 'Dr K Pass Ja Raha Hu', 'http://www.kids2win.com/api/videosFile/XHgiO_dr-k-pass-ja-raha-hu-hindi-fullscreen-funny-social.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lZXmR_dr-k-pass-ja-raha-hu-hindi-fullscreen-funny-social.jpg?h=270&drp=1', NULL, 0, 41506, 0, 0, 1, 0, 5198, 0),
(465, 1, 8, '5', 'server_url', 'Papita Le Lo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309186/rnhu2zholnllnaneauvm.mp4', '-', 'Funny', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309188/qd3j6irp6ng2eny1gjtx.jpg', NULL, 182, 445, 100, 0, 1, 1, 5196, 0),
(466, 1, 8, '5', 'server_url', 'Subah Se Lekar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337060/mn0tqvaiutbggelvnde0.mp4', '-', 'Funny', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337063/vh8fi5qmqfaxuebf5jvb.jpg', NULL, 171, 867, 71, 0, 1, 1, 5182, 0),
(467, 1, 8, '5', 'server_url', 'Virus Ko Utha Le', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313699/b21tyswtblhdhyyxpllm.mp4', '-', 'Funny', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313701/yss7quhu1kw0lr102gsu.jpg', NULL, 157, 884, 96, 0, 1, 1, 5116, 0),
(468, 1, 8, '5', 'server_url', 'Chocolate Day Special - Funny', 'http://www.kids2win.com/api/videosFile/aOVgW_chocolate-day-special-funny-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2MHDP_chocolate-day-special-funny-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 32103, 0, 0, 1, 0, 1896, 0),
(469, 1, 8, '5', 'server_url', 'Funny Husband', 'http://www.kids2win.com/api/videosFile/l6RrV_funny-husband.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aJsPE_funny-husband.jpg?h=270&drp=1', NULL, 0, 28934, 0, 0, 1, 0, 4109, 0),
(470, 1, 8, '5', 'server_url', 'Filhall - Funny Version', 'http://www.kids2win.com/api/videosFile/Qh0wk_filhall-funny-version-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cTN89_filhall-funny-version-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 27607, 0, 0, 1, 0, 3655, 0),
(471, 1, 8, '5', 'server_url', 'Alsi Aurat Ka Haat - Funny', 'http://www.kids2win.com/api/videosFile/th52I_alsi-aurat-ka-haat-hindi-funny-square.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/JW4Uy_alsi-aurat-ka-haat-hindi-funny-square.jpg?h=270&drp=1', NULL, 0, 10833, 0, 0, 1, 0, 3338, 0),
(472, 1, 8, '5', 'server_url', 'Cute Movement - Hindi - Funny', 'http://www.kids2win.com/api/videosFile/N4Cbl_cute-moment-hindi-romantic-funny-dailogs.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dG8xh_cute-moment-hindi-romantic-funny-dailogs.jpg?h=270&drp=1', NULL, 0, 56442, 0, 0, 1, 0, 3075, 0),
(473, 1, 8, '5', 'server_url', 'Swachh Bharat Ka Irada - Funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315744/xsuksnzl7cgr0ds6tvpx.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/4Qzn6_swachh-bharat-ka-irada-funny-hindi-bollywood.jpg?h=270&drp=1', NULL, 101, 679, 96, 0, 1, 1, 3052, 0),
(474, 1, 8, '5', 'server_url', 'Log Mujhe Pyaar Se - Funny', 'http://www.kids2win.com/api/videosFile/kU1qi_log-mujhe-pyaar-se-hindi-funny-square.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/seI9p_log-mujhe-pyaar-se-hindi-funny-square.jpg?h=270&drp=1', NULL, 0, 9398, 0, 0, 1, 0, 3029, 0),
(475, 1, 8, '5', 'server_url', 'Traffic Challan - Funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399212/lxm3ywcjoadaag9vjfpt.mp4', '-', 'Funny', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399214/gn1xz0zdsormp7bayxqg.jpg', NULL, 148, 483, 52, 0, 1, 1, 2977, 0),
(476, 1, 8, '5', 'server_url', 'Hawayein - Funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340328/ahxibse5kf99gieoieil.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/sbOd9_hawayein-funny-hindi.jpg?h=270&drp=1', NULL, 167, 900, 96, 0, 1, 1, 2635, 0),
(477, 1, 8, '5', 'server_url', 'Love Matlab Pyaar - Funny', 'http://www.kids2win.com/api/videosFile/oG72l_love-matlab-pyaar-hindi-funny.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/le5fK_love-matlab-pyaar-hindi-funny.jpg?h=270&drp=1', NULL, 0, 15034, 0, 0, 1, 0, 2511, 0),
(478, 1, 8, '5', 'server_url', 'Idhar Chali Main Udhar Chali', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315991/ym2cyaqxjrachkqnpv0a.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/h2OC4_idhar-chala-main-udhar-chali-funny-hindi-fullscreen-rainy.jpg?h=270&drp=1', NULL, 168, 436, 11, 0, 1, 1, 2431, 0),
(479, 1, 8, '5', 'server_url', 'Varun Dhawan And Alia Bhatt - Funny', 'http://www.kids2win.com/api/videosFile/vOSnL_varun-dhawan-and-alia-bhatt-funny-fullscreen-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tn5Te_varun-dhawan-and-alia-bhatt-funny-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 11650, 0, 0, 1, 0, 2309, 0),
(480, 1, 8, '5', 'server_url', 'Bahut Ho Gaya Mujhe Ab - Funny', 'http://www.kids2win.com/api/videosFile/JlG85_bahut-ho-gaya-mujhe-ab-tere-saat-nahi-rehna-funny-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/51vjB_bahut-ho-gaya-mujhe-ab-tere-saat-nahi-rehna-funny-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 12800, 0, 0, 1, 0, 2221, 0),
(481, 1, 8, '5', 'server_url', 'Funny Cricket Match Seen', 'http://www.kids2win.com/api/videosFile/mCoOa_funny-cricket-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HQ7vA_funny-cricket-hindi.jpg?h=270&drp=1', NULL, 0, 7272, 0, 0, 1, 0, 1832, 0),
(482, 1, 8, '5', 'server_url', '3 idiots funny', 'http://www.kids2win.com/api/videosFile/cofEg_3-idiots-funny-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sjrEU_3-idiots-funny-hindi.jpg?h=270&drp=1', NULL, 0, 28646, 1, 0, 1, 0, 512, 0),
(483, 1, 85, '5', 'server_url', 'Ye Kohli Chauka Mar Na', 'http://www.kids2win.com/api/videosFile/lqHvo_ye-kohli-chauka-mar-na-funny-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fKGwM_ye-kohli-chauka-mar-na-funny-hindi.jpg?h=270&drp=1', NULL, 0, 110532, 0, 0, 1, 0, 5502, 0),
(484, 1, 85, '5', 'server_url', 'Diya Jalav', 'http://www.kids2win.com/api/videosFile/BmIL8_diya-jalav-hindi-square-funny.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wagUZ_diya-jalav-hindi-square-funny.jpg?h=270&drp=1', NULL, 0, 52306, 0, 0, 1, 0, 5308, 0),
(485, 1, 85, '5', 'server_url', 'Bhai Kal Galti Se', 'http://www.kids2win.com/api/videosFile/UqQ2F_bhai-kal-galti-se-hindi-fullscreen-funny-social.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vBMCx_bhai-kal-galti-se-hindi-fullscreen-funny-social.jpg?h=270&drp=1', NULL, 0, 61034, 0, 0, 1, 0, 5291, 0),
(486, 1, 85, '5', 'server_url', 'Dr K Pass Ja Raha Hu', 'http://www.kids2win.com/api/videosFile/XHgiO_dr-k-pass-ja-raha-hu-hindi-fullscreen-funny-social.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lZXmR_dr-k-pass-ja-raha-hu-hindi-fullscreen-funny-social.jpg?h=270&drp=1', NULL, 0, 41506, 0, 0, 1, 0, 5198, 0),
(487, 1, 85, '5', 'server_url', 'Papita Le Lo', 'http://www.kids2win.com/api/videosFile/D7MbC_papita-le-lo-fullscreen-kiss-funny-social-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tWUjI_papita-le-lo-fullscreen-kiss-funny-social-hindi.jpg?h=270&drp=1', NULL, 0, 44278, 0, 0, 1, 0, 5196, 0),
(488, 1, 85, '5', 'server_url', 'Subah Se Lekar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335892/sovmvzncs0dz2jrfzcxr.mp4', '-', 'Funny', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335895/ibdcw6hz0r0ofr8jtaw1.jpg', NULL, 195, 855, 66, 0, 1, 1, 5182, 0),
(489, 1, 85, '5', 'server_url', 'Virus Ko Utha Le', 'http://www.kids2win.com/api/videosFile/TBdqu_virus-ko-utha-le-funny-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/g0m6X_virus-ko-utha-le-funny-hindi.jpg?h=270&drp=1', NULL, 0, 38396, 0, 0, 1, 0, 5116, 0),
(490, 1, 85, '5', 'server_url', 'Chocolate Day Special - Funny', 'http://www.kids2win.com/api/videosFile/aOVgW_chocolate-day-special-funny-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2MHDP_chocolate-day-special-funny-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 32103, 0, 0, 1, 0, 1896, 0),
(491, 1, 85, '5', 'server_url', 'Funny Husband', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335537/yteuxszoisu5deukj8xv.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/aJsPE_funny-husband.jpg?h=270&drp=1', NULL, 123, 630, 89, 0, 1, 1, 4109, 0),
(492, 1, 85, '5', 'server_url', 'Filhall - Funny Version', 'http://www.kids2win.com/api/videosFile/Qh0wk_filhall-funny-version-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cTN89_filhall-funny-version-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 27607, 0, 0, 1, 0, 3655, 0),
(493, 1, 85, '5', 'server_url', 'Alsi Aurat Ka Haat - Funny', 'http://www.kids2win.com/api/videosFile/th52I_alsi-aurat-ka-haat-hindi-funny-square.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/JW4Uy_alsi-aurat-ka-haat-hindi-funny-square.jpg?h=270&drp=1', NULL, 0, 10833, 0, 0, 1, 0, 3338, 0),
(494, 1, 85, '5', 'server_url', 'Cute Movement - Hindi - Funny', 'http://www.kids2win.com/api/videosFile/N4Cbl_cute-moment-hindi-romantic-funny-dailogs.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dG8xh_cute-moment-hindi-romantic-funny-dailogs.jpg?h=270&drp=1', NULL, 0, 56442, 0, 0, 1, 0, 3075, 0),
(495, 1, 85, '5', 'server_url', 'Swachh Bharat Ka Irada - Funny', 'http://www.kids2win.com/api/videosFile/H8AwD_swachh-bharat-ka-irada-funny-hindi-bollywood.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4Qzn6_swachh-bharat-ka-irada-funny-hindi-bollywood.jpg?h=270&drp=1', NULL, 0, 26431, 0, 0, 1, 0, 3052, 0),
(496, 1, 85, '5', 'server_url', 'Log Mujhe Pyaar Se - Funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609341230/jr3wmex0w3igtsfnzmzx.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/seI9p_log-mujhe-pyaar-se-hindi-funny-square.jpg?h=270&drp=1', NULL, 105, 562, 90, 0, 1, 1, 3029, 0),
(497, 1, 85, '5', 'server_url', 'Traffic Challan - Funny', 'http://www.kids2win.com/api/videosFile/3Gqm6_traffic-challan-funny-hindi-square.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/q5rcm_traffic-challan-funny-hindi-square.jpg?h=270&drp=1', NULL, 0, 14871, 0, 0, 1, 0, 2977, 0),
(498, 1, 85, '5', 'server_url', 'Hawayein - Funny', 'http://www.kids2win.com/api/videosFile/clLMC_hawayein-funny-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sbOd9_hawayein-funny-hindi.jpg?h=270&drp=1', NULL, 0, 14688, 0, 0, 1, 0, 2635, 0),
(499, 1, 85, '5', 'server_url', 'Love Matlab Pyaar - Funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311851/db9qthhqfisnzvhgbsuk.mp4', '-', 'Funny', 'Portrait', 'http://www.kids2win.com/api/thumbImage/le5fK_love-matlab-pyaar-hindi-funny.jpg?h=270&drp=1', NULL, 105, 492, 86, 0, 1, 1, 2511, 0),
(500, 1, 85, '5', 'server_url', 'Idhar Chali Main Udhar Chali', 'http://www.kids2win.com/api/videosFile/A0jTF_idhar-chala-main-udhar-chali-funny-hindi-fullscreen-rainy.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/h2OC4_idhar-chala-main-udhar-chali-funny-hindi-fullscreen-rainy.jpg?h=270&drp=1', NULL, 0, 17776, 0, 0, 1, 0, 2431, 0),
(501, 1, 85, '5', 'server_url', 'Varun Dhawan And Alia Bhatt - Funny', 'http://www.kids2win.com/api/videosFile/vOSnL_varun-dhawan-and-alia-bhatt-funny-fullscreen-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tn5Te_varun-dhawan-and-alia-bhatt-funny-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 11650, 0, 0, 1, 0, 2309, 0),
(502, 1, 85, '5', 'server_url', 'Bahut Ho Gaya Mujhe Ab - Funny', 'http://www.kids2win.com/api/videosFile/JlG85_bahut-ho-gaya-mujhe-ab-tere-saat-nahi-rehna-funny-hindi-fullscreen.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/51vjB_bahut-ho-gaya-mujhe-ab-tere-saat-nahi-rehna-funny-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 12800, 0, 0, 1, 0, 2221, 0),
(503, 1, 85, '5', 'server_url', 'Funny Cricket Match Seen', 'http://www.kids2win.com/api/videosFile/mCoOa_funny-cricket-hindi.mp4', '-', 'Funny', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HQ7vA_funny-cricket-hindi.jpg?h=270&drp=1', NULL, 0, 7272, 0, 0, 1, 0, 1832, 0),
(504, 1, 85, '5', 'server_url', '3 idiots funny', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609272479/hgwbs59yx4dpey5kxo4b.mp4', '-', 'Funny', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609272482/krhzvf6hewlwmnaur8uw.jpg', NULL, 100, 605, 97, 0, 1, 1, 512, 0),
(505, 1, 86, '5', 'server_url', 'Sai Baba Guruvar Special', 'http://www.kids2win.com/api/videosFile/sURQb_sai-baba-guruvar-special-h.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FtpvE_sai-baba-guruvar-special-h.jpg?h=270&drp=1', NULL, 0, 18767, 0, 0, 1, 0, 7696, 0),
(506, 1, 86, '5', 'server_url', 'Shravan Special', 'http://www.kids2win.com/api/videosFile/9iIAf_shravan-special-hindi-mahadev-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sLFdH_shravan-special-hindi-mahadev-god.jpg?h=270&drp=1', NULL, 0, 102973, 0, 0, 1, 0, 2606, 0),
(507, 1, 86, '5', 'server_url', 'Mahashivratri Special', 'http://www.kids2win.com/api/videosFile/1uiLz_mahashivratri-special-hindi-god-fullscreen-festival.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WAJmM_mahashivratri-special-hindi-god-fullscreen-festival.jpg?h=270&drp=1', NULL, 0, 79904, 0, 0, 1, 0, 4691, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(508, 1, 86, '5', 'server_url', 'Sai Baba Status', 'http://www.kids2win.com/api/videosFile/V8kv9_sai-baba-status-hindi-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tI8TR_sai-baba-status-hindi-god.jpg?h=270&drp=1', NULL, 0, 20894, 0, 0, 1, 0, 4407, 0),
(509, 1, 86, '5', 'server_url', 'Mahadev Status', 'http://www.kids2win.com/api/videosFile/c3vlb_mahadev-status-god-hindi-mahadev.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UgTNl_mahadev-status-god-hindi-mahadev.jpg?h=270&drp=1', NULL, 0, 64302, 0, 0, 1, 0, 3353, 0),
(510, 1, 86, '5', 'server_url', 'Krishna Seekh - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311823/rvohns3exoti4giqmusz.mp4', '-', 'God', 'Portrait', 'http://www.kids2win.com/api/thumbImage/pwugG_krishna-seekh-god-fullscreen-hindi.jpg?h=270&drp=1', NULL, 1, 757, 14, 0, 1, 1, 3073, 0),
(511, 1, 86, '5', 'server_url', 'Mahadev Status', 'http://www.kids2win.com/api/videosFile/yS7mL_mahadev-status-hindi-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/THzSO_mahadev-status-hindi-god.jpg?h=270&drp=1', NULL, 0, 35776, 0, 0, 1, 0, 3071, 0),
(512, 1, 86, '5', 'server_url', 'Morya - Fullscreen', 'http://www.kids2win.com/api/videosFile/FBDAf_morya-ganpati-fullscreen-hindi-festival.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/y6nes_morya-ganpati-fullscreen-hindi-festival.jpg?h=270&drp=1', NULL, 0, 34165, 0, 0, 1, 0, 2886, 0),
(513, 1, 86, '5', 'server_url', 'Shiv Shiv Mahadev', 'http://www.kids2win.com/api/videosFile/fKFqv_shiv-shiv-mahadev-god-hindi.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/il15o_shiv-shiv-mahadev-god-hindi.jpg?h=270&drp=1', NULL, 0, 49114, 0, 0, 1, 0, 2600, 0),
(514, 1, 86, '5', 'server_url', 'Somnath Mahadev', 'http://www.kids2win.com/api/videosFile/SH3M5_somnath-mahadev-hindi-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/foPJr_somnath-mahadev-hindi-god.jpg?h=270&drp=1', NULL, 0, 59631, 0, 0, 1, 0, 2599, 0),
(515, 1, 86, '5', 'server_url', 'Radha Me Hai Shyam', 'http://www.kids2win.com/api/videosFile/87iHA_radha-me-hai-shyam-tvserial-hindi-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4oNE6_radha-me-hai-shyam-tvserial-hindi-god.jpg?h=270&drp=1', NULL, 0, 45655, 0, 0, 1, 0, 2545, 0),
(516, 1, 86, '5', 'server_url', 'Guru Purnima - Fullscreen', 'http://www.kids2win.com/api/videosFile/ICFnA_guru-purnima-fullscreen-status-hindi-god.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/k0FwK_guru-purnima-fullscreen-status-hindi-god.jpg?h=270&drp=1', NULL, 0, 12216, 0, 0, 1, 0, 2524, 0),
(517, 1, 86, '5', 'server_url', 'Mere Sai - Guru Purnima Special', 'http://www.kids2win.com/api/videosFile/EQsmb_mere-sai-guru-purnima-special-hindi-god-lyrical.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/XZ4NK_mere-sai-guru-purnima-special-hindi-god-lyrical.jpg?h=270&drp=1', NULL, 0, 16586, 0, 0, 1, 0, 2523, 0),
(518, 1, 86, '5', 'server_url', 'Gurupurnima Special Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310734/tpxfnyyy5vf9kthujg1f.mp4', '-', 'God', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310737/m0fghddwrz9kazkeitjs.jpg', NULL, 155, 571, 77, 0, 1, 1, 2522, 0),
(519, 1, 86, '5', 'server_url', 'Guru Purnima 2019 Status', 'http://www.kids2win.com/api/videosFile/QGr2e_guru-purnima-2019-status-hindi.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yKzel_guru-purnima-2019-status-hindi.jpg?h=270&drp=1', NULL, 0, 5990, 0, 0, 1, 0, 2521, 0),
(520, 1, 86, '5', 'server_url', 'Mera Bhola Hai - Mahadev', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317289/o4uxphh4e04wxh6rzvmo.mp4', '-', 'God', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317291/ris3vbcck1uyvsf6sppf.jpg', NULL, 136, 507, 73, 0, 1, 1, 2506, 0),
(521, 1, 86, '5', 'server_url', 'Jai shree ram', 'http://www.kids2win.com/api/videosFile/eOzpF_jai-shree-ram-god-hindi.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/H6JrG_jai-shree-ram-god-hindi.jpg?h=270&drp=1', NULL, 0, 27182, 0, 0, 1, 0, 469, 0),
(522, 1, 86, '5', 'server_url', 'Shree Ganeshaya Dheemahi - Fullscreen', 'http://www.kids2win.com/api/videosFile/vHaJO_shree-ganeshaya-dheemahi-ganpati-hindi-fullscreen.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aXzxL_shree-ganeshaya-dheemahi-ganpati-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 15022, 0, 0, 1, 0, 2449, 0),
(523, 1, 86, '5', 'server_url', 'O aaye tere bhawan', 'http://www.kids2win.com/api/videosFile/q50sm_o-aaye-tere-bhawan-fullscreen-god-hindi.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vgIDz_o-aaye-tere-bhawan-fullscreen-god-hindi.jpg?h=270&drp=1', NULL, 0, 19990, 0, 0, 1, 0, 1158, 0),
(524, 1, 86, '5', 'server_url', 'Om Namaha Shivaya', 'http://www.kids2win.com/api/videosFile/Dufia_om-namaha-shivaya-hindi-fullscreen.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dEJOD_om-namaha-shivaya-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 91396, 0, 0, 1, 0, 1760, 0),
(525, 1, 86, '5', 'server_url', 'Shri krishna bhajan', 'http://www.kids2win.com/api/videosFile/r0wlk_shri-krishna-bhajan-god-hindi.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/T9f35_shri-krishna-bhajan-god-hindi.jpg?h=270&drp=1', NULL, 0, 15339, 0, 0, 1, 0, 1274, 0),
(526, 1, 86, '5', 'server_url', 'Shree krishna seekh', 'http://www.kids2win.com/api/videosFile/SPIm8_shree-krishna-seekh-god-hindi-dailogs.mp4', '-', 'God', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2Kj4c_shree-krishna-seekh-god-hindi-dailogs.jpg?h=270&drp=1', NULL, 0, 22865, 0, 0, 1, 0, 411, 0),
(527, 1, 87, '5', 'server_url', 'Bappa Wala Gana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609398931/opt6i0fd2kzedcmojthx.mp4', '-', 'Ganesh Chaturthi', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609398933/qzowf5mby2wmtlvg1sjj.jpg', NULL, 113, 608, 71, 0, 1, 1, 8035, 0),
(528, 1, 87, '5', 'server_url', 'Agle Baras Aana Hai Aana Hi Hoga', 'http://www.kids2win.com/api/videosFile/nZTRl_agle-baras-aana-hai-aana-hi-hoga-ganpati-h-fes.mp4', '-', 'Ganesh Chaturthi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/VDmt2_agle-baras-aana-hai-aana-hi-hoga-ganpati-h-fes.jpg?h=270&drp=1', NULL, 0, 22908, 0, 0, 1, 0, 7711, 0),
(529, 1, 87, '5', 'server_url', 'Shree Ganeshaya', 'http://www.kids2win.com/api/videosFile/Kcros_shree-ganesha-h-gan.mp4', '-', 'Ganesh Chaturthi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ep8wu_shree-ganesha-h-gan.jpg?h=270&drp=1', NULL, 0, 13892, 0, 0, 1, 0, 7630, 0),
(530, 1, 87, '5', 'server_url', 'Coming Soon - Lord Ganesha', 'http://www.kids2win.com/api/videosFile/OzkrK_coming-soon-lord-ganesha-h-gan.mp4', '-', 'Ganesh Chaturthi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/I58pB_coming-soon-lord-ganesha-h-gan.jpg?h=270&drp=1', NULL, 0, 26046, 0, 0, 1, 0, 7574, 0),
(531, 1, 87, '5', 'server_url', 'Morya - Fullscreen', 'http://www.kids2win.com/api/videosFile/FBDAf_morya-ganpati-fullscreen-hindi-festival.mp4', '-', 'Ganesh Chaturthi', 'Landscape', 'http://www.kids2win.com/api/thumbImage/y6nes_morya-ganpati-fullscreen-hindi-festival.jpg?h=270&drp=1', NULL, 0, 34165, 0, 0, 1, 0, 2886, 0),
(532, 1, 87, '5', 'server_url', 'Shree Ganeshaya Dheemahi - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318782/bszf5eejrvchidupshvh.mp4', '-', 'Ganesh Chaturthi', 'Portrait', 'http://www.kids2win.com/api/thumbImage/aXzxL_shree-ganeshaya-dheemahi-ganpati-hindi-fullscreen.jpg?h=270&drp=1', NULL, 108, 541, 55, 0, 1, 1, 2449, 0),
(533, 1, 88, '5', 'server_url', 'Krishna Inspiration Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352038/kijwgumyja0krkyezcps.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352040/rwuqfnepkxg2iadjs8f3.jpg', NULL, 125, 765, 30, 0, 1, 1, 8651, 0),
(534, 1, 88, '5', 'server_url', 'Manya Surve - Dialogue', 'http://www.kids2win.com/api/videosFile/TcySk_manya-surve-dialogue-h-dai-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/LtOV7_manya-surve-dialogue-h-dai-15.jpg?h=270&drp=1', NULL, 0, 27306, 0, 0, 1, 0, 8580, 0),
(535, 1, 88, '5', 'server_url', 'Jay Dwarkadhish', 'http://www.kids2win.com/api/videosFile/PqW42_jay-dwarkadhish-h-f-dai-fri-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/kXo9h_jay-dwarkadhish-h-f-dai-fri-15.jpg?h=270&drp=1', NULL, 0, 46778, 0, 0, 1, 0, 8456, 0),
(536, 1, 88, '5', 'server_url', 'Jab Jeb Me Money Ho Na', 'http://www.kids2win.com/api/videosFile/eOtPQ_jab-jeb-money-ho-na-h-sq-dai-atti.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/orihP_jab-jeb-money-ho-na-h-sq-dai-atti.jpg?h=270&drp=1', NULL, 0, 39898, 0, 0, 1, 0, 8388, 0),
(537, 1, 88, '5', 'server_url', 'Girl Respect', 'http://www.kids2win.com/api/videosFile/0ohks_girl-respect-h-sq-dai.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1mUui_girl-respect-h-sq-dai.jpg?h=270&drp=1', NULL, 0, 70567, 0, 0, 1, 0, 8212, 0),
(538, 1, 88, '5', 'server_url', 'Kabhi Kisi K Liye Kuch Kiya Hai', 'http://www.kids2win.com/api/videosFile/3NKcJ_kabhi-kisi-k-liye-kuch-kiya-hai-l-h-dai.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hRZLg_kabhi-kisi-k-liye-kuch-kiya-hai-l-h-dai.jpg?h=270&drp=1', NULL, 0, 103937, 0, 0, 1, 0, 7962, 0),
(539, 1, 88, '5', 'server_url', 'Motivation Status', 'http://www.kids2win.com/api/videosFile/ubs04_motivation-status-h-d-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jvJ0M_motivation-status-h-d-15.jpg?h=270&drp=1', NULL, 0, 75501, 0, 0, 1, 0, 7937, 0),
(540, 1, 88, '5', 'server_url', 'Ek-hi-zindagi-mili-thi-s-h-f-dai-15', 'http://www.kids2win.com/api/videosFile/tPSLx_ek-hi-zindagi-mili-thi-s-h-f-dai-.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vpQFu_ek-hi-zindagi-mili-thi-s-h-f-dai-.jpg?h=270&drp=1', NULL, 0, 82731, 0, 0, 1, 0, 7933, 0),
(541, 1, 88, '5', 'server_url', 'Kitna Pyar Hai', 'http://www.kids2win.com/api/videosFile/xhbNf_kitna-pyar-hai-l-h-shy-sq-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/i5o0q_kitna-pyar-hai-l-h-shy-sq-15.jpg?h=270&drp=1', NULL, 0, 82461, 0, 0, 1, 0, 7685, 0),
(542, 1, 88, '5', 'server_url', 'Sorry - Mujse Pyar Nai Hoga', 'http://www.kids2win.com/api/videosFile/FNKub_sorry-mujse-pyar-nai-hoga-s-h-f-15-dail.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dwvKV_sorry-mujse-pyar-nai-hoga-s-h-f-15-dail.jpg?h=270&drp=1', NULL, 0, 68698, 0, 0, 1, 0, 7507, 0),
(543, 1, 88, '5', 'server_url', 'Dil Bechara', 'http://www.kids2win.com/api/videosFile/8vMQP_dil-bechara-l-h.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BOtJg_dil-bechara-l-h.jpg?h=270&drp=1', NULL, 0, 67238, 0, 0, 1, 0, 7106, 0),
(544, 1, 88, '5', 'server_url', 'Sawan Me - Dialogue', 'http://www.kids2win.com/api/videosFile/Xb7qh_sawan-me-d-h-f-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/r7U5y_sawan-me-d-h-f-15.jpg?h=270&drp=1', NULL, 0, 98129, 0, 0, 1, 0, 6707, 0),
(545, 1, 88, '5', 'server_url', 'Carryminati Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609312412/npszxq9jpwtn1vf4op42.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609312414/skq2jrkextwcqn196r2b.jpg', NULL, 103, 660, 50, 0, 1, 1, 6650, 0),
(546, 1, 88, '5', 'server_url', 'Kya Kaha', 'http://www.kids2win.com/api/videosFile/elwAk_kya-kaha-shy-h-s.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6v9FT_kya-kaha-shy-h-s.jpg?h=270&drp=1', NULL, 0, 66341, 0, 0, 1, 0, 6499, 0),
(547, 1, 88, '5', 'server_url', 'Bade Logo Main', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337604/p4zgv7ni7oxhylty1ltx.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337606/kwc1yxwgktp1i8kynltt.jpg', NULL, 187, 606, 48, 0, 1, 1, 6493, 0),
(548, 1, 88, '5', 'server_url', 'Mere Samne - Attitude', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319731/ntx3vywpq3bmznu8uqpn.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319733/llio6jtz6vzjiunzjhd9.jpg', NULL, 190, 489, 42, 0, 1, 1, 6428, 0),
(549, 1, 88, '5', 'server_url', 'Best Life Motivational Dialogue', 'http://www.kids2win.com/api/videosFile/Ub23f_best-life-motivational-dialogue-sq-h.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OjAVx_best-life-motivational-dialogue-sq-h.jpg?h=270&drp=1', NULL, 0, 65339, 0, 0, 1, 0, 6422, 0),
(550, 1, 88, '5', 'server_url', 'Tum Wapas Mat Aana', 'http://www.kids2win.com/api/videosFile/bR3QY_tum-wapas-mat-aana-b-h.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7cnkP_tum-wapas-mat-aana-b-h.jpg?h=270&drp=1', NULL, 0, 380578, 0, 0, 1, 0, 6347, 0),
(551, 1, 88, '5', 'server_url', 'Meri Aashiqui', 'http://www.kids2win.com/api/videosFile/nqGgd_meri-aashiqui-l-h-ani-dialogue.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8CnWD_meri-aashiqui-l-h-ani-dialogue.jpg?h=270&drp=1', NULL, 0, 31533, 0, 0, 1, 0, 6320, 0),
(552, 1, 88, '5', 'server_url', 'Sanjay Dutt Attitude Dialogue', 'http://www.kids2win.com/api/videosFile/cvDuS_sanjay-dutt-attitude-f-dialogue-h-15.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SKj3a_sanjay-dutt-attitude-f-dialogue-h-15.jpg?h=270&drp=1', NULL, 0, 57278, 0, 0, 1, 0, 6247, 0),
(553, 1, 88, '5', 'server_url', 'Happy Mothers Day - Dialogue', 'http://www.kids2win.com/api/videosFile/xEo3d_happy-mothers-day-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nEflI_happy-mothers-day-hindi.jpg?h=270&drp=1', NULL, 0, 26279, 0, 0, 1, 0, 5885, 0),
(554, 1, 88, '5', 'server_url', 'Happy Mothers Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318434/cdrf3ohobt363wx7qtpm.mp4', '-', 'Dialogue', 'Portrait', 'http://www.kids2win.com/api/thumbImage/L4iaq_happy-mothers-day-hindi-fullscreen-15-sec.jpg?h=270&drp=1', NULL, 136, 567, 52, 0, 1, 1, 5875, 0),
(555, 1, 88, '5', 'server_url', 'Siddharth Malhotra Killer Dialogue', 'http://www.kids2win.com/api/videosFile/n6Ydp_siddharth-malhotra-killer-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Bkb7I_siddharth-malhotra-killer-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 69076, 0, 0, 1, 0, 5606, 0),
(556, 1, 88, '5', 'server_url', 'Sad Dialogue', 'http://www.kids2win.com/api/videosFile/t8u1Y_sad-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Z0Jhz_sad-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 58211, 0, 0, 1, 0, 5524, 0),
(557, 1, 88, '5', 'server_url', 'Sanju Baba Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401307/xqwykteroyabfrdhq2ih.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401309/qzcvaiyduowbndt3wlvb.jpg', NULL, 162, 797, 88, 0, 1, 1, 5379, 0),
(558, 1, 88, '5', 'server_url', 'Sanam Teri Kasam Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310301/gdg7ydhmmthevhihwkj3.mp4', '-', 'Dialogue', 'Portrait', 'http://www.kids2win.com/api/thumbImage/WAJ9Q_sanam-teri-kasam-dialogue-love-hindi.jpg?h=270&drp=1', NULL, 154, 492, 84, 0, 1, 1, 5186, 0),
(559, 1, 88, '5', 'server_url', 'Holi Status - Tv Serials', 'http://www.kids2win.com/api/videosFile/8OJTh_holi-status-tv-serials-dailogs-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YGN3k_holi-status-tv-serials-dailogs-hindi.jpg?h=270&drp=1', NULL, 0, 59581, 0, 0, 1, 0, 2165, 0),
(560, 1, 88, '5', 'server_url', 'Radha Krishna - Holi Special', 'http://www.kids2win.com/api/videosFile/P8qrt_radha-krishna-holi-special-status-tvserial-hindi-dailogs.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4l8yk_radha-krishna-holi-special-status-tvserial-hindi-dailogs.jpg?h=270&drp=1', NULL, 0, 107385, 0, 0, 1, 0, 2170, 0),
(561, 1, 88, '5', 'server_url', 'Love Status', 'http://www.kids2win.com/api/videosFile/t1l5m_love-status-hindi-dialogue.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HK70J_love-status-hindi-dialogue.jpg?h=270&drp=1', NULL, 0, 55223, 0, 0, 1, 0, 4710, 0),
(562, 1, 88, '5', 'server_url', 'Bulati Hai Magar Jaane Ka Nahi', 'http://www.kids2win.com/api/videosFile/RPSwW_bulati-hai-magar-jaane-ka-nahi-hindi-animated-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GkEQd_bulati-hai-magar-jaane-ka-nahi-hindi-animated-hindi.jpg?h=270&drp=1', NULL, 0, 57885, 0, 0, 1, 0, 4675, 0),
(563, 1, 88, '5', 'server_url', 'Happy Valentine\'s Day', 'http://www.kids2win.com/api/videosFile/57OsV_happy-valentines-day-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2uqB3_happy-valentines-day-hindi.jpg?h=270&drp=1', NULL, 0, 32859, 0, 0, 1, 0, 4623, 0),
(564, 1, 88, '5', 'server_url', 'Chocolate Me Mithas Rahe Na Rahe', 'http://www.kids2win.com/api/videosFile/aJoSm_chocolate-me-mithas-rahe-na-rahe-chocolate-day-dailogs-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3F9qm_chocolate-me-mithas-rahe-na-rahe-chocolate-day-dailogs-hindi.jpg?h=270&drp=1', NULL, 0, 16295, 0, 0, 1, 0, 1891, 0),
(565, 1, 88, '5', 'server_url', 'Ranveer Singh Love Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403349/rqqwiaaziv3obtgdduf8.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403351/vxkiwct87qu29fiaumab.jpg', NULL, 196, 762, 59, 0, 1, 1, 4544, 0),
(566, 1, 88, '5', 'server_url', 'Aashiqui 2 Dialogue', 'http://www.kids2win.com/api/videosFile/Y7vy4_aashiqui-2-dialogue-love-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hPAQj_aashiqui-2-dialogue-love-hindi.jpg?h=270&drp=1', NULL, 0, 60458, 0, 0, 1, 0, 4293, 0),
(567, 1, 88, '5', 'server_url', 'Killer Attitude Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308559/r8rm2mror5hqubsbbcpr.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308561/z9d4w4wyj3bnspjsvfjj.jpg', NULL, 194, 467, 36, 0, 1, 1, 4236, 0),
(568, 1, 88, '5', 'server_url', 'Aamir Khan Love Dialogue', 'http://www.kids2win.com/api/videosFile/tokR5_aamir-khan-love-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KLdlA_aamir-khan-love-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 31126, 0, 0, 1, 0, 4139, 0),
(569, 1, 88, '5', 'server_url', 'Motivation Dialogue Status', 'http://www.kids2win.com/api/videosFile/n5uGT_motivation-dialogue-status-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sSEf3_motivation-dialogue-status-hindi.jpg?h=270&drp=1', NULL, 0, 46795, 0, 0, 1, 0, 4107, 0),
(570, 1, 88, '5', 'server_url', 'Maari2 Attitude Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309397/vthehqcnkvis7jkrh0zv.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309400/haoepu8yu7o8k3xpnrfd.jpg', NULL, 120, 650, 26, 0, 1, 1, 4008, 0),
(571, 1, 88, '5', 'server_url', 'Love Dialogue Status', 'http://www.kids2win.com/api/videosFile/w1EUj_love-dialogue-status-hindi-animated.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3nXTD_love-dialogue-status-hindi-animated.jpg?h=270&drp=1', NULL, 0, 28308, 0, 0, 1, 0, 3928, 0),
(572, 1, 88, '5', 'server_url', 'Humko Mita Sake - Attitude', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340076/yujrcjhxp2xkq4gp0ix6.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609340079/nlpxaoggmpcvixot2dky.jpg', NULL, 153, 615, 86, 0, 1, 1, 3855, 0),
(573, 1, 88, '5', 'server_url', 'Love Dialogue Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402122/kvqyheq8qoiqyxgyltzd.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609402124/vd1cyqup0twjvlksjfcu.jpg', NULL, 103, 589, 81, 0, 1, 1, 3729, 0),
(574, 1, 88, '5', 'server_url', 'Love Dialogue', 'http://www.kids2win.com/api/videosFile/ZA3U9_love-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qAazi_love-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 22870, 0, 0, 1, 0, 3727, 0),
(575, 1, 88, '5', 'server_url', 'Tanhaji - Ajay Devgan Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320204/pwrxjfcctldendz0gon2.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320206/h9jnr4n086axsoxro31g.jpg', NULL, 181, 573, 93, 0, 1, 1, 3725, 0),
(576, 1, 88, '5', 'server_url', 'Motivation Dialogue Status', 'http://www.kids2win.com/api/videosFile/H1uo8_motivation-dialogue-status-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/f8FtS_motivation-dialogue-status-hindi.jpg?h=270&drp=1', NULL, 0, 44153, 0, 0, 1, 0, 3698, 0),
(577, 1, 88, '5', 'server_url', 'Love Dialogue', 'http://www.kids2win.com/api/videosFile/uB3Wn_love-dialogue-hindi-love.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/37cuy_love-dialogue-hindi-love.jpg?h=270&drp=1', NULL, 0, 18832, 0, 0, 1, 0, 3694, 0),
(578, 1, 88, '5', 'server_url', 'Best Motivational Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334662/ik0t2c4x4ffirwowexvh.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609334665/ij7luabri7bsw5nqnbk1.jpg', NULL, 147, 439, 69, 0, 1, 1, 3656, 0),
(579, 1, 88, '5', 'server_url', 'Love Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316666/ldemvkdztvzndr3c7nbq.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316669/lorjijtemk7cyalikhc8.jpg', NULL, 169, 500, 56, 0, 1, 1, 3651, 0),
(580, 1, 88, '5', 'server_url', 'Filhall - Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333416/vhseqpn185ce5v5in3b1.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609333418/hknafrlvkqhlnrgqxdvq.jpg', NULL, 174, 879, 17, 0, 1, 1, 3604, 0),
(581, 1, 88, '5', 'server_url', 'Khud Se Pyar Karna Sikh Lo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337986/dlc0aqfz4l18kbbce6ge.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337988/iimkgd4olrthlhuzqtvv.jpg', NULL, 168, 581, 22, 0, 1, 1, 3571, 0),
(582, 1, 88, '5', 'server_url', 'Shahid Kapoor Romantic Talks', 'http://www.kids2win.com/api/videosFile/bW2ju_shahid-kapoor-romantic-talks-fullscreen-hindi-dialogue.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dlUh1_shahid-kapoor-romantic-talks-fullscreen-hindi-dialogue.jpg?h=270&drp=1', NULL, 0, 21592, 0, 0, 1, 0, 3570, 0),
(583, 1, 88, '5', 'server_url', 'Salman Khan Dialogue', 'http://www.kids2win.com/api/videosFile/XUYCK_salman-khan-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BtCKc_salman-khan-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 22577, 0, 0, 1, 0, 3514, 0),
(584, 1, 88, '5', 'server_url', 'Sad Love Dialogue - Lyrical', 'http://www.kids2win.com/api/videosFile/IvQyZ_sad-love-dialogue-lyrical-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/87jet_sad-love-dialogue-lyrical-hindi.jpg?h=270&drp=1', NULL, 0, 19646, 0, 0, 1, 0, 3471, 0),
(585, 1, 88, '5', 'server_url', 'Shahrukh Khan - Dialogue', 'http://www.kids2win.com/api/videosFile/dkHv6_shahrukh-khan-dialogue-hindi-motivatinal.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/euOAg_shahrukh-khan-dialogue-hindi-motivatinal.jpg?h=270&drp=1', NULL, 0, 17993, 0, 0, 1, 0, 3460, 0),
(586, 1, 88, '5', 'server_url', 'Dhanush Love Dialogue', 'http://www.kids2win.com/api/videosFile/AZn63_dhanush-love-dialogue-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TlU8O_dhanush-love-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 25940, 0, 0, 1, 0, 3433, 0),
(587, 1, 88, '5', 'server_url', 'Mahakal - Dialogue', 'http://www.kids2win.com/api/videosFile/Mhlmc_mahakal-dialogue-mahadev-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AIb80_mahakal-dialogue-mahadev-hindi.jpg?h=270&drp=1', NULL, 0, 44894, 0, 0, 1, 0, 3245, 0),
(588, 1, 88, '5', 'server_url', 'Tere Jaane Ka Gham - Love', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319451/pwkpzknpgqlgkfxp3plz.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319453/sjp0eg1xhqz7xstggq02.jpg', NULL, 186, 697, 22, 0, 1, 1, 3238, 0),
(589, 1, 88, '5', 'server_url', 'Dussehra Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609320535/do00mmfdpdnb9qshmtmd.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609320538/jkw6u9rpumame8ncaayq.jpg', NULL, 116, 866, 89, 0, 1, 1, 3211, 0),
(590, 1, 88, '5', 'server_url', 'Love Feel Kiss', 'http://www.kids2win.com/api/videosFile/NLWB3_love-feel-kiss-romantic-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Csjgq_love-feel-kiss-romantic-hindi.jpg?h=270&drp=1', NULL, 0, 59441, 0, 0, 1, 0, 3190, 0),
(591, 1, 88, '5', 'server_url', 'Broken Heart Status - Sad', 'http://www.kids2win.com/api/videosFile/03ZrR_broken-heart-status-sad-hindi.mp4', '-', 'Dialogue', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3LRk8_broken-heart-status-sad-hindi.jpg?h=270&drp=1', NULL, 0, 125253, 0, 0, 1, 0, 3128, 0),
(592, 1, 88, '5', 'server_url', '2 October Kya Hai - Gandhi Jayanti Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338908/kew4ornyvcrqhxyzlq8u.mp4', '-', 'Dialogue', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338911/htfs0zltb75bstduvs9m.jpg', NULL, 192, 842, 52, 0, 1, 1, 3119, 0),
(593, 1, 89, '5', 'server_url', 'Ae Mere Dil', 'http://www.kids2win.com/api/videosFile/VTtcz_ae-mere-dil-s-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wP5iH_ae-mere-dil-s-h-ani.jpg?h=270&drp=1', NULL, 0, 126009, 0, 0, 1, 0, 8244, 0),
(594, 1, 89, '5', 'server_url', 'Jeene Laga Hoon', 'http://www.kids2win.com/api/videosFile/q2KAl_jeene-laga-hoon-f-h-l-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QRT0a_jeene-laga-hoon-f-h-l-ani.jpg?h=270&drp=1', NULL, 0, 54090, 0, 0, 1, 0, 8160, 0),
(595, 1, 89, '5', 'server_url', 'Aawara Shaam Hai', 'http://www.kids2win.com/api/videosFile/6l7CY_aawara-shaam-hai-ani-l-h.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nTqfN_aawara-shaam-hai-ani-l-h.jpg?h=270&drp=1', NULL, 0, 45836, 0, 0, 1, 0, 8159, 0),
(596, 1, 89, '5', 'server_url', 'I Feel Safe In Your Arms', 'http://www.kids2win.com/api/videosFile/eDuaX_i-feel-safe-in-your-arms-l-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/InO3Z_i-feel-safe-in-your-arms-l-h-ani.jpg?h=270&drp=1', NULL, 0, 71186, 0, 0, 1, 0, 8041, 0),
(597, 1, 89, '5', 'server_url', 'Dil Chahte Ho', 'http://www.kids2win.com/api/videosFile/GHDlz_dil-chahte-ho-s-h-f-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IyJ62_dil-chahte-ho-s-h-f-ani.jpg?h=270&drp=1', NULL, 0, 87461, 0, 0, 1, 0, 7934, 0),
(598, 1, 89, '5', 'server_url', 'Duniyaa', 'http://www.kids2win.com/api/videosFile/zaL2D_duniyaa-l-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AXqMW_duniyaa-l-h-ani.jpg?h=270&drp=1', NULL, 0, 47700, 0, 0, 1, 0, 7854, 0),
(599, 1, 89, '5', 'server_url', 'Hasi Ban Gaye', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310595/jztstrsc1ttksa1tmxed.mp4', '-', 'Animated', 'Portrait', 'http://www.kids2win.com/api/thumbImage/r2cvp_hasi-ban-gaye-l-h-fem-ani.jpg?h=270&drp=1', NULL, 125, 682, 54, 0, 1, 1, 7582, 0),
(600, 1, 89, '5', 'server_url', 'Bekhayali', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609334759/viwr77fsi7fjxpripos1.mp4', '-', 'Animated', 'Portrait', 'http://www.kids2win.com/api/thumbImage/eF0aI_bekhayali-ani-s-h.jpg?h=270&drp=1', NULL, 140, 433, 46, 0, 1, 1, 7476, 0),
(601, 1, 89, '5', 'server_url', 'Meri Mehbooba', 'http://www.kids2win.com/api/videosFile/dqMJ4_meri-mehbooba-s-ani-h.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UvBSC_meri-mehbooba-s-ani-h.jpg?h=270&drp=1', NULL, 0, 45210, 0, 0, 1, 0, 7407, 0),
(602, 1, 89, '5', 'server_url', 'Bakri Eid Mubarak', 'http://www.kids2win.com/api/videosFile/JNhbf_bakri-eid-mubarak-festival-eid-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qzyIJ_bakri-eid-mubarak-festival-eid-hindi-animated.jpg?h=270&drp=1', NULL, 0, 14006, 0, 0, 1, 0, 2628, 0),
(603, 1, 89, '5', 'server_url', 'Main Jahaan Rahoon', 'http://www.kids2win.com/api/videosFile/HyPFl_main-jahaan-rahoon-l-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oHryL_main-jahaan-rahoon-l-h-ani.jpg?h=270&drp=1', NULL, 0, 52641, 0, 0, 1, 0, 7191, 0),
(604, 1, 89, '5', 'server_url', 'Heer Ranjha', 'http://www.kids2win.com/api/videosFile/VfU8t_heer-ranjha-l-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EGuVO_heer-ranjha-l-h-ani.jpg?h=270&drp=1', NULL, 0, 47725, 0, 0, 1, 0, 7184, 0),
(605, 1, 89, '5', 'server_url', 'Happy Father Day Status', 'http://www.kids2win.com/api/videosFile/9lY17_happy-father-day-status-h-f-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0hCwA_happy-father-day-status-h-f-ani.jpg?h=270&drp=1', NULL, 0, 14997, 0, 0, 1, 0, 6565, 0),
(606, 1, 89, '5', 'server_url', 'Ye Musam Ki Barish', 'http://www.kids2win.com/api/videosFile/NOXvk_ye-musam-ki-barish-l-h-rainy-anim.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mDiVa_ye-musam-ki-barish-l-h-rainy-anim.jpg?h=270&drp=1', NULL, 0, 74930, 0, 0, 1, 0, 6402, 0),
(607, 1, 89, '5', 'server_url', 'Main Yahan Tu Wahan', 'http://www.kids2win.com/api/videosFile/cvu8A_main-yahan-tu-wahan-s-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FevNU_main-yahan-tu-wahan-s-h-ani.jpg?h=270&drp=1', NULL, 0, 33832, 0, 0, 1, 0, 6332, 0),
(608, 1, 89, '5', 'server_url', 'Meri Aashiqui', 'http://www.kids2win.com/api/videosFile/nqGgd_meri-aashiqui-l-h-ani-dialogue.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8CnWD_meri-aashiqui-l-h-ani-dialogue.jpg?h=270&drp=1', NULL, 0, 31533, 0, 0, 1, 0, 6320, 0),
(609, 1, 89, '5', 'server_url', 'Arziyaan', 'http://www.kids2win.com/api/videosFile/gIKCV_arziyaan-hindi-animated-love.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mQBfT_arziyaan-hindi-animated-love.jpg?h=270&drp=1', NULL, 0, 31497, 0, 0, 1, 0, 6313, 0),
(610, 1, 89, '5', 'server_url', 'Tum Hi Aana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338579/dw8yl1e9ubnc0o6scwk1.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338583/yircmvgjinnqnkvqrytg.jpg', NULL, 188, 650, 23, 0, 1, 1, 6249, 0),
(611, 1, 89, '5', 'server_url', 'Mausam Ki Barish', 'http://www.kids2win.com/api/videosFile/vGMYP_mausam-ki-barish-love-hindi-animated-rain-special.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/VYN6j_mausam-ki-barish-love-hindi-animated-rain-special.jpg?h=270&drp=1', NULL, 0, 82703, 0, 0, 1, 0, 2324, 0),
(612, 1, 89, '5', 'server_url', 'Jinke Liye', 'http://www.kids2win.com/api/videosFile/4KjqW_jinke-liye-h-s-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aQSYs_jinke-liye-h-s-ani.jpg?h=270&drp=1', NULL, 0, 31969, 0, 0, 1, 0, 6133, 0),
(613, 1, 89, '5', 'server_url', 'Mera Bhai Tu - Friendship', 'http://www.kids2win.com/api/videosFile/JyONG_mera-bhai-tu-animated-hindi-friendship-lyrical.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rXlvx_mera-bhai-tu-animated-hindi-friendship-lyrical.jpg?h=270&drp=1', NULL, 0, 53006, 0, 0, 1, 0, 2490, 0),
(614, 1, 89, '5', 'server_url', 'Broken Heart', 'http://www.kids2win.com/api/videosFile/Ul8bN_broken-heart-s-h-ani.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hG8RP_broken-heart-s-h-ani.jpg?h=270&drp=1', NULL, 0, 25491, 0, 0, 1, 0, 6052, 0),
(615, 1, 89, '5', 'server_url', 'Tere Bina', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609272489/pvuhene0ltquqbim4jg1.mp4', '-', 'Animated', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609272492/zodemdqwlehx50vehy8n.jpg', NULL, 134, 630, 39, 0, 1, 1, 6046, 0),
(616, 1, 89, '5', 'server_url', 'Malang Title Track', 'http://www.kids2win.com/api/videosFile/XyTku_malang-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/49xhs_malang-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 20349, 0, 0, 1, 0, 5788, 0),
(617, 1, 89, '5', 'server_url', 'Pyaar Karona', 'http://www.kids2win.com/api/videosFile/GCUXY_pyaar-karona-hindi-fullscreen-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tuLsH_pyaar-karona-hindi-fullscreen-animated.jpg?h=270&drp=1', NULL, 0, 29704, 0, 0, 1, 0, 5738, 0),
(618, 1, 89, '5', 'server_url', 'Kuch Is Tarah -Atif Aslam', 'http://www.kids2win.com/api/videosFile/g7FoV_kuch-is-tarah-sad-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BMcIK_kuch-is-tarah-sad-hindi.jpg?h=270&drp=1', NULL, 0, 35102, 0, 0, 1, 0, 5715, 0),
(620, 1, 89, '5', 'server_url', 'Arziyaan', 'http://www.kids2win.com/api/videosFile/S75yV_arziyaan-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UbTdy_arziyaan-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 14745, 0, 0, 1, 0, 5559, 0),
(621, 1, 89, '5', 'server_url', 'Aaj Bhi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310008/jzb4ibwmd1t9whytxuyf.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310010/a27q6g7lkejkxvm8sqm8.jpg', NULL, 113, 723, 96, 0, 1, 1, 5558, 0),
(622, 1, 89, '5', 'server_url', 'Dil Maang Raha Hai', 'http://www.kids2win.com/api/videosFile/1BZlt_dil-maang-raha-hai-love-hindi-square-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Ha6pS_dil-maang-raha-hai-love-hindi-square-animated.jpg?h=270&drp=1', NULL, 0, 37638, 0, 0, 1, 0, 5414, 0),
(623, 1, 89, '5', 'server_url', 'Nasha Yeh Pyar Ka', 'http://www.kids2win.com/api/videosFile/nCKh1_nasha-yeh-pyar-ka-love-hindi-fullscreen-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pw9uY_nasha-yeh-pyar-ka-love-hindi-fullscreen-animated.jpg?h=270&drp=1', NULL, 0, 25405, 0, 0, 1, 0, 5389, 0),
(624, 1, 89, '5', 'server_url', 'Ravan Ravan Hoon Main', 'http://www.kids2win.com/api/videosFile/qoSXi_ravan-ravan-hoon-main-hindi-attitude.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/n9XBW_ravan-ravan-hoon-main-hindi-attitude.jpg?h=270&drp=1', NULL, 0, 10859, 0, 0, 1, 0, 5383, 0),
(625, 1, 89, '5', 'server_url', 'Pal Pal Dil Ke Paas', 'http://www.kids2win.com/api/videosFile/X8sp7_pal-pal-dil-ke-paas-fullscreen-animated-love-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pDmhT_pal-pal-dil-ke-paas-fullscreen-animated-love-hindi.jpg?h=270&drp=1', NULL, 0, 15130, 0, 0, 1, 0, 5381, 0),
(626, 1, 89, '5', 'server_url', 'Main Dhoondne Ko Zamaane Mein', 'http://www.kids2win.com/api/videosFile/wtlrU_main-dhoondne-ko-zamaane-mein-sad-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/okNFz_main-dhoondne-ko-zamaane-mein-sad-hindi-animated.jpg?h=270&drp=1', NULL, 0, 27944, 0, 0, 1, 0, 5212, 0),
(627, 1, 89, '5', 'server_url', 'Ye Pyar Nahi To Kya Hai Yeh', 'http://www.kids2win.com/api/videosFile/EmlGv_ye-pyar-nahi-to-kya-hai-yeh-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dC3qJ_ye-pyar-nahi-to-kya-hai-yeh-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 20823, 0, 0, 1, 0, 5200, 0),
(628, 1, 89, '5', 'server_url', 'Sun Le Saathiya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609272484/ncivgjf43l588glj4ijz.mp4', '-', 'Animated', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609272487/xlwh7oxegncw3bsldxyu.jpg', NULL, 146, 726, 31, 0, 1, 1, 5168, 0),
(629, 1, 89, '5', 'server_url', 'Dil Laya Dimaag Laya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333768/h8qstld9q75kf3c1qpq6.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609333770/s1aih3yjlmsvon0gzpcd.jpg', NULL, 170, 779, 15, 0, 1, 1, 5164, 0),
(630, 1, 89, '5', 'server_url', 'Jaan Meri Ja Rahi Sanam - Love', 'http://www.kids2win.com/api/videosFile/Orso5_jaan-meri-ja-rahi-sanam-love-hindi-animated-female%20version.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5XIwZ_jaan-meri-ja-rahi-sanam-love-hindi-animated-female%20version.jpg?h=270&drp=1', NULL, 0, 76904, 0, 0, 1, 0, 4998, 0),
(631, 1, 89, '5', 'server_url', 'Soniyo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316923/fsekhu9mijjeqmlzgsxx.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316925/cn7zpujybtghigsouybw.jpg', NULL, 118, 896, 82, 0, 1, 1, 4952, 0),
(632, 1, 89, '5', 'server_url', 'Hamdard - Female Version', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399134/cpy5cbfahddddnm18mfk.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399136/qaai6ssg44dqtfnysa6q.jpg', NULL, 108, 663, 51, 0, 1, 1, 4904, 0),
(633, 1, 89, '5', 'server_url', 'Falak Tak Chal Sath Mere', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271447/eejn1mu6upxqxetiahh4.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271450/cxfovijbaod3nqnd2hcm.jpg', NULL, 146, 528, 76, 0, 1, 1, 0, 0),
(634, 1, 89, '5', 'server_url', 'Happy Holi Special Whatsapp Status', 'http://www.kids2win.com/api/videosFile/xErFf_happy-holi-special-whatsapp-status-animated-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1qzEK_happy-holi-special-whatsapp-status-animated-hindi.jpg?h=270&drp=1', NULL, 0, 21471, 0, 0, 1, 0, 2163, 0),
(635, 1, 89, '5', 'server_url', 'Kabhi Toh Paas Mere Aao', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340028/kow1vlby63fer11arsmj.mp4', '-', 'Animated', 'Portrait', 'http://www.kids2win.com/api/thumbImage/aFnqZ_kabhi-toh-paas-mere-aao-animated-love-hindi-sqare.jpg?h=270&drp=1', NULL, 113, 657, 61, 0, 1, 1, 4831, 0),
(636, 1, 89, '5', 'server_url', 'Kabhi Shaam Dhale', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318487/nhftqa8afkqa4pz8cfr4.mp4', '-', 'Animated', 'Portrait', 'http://www.kids2win.com/api/thumbImage/CJxtw_kabhi-shaam-dhale-love-hindi-animated.jpg?h=270&drp=1', NULL, 161, 569, 85, 0, 1, 1, 1660, 0),
(637, 1, 89, '5', 'server_url', 'Tu Mujhe Soch Kabhi', 'http://www.kids2win.com/api/videosFile/TY5sh_tu-mujhe-soch-kabhi-animated-fullscreen-hindi-love.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vuExa_tu-mujhe-soch-kabhi-animated-fullscreen-hindi-love.jpg?h=270&drp=1', NULL, 0, 13419, 0, 0, 1, 0, 4810, 0),
(638, 1, 89, '5', 'server_url', 'Zinda Hoon Yaar', 'http://www.kids2win.com/api/videosFile/GUn3b_zinda-hoon-yaar-sad-animated-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dDjIT_zinda-hoon-yaar-sad-animated-hindi.jpg?h=270&drp=1', NULL, 0, 10783, 0, 0, 1, 0, 4809, 0),
(639, 1, 89, '5', 'server_url', 'Humraah', 'http://www.kids2win.com/api/videosFile/7T6LD_humraah-animated-love-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Lr2VH_humraah-animated-love-hindi.jpg?h=270&drp=1', NULL, 0, 10782, 0, 0, 1, 0, 4806, 0),
(640, 1, 89, '5', 'server_url', 'Aankhon Mein Tera Hi Chehra', 'http://www.kids2win.com/api/videosFile/b5nKw_aankhon-mein-tera-hi-chehra-animated-sad-hindi.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cmweD_aankhon-mein-tera-hi-chehra-animated-sad-hindi.jpg?h=270&drp=1', NULL, 0, 35651, 0, 0, 1, 0, 4798, 0),
(641, 1, 89, '5', 'server_url', 'Tu jo kehde agar toh main jeena chod', 'http://www.kids2win.com/api/videosFile/9MFm3_tu-jo-kehde-agar-toh-main-jeena-chod-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OW4NH_tu-jo-kehde-agar-toh-main-jeena-chod-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 9261, 0, 0, 1, 0, 783, 0),
(642, 1, 89, '5', 'server_url', 'Phir Kabhi', 'http://www.kids2win.com/api/videosFile/nkUtC_phir-kabhi-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hqAyU_phir-kabhi-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 31497, 0, 0, 1, 0, 4789, 0),
(643, 1, 89, '5', 'server_url', 'Tu Bhi Royega', 'http://www.kids2win.com/api/videosFile/nNbr5_tu-bhi-royega-sad-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PyJMo_tu-bhi-royega-sad-hindi-animated.jpg?h=270&drp=1', NULL, 0, 13622, 0, 0, 1, 0, 4776, 0),
(644, 1, 89, '5', 'server_url', 'Kya Tujhe Ab Ye Dil Bataye', 'http://www.kids2win.com/api/videosFile/5egqa_kya-tujhe-ab-animated-love-hindi-square.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Tiyc1_kya-tujhe-ab-animated-love-hindi-square.jpg?h=270&drp=1', NULL, 0, 18712, 0, 0, 1, 0, 4753, 0),
(645, 1, 89, '5', 'server_url', 'Bulati Hai Magar Jaane Ka Nahi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353532/jbxjxpchlmv6xbqh8h1z.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609353534/lpo1hzu43tkjcyro0kqc.jpg', NULL, 127, 446, 87, 0, 1, 1, 4675, 0),
(646, 1, 89, '5', 'server_url', 'Koi Bhi Aisa Lamha Nahi Hai - Jannat', 'http://www.kids2win.com/api/videosFile/TYpua_koi-bhi-aisa-lamha-nahi-hai-jannat-love-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qIcQo_koi-bhi-aisa-lamha-nahi-hai-jannat-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 30511, 0, 0, 1, 0, 1619, 0),
(647, 1, 89, '5', 'server_url', 'Happy Valentine', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399560/vgod8tqsjizxzf6fxqwm.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399562/ana2ntf1lrlkrrbixagl.jpg', NULL, 150, 426, 79, 0, 1, 1, 4617, 0),
(648, 1, 89, '5', 'server_url', 'Valentine Day Special Shayari', 'http://www.kids2win.com/api/videosFile/DPeAH_valentine-day-shayari-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8a2y9_valentine-day-shayari-hindi-animated.jpg?h=270&drp=1', NULL, 0, 47091, 0, 0, 1, 0, 1920, 0),
(649, 1, 89, '5', 'server_url', 'Promise Day Special Animated', 'http://www.kids2win.com/api/videosFile/6XIT0_promise-day-special-animated-hindi-love.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZOk4A_promise-day-special-animated-hindi-love.jpg?h=270&drp=1', NULL, 0, 15844, 0, 0, 1, 0, 1918, 0),
(650, 1, 89, '5', 'server_url', 'Ye Vada Raha Best Promise', 'http://www.kids2win.com/api/videosFile/9KJHP_ye-vada-raha-best-promise-hindi-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/xWNBg_ye-vada-raha-best-promise-hindi-animated.jpg?h=270&drp=1', NULL, 0, 25057, 0, 0, 1, 0, 1922, 0),
(651, 1, 89, '5', 'server_url', 'Teddy Bear To You', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353520/yzuwm5zmhywjzzt9nnue.mp4', '-', 'Animated', 'Portrait', 'http://www.kids2win.com/api/thumbImage/5bZY9_teddy-bear-to-you-hindi-animated.jpg?h=270&drp=1', NULL, 175, 733, 15, 0, 1, 1, 1904, 0),
(652, 1, 89, '5', 'server_url', 'Tere Naam', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609351769/bmacdw3kqhhtmvwo9xbm.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609351771/uxdksqds9jg1349w0edm.jpg', NULL, 179, 736, 21, 0, 1, 1, 4587, 0),
(653, 1, 90, '5', 'server_url', 'Tum Hi Aana - Unplugged', 'http://www.kids2win.com/api/videosFile/75YgJ_tum-hi-aana-ly-h-unp.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Hx7PF_tum-hi-aana-ly-h-unp.jpg?h=270&drp=1', NULL, 0, 89703, 0, 0, 1, 0, 8168, 0),
(654, 1, 90, '5', 'server_url', 'Kasam Ki Kasam', 'http://www.kids2win.com/api/videosFile/f6g5s_kasam-ki-kasam-unp-l-h.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DmAJ0_kasam-ki-kasam-unp-l-h.jpg?h=270&drp=1', NULL, 0, 96035, 0, 0, 1, 0, 8038, 0),
(655, 1, 90, '5', 'server_url', 'Jab Kisi Ko - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609270131/xtau41byt30vgs6h7cr4.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609270133/xqsdko5cjaipwzlht3sh.jpg', NULL, 171, 850, 45, 0, 1, 1, 0, 0),
(656, 1, 90, '5', 'server_url', 'Dil laga liya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338876/fpn891kvcoiidq86xpoo.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338878/s7uxsp8mvscgshbjxamj.jpg', NULL, 103, 881, 19, 0, 1, 1, 7490, 0),
(657, 1, 90, '5', 'server_url', 'Ladki Nahi Hai Wo', 'http://www.kids2win.com/api/videosFile/Qb4kx_ladki-nahi-hai-wo-l-unp-h.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/q6ASJ_ladki-nahi-hai-wo-l-unp-h.jpg?h=270&drp=1', NULL, 0, 100018, 0, 0, 1, 0, 7160, 0),
(658, 1, 90, '5', 'server_url', 'Ab Hai Samne', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319054/bsey6f5ubg7zx0wazzvq.mp4', '-', 'Unplugged', 'Portrait', 'http://www.kids2win.com/api/thumbImage/Ja72P_ab-hai-samne-l-h-unp.jpg?h=270&drp=1', NULL, 101, 615, 48, 0, 1, 1, 7151, 0),
(659, 1, 90, '5', 'server_url', 'Best Love Song - Labon Ko', 'http://www.kids2win.com/api/videosFile/WQyN1_best-love-song-labon-ko-l-h-unp.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/eIYKZ_best-love-song-labon-ko-l-h-unp.jpg?h=270&drp=1', NULL, 0, 268374, 0, 0, 1, 0, 6867, 0),
(660, 1, 90, '5', 'server_url', 'Labon Ko', 'http://www.kids2win.com/api/videosFile/aVW15_labon-ko-l-ly-h-unp.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/aG8Fs_labon-ko-l-ly-h-unp.jpg?h=270&drp=1', NULL, 0, 102389, 0, 0, 1, 0, 6723, 0),
(661, 1, 90, '5', 'server_url', 'Woh Jab Kehte Thhe', 'http://www.kids2win.com/api/videosFile/BAjaU_woh-jab-kehte-thhe-sq-s-h-unp-m.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2nKgM_woh-jab-kehte-thhe-sq-s-h-unp-m.jpg?h=270&drp=1', NULL, 0, 137077, 0, 0, 1, 0, 6666, 0),
(662, 1, 90, '5', 'server_url', 'Dil Tod Ke - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313634/wqcnld2dksoqv7ydepts.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313636/iin1x2nl8ghzvkq5wbfs.jpg', NULL, 164, 403, 21, 0, 1, 1, 6465, 0),
(663, 1, 90, '5', 'server_url', 'Sansoon Mein Badi Bekarari', 'http://www.kids2win.com/api/videosFile/D5Ped_sansoon-mein-badi-bekarari-s-h-unp.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/uaH0D_sansoon-mein-badi-bekarari-s-h-unp.jpg?h=270&drp=1', NULL, 0, 44347, 0, 0, 1, 0, 6353, 0),
(664, 1, 90, '5', 'server_url', 'O Khuda - Unplugged', 'http://www.kids2win.com/api/videosFile/Sf3Xw_o-khuda-unplugged-sad-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1pHaY_o-khuda-unplugged-sad-hindi.jpg?h=270&drp=1', NULL, 0, 52625, 0, 0, 1, 0, 1767, 0),
(665, 1, 90, '5', 'server_url', 'Dil To Pagal Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/hX68p_dil-to-pagal-hai-unplugged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/96c7O_dil-to-pagal-hai-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 0, 56526, 0, 0, 1, 0, 2032, 0),
(666, 1, 90, '5', 'server_url', 'Dil To Pagal Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/IAiZS_Dil-to-Pagal-Hai-love-hindi-female.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vBcFP_Dil-to-Pagal-Hai-love-hindi-female.jpg?h=270&drp=1', NULL, 0, 179589, 0, 0, 1, 0, 5705, 0),
(667, 1, 90, '5', 'server_url', 'Aaj Din Chadheya - Unplugged', 'http://www.kids2win.com/api/videosFile/wto9N_aaj-din-chadheya-love-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dyNEB_aaj-din-chadheya-love-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 75567, 0, 0, 1, 0, 5500, 0),
(668, 1, 90, '5', 'server_url', 'Abhi Mujh Mein Kahin - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402520/mebko2mgol15wbzmrhwc.mp4', '-', 'Unplugged', 'Portrait', 'http://www.kids2win.com/api/thumbImage/nVH0r_abhi-mujh-mein-kahin-love-hindi-unplugged.jpg?h=270&drp=1', NULL, 118, 500, 25, 0, 1, 1, 5474, 0),
(669, 1, 90, '5', 'server_url', 'Tumse Milne Ko Dil - Unplugged', 'http://www.kids2win.com/api/videosFile/2cVMv_tumse-milne-ko-dil-unplugged-love-hindi-square.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5ckAo_tumse-milne-ko-dil-unplugged-love-hindi-square.jpg?h=270&drp=1', NULL, 0, 45966, 0, 0, 1, 0, 5426, 0),
(670, 1, 90, '5', 'server_url', 'Main Hoon Hero Tera', 'http://www.kids2win.com/api/videosFile/nbZdV_main-hoon-hero-tera-lyrical-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/t4CiE_main-hoon-hero-tera-lyrical-love-hindi.jpg?h=270&drp=1', NULL, 0, 40691, 0, 0, 1, 0, 5310, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(671, 1, 90, '5', 'server_url', 'Tera Ban Jaunga - Unplugged', 'http://www.kids2win.com/api/videosFile/x4Xc8_tera-ban-jaunga-romantic-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7mlAo_tera-ban-jaunga-romantic-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 91663, 0, 0, 1, 0, 5228, 0),
(672, 1, 90, '5', 'server_url', 'Phir Na Milen Kabhi - Reprise', 'http://www.kids2win.com/api/videosFile/S2jog_phir-na-milen-kabhi-reprise-sad-hindi-lyrical-square.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EwpyB_phir-na-milen-kabhi-reprise-sad-hindi-lyrical-square.jpg?h=270&drp=1', NULL, 0, 34344, 0, 0, 1, 0, 5193, 0),
(673, 1, 90, '5', 'server_url', 'Aaj Din Chadheya - Unplugged', 'http://www.kids2win.com/api/videosFile/TV6yH_aaj-din-chadheya-love-square-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZmcD2_aaj-din-chadheya-love-square-unplugged.jpg?h=270&drp=1', NULL, 0, 37264, 0, 0, 1, 0, 5082, 0),
(674, 1, 90, '5', 'server_url', 'Hum to dil se hare', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335841/cehiozlgqxcgaqtyrgbt.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335844/wh27wi02jwwc4bjfacse.jpg', NULL, 185, 756, 36, 0, 1, 1, 1312, 0),
(675, 1, 90, '5', 'server_url', 'Tu Hi Haqeeqat - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337952/rtib0termvlnwlabhwnp.mp4', '-', 'Unplugged', 'Portrait', 'http://www.kids2win.com/api/thumbImage/vZ2yP_tu-hi-haqeeqat-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 169, 513, 35, 0, 1, 1, 5004, 0),
(676, 1, 90, '5', 'server_url', 'Ye Dooriyan - Unplugged', 'http://www.kids2win.com/api/videosFile/Qsjhq_ye-dooriyan-unplugged-sad-hindi-lyrical.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QcjwX_ye-dooriyan-unplugged-sad-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 40839, 0, 0, 1, 0, 4871, 0),
(677, 1, 90, '5', 'server_url', 'Aankhon Mein Tera Hi Chehra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317052/e1py9zgwrmtdje27hpid.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317054/n2dwbupe1gzh4jakd8ee.jpg', NULL, 153, 526, 37, 0, 1, 1, 4798, 0),
(678, 1, 90, '5', 'server_url', 'Aankhein Khuli - Unplugged', 'http://www.kids2win.com/api/videosFile/lIFTD_aankhein-khuli-love-lyrical-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DB8CY_aankhein-khuli-love-lyrical-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 44962, 0, 0, 1, 0, 4796, 0),
(679, 1, 90, '5', 'server_url', 'Aaj Din Chadheya - Unplugged', 'http://www.kids2win.com/api/videosFile/0TUWt_aaj-din-chadheya-love-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/p2vHq_aaj-din-chadheya-love-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 16591, 0, 0, 1, 0, 4755, 0),
(680, 1, 90, '5', 'server_url', 'Is Tarah Aashiqui Ka - Unplugged', 'http://www.kids2win.com/api/videosFile/tDkZI_is-tarah-aashiqui-ka-unplugged-oldsongs-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dz4Kb_is-tarah-aashiqui-ka-unplugged-oldsongs-hindi.jpg?h=270&drp=1', NULL, 0, 93989, 0, 0, 1, 0, 2070, 0),
(681, 1, 90, '5', 'server_url', 'Ye Dooriyan - Lyrical', 'http://www.kids2win.com/api/videosFile/5RC6n_ye-dooriyan-lyrical-sad-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/e6T9u_ye-dooriyan-lyrical-sad-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 25268, 0, 0, 1, 0, 4727, 0),
(682, 1, 90, '5', 'server_url', 'Mera Bhola Hai Bhandari - Unplugged', 'http://www.kids2win.com/api/videosFile/nKc5z_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7K3iz_mera-bhola-hai-bhandari-mahadev-hindi-fullscreen-unplugged.jpg?h=270&drp=1', NULL, 0, 127271, 0, 0, 1, 0, 4702, 0),
(683, 1, 90, '5', 'server_url', 'Main Yahaan Hoon - Unplugged', 'http://www.kids2win.com/api/videosFile/SnmC2_main-yahaan-hoon-hindi-love-square-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/T65tE_main-yahaan-hoon-hindi-love-square-unplugged.jpg?h=270&drp=1', NULL, 0, 47859, 0, 0, 1, 0, 4484, 0),
(684, 1, 90, '5', 'server_url', 'Baatein Ye Kabhi Na - Unplugged', 'http://www.kids2win.com/api/videosFile/mVsji_baatein-ye-kabhi-na-lyrical-love-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WmayB_baatein-ye-kabhi-na-lyrical-love-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 34913, 0, 0, 1, 0, 4479, 0),
(685, 1, 90, '5', 'server_url', 'Sab Kuch Bhula Diya - Unplugged', 'http://www.kids2win.com/api/videosFile/we8aW_sab-kuch-bhula-diya-sad-square-hindi-unpluged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/v4nOj_sab-kuch-bhula-diya-sad-square-hindi-unpluged.jpg?h=270&drp=1', NULL, 0, 37671, 0, 0, 1, 0, 4467, 0),
(686, 1, 90, '5', 'server_url', 'Jitni dafa', 'http://www.kids2win.com/api/videosFile/jmQX3_jitni-dafa-unplugged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IaKTA_jitni-dafa-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 0, 27015, 0, 0, 1, 0, 1294, 0),
(687, 1, 90, '5', 'server_url', 'Aankhon Mein Tera Hi Chehra', 'http://www.kids2win.com/api/videosFile/5sLPJ_aankhon-mein-tera-hi-chehra-sad-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tPS3H_aankhon-mein-tera-hi-chehra-sad-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 51546, 0, 0, 1, 0, 4163, 0),
(688, 1, 90, '5', 'server_url', 'Aaye Ho Meri Zindagi Mein', 'http://www.kids2win.com/api/videosFile/NQRcD_aaye-ho-meri-zindagi-mein-unplugged-love-hindi-tv-sreial.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8B4lK_aaye-ho-meri-zindagi-mein-unplugged-love-hindi-tv-sreial.jpg?h=270&drp=1', NULL, 0, 68464, 0, 0, 1, 0, 3944, 0),
(689, 1, 90, '5', 'server_url', 'Aaye ho meri zindagi mein', 'http://www.kids2win.com/api/videosFile/r1AJe_aaye-ho-meri-zindagi-mein-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YraM4_aaye-ho-meri-zindagi-mein-love-hindi.jpg?h=270&drp=1', NULL, 0, 19281, 0, 0, 1, 0, 39, 0),
(690, 1, 90, '5', 'server_url', 'Manga Jo Mera Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/AuTyO_manga-jo-mera-hai-sad-hindi-square-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ztLAe_manga-jo-mera-hai-sad-hindi-square-unplugged.jpg?h=270&drp=1', NULL, 0, 36184, 0, 0, 1, 0, 3864, 0),
(691, 1, 90, '5', 'server_url', 'Tum Jo Aaye', 'http://www.kids2win.com/api/videosFile/er9Gd_tum-jo-aaye-sad-animated-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vPqGo_tum-jo-aaye-sad-animated-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 34645, 0, 0, 1, 0, 3726, 0),
(692, 1, 90, '5', 'server_url', 'Mujhe Neend Na Aaye - Unplugged', 'http://www.kids2win.com/api/videosFile/bKwqW_mujhe-neend-na-aaye-femail-version-love-hindi-fullscreen-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/i0fNM_mujhe-neend-na-aaye-femail-version-love-hindi-fullscreen-unplugged.jpg?h=270&drp=1', NULL, 0, 113485, 0, 0, 1, 0, 3594, 0),
(693, 1, 90, '5', 'server_url', 'Sawan Aaya Hai - Female Version', 'http://www.kids2win.com/api/videosFile/HpB32_sawan-aaya-hai-unplugged-hindi-love-female-version.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4ivDO_sawan-aaya-hai-unplugged-hindi-love-female-version.jpg?h=270&drp=1', NULL, 0, 27264, 0, 0, 1, 0, 1813, 0),
(694, 1, 90, '5', 'server_url', 'Dil Tod Ke - Lyrical', 'http://www.kids2win.com/api/videosFile/Z1Ql5_dil-tod-ke-sad-lyrical-hindi-unpulgged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dotOf_dil-tod-ke-sad-lyrical-hindi-unpulgged.jpg?h=270&drp=1', NULL, 0, 21962, 0, 0, 1, 0, 3523, 0),
(695, 1, 90, '5', 'server_url', 'Dil Tod Ke - Unplugged', 'http://www.kids2win.com/api/videosFile/PjRaO_dil-tod-ke-broken-sad-square-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oce86_dil-tod-ke-broken-sad-square-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 22330, 0, 0, 1, 0, 3511, 0),
(696, 1, 90, '5', 'server_url', 'Aaj Din Chadheya - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333489/jwwtgtkheps4wjfsj4og.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609333492/oaaxy0eulxbmnnzfsdwq.jpg', NULL, 155, 645, 10, 0, 1, 1, 3488, 0),
(697, 1, 90, '5', 'server_url', 'Main Teri Ho Gayi - Unplugged', 'http://www.kids2win.com/api/videosFile/hf9zn_main-teri-ho-gayi-unpluged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/D4ixU_main-teri-ho-gayi-unpluged-love-hindi.jpg?h=270&drp=1', NULL, 0, 16429, 0, 0, 1, 0, 3464, 0),
(698, 1, 90, '5', 'server_url', 'Mana Ke Tum Saath Nahi Ho', 'http://www.kids2win.com/api/videosFile/4nueH_mana-ke-tum-saath-nahi-ho-love-hindi-lyrical.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sM4Vx_mana-ke-tum-saath-nahi-ho-love-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 10603, 0, 0, 1, 0, 3429, 0),
(699, 1, 90, '5', 'server_url', 'Dil Lena Khel Hai Dildar Ka - Unlpluged', 'http://www.kids2win.com/api/videosFile/TZgfD_dil-lena-khel-hai-dildar-ka-sad-animated-hindi-unlpluged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nFwXG_dil-lena-khel-hai-dildar-ka-sad-animated-hindi-unlpluged.jpg?h=270&drp=1', NULL, 0, 12113, 0, 0, 1, 0, 3386, 0),
(700, 1, 90, '5', 'server_url', 'Ladki Badi Anjani Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314059/mfzltzgri6x1wjetypop.mp4', '-', 'Unplugged', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609314061/lact2uccfockmgnla9hn.jpg', NULL, 123, 615, 33, 0, 1, 1, 3176, 0),
(701, 1, 90, '5', 'server_url', 'Dil sambhal ja zara', 'http://www.kids2win.com/api/videosFile/yorZG_dil-sambhal-ja-zara-female%20version-unplugged-love-hindi-animated.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9Q2nE_dil-sambhal-ja-zara-female%20version-unplugged-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 42314, 0, 0, 1, 0, 1239, 0),
(702, 1, 90, '5', 'server_url', 'Dil kehta hai - Unplugged', 'http://www.kids2win.com/api/videosFile/ox6qL_dil-kehta-hai-unplugged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SxYjJ_dil-kehta-hai-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 0, 13264, 0, 0, 1, 0, 1352, 0),
(703, 1, 90, '5', 'server_url', 'Agar Tum Na Hote - Unplugged', 'http://www.kids2win.com/api/videosFile/PCNKf_agar-tum-na-hote-love-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dnwZN_agar-tum-na-hote-love-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 8118, 0, 0, 1, 0, 3140, 0),
(704, 1, 90, '5', 'server_url', 'Chaha hai tujhko - Unplugged', 'http://www.kids2win.com/api/videosFile/PYrjz_chaha-hai-tujhko-unplugged-love-hindi-animated.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8XvHL_chaha-hai-tujhko-unplugged-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 11554, 0, 0, 1, 0, 936, 0),
(705, 1, 90, '5', 'server_url', 'Tera Chehra - Adnan Sami - Unplugged', 'http://www.kids2win.com/api/videosFile/BQM9A_tera-chehra-adnan-sami-love-hindi-fullscreen-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BTUpg_tera-chehra-adnan-sami-love-hindi-fullscreen-unplugged.jpg?h=270&drp=1', NULL, 0, 18810, 0, 0, 1, 0, 3046, 0),
(706, 1, 90, '5', 'server_url', 'Jab Se Mera Dil - Armaan Malik', 'http://www.kids2win.com/api/videosFile/d9KnJ_jab-se-mera-dil-armaan-malik-unplugged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KboiG_jab-se-mera-dil-armaan-malik-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 0, 8477, 0, 0, 1, 0, 1600, 0),
(707, 1, 90, '5', 'server_url', 'Kyun Rabba - Armaan Malik - Unplugged', 'http://www.kids2win.com/api/videosFile/20edp_kyun-rabba-armaan-malik-unplugged-sad-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rAaxv_kyun-rabba-armaan-malik-unplugged-sad-hindi.jpg?h=270&drp=1', NULL, 0, 10705, 0, 0, 1, 0, 2080, 0),
(708, 1, 90, '5', 'server_url', 'Tumse Milne Ko Dil Karta Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/tDEsc_tumse-milne-ko-dil-karta-hai-unplugged-love-hindi.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WgFHP_tumse-milne-ko-dil-karta-hai-unplugged-love-hindi.jpg?h=270&drp=1', NULL, 0, 14530, 0, 0, 1, 0, 2047, 0),
(709, 1, 90, '5', 'server_url', 'Nazar Ke Samne - Unplugged', 'http://www.kids2win.com/api/videosFile/T3Kz6_nazar-ke-samne-animated-love-hindi-lyrical-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wA0IT_nazar-ke-samne-animated-love-hindi-lyrical-unplugged.jpg?h=270&drp=1', NULL, 0, 7311, 0, 0, 1, 0, 2957, 0),
(710, 1, 90, '5', 'server_url', 'Ik Tarfa Bhi Pyaar Naa Jane - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337709/sewzbe3zgs2uo2dwtpo9.mp4', '-', 'Unplugged', 'Portrait', 'http://www.kids2win.com/api/thumbImage/XnZRy_ik-tarfa-bhi-pyaar-naa-jane-sad-hindi-fullscreen-lyrical-unplugged.jpg?h=270&drp=1', NULL, 133, 675, 81, 0, 1, 1, 2899, 0),
(711, 1, 90, '5', 'server_url', 'Tumse Kitna Pyar Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/glymB_tumse-kitna-pyar-hai-sad-hindi-oldsongs-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7qYyK_tumse-kitna-pyar-hai-sad-hindi-oldsongs-unplugged.jpg?h=270&drp=1', NULL, 0, 50036, 0, 0, 1, 0, 2896, 0),
(712, 1, 90, '5', 'server_url', 'Pagli Tu Kyun Ro Rahi Hai - Unplugged', 'http://www.kids2win.com/api/videosFile/7xJNC_pagli-tu-kyun-ro-rahi-hai-sad-hindi-unplugged.mp4', '-', 'Unplugged', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AB1LK_pagli-tu-kyun-ro-rahi-hai-sad-hindi-unplugged.jpg?h=270&drp=1', NULL, 0, 7808, 0, 0, 1, 0, 2854, 0),
(713, 1, 91, '5', 'server_url', 'Dil Hai Ke Manta Nahin', 'http://www.kids2win.com/api/videosFile/jgrI9_dil-hai-ke-manta-nahin-l-h.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fWnMp_dil-hai-ke-manta-nahin-l-h.jpg?h=270&drp=1', NULL, 0, 66667, 0, 0, 1, 0, 8080, 0),
(714, 1, 91, '5', 'server_url', 'Betabi Kya Hoti Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352104/a3a4vqsprm2deuzmcuyf.mp4', '-', 'Old Songs', 'Portrait', 'http://www.kids2win.com/api/thumbImage/E2nuH_betabi-kya-hoti-hai-l-h-old.jpg?h=270&drp=1', NULL, 191, 542, 93, 0, 1, 1, 8079, 0),
(715, 1, 91, '5', 'server_url', 'Baazigar O Baazigar', 'http://www.kids2win.com/api/videosFile/Xf4IU_baazigar-o-baazigar-l-h-f-o.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Ns06L_baazigar-o-baazigar-l-h-f-o.jpg?h=270&drp=1', NULL, 0, 85669, 0, 0, 1, 0, 6439, 0),
(716, 1, 91, '5', 'server_url', 'Mujhe Neend Na Aaye', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353214/a3s74i4mmuzmsd6j7idk.mp4', '-', 'Old Songs', 'Portrait', 'http://www.kids2win.com/api/thumbImage/PZlFO_mujhe-neend-na-aaye-l-f-h-old.jpg?h=270&drp=1', NULL, 154, 801, 25, 0, 1, 1, 6429, 0),
(717, 1, 91, '5', 'server_url', 'Mauka Milega To Hum Bata Denge', 'http://www.kids2win.com/api/videosFile/F9MXU_mauka-milega-to-hum-bata-denge-love-hindi-oldsongs.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Xc0aW_mauka-milega-to-hum-bata-denge-love-hindi-oldsongs.jpg?h=270&drp=1', NULL, 0, 64268, 0, 0, 1, 0, 1565, 0),
(718, 1, 91, '5', 'server_url', 'Tere Dar Par Sanam', 'http://www.kids2win.com/api/videosFile/1V4lk_tere-dar-par-sanam-love-hindi-oldsongs.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IpsjV_tere-dar-par-sanam-love-hindi-oldsongs.jpg?h=270&drp=1', NULL, 0, 78433, 0, 0, 1, 0, 6024, 0),
(719, 1, 91, '5', 'server_url', 'Nazar Ke Samne', 'http://www.kids2win.com/api/videosFile/jCmqU_nazar-ke-samne-love-hindi-old-songs-fullscreen.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/LKg1d_nazar-ke-samne-love-hindi-old-songs-fullscreen.jpg?h=270&drp=1', NULL, 0, 59162, 0, 0, 1, 0, 6002, 0),
(720, 1, 91, '5', 'server_url', 'Chand Taron Me Nazar Aaye', 'http://www.kids2win.com/api/videosFile/HKx98_chand-taron-me-nazar-aaye-romantic-hindi-oldsongs.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HAD87_chand-taron-me-nazar-aaye-romantic-hindi-oldsongs.jpg?h=270&drp=1', NULL, 0, 217567, 0, 0, 1, 0, 5977, 0),
(721, 1, 91, '5', 'server_url', 'Aankh Hai Bhari Bhari', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609342395/zyaenhndwlufhdc5qooe.mp4', '-', 'Old Songs', 'Portrait', 'http://www.kids2win.com/api/thumbImage/wiCaO_aankh-hai-bhari-bhari-love-hindi-15-sec.jpg?h=270&drp=1', NULL, 178, 417, 66, 0, 1, 1, 5920, 0),
(722, 1, 91, '5', 'server_url', 'Itna Mai Chahu Tujhe', 'http://www.kids2win.com/api/videosFile/WBnpg_itna-mai-chahu-tujhe-old-song-hindi-love-fullscreen.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/o5pr1_itna-mai-chahu-tujhe-old-song-hindi-love-fullscreen.jpg?h=270&drp=1', NULL, 0, 79706, 0, 0, 1, 0, 5709, 0),
(723, 1, 91, '5', 'server_url', 'Bheegi Hui Hai Raat Magar - Old Song', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318527/yglgczja7qyr14kfzkni.mp4', '-', 'Old Songs', 'Portrait', 'http://www.kids2win.com/api/thumbImage/sBubz_bheegi-hui-hai-raat-magar-old-songs-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 141, 567, 17, 0, 1, 1, 5003, 0),
(724, 1, 91, '5', 'server_url', 'Yaad Rahegi Holi Re', 'http://www.kids2win.com/api/videosFile/kjT0M_yaad-rahegi-holi-re-hindi-oladsong.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/O8KLV_yaad-rahegi-holi-re-hindi-oladsong.jpg?h=270&drp=1', NULL, 0, 60048, 0, 0, 1, 0, 2166, 0),
(725, 1, 91, '5', 'server_url', 'Is Tarah Aashiqui Ka - Unplugged', 'http://www.kids2win.com/api/videosFile/tDkZI_is-tarah-aashiqui-ka-unplugged-oldsongs-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dz4Kb_is-tarah-aashiqui-ka-unplugged-oldsongs-hindi.jpg?h=270&drp=1', NULL, 0, 93990, 0, 0, 1, 0, 2070, 0),
(726, 1, 91, '5', 'server_url', 'Is Tarah Aashiqui Ka', 'http://www.kids2win.com/api/videosFile/QoG04_is-tarah-aashiqui-ka-oldsongs-hindi-love.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oW5K6_is-tarah-aashiqui-ka-oldsongs-hindi-love.jpg?h=270&drp=1', NULL, 0, 65307, 0, 0, 1, 0, 4709, 0),
(727, 1, 91, '5', 'server_url', 'Teri Baahon Mein - Hug Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399517/mkz6gez0onlrnx8rxa5g.mp4', '-', 'Old Songs', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399521/sf4zwehsck9fxpogd2j4.jpg', NULL, 139, 770, 26, 0, 1, 1, 4602, 0),
(728, 1, 91, '5', 'server_url', 'Hum Asie Karenge Pyar', 'http://www.kids2win.com/api/videosFile/fDbvl_hum-asie-karenge-pyar-love-hindi-old-songs-tv-serial.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/w1LyQ_hum-asie-karenge-pyar-love-hindi-old-songs-tv-serial.jpg?h=270&drp=1', NULL, 0, 119651, 0, 0, 1, 0, 4307, 0),
(729, 1, 91, '5', 'server_url', 'Mujhe Neend Na Aaye - Unplugged', 'http://www.kids2win.com/api/videosFile/bKwqW_mujhe-neend-na-aaye-femail-version-love-hindi-fullscreen-unplugged.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/i0fNM_mujhe-neend-na-aaye-femail-version-love-hindi-fullscreen-unplugged.jpg?h=270&drp=1', NULL, 0, 113485, 0, 0, 1, 0, 3594, 0),
(730, 1, 91, '5', 'server_url', 'Dil Hai Tumhaara', 'http://www.kids2win.com/api/videosFile/L19CT_aaj-se-jaaneman-dil-hai-tumhara-love-oldsongs-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4nUZz_aaj-se-jaaneman-dil-hai-tumhara-love-oldsongs-hindi.jpg?h=270&drp=1', NULL, 0, 65084, 0, 0, 1, 0, 267, 0),
(731, 1, 91, '5', 'server_url', 'Aapke Pyaar Mein - Love', 'http://www.kids2win.com/api/videosFile/f0zwH_aapke-pyaar-mein-love-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wpA3y_aapke-pyaar-mein-love-hindi.jpg?h=270&drp=1', NULL, 0, 32785, 0, 0, 1, 0, 3044, 0),
(732, 1, 91, '5', 'server_url', 'Agar tum na hote - Old songs', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314898/suag9j0tecrhstc7bwql.mp4', '-', 'Old Songs', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609314901/dfhorjzpsn3cctyskhxn.jpg', NULL, 170, 706, 11, 0, 1, 1, 975, 0),
(733, 1, 91, '5', 'server_url', 'Pal Pal Dil Ke Paas Tum Rehte Ho', 'http://www.kids2win.com/api/videosFile/AVjPN_pal-pal-dil-ke-paas-tum-rehte-ho-love-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mA9TB_pal-pal-dil-ke-paas-tum-rehte-ho-love-hindi.jpg?h=270&drp=1', NULL, 0, 49498, 0, 0, 1, 0, 3003, 0),
(734, 1, 91, '5', 'server_url', 'Pal Pal Dil Ke Paas - Female Version', 'http://www.kids2win.com/api/videosFile/GBZSM_pal-pal-dil-ke-paas-female-version-fullscreen-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wCvUJ_pal-pal-dil-ke-paas-female-version-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 35225, 0, 0, 1, 0, 2995, 0),
(735, 1, 91, '5', 'server_url', 'Tere Dar Par Sanam - Love', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399424/ifuncraokijqgcpdprea.mp4', '-', 'Old Songs', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399427/iqbesmf8t3zjcy59ewre.jpg', NULL, 157, 857, 87, 0, 1, 1, 2966, 0),
(736, 1, 91, '5', 'server_url', 'Tumse Kitna Pyar Hai - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609398854/qbqgxz74kvngfslgbbyv.mp4', '-', 'Old Songs', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609398856/mfk9flmirycfjxoccejg.jpg', NULL, 140, 462, 47, 0, 1, 1, 2896, 0),
(737, 1, 91, '5', 'server_url', 'Mauka Milega To Hum - Old Songs - Unplugged', 'http://www.kids2win.com/api/videosFile/W8Lbs_mauka-milega-to-hum-sad-hindi-oldsongs-unplugged.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UvnzC_mauka-milega-to-hum-sad-hindi-oldsongs-unplugged.jpg?h=270&drp=1', NULL, 0, 36434, 0, 0, 1, 0, 2743, 0),
(738, 1, 91, '5', 'server_url', 'Tumhe dekhen meri ankhen', 'http://www.kids2win.com/api/videosFile/9WzGy_tumhe-dekhe-meri-aankhen-love-oldsongs-hindi.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lipXd_tumhe-dekhe-meri-aankhen-love-oldsongs-hindi.jpg?h=270&drp=1', NULL, 0, 131845, 0, 0, 1, 0, 549, 0),
(739, 1, 91, '5', 'server_url', 'Beech Safar Mein Kahin - Old Song', 'http://www.kids2win.com/api/videosFile/hLFUH_beech-safar-mein-kahin-love-hindi-oldsong-fullscreen.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9bYGn_beech-safar-mein-kahin-love-hindi-oldsong-fullscreen.jpg?h=270&drp=1', NULL, 0, 29015, 0, 0, 1, 0, 2540, 0),
(740, 1, 91, '5', 'server_url', 'Bahut pyar karte hain', 'http://www.kids2win.com/api/videosFile/VOGz3_bahut-pyar-karte-hain-hindi-love-female-version.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dIo8p_bahut-pyar-karte-hain-hindi-love-female-version.jpg?h=270&drp=1', NULL, 0, 39479, 0, 0, 1, 0, 536, 0),
(741, 1, 91, '5', 'server_url', '90\'s Mashup', 'http://www.kids2win.com/api/videosFile/Jy28B_90s-mashup-old-songs-love-hindi-fullscreen.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4vPd0_90s-mashup-old-songs-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 79302, 0, 0, 1, 0, 1335, 0),
(742, 1, 91, '5', 'server_url', 'Mera Dil Bhi Kitna Pagal Hai - Full Screen', 'http://www.kids2win.com/api/videosFile/fqnAJ_mera-dil-bhi-kitna-pagal-hai-love-hindi-fullscreen-lyrical.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/vnWy0_mera-dil-bhi-kitna-pagal-hai-love-hindi-fullscreen-lyrical.jpg?h=270&drp=1', NULL, 0, 30849, 0, 0, 1, 0, 2430, 0),
(743, 1, 91, '5', 'server_url', 'Dil Laga Liya - Old Song', 'http://www.kids2win.com/api/videosFile/ahNbf_dil-laga-liya-love-hindi-fullscreen-old-song.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/R3abL_dil-laga-liya-love-hindi-fullscreen-old-song.jpg?h=270&drp=1', NULL, 0, 24289, 0, 0, 1, 0, 2424, 0),
(744, 1, 91, '5', 'server_url', 'Saaton Janam Mein Tere', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609314833/fnbhkgrltmog09h6feet.mp4', '-', 'Old Songs', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609314836/ckeonywjqqvxkukygkxy.jpg', NULL, 137, 815, 12, 0, 1, 1, 2403, 0),
(745, 1, 91, '5', 'server_url', 'Dard Jo Tumne Diya', 'http://www.kids2win.com/api/videosFile/bowKF_dard-jo-tumne-diya-sad-hindi-oldsongs.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/f3lAI_dard-jo-tumne-diya-sad-hindi-oldsongs.jpg?h=270&drp=1', NULL, 0, 42081, 0, 0, 1, 0, 2377, 0),
(746, 1, 91, '5', 'server_url', 'Mere Mehboob Ki Tooti - Old Song', 'http://www.kids2win.com/api/videosFile/qkw1D_mere-mehboob-ki-tooti-oldsongs-sad-hindi-breakup.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/uRCKl_mere-mehboob-ki-tooti-oldsongs-sad-hindi-breakup.jpg?h=270&drp=1', NULL, 0, 97018, 0, 0, 1, 0, 2374, 0),
(747, 1, 91, '5', 'server_url', 'Tu Deewana Pagal Mera Ho Gaya', 'http://www.kids2win.com/api/videosFile/rhW5s_tu-deewana-pagal-mera-ho-gaya-love-hindi-oldsongs-fullscreen.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ashSo_tu-deewana-pagal-mera-ho-gaya-love-hindi-oldsongs-fullscreen.jpg?h=270&drp=1', NULL, 0, 20154, 0, 0, 1, 0, 2372, 0),
(748, 1, 91, '5', 'server_url', 'Barsaat Ke Din Aaye', 'http://www.kids2win.com/api/videosFile/L62ql_barsaat-ke-din-aaye-love-hindi-rain-special.mp4', '-', 'Old Songs', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6f5Cb_barsaat-ke-din-aaye-love-hindi-rain-special.jpg?h=270&drp=1', NULL, 0, 56289, 0, 0, 1, 0, 2319, 0),
(750, 1, 91, '5', 'server_url', 'Mera Dil Bhi Kitna Pagal Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317308/c9vt2nglskatrlpyjnt4.mp4', '-', 'Old Songs', 'Portrait', 'http://www.kids2win.com/api/thumbImage/mKC5M_mera-dil-bhi-kitna-pagal-hai-love-hindi-fullscreen-oldsongs.jpg?h=270&drp=1', NULL, 100, 692, 83, 0, 1, 1, 1621, 0),
(751, 1, 92, '5', 'server_url', 'Le Jao Neend Meri - Korean Mix', 'http://www.kids2win.com/api/videosFile/BwtCx_le-jao-neend-meri-korean-mix-hindi-sad.mp4', '-', 'Korean Mix', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AnLTO_le-jao-neend-meri-korean-mix-hindi-sad.jpg?h=270&drp=1', NULL, 0, 170803, 0, 0, 1, 0, 2114, 0),
(752, 1, 92, '5', 'server_url', 'Dil Kehta Hai Chal Unse Mil', 'http://www.kids2win.com/api/videosFile/KD8aW_dil-kehta-hai-chal-unse-mil-love-hindi-fullscreen-korean-mix%20.mp4', '-', 'Korean Mix', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dANxH_dil-kehta-hai-chal-unse-mil-love-hindi-fullscreen-korean-mix%20.jpg?h=270&drp=1', NULL, 0, 93303, 0, 0, 1, 0, 2481, 0),
(753, 1, 92, '5', 'server_url', 'Hum Jaise Jee Rahe Hain - Korean Mix', 'http://www.kids2win.com/api/videosFile/9smux_hum-jaise-jee-rahe-hain-love-hindi-korean-mix.mp4', '-', 'Korean Mix', 'Landscape', 'http://www.kids2win.com/api/thumbImage/xTFG9_hum-jaise-jee-rahe-hain-love-hindi-korean-mix.jpg?h=270&drp=1', NULL, 0, 168440, 0, 0, 1, 0, 2119, 0),
(754, 1, 92, '5', 'server_url', 'Jadu Sa Jaise Koi Chalne', 'http://www.kids2win.com/api/videosFile/V6ghA_jadu-sa-jaise-koi-chalne-love-hindi-korean-romantic.mp4', '-', 'Korean Mix', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rxFGH_jadu-sa-jaise-koi-chalne-love-hindi-korean-romantic.jpg?h=270&drp=1', NULL, 0, 146580, 0, 0, 1, 0, 2218, 0),
(755, 1, 93, '5', 'server_url', 'Happy New Year 2020 - Lyrical', 'http://www.kids2win.com/api/videosFile/UK5y0_happy-new-year-2020-fullscreen-hindi-lyrical.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FtBRi_happy-new-year-2020-fullscreen-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 7446, 0, 0, 1, 0, 4079, 0),
(756, 1, 93, '5', 'server_url', 'Swag Se Swagat', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317805/ocuhjiszgbpp1swxmu6i.mp4', '-', 'Happy New Year', 'Portrait', 'http://www.kids2win.com/api/thumbImage/UEI6c_swag-se-swagat-new-year-hindi-fullscreen.jpg?h=270&drp=1', NULL, 196, 566, 97, 0, 1, 1, 4078, 0),
(757, 1, 93, '5', 'server_url', 'Aane Wale Saal Ko Salaam', 'http://www.kids2win.com/api/videosFile/RO3Vu_aane-wale-saal-ko-salaam-hindi-fullscreen-new-year.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RkuHv_aane-wale-saal-ko-salaam-hindi-fullscreen-new-year.jpg?h=270&drp=1', NULL, 0, 7794, 0, 0, 1, 0, 4076, 0),
(758, 1, 93, '5', 'server_url', 'Alvida 2019 - Fullscreen', 'http://www.kids2win.com/api/videosFile/RvDQM_alvida-2019-fullscreen-hindi-lyrical.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/zpwk5_alvida-2019-fullscreen-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 6356, 0, 0, 1, 0, 4075, 0),
(759, 1, 93, '5', 'server_url', 'New Year Se Pehle', 'http://www.kids2win.com/api/videosFile/Y7aih_new-year-se-pehle-hindi-shayari-fullscreen.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YNKzf_new-year-se-pehle-hindi-shayari-fullscreen.jpg?h=270&drp=1', NULL, 0, 3047, 0, 0, 1, 0, 4072, 0),
(760, 1, 93, '5', 'server_url', 'Happy New Year 2020 Party', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399234/l4qgj1tjd9uzsqj5auqz.mp4', '-', 'Happy New Year', 'Portrait', 'http://www.kids2win.com/api/thumbImage/Uysup_happy-new-year-2020-party-hindi-fullscreen.jpg?h=270&drp=1', NULL, 198, 786, 66, 0, 1, 1, 4070, 0),
(761, 1, 93, '5', 'server_url', 'Direct Dil Se New Year', 'http://www.kids2win.com/api/videosFile/WmfVt_direct-dil-se-new-year-fullscreen-hindi.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HxePw_direct-dil-se-new-year-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 4449, 0, 0, 1, 0, 4069, 0),
(762, 1, 93, '5', 'server_url', '31st December Party', 'http://www.kids2win.com/api/videosFile/Rv4H1_31st-december-party-fullscreen-hindi-party.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FCJYI_31st-december-party-fullscreen-hindi-party.jpg?h=270&drp=1', NULL, 0, 10239, 0, 0, 1, 0, 4068, 0),
(763, 1, 93, '5', 'server_url', 'New Year Se Pehle', 'http://www.kids2win.com/api/videosFile/MyxRN_new-year-se-pehle-hindi-fullscreen.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3VElh_new-year-se-pehle-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 2931, 0, 0, 1, 0, 4066, 0),
(764, 1, 93, '5', 'server_url', 'New Year Status - Shayari', 'http://www.kids2win.com/api/videosFile/Hfpce_new-year-status-hindi-sad-lyrical.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1LRjw_new-year-status-hindi-sad-lyrical.jpg?h=270&drp=1', NULL, 0, 10514, 0, 0, 1, 0, 4065, 0),
(765, 1, 93, '5', 'server_url', 'I Hope 2020', 'http://www.kids2win.com/api/videosFile/Szk5F_i-hope-2020-new-year.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3q9ed_i-hope-2020-new-year.jpg?h=270&drp=1', NULL, 0, 3512, 0, 0, 1, 0, 4064, 0),
(766, 1, 93, '5', 'server_url', 'New Year Shayari', 'http://www.kids2win.com/api/videosFile/O8NDP_new-year-shayari-hindi-square.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/97Uto_new-year-shayari-hindi-square.jpg?h=270&drp=1', NULL, 0, 8936, 0, 0, 1, 0, 4059, 0),
(767, 1, 93, '5', 'server_url', 'Happy New Year - Shayari', 'http://www.kids2win.com/api/videosFile/kzdXt_happy-new-year-shayri-hindi-sad.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rw3C5_happy-new-year-shayri-hindi-sad.jpg?h=270&drp=1', NULL, 0, 12335, 0, 0, 1, 0, 4044, 0),
(768, 1, 93, '5', 'server_url', 'New Year Scene', 'http://www.kids2win.com/api/videosFile/r4hSo_new-year-scene-hindi-new-year.mp4', '-', 'Happy New Year', 'Landscape', 'http://www.kids2win.com/api/thumbImage/edjHr_new-year-scene-hindi-new-year.jpg?h=270&drp=1', NULL, 0, 6805, 0, 0, 1, 0, 3987, 0),
(769, 1, 94, '5', 'server_url', 'Ae Watan - Lyrical', 'http://www.kids2win.com/api/videosFile/dCmen_ae-watan-ly-h-ind.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0ygoQ_ae-watan-ly-h-ind.jpg?h=270&drp=1', NULL, 0, 46978, 0, 0, 1, 0, 7554, 0),
(770, 1, 94, '5', 'server_url', '15 August Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317512/ifddqyxskh4umb96rflj.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/nEP9N_15-august-special-h-ind.jpg?h=270&drp=1', NULL, 121, 584, 70, 0, 1, 1, 7553, 0),
(771, 1, 94, '5', 'server_url', 'Happy Independence Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309097/qkon3t9tw0wmb5hbttkp.mp4', '-', 'Independence Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309099/habyd9kxi9izob8ogjhi.jpg', NULL, 116, 529, 34, 0, 1, 1, 7552, 0),
(772, 1, 94, '5', 'server_url', 'Ae Watan Watan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401859/t5rxpta0scpdibh2rqlo.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/yf1jV_ae-watan-watan-h-ind.jpg?h=270&drp=1', NULL, 125, 802, 20, 0, 1, 1, 7551, 0),
(773, 1, 94, '5', 'server_url', 'Happy Independence Day - Fullscreen', 'http://www.kids2win.com/api/videosFile/yoMgd_happy-republic-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/f9IPV_happy-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 43684, 0, 0, 1, 0, 4434, 0),
(774, 1, 94, '5', 'server_url', 'Bharat Ki Beti', 'http://www.kids2win.com/api/videosFile/zQqtu_bharat-ki-beti-h-f-ind.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QMbd4_bharat-ki-beti-h-f-ind.jpg?h=270&drp=1', NULL, 0, 17402, 0, 0, 1, 0, 7529, 0),
(775, 1, 94, '5', 'server_url', 'Ae Watan Watan Mere Aabaad Rahe Tu', 'http://www.kids2win.com/api/videosFile/HlExD_ae-watan-mere-watan-aabad-rahe-tu-hindi-republic-day.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZnkpL_ae-watan-mere-watan-aabad-rahe-tu-hindi-republic-day.jpg?h=270&drp=1', NULL, 0, 25144, 0, 0, 1, 0, 1738, 0),
(776, 1, 94, '5', 'server_url', 'Ae Watan', 'http://www.kids2win.com/api/videosFile/XtvzV_ae-watan-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Ma7e2_ae-watan-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 22340, 0, 0, 1, 0, 4433, 0),
(777, 1, 94, '5', 'server_url', 'Teri Mitti - Independence Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318751/fdesdjawbmfxstb0kvz3.mp4', '-', 'Independence Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609318753/lvnogvd0ndhaehthbcpw.jpg', NULL, 146, 451, 97, 0, 1, 1, 4432, 0),
(778, 1, 94, '5', 'server_url', 'Vande Mataram', 'http://www.kids2win.com/api/videosFile/ZUki1_vande-mataram-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nch8G_vande-mataram-hindi.jpg?h=270&drp=1', NULL, 0, 22929, 0, 0, 1, 0, 4431, 0),
(779, 1, 94, '5', 'server_url', 'Jana Gana Mana', 'http://www.kids2win.com/api/videosFile/krIwV_jana-gana-mana-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/X2Qw8_jana-gana-mana-hindi.jpg?h=270&drp=1', NULL, 0, 9174, 0, 0, 1, 0, 4430, 0),
(780, 1, 94, '5', 'server_url', 'Republic Day Inspirational', 'http://www.kids2win.com/api/videosFile/MO0kU_republic-day-inspirational-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/V40i5_republic-day-inspirational-hindi.jpg?h=270&drp=1', NULL, 0, 11838, 0, 0, 1, 0, 4404, 0),
(781, 1, 94, '5', 'server_url', 'Maa Tujhe Salaam - A R Rahman', 'http://www.kids2win.com/api/videosFile/P4NXw_maa-tujhe-salaam-hindi-india.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ADzYX_maa-tujhe-salaam-hindi-india.jpg?h=270&drp=1', NULL, 0, 21116, 0, 0, 1, 0, 4397, 0),
(782, 1, 94, '5', 'server_url', 'Ae Watan', 'http://www.kids2win.com/api/videosFile/Wngu8_ae-watan-hindi-india.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5oMzd_ae-watan-hindi-india.jpg?h=270&drp=1', NULL, 0, 14115, 0, 0, 1, 0, 4381, 0),
(783, 1, 94, '5', 'server_url', 'Ae Watan Watan', 'http://www.kids2win.com/api/videosFile/fx0zI_ae-watan-watan-mere-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Dr5uF_ae-watan-watan-mere-hindi.jpg?h=270&drp=1', NULL, 0, 9976, 0, 0, 1, 0, 4378, 0),
(784, 1, 94, '5', 'server_url', '26 January - Republic Day', 'http://www.kids2win.com/api/videosFile/t4f0x_26-january-status-hindi-republic.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/zMN2m_26-january-status-hindi-republic.jpg?h=270&drp=1', NULL, 0, 8873, 0, 0, 1, 0, 4376, 0),
(785, 1, 94, '5', 'server_url', 'Ae Watan Watan', 'http://www.kids2win.com/api/videosFile/b8mDr_ae-watan-watan-hindi-animated-india.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZON0X_ae-watan-watan-hindi-animated-india.jpg?h=270&drp=1', NULL, 0, 11530, 0, 0, 1, 0, 4366, 0),
(786, 1, 94, '5', 'server_url', 'Ek Tera Naam Hai Saancha', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313059/porgxvde4znj1k0flbch.mp4', '-', 'Independence Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313061/x0lnifk9kbxrl6x4woss.jpg', NULL, 130, 482, 90, 0, 1, 1, 4358, 0),
(787, 1, 94, '5', 'server_url', 'Maa Tujhe Salaam - A R Rahman', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333446/jvghdeqdianpoy8yjstg.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/XvUbO_maa-tujhe-salaam-a-r-rahman-fullscreen-republic-day.jpg?h=270&drp=1', NULL, 185, 820, 43, 0, 1, 1, 1665, 0),
(788, 1, 94, '5', 'server_url', 'Desh Mere Tu Jeeta Rahe - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337443/qje55pri5w504s6oq4sr.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/krCGI_desh-mere-tu-jeeta-rahe-india-hindi-fullscreen.jpg?h=270&drp=1', NULL, 195, 564, 95, 0, 1, 1, 2685, 0),
(789, 1, 94, '5', 'server_url', 'Ae Watan - Lyrical', 'http://www.kids2win.com/api/videosFile/uNdlM_ae-watan-india-hindi-independence-day-lyrical.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fG4qV_ae-watan-india-hindi-independence-day-lyrical.jpg?h=270&drp=1', NULL, 0, 8832, 0, 0, 1, 0, 2684, 0),
(790, 1, 94, '5', 'server_url', 'Naina Ashq Na Ho', 'http://www.kids2win.com/api/videosFile/WZAsD_naina-ashq-na-ho-arijit-singh-independence-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QwOoy_naina-ashq-na-ho-arijit-singh-independence-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 11102, 0, 0, 1, 0, 2673, 0),
(791, 1, 94, '5', 'server_url', 'Chak De India', 'http://www.kids2win.com/api/videosFile/OnJLs_chak-de-india-independence-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/kruAz_chak-de-india-independence-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 10366, 0, 0, 1, 0, 2672, 0),
(792, 1, 94, '5', 'server_url', 'Ish Pe Jaan Lutau Re - Republic Day', 'http://www.kids2win.com/api/videosFile/SIGjd_ish-pe-jaan-lutau-re-republic-day-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CRsUm_ish-pe-jaan-lutau-re-republic-day-hindi.jpg?h=270&drp=1', NULL, 0, 10680, 0, 0, 1, 0, 1720, 0),
(793, 1, 94, '5', 'server_url', 'Mere Gaon Mein Hai Jo Wo Gali', 'http://www.kids2win.com/api/videosFile/a5MBO_mere-gaon-me-hai-jo-wo-gali-republic-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/l8yaU_mere-gaon-me-hai-jo-wo-gali-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 11343, 0, 0, 1, 0, 1588, 0),
(794, 1, 94, '5', 'server_url', 'Challa Main Lad Janaa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316642/oyinyi8v7n4gseujqo7b.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/aMzTA_challa-main-lad-janaa-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 151, 530, 29, 0, 1, 1, 1648, 0),
(795, 1, 94, '5', 'server_url', 'Des Rangila Rangila', 'http://www.kids2win.com/api/videosFile/jECcD_desh-rangila-rangila-republic-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZYBMO_desh-rangila-rangila-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 10573, 0, 0, 1, 0, 1723, 0),
(796, 1, 94, '5', 'server_url', 'Happy Republic Day 2019', 'http://www.kids2win.com/api/videosFile/HfYJj_happy-republic-day-2019-animated.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HpvZA_happy-republic-day-2019-animated.jpg?h=270&drp=1', NULL, 0, 9975, 0, 0, 1, 0, 1736, 0),
(797, 1, 94, '5', 'server_url', 'Vande Mataram - A.R. Rahman', 'http://www.kids2win.com/api/videosFile/PdWt7_vande-mataram-republic-day-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pZYsh_vande-mataram-republic-day-hindi.jpg?h=270&drp=1', NULL, 0, 13922, 0, 0, 1, 0, 1593, 0),
(798, 1, 94, '5', 'server_url', 'Ae Watan Watan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311225/oqal4mfrzbl7h67pdzg5.mp4', '-', 'Independence Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/JZaR0_26-january-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 163, 487, 74, 0, 1, 1, 1642, 0),
(799, 1, 94, '5', 'server_url', 'Ae Gujarane Wali', 'http://www.kids2win.com/api/videosFile/ud9y7_ae-gujarane-wali-hawa-bata-republic-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RSLnI_ae-gujarane-wali-hawa-bata-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 10454, 0, 0, 1, 0, 1603, 0),
(800, 1, 94, '5', 'server_url', 'Ae Watan Watan Mere', 'http://www.kids2win.com/api/videosFile/X5dIh_ae-watan-mere-watan-aabad-rahe-tu-republic-day-hindi-fullscreen.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pbTUI_ae-watan-mere-watan-aabad-rahe-tu-republic-day-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 8406, 0, 0, 1, 0, 1616, 0),
(801, 1, 94, '5', 'server_url', 'Ae Watan Watan - Republic Day', 'http://www.kids2win.com/api/videosFile/9NBD8_ae-watan-watan-republic-day-fullscreen-hindi.mp4', '-', 'Independence Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dmuaK_ae-watan-watan-republic-day-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 7653, 0, 0, 1, 0, 1725, 0),
(802, 1, 95, '5', 'server_url', 'Happy Valentine\'s Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311286/xjsbybn2t2zze9n8mw0q.mp4', '-', 'Valentine Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609311288/wmagt861y7gqn6peieud.jpg', NULL, 186, 569, 17, 0, 1, 1, 4623, 0),
(803, 1, 95, '5', 'server_url', 'Valentine\'s Day Special - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318412/i6fjeqiiplsffo1q1zam.mp4', '-', 'Valentine Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609318413/umolif27ukdf5kbtnrat.jpg', NULL, 185, 416, 67, 0, 1, 1, 4622, 0),
(804, 1, 95, '5', 'server_url', 'Happy Valentine\'s Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399618/bkubdi83tuubtmx5ytp5.mp4', '-', 'Valentine Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399621/izsreps4kcgu5niccm7c.jpg', NULL, 102, 693, 93, 0, 1, 1, 4616, 0),
(805, 1, 95, '5', 'server_url', 'Happy Valentine', 'http://www.kids2win.com/api/videosFile/7EaAJ_happy-valentine-hindi-animated.mp4', '-', 'Valentine Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8eB7b_happy-valentine-hindi-animated.jpg?h=270&drp=1', NULL, 0, 10582, 0, 0, 1, 0, 4617, 0),
(806, 1, 95, '5', 'server_url', 'Valentine Day Special Shayari', 'http://www.kids2win.com/api/videosFile/DPeAH_valentine-day-shayari-hindi-animated.mp4', '-', 'Valentine Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8a2y9_valentine-day-shayari-hindi-animated.jpg?h=270&drp=1', NULL, 0, 47091, 0, 0, 1, 0, 1920, 0),
(807, 1, 95, '5', 'server_url', 'Valentine\'s Day Special - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340129/kqc2uf67kkkv3knxfyss.mp4', '-', 'Valentine Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/i98zx_Valentine-day-special-fullscreen-love-hindi.jpg?h=270&drp=1', NULL, 190, 743, 15, 0, 1, 1, 1949, 0),
(808, 1, 95, '5', 'server_url', '12 Feb Kiss Day Special', 'http://www.kids2win.com/api/videosFile/LFOA0_12feb-kiss-day-special-fullscreen-hindi-valentine.mp4', '-', 'Valentine Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/qi5sD_12feb-kiss-day-special-fullscreen-hindi-valentine.jpg?h=270&drp=1', NULL, 0, 22090, 0, 0, 1, 0, 1938, 0),
(809, 1, 95, '5', 'server_url', '8th Feb Propose Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337122/w39tmixuaj2uybc4trni.mp4', '-', 'Valentine Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/jGZrM_8th-feb-propose-day-special-fullscreen-valentine-love-hindi.jpg?h=270&drp=1', NULL, 148, 816, 83, 0, 1, 1, 1881, 0),
(810, 1, 95, '5', 'server_url', 'Tu Hi Ye Mujhko Bata De - Propose Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336428/a5ukag8b93x7hoqq0w79.mp4', '-', 'Valentine Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/Gl3qj_tu-hi-ye-mujhko-bataad-propose-day-valentine-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 170, 659, 18, 0, 1, 1, 1882, 0),
(811, 1, 95, '5', 'server_url', 'Tumse Milne Ko Dil Karta Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319614/vn7cenifysqip5xrtthn.mp4', '-', 'Valentine Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/k2OUP_tumse-milne-ko-dil-karta-hai-love-hindi-fullscreen-kiss-day-valentine.jpg?h=270&drp=1', NULL, 200, 775, 96, 0, 1, 1, 1940, 0),
(812, 1, 95, '5', 'server_url', 'Dil Mein Ho Tum', 'http://www.kids2win.com/api/videosFile/eACsq_propose-day-dil-mein-ho-tum-love-hindi.mp4', '-', 'Valentine Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8SoML_propose-day-dil-mein-ho-tum-love-hindi.jpg?h=270&drp=1', NULL, 0, 25457, 0, 0, 1, 0, 1897, 0),
(813, 1, 95, '5', 'server_url', 'Valentine Special - Thoda Aur', 'http://www.kids2win.com/api/videosFile/WqfFA_valentine-special-thoda-aur-love-hindi.mp4', '-', 'Valentine Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4KZS5_valentine-special-thoda-aur-love-hindi.jpg?h=270&drp=1', NULL, 0, 16335, 0, 0, 1, 0, 1899, 0),
(814, 1, 96, '5', 'server_url', 'Duniya Se Tujhko Churake', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609351788/bkxcsfjri5qunbli27yv.mp4', '-', 'Hug Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609351790/t03t2dhlwyhk0sozmqcd.jpg', NULL, 100, 818, 40, 0, 1, 1, 4609, 0),
(815, 1, 96, '5', 'server_url', 'Teri Baahon Mein - Hug Day Special', 'http://www.kids2win.com/api/videosFile/XsgxO_teri-baahon-mein-hug-day-special-hindi-old-songs.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lAQ4f_teri-baahon-mein-hug-day-special-hindi-old-songs.jpg?h=270&drp=1', NULL, 0, 70867, 0, 0, 1, 0, 4602, 0),
(816, 1, 96, '5', 'server_url', 'Tere bina jeena saza ho gaya', 'http://www.kids2win.com/api/videosFile/57blS_tere-bina-jeena-saza-ho-gaya-romantic-tvserial-hindi.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Qpbsl_tere-bina-jeena-saza-ho-gaya-romantic-tvserial-hindi.jpg?h=270&drp=1', NULL, 0, 228189, 0, 0, 1, 0, 1221, 0),
(817, 1, 96, '5', 'server_url', 'Hug Day Bollywood Special', 'http://www.kids2win.com/api/videosFile/foJIX_hug-day-bollywood-special-hindi-hug-day.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/w4IfR_hug-day-bollywood-special-hindi-hug-day.jpg?h=270&drp=1', NULL, 0, 18898, 0, 0, 1, 0, 4601, 0),
(818, 1, 96, '5', 'server_url', 'Happy Hug Day - Tu Jo Mila', 'http://www.kids2win.com/api/videosFile/tECLl_happy-hug-day-sad-hindi-tu-jo-mila.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8M9DZ_happy-hug-day-sad-hindi-tu-jo-mila.jpg?h=270&drp=1', NULL, 0, 17010, 0, 0, 1, 0, 4600, 0),
(819, 1, 96, '5', 'server_url', 'Tera Hua - Hug Day Special', 'http://www.kids2win.com/api/videosFile/fgMkz_tera-hua-hug-day-special-hindi.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4X8Qp_tera-hua-hug-day-special-hindi.jpg?h=270&drp=1', NULL, 0, 30579, 0, 0, 1, 0, 4599, 0),
(820, 1, 96, '5', 'server_url', 'Tere karib aa raha hu', 'http://www.kids2win.com/api/videosFile/I7Zix_tere-karib-aa-raha-hu-hindi-love.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5AS0c_tere-karib-aa-raha-hu-hindi-love.jpg?h=270&drp=1', NULL, 0, 20727, 0, 0, 1, 0, 989, 0),
(821, 1, 96, '5', 'server_url', 'Happy Hug Day - Fullscreen', 'http://www.kids2win.com/api/videosFile/SMOYW_happy-hug-day-fullscreen-hindi.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/MJCsm_happy-hug-day-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 17125, 0, 0, 1, 0, 1929, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(822, 1, 96, '5', 'server_url', 'Hug Day Special - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339496/r8vcvt4dtomccsfa5pjl.mp4', '-', 'Hug Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/Mjdbx_hug-day-fullscreen-hindi.jpg?h=270&drp=1', NULL, 172, 827, 83, 0, 1, 1, 1930, 0),
(823, 1, 96, '5', 'server_url', 'Chahu Pass Pass Aana - Hug Day Special', 'http://www.kids2win.com/api/videosFile/CWBZr_chahu-pass-pass-aana-hug-day-special-hindi.mp4', '-', 'Hug Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NUxnX_chahu-pass-pass-aana-hug-day-special-hindi.jpg?h=270&drp=1', NULL, 0, 24391, 0, 0, 1, 0, 1933, 0),
(824, 1, 96, '5', 'server_url', 'Aawara Shaam Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399478/p3bar7mloedf5ju21wvo.mp4', '-', 'Hug Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/ehZp4_aawara-shaam-hai-love-hindi-fullscreen-hug-day.jpg?h=270&drp=1', NULL, 119, 712, 87, 0, 1, 1, 4592, 0),
(825, 1, 97, '5', 'server_url', 'Valentine Day Special Shayari', 'http://www.kids2win.com/api/videosFile/DPeAH_valentine-day-shayari-hindi-animated.mp4', '-', 'Promise Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8a2y9_valentine-day-shayari-hindi-animated.jpg?h=270&drp=1', NULL, 0, 47091, 0, 0, 1, 0, 1920, 0),
(826, 1, 97, '5', 'server_url', 'Happy Promise Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311955/ocviscogpckyhzouw1z7.mp4', '-', 'Promise Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/E5lnW_happy-promise-day-special-fullscreen-hindi.jpg?h=270&drp=1', NULL, 174, 697, 85, 0, 1, 1, 1917, 0),
(827, 1, 97, '5', 'server_url', 'Promise Day Special Animated', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609341533/tqwllq4yecxnyuaxmxi1.mp4', '-', 'Promise Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609341535/dpykdiwhraa05kdlrddn.jpg', NULL, 106, 773, 56, 0, 1, 1, 1918, 0),
(828, 1, 97, '5', 'server_url', 'Ye Vada Raha Best Promise', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609335034/xkwi3jakfkfe5ro789pl.mp4', '-', 'Promise Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609335037/frqg0uab8ojr9fslnrct.jpg', NULL, 123, 714, 45, 0, 1, 1, 1922, 0),
(829, 1, 97, '5', 'server_url', '11 February Promise Day Special', 'http://www.kids2win.com/api/videosFile/uzGpy_11-february-romise-day-special-fullscreen-hindi.mp4', '-', 'Promise Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Zv4QT_11-february-romise-day-special-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 13097, 0, 0, 1, 0, 1925, 0),
(830, 1, 97, '5', 'server_url', 'Vada Hai Vada - Promise Day Special', 'http://www.kids2win.com/api/videosFile/GOKwM_vada-hai-vada-promise-day-special-fullscreen-hindi-love.mp4', '-', 'Promise Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/MkcLZ_vada-hai-vada-promise-day-special-fullscreen-hindi-love.jpg?h=270&drp=1', NULL, 0, 36913, 0, 0, 1, 0, 1926, 0),
(831, 1, 98, '5', 'server_url', 'Teddy Day Special', 'http://www.kids2win.com/api/videosFile/TYLyq_teddy-day-special-love-hindi-fullscreen.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9KYc3_teddy-day-special-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 34153, 0, 0, 1, 0, 1903, 0),
(832, 1, 98, '5', 'server_url', 'Teddy Bear To You', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309860/k6vh4fxmpx18lk532uts.mp4', '-', 'Teddy Day', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309863/qxw9cnzi5ompyxomfv5m.jpg', NULL, 195, 591, 34, 0, 1, 1, 1904, 0),
(833, 1, 98, '5', 'server_url', '10th Feb Happy Teddy Day', 'http://www.kids2win.com/api/videosFile/Lxpjt_10th-feb-happy-teddy-day-female-version-fullscreen-hindi.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/C7NKL_10th-feb-happy-teddy-day-female-version-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 21908, 0, 0, 1, 0, 1905, 0),
(834, 1, 98, '5', 'server_url', 'Dil Diyan Gallan - Teddy Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310334/bxmq0zgk49mwbbpfegzb.mp4', '-', 'Teddy Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310336/hbilwop1sy1wj0bmuk7l.jpg', NULL, 134, 689, 57, 0, 1, 1, 1907, 0),
(835, 1, 98, '5', 'server_url', 'Janu Janu Janu - Teddy Day Special', 'http://www.kids2win.com/api/videosFile/V1oxB_janu-janu-janu-teddy-day-special-hindi-fullscreen.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4HjBO_janu-janu-janu-teddy-day-special-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 11431, 0, 0, 1, 0, 1910, 0),
(836, 1, 98, '5', 'server_url', 'Teddy Day Special - Fullscreen', 'http://www.kids2win.com/api/videosFile/mJacL_teddy-day-special-fullscree-hindi.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/1agGF_teddy-day-special-fullscree-hindi.jpg?h=270&drp=1', NULL, 0, 6300, 0, 0, 1, 0, 4593, 0),
(837, 1, 98, '5', 'server_url', 'Tumhe Yuhi Chahenge - Teddy Day', 'http://www.kids2win.com/api/videosFile/DGELi_tumhe-yuhi-chahenge-female-version-teddy-day-hindi-fullscreen-animated.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/XS86q_tumhe-yuhi-chahenge-female-version-teddy-day-hindi-fullscreen-animated.jpg?h=270&drp=1', NULL, 0, 42256, 0, 0, 1, 0, 1911, 0),
(838, 1, 98, '5', 'server_url', 'Bhej Raha Hu Teddy - Teddy Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308540/inumbflsnkrawqse9quh.mp4', '-', 'Teddy Day', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308542/ffeovbyei1jtr1qe1nbz.jpg', NULL, 105, 786, 54, 0, 1, 1, 1912, 0),
(839, 1, 98, '5', 'server_url', 'Suno acha nahi hota', 'http://www.kids2win.com/api/videosFile/Jt1RY_suno-acha-nahi-hota-hindi-love.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8iCN1_suno-acha-nahi-hota-hindi-love.jpg?h=270&drp=1', NULL, 0, 15079, 0, 0, 1, 0, 790, 0),
(840, 1, 98, '5', 'server_url', 'Thoda Aur Thoda Aur', 'http://www.kids2win.com/api/videosFile/JkcSj_thoda-aur-thoda-aur-hindi.mp4', '-', 'Teddy Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/MPGQe_thoda-aur-thoda-aur-hindi.jpg?h=270&drp=1', NULL, 0, 15547, 0, 0, 1, 0, 1909, 0),
(841, 1, 98, '5', 'server_url', '10 Feb Happy Teddy Day', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401227/erciyfrgsgthgpvksmdt.mp4', '-', 'Teddy Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401230/zfazp61y3sahcios0tok.jpg', NULL, 162, 667, 76, 0, 1, 1, 1908, 0),
(842, 1, 99, '5', 'server_url', 'Chocolate Day Special', 'http://www.kids2win.com/api/videosFile/lAzbt_chocolate-day-special-hindi-square.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ZEG76_chocolate-day-special-hindi-square.jpg?h=270&drp=1', NULL, 0, 17562, 0, 0, 1, 0, 4578, 0),
(843, 1, 99, '5', 'server_url', 'Chocolate Day Special', 'http://www.kids2win.com/api/videosFile/LGB2c_chocolate-day-special-love-hindi.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SwUP4_chocolate-day-special-love-hindi.jpg?h=270&drp=1', NULL, 0, 13600, 0, 0, 1, 0, 1887, 0),
(844, 1, 99, '5', 'server_url', 'Chocolate Me Mithas Rahe Na Rahe', 'http://www.kids2win.com/api/videosFile/aJoSm_chocolate-me-mithas-rahe-na-rahe-chocolate-day-dailogs-hindi.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3F9qm_chocolate-me-mithas-rahe-na-rahe-chocolate-day-dailogs-hindi.jpg?h=270&drp=1', NULL, 0, 16295, 0, 0, 1, 0, 1891, 0),
(845, 1, 99, '5', 'server_url', 'Happy Chocolate Day 9 February', 'http://www.kids2win.com/api/videosFile/ihdAI_happy-chocolate-day-9-february-love-hindi.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6BkvO_happy-chocolate-day-9-february-love-hindi.jpg?h=270&drp=1', NULL, 0, 7832, 0, 0, 1, 0, 1892, 0),
(846, 1, 99, '5', 'server_url', 'Chocolate Day 9 February', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339424/k4l8rfit5xb7z0hrbk3d.mp4', '-', 'Chocolate Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609339427/nfaegtzemdlolfzwrjeh.jpg', NULL, 141, 778, 94, 0, 1, 1, 1895, 0),
(847, 1, 99, '5', 'server_url', 'Chocolate Day Special - Funny', 'http://www.kids2win.com/api/videosFile/aOVgW_chocolate-day-special-funny-hindi-fullscreen.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2MHDP_chocolate-day-special-funny-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 32103, 0, 0, 1, 0, 1896, 0),
(848, 1, 99, '5', 'server_url', 'Musafir - Tv Serial', 'http://www.kids2win.com/api/videosFile/7ipFH_musafir-love-hindi-chocolate-day-fullscreen-tv-serial.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/C7fi3_musafir-love-hindi-chocolate-day-fullscreen-tv-serial.jpg?h=270&drp=1', NULL, 0, 57546, 0, 0, 1, 0, 3851, 0),
(849, 1, 99, '5', 'server_url', 'Happy Chocolate Day', 'http://www.kids2win.com/api/videosFile/z4xYD_happy-chocolate-day-love-hindi.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/h9EfY_happy-chocolate-day-love-hindi.jpg?h=270&drp=1', NULL, 0, 11529, 0, 0, 1, 0, 1890, 0),
(850, 1, 99, '5', 'server_url', 'Pyar Ka Tyohar - Chocolate Day', 'http://www.kids2win.com/api/videosFile/M5T3J_pyar-ka-tyohar-chocolate-day-special-love-hindi-animated.mp4', '-', 'Chocolate Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/kFgqt_pyar-ka-tyohar-chocolate-day-special-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 19138, 0, 0, 1, 0, 1889, 0),
(851, 1, 99, '5', 'server_url', 'Chocolate Day 9 Feb', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339518/j8aficlrqs2zuolc40r0.mp4', '-', 'Chocolate Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609339520/qkn2qbxfxijmxbmzwpit.jpg', NULL, 157, 613, 12, 0, 1, 1, 1888, 0),
(852, 1, 100, '5', 'server_url', 'Excuse Me', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337370/wbmf7mw9vifuibhnglvy.mp4', '-', 'Propose Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/pAPoY_excuse-me-love-hindi-fullscreen-social.jpg?h=270&drp=1', NULL, 183, 777, 52, 0, 1, 1, 5275, 0),
(853, 1, 100, '5', 'server_url', 'Propose Day Status', 'http://www.kids2win.com/api/videosFile/uJal0_propose-day-status-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CZ1av_propose-day-status-hindi.jpg?h=270&drp=1', NULL, 0, 49790, 0, 0, 1, 0, 4572, 0),
(854, 1, 100, '5', 'server_url', 'Propose Day Status', 'http://www.kids2win.com/api/videosFile/JCohF_propose-day-status-hindi-animated.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/381bO_propose-day-status-hindi-animated.jpg?h=270&drp=1', NULL, 0, 60305, 0, 0, 1, 0, 4571, 0),
(855, 1, 100, '5', 'server_url', 'Propose Day Special', 'http://www.kids2win.com/api/videosFile/WrVok_propose-day-special-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bN0OD_propose-day-special-hindi.jpg?h=270&drp=1', NULL, 0, 18721, 0, 0, 1, 0, 4569, 0),
(856, 1, 100, '5', 'server_url', 'Propose Day Special', 'http://www.kids2win.com/api/videosFile/1Ho60_propose-day-special-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/geyLp_propose-day-special-hindi.jpg?h=270&drp=1', NULL, 0, 18291, 0, 0, 1, 0, 4568, 0),
(857, 1, 100, '5', 'server_url', 'Varun Dhawan - Propose Status', 'http://www.kids2win.com/api/videosFile/YUprN_varun-dhawan-propose-status-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jZo5B_varun-dhawan-propose-status-hindi.jpg?h=270&drp=1', NULL, 0, 17320, 0, 0, 1, 0, 4567, 0),
(858, 1, 100, '5', 'server_url', 'Taaron Ka Chamakta', 'http://www.kids2win.com/api/videosFile/M4xwP_taaron-ka-chamakta-love-hindi-prapose-day.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NQSsd_taaron-ka-chamakta-love-hindi-prapose-day.jpg?h=270&drp=1', NULL, 0, 23372, 0, 0, 1, 0, 4565, 0),
(860, 1, 100, '5', 'server_url', 'Happy Propose Day - Fullscreen', 'http://www.kids2win.com/api/videosFile/PtTMB_happy-propose-day-fullscreen-hindi-love.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ijUIc_happy-propose-day-fullscreen-hindi-love.jpg?h=270&drp=1', NULL, 0, 10586, 0, 0, 1, 0, 1879, 0),
(861, 1, 100, '5', 'server_url', 'Happy Propose Day!', 'http://www.kids2win.com/api/videosFile/qFy5M_happy-propose-day-love-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YZi4P_happy-propose-day-love-hindi.jpg?h=270&drp=1', NULL, 0, 12426, 0, 0, 1, 0, 1880, 0),
(862, 1, 100, '5', 'server_url', '8th Feb Propose Day Special', 'http://www.kids2win.com/api/videosFile/AZHhy_8th-feb-propose-day-special-fullscreen-valentine-love-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jGZrM_8th-feb-propose-day-special-fullscreen-valentine-love-hindi.jpg?h=270&drp=1', NULL, 0, 13988, 0, 0, 1, 0, 1881, 0),
(863, 1, 100, '5', 'server_url', 'Tu Hi Ye Mujhko Bata De - Propose Day', 'http://www.kids2win.com/api/videosFile/K128r_tu-hi-ye-mujhko-bataad-propose-day-valentine-love-hindi-fullscreen.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Gl3qj_tu-hi-ye-mujhko-bataad-propose-day-valentine-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 20772, 0, 0, 1, 0, 1882, 0),
(864, 1, 100, '5', 'server_url', 'Pal Pal Dil Ke Paas - Love', 'http://www.kids2win.com/api/videosFile/NhU5W_pal-pal-dil-ke-paas-love-propose-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OXmNK_pal-pal-dil-ke-paas-love-propose-hindi.jpg?h=270&drp=1', NULL, 0, 14399, 0, 0, 1, 0, 3019, 0),
(865, 1, 100, '5', 'server_url', 'Dil Mein Ho Tum', 'http://www.kids2win.com/api/videosFile/eACsq_propose-day-dil-mein-ho-tum-love-hindi.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8SoML_propose-day-dil-mein-ho-tum-love-hindi.jpg?h=270&drp=1', NULL, 0, 25457, 0, 0, 1, 0, 1897, 0),
(866, 1, 100, '5', 'server_url', 'Tu Jaane Na - Unplugged', 'http://www.kids2win.com/api/videosFile/XTMpx_tu-jaane-na-unplugged-love-hindi-prapose-day.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Xl8iP_tu-jaane-na-unplugged-love-hindi-prapose-day.jpg?h=270&drp=1', NULL, 0, 23970, 0, 0, 1, 0, 2149, 0),
(867, 1, 100, '5', 'server_url', 'Happy Propose Day 8 Feb', 'http://www.kids2win.com/api/videosFile/qkB51_happy-propose-day-8feb-hindi-fullscreen.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dQ8Mo_happy-propose-day-8feb-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 20553, 0, 0, 1, 0, 1883, 0),
(868, 1, 100, '5', 'server_url', 'Propose Day Special', 'http://www.kids2win.com/api/videosFile/wEVjm_propose-day-special-hindi-fullscreen.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/j9V1S_propose-day-special-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 7158, 0, 0, 1, 0, 1875, 0),
(869, 1, 100, '5', 'server_url', 'Smart Propose - Fullscreen', 'http://www.kids2win.com/api/videosFile/EnJde_smart-propose-day-love-hindi-fullscreen.mp4', '-', 'Propose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/BKpTf_smart-propose-day-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 11773, 0, 0, 1, 0, 1794, 0),
(870, 1, 101, '5', 'server_url', 'Pehla Pehla Pyar - Rose Day Special', 'http://www.kids2win.com/api/videosFile/KEQ20_pehla-pehla-pyar-rose-day-special-love-hindi.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UJOoD_pehla-pehla-pyar-rose-day-special-love-hindi.jpg?h=270&drp=1', NULL, 0, 33260, 0, 0, 1, 0, 1847, 0),
(871, 1, 101, '5', 'server_url', 'Happy Rose Day', 'http://www.kids2win.com/api/videosFile/1idZl_happy-rose-day-love-hindi-animated.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7ZBlX_happy-rose-day-love-hindi-animated.jpg?h=270&drp=1', NULL, 0, 16698, 0, 0, 1, 0, 1849, 0),
(872, 1, 101, '5', 'server_url', 'Adaayein Bhi Hain - Rose Day Special', 'http://www.kids2win.com/api/videosFile/9tAdJ_adaayein-bhi-hain-rose-day-love-hindi-fullscreen.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/w3M4a_adaayein-bhi-hain-rose-day-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 7778, 0, 0, 1, 0, 1854, 0),
(873, 1, 101, '5', 'server_url', 'Rose Day Special', 'http://www.kids2win.com/api/videosFile/mUVzs_rose-day-special-love-hindi.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nQlO0_rose-day-special-love-hindi.jpg?h=270&drp=1', NULL, 0, 10856, 0, 0, 1, 0, 1856, 0),
(874, 1, 101, '5', 'server_url', 'Bol Do Na Zara', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313324/xbgdjly8uuxl9bj7hi19.mp4', '-', 'Rose Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313327/hpagzep9en8rsmdtd4ue.jpg', NULL, 197, 586, 73, 0, 1, 1, 1851, 0),
(875, 1, 101, '5', 'server_url', 'Tujhko Jo Paaya', 'http://www.kids2win.com/api/videosFile/pbsfL_tujhko-jo-paaya-love-hindi-rose-day.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NWKM1_tujhko-jo-paaya-love-hindi-rose-day.jpg?h=270&drp=1', NULL, 0, 11510, 0, 0, 1, 0, 3086, 0),
(876, 1, 101, '5', 'server_url', 'Rose Day Special - Pyaar Hota Ja Raha', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401592/zschvzdh1x6riwmr5gi1.mp4', '-', 'Rose Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609401594/ule3lbugzswtrkgacowl.jpg', NULL, 165, 842, 19, 0, 1, 1, 1853, 0),
(877, 1, 101, '5', 'server_url', 'Dil Mein Ho Tum Aankhon Mein Tum', 'http://www.kids2win.com/api/videosFile/D6PXR_dil-mein-ho-tum-aankhon-mein-tum-love-hindi.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/N4KJV_dil-mein-ho-tum-aankhon-mein-tum-love-hindi.jpg?h=270&drp=1', NULL, 0, 9409, 0, 0, 1, 0, 1650, 0),
(878, 1, 101, '5', 'server_url', 'Dil Mein Ho Tum - Unplugged', 'http://www.kids2win.com/api/videosFile/tZE9F_dil-mein-ho-tum-unplugged-love-hindi-roseday.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IHjsR_dil-mein-ho-tum-unplugged-love-hindi-roseday.jpg?h=270&drp=1', NULL, 0, 11816, 0, 0, 1, 0, 1996, 0),
(879, 1, 101, '5', 'server_url', 'Meri Aashiqui - Rose Day Special', 'http://www.kids2win.com/api/videosFile/FsuLj_meri-aashiqui-rose-day-love-hindi-fullscreen.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/lSbKA_meri-aashiqui-rose-day-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 13119, 0, 0, 1, 0, 1857, 0),
(880, 1, 101, '5', 'server_url', 'Khali Khali Dil Ko - Fullscreen', 'http://www.kids2win.com/api/videosFile/Ddn3a_khali-khali-dil-ko-rose-day-fullscreen-love-hindi.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yJhBU_khali-khali-dil-ko-rose-day-fullscreen-love-hindi.jpg?h=270&drp=1', NULL, 0, 13225, 0, 0, 1, 0, 1855, 0),
(881, 1, 101, '5', 'server_url', 'Dil Me Ho Tum - Rose Day Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337655/vcpltonmyrx5wkwlj2lb.mp4', '-', 'Rose Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/WjpwE_dil-me-ho-tum-rose-day-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 101, 612, 37, 0, 1, 1, 1862, 0),
(882, 1, 101, '5', 'server_url', 'Kitani Hasi Ye Mulakate Hai', 'http://www.kids2win.com/api/videosFile/uyn3m_kitani-hasi-ye-mulakate-hai-rose-day-love-hindi.mp4', '-', 'Rose Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/VNrZx_kitani-hasi-ye-mulakate-hai-rose-day-love-hindi.jpg?h=270&drp=1', NULL, 0, 22597, 0, 0, 1, 0, 1861, 0),
(883, 1, 102, '5', 'server_url', 'Jay Dwarkadhish', 'http://www.kids2win.com/api/videosFile/PqW42_jay-dwarkadhish-h-f-dai-fri-15.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/kXo9h_jay-dwarkadhish-h-f-dai-fri-15.jpg?h=270&drp=1', NULL, 0, 46778, 0, 0, 1, 0, 8456, 0),
(884, 1, 102, '5', 'server_url', 'Mashup Friendship', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309704/aozhabvofemf3slewk8h.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309706/brokql7zpvbbmccrpcb1.jpg', NULL, 158, 838, 65, 0, 1, 1, 7456, 0),
(885, 1, 102, '5', 'server_url', 'Friendship Special Mashup', 'http://www.kids2win.com/api/videosFile/cCg9D_friendship-special-mashup-ly-h.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/P4nw1_friendship-special-mashup-ly-h.jpg?h=270&drp=1', NULL, 0, 50740, 0, 0, 1, 0, 7404, 0),
(886, 1, 102, '5', 'server_url', 'Friends Forever', 'http://www.kids2win.com/api/videosFile/39gs7_friends-forever-ly-h.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/WNEdD_friends-forever-ly-h.jpg?h=270&drp=1', NULL, 0, 52430, 0, 0, 1, 0, 7403, 0),
(887, 1, 102, '5', 'server_url', 'Friendship Day', 'http://www.kids2win.com/api/videosFile/0Y18E_friendship-day-ly-h.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2CHZM_friendship-day-ly-h.jpg?h=270&drp=1', NULL, 0, 59459, 0, 0, 1, 0, 7364, 0),
(888, 1, 102, '5', 'server_url', 'Teri Yaari', 'http://www.kids2win.com/api/videosFile/bf18p_teri-yaari-fri-h.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/RFcE7_teri-yaari-fri-h.jpg?h=270&drp=1', NULL, 0, 54504, 0, 0, 1, 0, 6595, 0),
(889, 1, 102, '5', 'server_url', 'Mera Bhai', 'http://www.kids2win.com/api/videosFile/mdGU5_mera-bhai-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4jRqL_mera-bhai-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 50914, 0, 0, 1, 0, 6078, 0),
(890, 1, 102, '5', 'server_url', 'Mera Bhai Tu Meri Jaan Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316097/w7kyryehqp8whhyclidg.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316099/prkh4dl9kzik2xved6xm.jpg', NULL, 160, 548, 58, 0, 1, 1, 2477, 0),
(891, 1, 102, '5', 'server_url', 'Mera Bhai Tu - Friendship', 'http://www.kids2win.com/api/videosFile/JyONG_mera-bhai-tu-animated-hindi-friendship-lyrical.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rXlvx_mera-bhai-tu-animated-hindi-friendship-lyrical.jpg?h=270&drp=1', NULL, 0, 53006, 0, 0, 1, 0, 2490, 0),
(892, 1, 102, '5', 'server_url', 'Mera Bhai Tu Meri Jaan Hai - Hindi', 'http://www.kids2win.com/api/videosFile/1vGaQ_mera-bhai-tu-meri-jaan-hai-lyrical-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/g1rDG_mera-bhai-tu-meri-jaan-hai-lyrical-hindi.jpg?h=270&drp=1', NULL, 0, 38290, 0, 0, 1, 0, 2498, 0),
(893, 1, 102, '5', 'server_url', 'Mera Bhai - Lyrical', 'http://www.kids2win.com/api/videosFile/Q9uVr_mera-bhai-lyrical-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rCqH9_mera-bhai-lyrical-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 31262, 0, 0, 1, 0, 5577, 0),
(894, 1, 102, '5', 'server_url', 'Mera Bhai', 'http://www.kids2win.com/api/videosFile/bVgp4_mera-bhai-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/kmXYQ_mera-bhai-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 26642, 0, 0, 1, 0, 5576, 0),
(895, 1, 102, '5', 'server_url', 'Faaslon Mein', 'http://www.kids2win.com/api/videosFile/YJ6ua_faaslon-mein-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DtUX4_faaslon-mein-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 37736, 0, 0, 1, 0, 5021, 0),
(896, 1, 102, '5', 'server_url', 'Mere Liye Tum Kaafi Ho - Lyrical', 'http://www.kids2win.com/api/videosFile/rH7LN_mere-liye-tum-kaafi-ho-lyrical-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cD12q_mere-liye-tum-kaafi-ho-lyrical-hindi.jpg?h=270&drp=1', NULL, 0, 33189, 0, 0, 1, 0, 4982, 0),
(897, 1, 102, '5', 'server_url', 'Tera Yaar Hoon Main', 'http://www.kids2win.com/api/videosFile/xTKSe_tera-yaar-hoon-main-friendship-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/daBng_tera-yaar-hoon-main-friendship-hindi.jpg?h=270&drp=1', NULL, 0, 29490, 0, 0, 1, 0, 4958, 0),
(898, 1, 102, '5', 'server_url', 'Friendship Mashup', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353193/ckgh1lnrxresoe55seog.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609353195/pompirimy8yeug8dpb5i.jpg', NULL, 132, 562, 77, 0, 1, 1, 4135, 0),
(899, 1, 102, '5', 'server_url', 'Tera Yaar Hoon Main - Female Version', 'http://www.kids2win.com/api/videosFile/HCBDM_tera-yaar-hoon-main-female-version-freinship-hindi-lyrical.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Q25ba_tera-yaar-hoon-main-female-version-freinship-hindi-lyrical.jpg?h=270&drp=1', NULL, 0, 56030, 0, 0, 1, 0, 3039, 0),
(900, 1, 102, '5', 'server_url', 'Yaara Teri Yaari - Friendship', 'http://www.kids2win.com/api/videosFile/a6oFt_yaara-teri-yaari-friendship-hindi-animated.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wAmBx_yaara-teri-yaari-friendship-hindi-animated.jpg?h=270&drp=1', NULL, 0, 26695, 0, 0, 1, 0, 3018, 0),
(901, 1, 102, '5', 'server_url', 'Tera Yaar Hoon Main - Friendship', 'http://www.kids2win.com/api/videosFile/wJiZT_tera-yaar-hoon-main-friendship-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9knOq_tera-yaar-hoon-main-friendship-hindi.jpg?h=270&drp=1', NULL, 0, 17395, 0, 0, 1, 0, 2598, 0),
(902, 1, 102, '5', 'server_url', 'Dosti Ka Malab - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317213/izkrvfgzbqxfeoahftt8.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317215/pdjop1bjpzgfyo28jqth.jpg', NULL, 146, 465, 28, 0, 1, 1, 2596, 0),
(903, 1, 102, '5', 'server_url', 'Happy Friendship Day - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271003/b9bzqrrl0q0cmnvci2ge.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271006/ag70hq5rdz43v10ydqd1.jpg', NULL, 159, 584, 37, 0, 1, 1, 0, 0),
(904, 1, 102, '5', 'server_url', 'Mere Dil Ki Ye Dua Hai - Friendship', 'http://www.kids2win.com/api/videosFile/wdyvG_mere-dil-ki-ye-dua-hai-friendship-hindi-animated-lyrical.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TBjty_mere-dil-ki-ye-dua-hai-friendship-hindi-animated-lyrical.jpg?h=270&drp=1', NULL, 0, 23935, 0, 0, 1, 0, 2593, 0),
(905, 1, 102, '5', 'server_url', 'Tera Yaar Hoon Main', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316956/vrolg1yytgeflmam30ry.mp4', '-', 'Friendship Day', 'Portrait', 'http://www.kids2win.com/api/thumbImage/NocO4_tera-yaar-hoon-main-animated-hindi-friendship.jpg?h=270&drp=1', NULL, 155, 504, 81, 0, 1, 1, 2592, 0),
(906, 1, 102, '5', 'server_url', 'Yeh Dosti Hum Nahi Todenge', 'http://www.kids2win.com/api/videosFile/5IOhR_yeh-dosti-hum-nahi-todenge-animated-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hs5VQ_yeh-dosti-hum-nahi-todenge-animated-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 28550, 0, 0, 1, 0, 2590, 0),
(907, 1, 102, '5', 'server_url', 'Boys Friendship Status', 'http://www.kids2win.com/api/videosFile/bxje9_boys-friendship-status-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/s4goS_boys-friendship-status-hindi.jpg?h=270&drp=1', NULL, 0, 11477, 0, 0, 1, 0, 2589, 0),
(908, 1, 102, '5', 'server_url', 'Tera Yaar Hoon Main - Hindi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308018/fo0wzllwixx2xsoyk6wn.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308021/dlllkgbxmyvllqqnimwc.jpg', NULL, 153, 501, 21, 0, 1, 1, 2588, 0),
(909, 1, 102, '5', 'server_url', 'Best Friendship Status - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609272470/qkpnnviecnqjcndjkdhj.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609272472/qaaas2vvnfnc2215skps.jpg', NULL, 183, 888, 78, 0, 1, 1, 2587, 0),
(910, 1, 102, '5', 'server_url', 'Best Friendship Status', 'http://www.kids2win.com/api/videosFile/mWsMb_best-friendship-status-hindi-animated-lyrical.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/UMRs8_best-friendship-status-hindi-animated-lyrical.jpg?h=270&drp=1', NULL, 0, 11931, 0, 0, 1, 0, 2586, 0),
(911, 1, 102, '5', 'server_url', 'Friendship Day Status For Girls', 'http://www.kids2win.com/api/videosFile/PDu6e_friendship-day-status-for-girls-hindi-animated-lyrical.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/G2IXx_friendship-day-status-for-girls-hindi-animated-lyrical.jpg?h=270&drp=1', NULL, 0, 16664, 0, 0, 1, 0, 2584, 0),
(912, 1, 102, '5', 'server_url', 'Friendship Day Special - Shayari', 'http://www.kids2win.com/api/videosFile/atwvk_friendship-day-special-status-shayari-hindi-animated.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QhFMT_friendship-day-special-status-shayari-hindi-animated.jpg?h=270&drp=1', NULL, 0, 3751, 0, 0, 1, 0, 2583, 0),
(913, 1, 102, '5', 'server_url', 'Mere Dil Ki Ye Dua Hai - Friendship', 'http://www.kids2win.com/api/videosFile/kBRNI_mere-dil-ki-ye-dua-hai-hindi-friendship-animated.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/tr7jJ_mere-dil-ki-ye-dua-hai-hindi-friendship-animated.jpg?h=270&drp=1', NULL, 0, 32298, 0, 0, 1, 0, 2582, 0),
(914, 1, 102, '5', 'server_url', 'Yeh Dosti Hum Nahi Todenge', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609333745/dv65uxuaf5ivevmenei3.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609333748/vy3heuoy5gk7grnc0f9k.jpg', NULL, 178, 809, 44, 0, 1, 1, 1489, 0),
(915, 1, 102, '5', 'server_url', 'Yeh dosti hum nahi todenge', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403076/pamwykgobd2ka4x7sedw.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403078/kiiahb0ahslig8ewrt6b.jpg', NULL, 109, 564, 29, 0, 1, 1, 747, 0),
(916, 1, 102, '5', 'server_url', 'Mere dil ki ye dua hai', 'http://www.kids2win.com/api/videosFile/i926V_mere-dil-ki-ye-dua-hai-female-version-love-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/jKm1d_mere-dil-ki-ye-dua-hai-female-version-love-hindi.jpg?h=270&drp=1', NULL, 1, 13151, 0, 0, 1, 0, 1078, 0),
(917, 1, 102, '5', 'server_url', 'Tere jaisa yaar kahan', 'http://www.kids2win.com/api/videosFile/RfCDo_tere-jaisa-yaar-kahan-friendship-hindi-animated.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/uJzpN_tere-jaisa-yaar-kahan-friendship-hindi-animated.jpg?h=270&drp=1', NULL, 0, 15195, 0, 0, 1, 0, 623, 0),
(918, 1, 102, '5', 'server_url', 'Mera Bhai Tu Meri Jaan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337413/rpbljofn6glxlbwdtlzt.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337415/n8stcdpwzxxdewiebemn.jpg', NULL, 192, 833, 23, 0, 1, 1, 2487, 0),
(919, 1, 102, '5', 'server_url', 'Mera Bhai Tu - Lyrical', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609270999/efozernxsgqrn60iikhy.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271001/girv9p3fugeuctsxvm1l.jpg', NULL, 173, 466, 84, 0, 1, 1, 0, 0),
(920, 1, 102, '5', 'server_url', 'Mera Bhai Tu', 'http://www.kids2win.com/api/videosFile/9lNeU_mera-bhai-tu-friendship-animated-hindi.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/H3Fbv_mera-bhai-tu-friendship-animated-hindi.jpg?h=270&drp=1', NULL, 0, 8513, 0, 0, 1, 0, 2479, 0),
(921, 1, 102, '5', 'server_url', 'Eid Special - Friendship', 'http://www.kids2win.com/api/videosFile/JYpVf_eid-special-friendship-hindi-fullscreen.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/H5NGp_eid-special-friendship-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 6240, 0, 0, 1, 0, 2307, 0),
(922, 1, 102, '5', 'server_url', 'Zindagi Jeene Ka Maza Tab Aata Hai Dost', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403320/t0oyonggmovrz3x0lgje.mp4', '-', 'Friendship Day', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403323/x0lnbpih0xjnhqmailcs.jpg', NULL, 174, 652, 45, 0, 1, 1, 2222, 0),
(923, 1, 102, '5', 'server_url', 'Kaun Kehta Hai Dosti', 'http://www.kids2win.com/api/videosFile/oR6mf_kaun-kehta-hai-dosti-hindi-friendship.mp4', '-', 'Friendship Day', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IAx6W_kaun-kehta-hai-dosti-hindi-friendship.jpg?h=270&drp=1', NULL, 0, 19783, 0, 0, 1, 0, 1965, 0),
(924, 1, 103, '5', 'server_url', 'Chal Ghar Chalen', 'http://www.kids2win.com/api/videosFile/GlIsM_chal-ghar-chalen-sad-hindi-emotional-fullscreen.mp4', '-', 'Emotional', 'Landscape', 'http://www.kids2win.com/api/thumbImage/dlF50_chal-ghar-chalen-sad-hindi-emotional-fullscreen.jpg?h=270&drp=1', NULL, 0, 185866, 0, 0, 1, 0, 5421, 0),
(925, 1, 103, '5', 'server_url', 'Phir Bhi Tumko Chaahunga', 'http://www.kids2win.com/api/videosFile/oHmET_phir-bhi-tumko-chaahunga-emotional-hindi-sad.mp4', '-', 'Emotional', 'Landscape', 'http://www.kids2win.com/api/thumbImage/QX5UC_phir-bhi-tumko-chaahunga-emotional-hindi-sad.jpg?h=270&drp=1', NULL, 0, 92621, 0, 0, 1, 0, 5194, 0),
(926, 1, 103, '5', 'server_url', 'Desh Mere Tu Jeeta Rahe - Fullscreen', 'http://www.kids2win.com/api/videosFile/KBPvO_desh-mere-tu-jeeta-rahe-india-hindi-fullscreen.mp4', '-', 'Emotional', 'Landscape', 'http://www.kids2win.com/api/thumbImage/krCGI_desh-mere-tu-jeeta-rahe-india-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 51417, 0, 0, 1, 0, 2685, 0),
(927, 1, 104, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/t9awf_teri-aankhon-mein-rain-l-h-.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AuYWM_teri-aankhon-mein-rain-l-h-.jpg?h=270&drp=1', NULL, 0, 62109, 0, 0, 1, 0, 8316, 0),
(928, 1, 104, '5', 'server_url', 'Teri Aankhon Mein', 'http://www.kids2win.com/api/videosFile/tm9jo_teri-aankhon-mein-l-h-rain.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KgJn9_teri-aankhon-mein-l-h-rain.jpg?h=270&drp=1', NULL, 0, 54614, 0, 0, 1, 0, 8309, 0),
(929, 1, 104, '5', 'server_url', 'Jaise Baarish Karde Tar', 'http://www.kids2win.com/api/videosFile/QnKqj_jaise-baarish-karde-tar-h-sq-rai.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CI4hV_jaise-baarish-karde-tar-h-sq-rai.jpg?h=270&drp=1', NULL, 0, 51979, 0, 0, 1, 0, 7932, 0),
(930, 1, 104, '5', 'server_url', 'Waterfall', 'http://www.kids2win.com/api/videosFile/uoWIM_waterfall-rainy-f-h-15.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/byL0A_waterfall-rainy-f-h-15.jpg?h=270&drp=1', NULL, 0, 51163, 0, 0, 1, 0, 7931, 0),
(931, 1, 104, '5', 'server_url', 'Kesi He Ye Duriya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319634/bafp5d4oi5esxsctey8n.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319636/hzrrqlz8ikioa6etbric.jpg', NULL, 161, 616, 32, 0, 1, 1, 7721, 0),
(932, 1, 104, '5', 'server_url', 'Ke Suna Hai Barish Me', 'http://www.kids2win.com/api/videosFile/BdXGj_ke-suna-hai-barish-me-l-h-shy-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/b73Wo_ke-suna-hai-barish-me-l-h-shy-rainy.jpg?h=270&drp=1', NULL, 0, 82954, 0, 0, 1, 0, 7535, 0),
(933, 1, 104, '5', 'server_url', 'Mitti Di Khushboo', 'http://www.kids2win.com/api/videosFile/t3BeT_mitti-di-khushboo-l-h-f-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IFNvQ_mitti-di-khushboo-l-h-f-rainy.jpg?h=270&drp=1', NULL, 0, 60895, 0, 0, 1, 0, 7532, 0),
(934, 1, 104, '5', 'server_url', 'Toota Jo Kabhi Tara', 'http://www.kids2win.com/api/videosFile/YSo7V_toota-jo-kabhi-tara-rainy-l-h-f.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0hA8E_toota-jo-kabhi-tara-rainy-l-h-f.jpg?h=270&drp=1', NULL, 0, 81877, 0, 0, 1, 0, 7451, 0),
(935, 1, 104, '5', 'server_url', 'Rainy Special', 'http://www.kids2win.com/api/videosFile/DYQKX_humnava-l-f-rainy-h.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/SBPoT_humnava-l-f-rainy-h.jpg?h=270&drp=1', NULL, 0, 66190, 0, 0, 1, 0, 7441, 0),
(936, 1, 104, '5', 'server_url', 'Brother Sister Emotional', 'http://www.kids2win.com/api/videosFile/yUqZr_brother-sister-emotional-rakhi-h.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Wsh94_brother-sister-emotional-rakhi-h.jpg?h=270&drp=1', NULL, 0, 38787, 0, 0, 1, 0, 7410, 0),
(937, 1, 104, '5', 'server_url', 'Piya Aaye Na', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338827/kgyuexixhyjok6hxlnxt.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338830/t7qhtqr0wqhnya8wtfhd.jpg', NULL, 194, 852, 71, 0, 1, 1, 7409, 0),
(938, 1, 104, '5', 'server_url', 'Boond Boond', 'http://www.kids2win.com/api/videosFile/R7QlU_boond-boond-h-l-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/i9WP1_boond-boond-h-l-rainy.jpg?h=270&drp=1', NULL, 0, 96932, 0, 0, 1, 0, 7086, 0),
(939, 1, 104, '5', 'server_url', 'Baarish Lete Aana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609340435/fqqahjjfr2orbtezv6x6.mp4', '-', 'Rainy Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/cFwH7_baarish-lete-aana-s-h-rainy.jpg?h=270&drp=1', NULL, 100, 569, 91, 0, 1, 1, 7075, 0),
(940, 1, 104, '5', 'server_url', 'Woh Kisi Aur', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337103/z0ifabk02uhr0a01tfta.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337106/eogjwg9prf7d1uw7qjn3.jpg', NULL, 128, 876, 40, 0, 1, 1, 6936, 0),
(941, 1, 104, '5', 'server_url', 'Hawaaon Mein Bahenge', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352122/zzsfg7ujsmvghggohckf.mp4', '-', 'Rainy Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/qflpV_hawaaon-mein-bahenge-rainy-h-f-l.jpg?h=270&drp=1', NULL, 124, 413, 71, 0, 1, 1, 6921, 0),
(942, 1, 104, '5', 'server_url', 'Kaun Mera', 'http://www.kids2win.com/api/videosFile/SVt3c_kaun-mera-l-h-f-rainy-15.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/iunf9_kaun-mera-l-h-f-rainy-15.jpg?h=270&drp=1', NULL, 0, 149459, 0, 0, 1, 0, 6726, 0),
(943, 1, 104, '5', 'server_url', 'Sawan Aaya Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316116/qyndjcqwrxxgtbw0ddmo.mp4', '-', 'Rainy Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/L1FYG_sawan-aaya-hai-l-h-f-15-rainy.jpg?h=270&drp=1', NULL, 160, 532, 62, 0, 1, 1, 6712, 0),
(944, 1, 104, '5', 'server_url', 'Main Rahoon Ya Na Rahoon', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609400622/mychbjyhdfkucykxfiw2.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609400624/tej6jydc6vaww2izboos.jpg', NULL, 134, 689, 27, 0, 1, 1, 6611, 0),
(945, 1, 104, '5', 'server_url', 'Ye Musam Ki Barish', 'http://www.kids2win.com/api/videosFile/MOcIV_ye-musam-ki-barish-l-h-rainy-ly.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/By6uU_ye-musam-ki-barish-l-h-rainy-ly.jpg?h=270&drp=1', NULL, 0, 101374, 0, 0, 1, 0, 6418, 0),
(946, 1, 104, '5', 'server_url', 'Kisi Roz Baarish Jo Aaye', 'http://www.kids2win.com/api/videosFile/H7S9c_kisi-roz-baarish-jo-aaye-l-h-ly-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/EVD97_kisi-roz-baarish-jo-aaye-l-h-ly-rainy.jpg?h=270&drp=1', NULL, 0, 41228, 0, 0, 1, 0, 6408, 0),
(947, 1, 104, '5', 'server_url', 'Ye Musam Ki Barish', 'http://www.kids2win.com/api/videosFile/NOXvk_ye-musam-ki-barish-l-h-rainy-anim.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/mDiVa_ye-musam-ki-barish-l-h-rainy-anim.jpg?h=270&drp=1', NULL, 0, 74930, 0, 0, 1, 0, 6402, 0),
(948, 1, 104, '5', 'server_url', 'Kisi Roz Baarish Jo Aaye', 'http://www.kids2win.com/api/videosFile/RXh7v_kisi-roz-baarish-jo-aaye-l-h-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hZolE_kisi-roz-baarish-jo-aaye-l-h-rainy.jpg?h=270&drp=1', NULL, 0, 47995, 0, 0, 1, 0, 6397, 0),
(949, 1, 104, '5', 'server_url', 'Mausam Ki Barish', 'http://www.kids2win.com/api/videosFile/vGMYP_mausam-ki-barish-love-hindi-animated-rain-special.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/VYN6j_mausam-ki-barish-love-hindi-animated-rain-special.jpg?h=270&drp=1', NULL, 0, 82703, 1, 0, 1, 0, 2324, 0),
(950, 1, 104, '5', 'server_url', 'Sawan Aaya Hai', 'http://www.kids2win.com/api/videosFile/dnRMD_sawan-aaya-hai-love-hindi-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/gouvl_sawan-aaya-hai-love-hindi-rainy.jpg?h=270&drp=1', NULL, 0, 38019, 0, 0, 1, 0, 5817, 0),
(951, 1, 104, '5', 'server_url', 'Tere Sang Yaara', 'http://www.kids2win.com/api/videosFile/K64rX_tere-sang-yaara-love-hindi-square-rain.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8AdyX_tere-sang-yaara-love-hindi-square-rain.jpg?h=270&drp=1', NULL, 0, 48360, 0, 0, 1, 0, 5538, 0),
(952, 1, 104, '5', 'server_url', 'Is Tarah Aashiqui Ka - Unplugged', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609321120/frkgz32rvwh2yhjoqsdh.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609321122/kexlwsu55hcercg5030m.jpg', NULL, 144, 746, 30, 0, 1, 1, 2070, 0),
(953, 1, 104, '5', 'server_url', 'Main Ishq Uska', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609399265/ra7pjyco3f6qjjajhuoa.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609399267/gyczsgyap6emfeutnryv.jpg', NULL, 141, 687, 64, 0, 1, 1, 4486, 0),
(954, 1, 104, '5', 'server_url', 'Woh Baarishein', 'http://www.kids2win.com/api/videosFile/6Sjzw_woh-baarishein-sad-fullscreen-hindi-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/4YETN_woh-baarishein-sad-fullscreen-hindi-rainy.jpg?h=270&drp=1', NULL, 0, 42275, 0, 0, 1, 0, 3848, 0),
(955, 1, 104, '5', 'server_url', 'Love Dialogue', 'http://www.kids2win.com/api/videosFile/t3N9B_love-dialogue-love-hindi-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Dmei4_love-dialogue-love-hindi-rainy.jpg?h=270&drp=1', NULL, 0, 88395, 0, 0, 1, 0, 3651, 0),
(956, 1, 104, '5', 'server_url', 'Sawan Aaya Hai', 'http://www.kids2win.com/api/videosFile/abCnM_sawan-aaya-hai-tv-serial-fullscreen-hindi-love-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YSNFQ_sawan-aaya-hai-tv-serial-fullscreen-hindi-love-rainy.jpg?h=270&drp=1', NULL, 0, 79163, 0, 0, 1, 0, 3568, 0),
(957, 1, 104, '5', 'server_url', 'Pachtaoge', 'http://www.kids2win.com/api/videosFile/JcAZu_pachtaoge-rainy-sad-hindi-fullscreen.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hZPD5_pachtaoge-rainy-sad-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 16804, 0, 0, 1, 0, 3152, 0),
(958, 1, 104, '5', 'server_url', 'Tum Ho', 'http://www.kids2win.com/api/videosFile/6bHkP_tum-ho-love-hindi-rainy-fullscreen.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/67HOs_tum-ho-love-hindi-rainy-fullscreen.jpg?h=270&drp=1', NULL, 0, 22898, 0, 0, 1, 0, 3033, 0),
(959, 1, 104, '5', 'server_url', 'Tum Ho Paas Mere', 'http://www.kids2win.com/api/videosFile/5MfGN_tum-ho-paas-mere-sad-hindi.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/szDmZ_tum-ho-paas-mere-sad-hindi.jpg?h=270&drp=1', NULL, 0, 14762, 0, 0, 1, 0, 1463, 0),
(960, 1, 104, '5', 'server_url', 'Tera Hua - Atif Aslam', 'http://www.kids2win.com/api/videosFile/UXBa6_tera-hua-atif-aslam-love-hindi-lyrical-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/R8Kfd_tera-hua-atif-aslam-love-hindi-lyrical-rainy.jpg?h=270&drp=1', NULL, 0, 32122, 0, 0, 1, 0, 2693, 0),
(961, 1, 104, '5', 'server_url', 'Sang Tere Paniyon Sa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338624/w9wblvffjqsckbfdyyil.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609338627/qtkwqqiw8wfk5yuxfuc8.jpg', NULL, 186, 562, 30, 0, 1, 1, 2682, 0),
(962, 1, 104, '5', 'server_url', 'Tum Hi Ho - Tv Serial', 'http://www.kids2win.com/api/videosFile/t1XNJ_Tum-Hi-Ho-tv-serial-rainy-hindi-love.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/wJuAW_Tum-Hi-Ho-tv-serial-rainy-hindi-love.jpg?h=270&drp=1', NULL, 0, 42240, 0, 0, 1, 0, 2645, 0),
(963, 1, 104, '5', 'server_url', 'Ye Musam Ki Barish - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609321171/yss1bz8uuzibjudg4mpy.mp4', '-', 'Rainy Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/D9egp_ye-musam-ki-barish-fullscreen-rainy-love.jpg?h=270&drp=1', NULL, 123, 888, 15, 0, 1, 1, 2574, 0),
(964, 1, 104, '5', 'server_url', 'Itni Si Baat Hain - Love', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309213/d5pd55esmuncfuzgpyo0.mp4', '-', 'Rainy Special', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309215/j6urk5i2dikgv20wcpo4.jpg', NULL, 1, 524, 23, 0, 1, 1, 2563, 0),
(965, 1, 104, '5', 'server_url', 'Hai Khwab Tu Tabeer - Love', 'http://www.kids2win.com/api/videosFile/oubva_hai-khwab-tu-tabeer-love-hindi-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FZ0UV_hai-khwab-tu-tabeer-love-hindi-rainy.jpg?h=270&drp=1', NULL, 0, 13677, 0, 0, 1, 0, 2555, 0),
(966, 1, 104, '5', 'server_url', 'Best Birthday Ever - Fullscreen', 'http://www.kids2win.com/api/videosFile/zltgj_best-birthday-ever-fullscreen.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rl24B_best-birthday-ever-fullscreen.jpg?h=270&drp=1', NULL, 0, 5468, 0, 0, 1, 0, 2518, 0),
(967, 1, 104, '5', 'server_url', 'Jadon Ambara Barasya Paani', 'http://www.kids2win.com/api/videosFile/CMRTU_jadon-ambara-barasya-paani-love-hindi-rainy-lyrical.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sax50_jadon-ambara-barasya-paani-love-hindi-rainy-lyrical.jpg?h=270&drp=1', NULL, 0, 12642, 0, 0, 1, 0, 2509, 0),
(968, 1, 104, '5', 'server_url', 'Idhar Chali Main Udhar Chali', 'http://www.kids2win.com/api/videosFile/A0jTF_idhar-chala-main-udhar-chali-funny-hindi-fullscreen-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/h2OC4_idhar-chala-main-udhar-chali-funny-hindi-fullscreen-rainy.jpg?h=270&drp=1', NULL, 0, 17776, 0, 0, 1, 0, 2431, 0),
(969, 1, 104, '5', 'server_url', 'O Mehendi Pyaar Wali', 'http://www.kids2win.com/api/videosFile/hGEPq_mehndi-pyar-wali-hatho-pe-lgaogi-sad-hindi-rainy.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/pIsvV_mehndi-pyar-wali-hatho-pe-lgaogi-sad-hindi-rainy.jpg?h=270&drp=1', NULL, 0, 12885, 0, 0, 1, 0, 2429, 0),
(970, 1, 104, '5', 'server_url', 'Barish Song Status', 'http://www.kids2win.com/api/videosFile/ZPNTV_barish-song-status-love-hindi-rain-special.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/j9qRW_barish-song-status-love-hindi-rain-special.jpg?h=270&drp=1', NULL, 0, 25642, 0, 0, 1, 0, 2320, 0),
(971, 1, 104, '5', 'server_url', 'Barsaat Ke Din Aaye', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319692/iikotllzvysqzxt8dwza.mp4', '-', 'Rainy Special', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319696/ovwqk8vx5ocwzd6zuxxf.jpg', NULL, 198, 600, 22, 0, 1, 1, 2319, 0),
(972, 1, 104, '5', 'server_url', 'Arziyaan - Fullscreen', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315796/ueyqp3dcarxnhnidn8ry.mp4', '-', 'Rainy Special', 'Portrait', 'http://www.kids2win.com/api/thumbImage/vk7aV_arziyaan-love-hindi-fullscreen.jpg?h=270&drp=1', NULL, 103, 786, 57, 0, 1, 1, 2241, 0),
(973, 1, 104, '5', 'server_url', 'Bulave Tuze Yaar Aaj Meri Galiya - Female Version', 'http://www.kids2win.com/api/videosFile/fY8jE_bulave-tuze-yaar-aaj-meri-galiya-female-version-love-hindi.mp4', '-', 'Rainy Special', 'Landscape', 'http://www.kids2win.com/api/thumbImage/0godz_bulave-tuze-yaar-aaj-meri-galiya-female-version-love-hindi.jpg?h=270&drp=1', NULL, 0, 16361, 0, 0, 1, 0, 2235, 0),
(974, 1, 105, '5', 'server_url', 'Happy Makar Sankranti!', 'http://www.kids2win.com/api/videosFile/6iEAl_happy-makar-sankranti-animated-hindi-festival.mp4', '-', 'Makar Sankranti', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7pTqC_happy-makar-sankranti-animated-hindi-festival.jpg?h=270&drp=1', NULL, 0, 10690, 0, 0, 1, 0, 4263, 0),
(975, 1, 105, '5', 'server_url', 'Happy Makar Sankranti - Lyrical', 'http://www.kids2win.com/api/videosFile/7yFQd_happy-makar-sankranti-hindi-lyrical-festival.mp4', '-', 'Makar Sankranti', 'Landscape', 'http://www.kids2win.com/api/thumbImage/7MGA4_happy-makar-sankranti-hindi-lyrical-festival.jpg?h=270&drp=1', NULL, 0, 7980, 0, 0, 1, 0, 4262, 0),
(976, 1, 105, '5', 'server_url', 'Uttarayan Special - Makar Sankranti', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313923/bjti5a2ohiav6cjbfk89.mp4', '-', 'Makar Sankranti', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313925/j7xacqkm4zrnwdwhflvh.jpg', NULL, 119, 583, 66, 0, 1, 1, 4261, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(977, 1, 105, '5', 'server_url', 'Makar Sankranti - Uttarayan Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403394/s1axyftpqxpwm1f0ucyd.mp4', '-', 'Makar Sankranti', 'Portrait', 'http://www.kids2win.com/api/thumbImage/MX5qT_makar-sankranti-uttarayan-special-hindi-fullscreen-festival.jpg?h=270&drp=1', NULL, 102, 563, 24, 0, 1, 1, 4211, 0),
(978, 1, 105, '5', 'server_url', 'Makar Sankranti - Festival', 'http://www.kids2win.com/api/videosFile/hOBai_makar-sankranti-hindi-festival.mp4', '-', 'Makar Sankranti', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NkR2i_makar-sankranti-hindi-festival.jpg?h=270&drp=1', NULL, 0, 6083, 0, 0, 1, 0, 4203, 0),
(979, 1, 107, '5', 'server_url', 'Guru Purnima Special', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402107/fhzwq4hpnpqbpsfujd7l.mp4', '-', 'Guru Purnima', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609402109/izchxpx4ubui0vwslkfb.jpg', NULL, 136, 493, 79, 0, 1, 1, 6940, 0),
(980, 1, 107, '5', 'server_url', 'Guru Purnima Wishes', 'http://www.kids2win.com/api/videosFile/J3EjZ_guru-purnima-wishes.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hcgDJ_guru-purnima-wishes.jpg?h=270&drp=1', NULL, 0, 18107, 0, 0, 1, 0, 6919, 0),
(981, 1, 107, '5', 'server_url', 'Happy Guru Purnima', 'http://www.kids2win.com/api/videosFile/QfBY0_happy-guru-purnima-h-fes.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oY19H_happy-guru-purnima-h-fes.jpg?h=270&drp=1', NULL, 0, 17167, 0, 0, 1, 0, 6918, 0),
(982, 1, 107, '5', 'server_url', 'Guru Purnima Special', 'http://www.kids2win.com/api/videosFile/paAT9_guru-purnima-special-f-h.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/8ZX3S_guru-purnima-special-f-h.jpg?h=270&drp=1', NULL, 0, 13023, 0, 0, 1, 0, 6917, 0),
(983, 1, 107, '5', 'server_url', 'Guru Purnima - Fullscreen', 'http://www.kids2win.com/api/videosFile/ICFnA_guru-purnima-fullscreen-status-hindi-god.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/k0FwK_guru-purnima-fullscreen-status-hindi-god.jpg?h=270&drp=1', NULL, 0, 12216, 0, 0, 1, 0, 2524, 0),
(984, 1, 107, '5', 'server_url', 'Mere Sai - Guru Purnima Special', 'http://www.kids2win.com/api/videosFile/EQsmb_mere-sai-guru-purnima-special-hindi-god-lyrical.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/XZ4NK_mere-sai-guru-purnima-special-hindi-god-lyrical.jpg?h=270&drp=1', NULL, 0, 16586, 0, 0, 1, 0, 2523, 0),
(985, 1, 107, '5', 'server_url', 'Guru Purnima 2019 Status', 'http://www.kids2win.com/api/videosFile/QGr2e_guru-purnima-2019-status-hindi.mp4', '-', 'Guru Purnima', 'Landscape', 'http://www.kids2win.com/api/thumbImage/yKzel_guru-purnima-2019-status-hindi.jpg?h=270&drp=1', NULL, 0, 5990, 0, 0, 1, 0, 2521, 0),
(986, 1, 108, '5', 'server_url', 'Jab Jeb Me Money Ho Na', 'http://www.kids2win.com/api/videosFile/eOtPQ_jab-jeb-money-ho-na-h-sq-dai-atti.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/orihP_jab-jeb-money-ho-na-h-sq-dai-atti.jpg?h=270&drp=1', NULL, 0, 39898, 0, 0, 1, 0, 8388, 0),
(987, 1, 108, '5', 'server_url', 'Carryminati Special', 'http://www.kids2win.com/api/videosFile/i4TQp_carryminati-special-status-ly-ati-h.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/oyjFl_carryminati-special-status-ly-ati-h.jpg?h=270&drp=1', NULL, 0, 139464, 0, 0, 1, 0, 6650, 0),
(988, 1, 108, '5', 'server_url', 'Bade Logo Main', 'http://www.kids2win.com/api/videosFile/1wxvz_bade-logo-main-h-s-shy.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/9HcF4_bade-logo-main-h-s-shy.jpg?h=270&drp=1', NULL, 1, 75244, 1, 0, 1, 0, 6493, 0),
(989, 1, 108, '5', 'server_url', 'Mere Samne - Attitude', 'http://www.kids2win.com/api/videosFile/ha2L1_mere-samne-h-d-ati.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/rK4s6_mere-samne-h-d-ati.jpg?h=270&drp=1', NULL, 0, 82022, 0, 0, 1, 0, 6428, 0),
(990, 1, 108, '5', 'server_url', 'Attitude Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313715/kvzdjinl9tlb0zfxomgg.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313717/uaczjnlwcrapbh3d2ztc.jpg', NULL, 197, 873, 44, 0, 1, 1, 6179, 0),
(991, 1, 108, '5', 'server_url', 'Tera Baap Aaya', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609401533/iktzapwyddmbdaf4rcee.mp4', '-', 'Attitude', 'Portrait', 'http://www.kids2win.com/api/thumbImage/JF3Sv_tera-baap-aaya-attitude-hindi-fullscreen.jpg?h=270&drp=1', NULL, 135, 786, 83, 0, 1, 1, 6018, 0),
(992, 1, 108, '5', 'server_url', 'Ek Chumma - Attitude', 'http://www.kids2win.com/api/videosFile/aAl6j_ek-chumma-attitude-fullscreen-hindi-social.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/enyDX_ek-chumma-attitude-fullscreen-hindi-social.jpg?h=270&drp=1', NULL, 0, 17929, 0, 0, 1, 0, 5675, 0),
(993, 1, 108, '5', 'server_url', 'Siddharth Malhotra Killer Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271012/p56jotwgjd3dafhses4p.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271015/vguieagji3hqw4kmfuod.jpg', NULL, 107, 734, 51, 0, 1, 1, 0, 0),
(994, 1, 108, '5', 'server_url', 'Bol Bol Bol', 'http://www.kids2win.com/api/videosFile/h8gBY_bol-bol-bol-marathi-hindi.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/3NQuR_bol-bol-bol-marathi-hindi.jpg?h=270&drp=1', NULL, 0, 16591, 0, 0, 1, 0, 5459, 0),
(995, 1, 108, '5', 'server_url', 'Sanju Baba Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310041/dqazhswhcozvuwvfil0c.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609310043/w3be1zb7vgrykvpxfd0i.jpg', NULL, 193, 799, 20, 0, 1, 1, 5379, 0),
(996, 1, 108, '5', 'server_url', 'Ho Ja Mast Malang Tu', 'http://www.kids2win.com/api/videosFile/FWrbg_ho-ja-mast-malang-tu-attitude-hindi-fullscreen.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/JgUY9_ho-ja-mast-malang-tu-attitude-hindi-fullscreen.jpg?h=270&drp=1', NULL, 0, 21974, 0, 0, 1, 0, 4867, 0),
(997, 1, 108, '5', 'server_url', 'Khalnayak Hoon Main', 'http://www.kids2win.com/api/videosFile/nZKy9_khalnayak-hoon-main-hindi-square-attitude.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/N5uLy_khalnayak-hoon-main-hindi-square-attitude.jpg?h=270&drp=1', NULL, 0, 17228, 0, 0, 1, 0, 4539, 0),
(998, 1, 108, '5', 'server_url', 'Killer Attitude Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313946/zkpplzlinwpwaetx7wxl.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313949/n9x4lfhceyfqurzlxckb.jpg', NULL, 132, 490, 59, 0, 1, 1, 4236, 0),
(999, 1, 108, '5', 'server_url', 'Sonakshi Sinha - Attitude Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316999/jxpqfnybajvskjtxffh1.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317001/vxl8ojfjqedi4qeidzgp.jpg', NULL, 153, 732, 23, 0, 1, 1, 4095, 0),
(1000, 1, 108, '5', 'server_url', 'Maari2 Attitude Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337148/g8hvucezsbm8pvpgq4m9.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337151/rkqeckbzaixqqcg0wy1z.jpg', NULL, 112, 769, 36, 0, 1, 1, 4008, 0),
(1001, 1, 108, '5', 'server_url', 'Ghamand Kar - Tanhaji', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402427/taidlhg1oehpv5kjav1w.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609402429/v8yrwl4p9ijg7c746oy3.jpg', NULL, 137, 461, 54, 0, 1, 1, 3998, 0),
(1002, 1, 108, '5', 'server_url', 'Humko Mita Sake - Attitude', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609339456/v4irujw7vcoiul7omatc.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609339459/nm3auv78gl5k0bwamkbk.jpg', NULL, 164, 664, 54, 0, 1, 1, 3855, 0),
(1003, 1, 108, '5', 'server_url', 'Attitude Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609338528/nnpns72zowi8nz7qwmyg.mp4', '-', 'Attitude', 'Portrait', 'http://www.kids2win.com/api/thumbImage/rapb5_bhaigiri-attitude-remix-hindi-square.jpg?h=270&drp=1', NULL, 115, 615, 42, 0, 1, 1, 3542, 0),
(1004, 1, 108, '5', 'server_url', 'Rowdy Boys Status', 'http://www.kids2win.com/api/videosFile/QBIiG_rowdy-boys-status-attitude-hindi.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TH9st_rowdy-boys-status-attitude-hindi.jpg?h=270&drp=1', NULL, 0, 10574, 0, 0, 1, 0, 3525, 0),
(1005, 1, 108, '5', 'server_url', 'Killer Attitude Status For Boys', 'http://www.kids2win.com/api/videosFile/cwgzD_killer-attitude-status-for-boys-breakup-fullscreen-hindi.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TfC6N_killer-attitude-status-for-boys-breakup-fullscreen-hindi.jpg?h=270&drp=1', NULL, 0, 111612, 0, 0, 1, 0, 3476, 0),
(1006, 1, 108, '5', 'server_url', 'Attitude Whatsapp Status - Shayari', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316380/prs67rzl3gab1ukcbqsi.mp4', '-', 'Attitude', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609316381/odttxiay4wuzlonpy3k6.jpg', NULL, 175, 759, 62, 0, 1, 1, 2822, 0),
(1007, 1, 108, '5', 'server_url', 'Killer Status For Boys - Attitude', 'http://www.kids2win.com/api/videosFile/WAC52_killer-status-for-boys-attitude-hindi-shayari.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/B9Aks_killer-status-for-boys-attitude-hindi-shayari.jpg?h=270&drp=1', NULL, 0, 7370, 0, 0, 1, 0, 2482, 0),
(1008, 1, 108, '5', 'server_url', 'Boys Attitude Whatsapp Status', 'http://www.kids2win.com/api/videosFile/cvU9u_boys-attitude-whatsapp-status-hindi.mp4', '-', 'Attitude', 'Landscape', 'http://www.kids2win.com/api/thumbImage/sZnDc_boys-attitude-whatsapp-status-hindi.jpg?h=270&drp=1', NULL, 0, 9654, 0, 0, 1, 0, 2229, 0),
(1009, 1, 109, '5', 'server_url', 'Krishna Inspiration Dialogue', 'http://www.kids2win.com/api/videosFile/Gswt1_krishna-inspiration-dialogue-h.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/bF14s_krishna-inspiration-dialogue-h.jpg?h=270&drp=1', NULL, 0, 14742, 0, 0, 1, 0, 8651, 0),
(1010, 1, 109, '5', 'server_url', 'Focus On Goals', 'http://www.kids2win.com/api/videosFile/dz1SK_focus-on-goals-h-moti.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/CR9ZU_focus-on-goals-h-moti.jpg?h=270&drp=1', NULL, 0, 57727, 0, 0, 1, 0, 8175, 0),
(1011, 1, 109, '5', 'server_url', 'Never Accept Negativity', 'http://www.kids2win.com/api/videosFile/N8afq_never-accept-negativity-h-sq-e.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YInCc_never-accept-negativity-h-sq-e.jpg?h=270&drp=1', NULL, 0, 25431, 0, 0, 1, 0, 7990, 0),
(1012, 1, 109, '5', 'server_url', 'Motivation Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317872/ef3cmh8azxyxswyqiijx.mp4', '-', 'Motivational', 'Portrait', 'http://www.kids2win.com/api/thumbImage/jvJ0M_motivation-status-h-d-15.jpg?h=270&drp=1', NULL, 150, 632, 41, 0, 1, 1, 7937, 0),
(1013, 1, 109, '5', 'server_url', 'Motivated - Zindagi Ka Usul', 'http://www.kids2win.com/api/videosFile/NfOgq_motivated-zindagi-ka-usul-bana-lo-h-f-shy.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/eOKxP_motivated-zindagi-ka-usul-bana-lo-h-f-shy.jpg?h=270&drp=1', NULL, 1, 24526, 0, 0, 1, 0, 7531, 0),
(1014, 1, 109, '5', 'server_url', 'Best Life Motivational Dialogue', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609310371/roucvk8jq028fc8b6dux.mp4', '-', 'Motivational', 'Portrait', 'http://www.kids2win.com/api/thumbImage/OjAVx_best-life-motivational-dialogue-sq-h.jpg?h=270&drp=1', NULL, 159, 406, 48, 0, 1, 1, 6422, 0),
(1015, 1, 109, '5', 'server_url', 'Motivation Dialogue Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609319712/bjerpzpaeno6ca0rxalz.mp4', '-', 'Motivational', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609319714/qz1gmov1oofyai71iynb.jpg', NULL, 1, 834, 47, 0, 1, 1, 4107, 0),
(1016, 1, 109, '5', 'server_url', 'Motivation Dialogue Status', 'http://www.kids2win.com/api/videosFile/H1uo8_motivation-dialogue-status-hindi.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/f8FtS_motivation-dialogue-status-hindi.jpg?h=270&drp=1', NULL, 1, 44153, 0, 0, 1, 0, 3698, 0),
(1017, 1, 109, '5', 'server_url', 'Best Motivational Dialogue', 'http://www.kids2win.com/api/videosFile/o0S8D_best-motivational-dialogue-hindi.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Lov4t_best-motivational-dialogue-hindi.jpg?h=270&drp=1', NULL, 0, 53731, 0, 0, 1, 0, 3656, 0),
(1018, 1, 109, '5', 'server_url', 'Most Beautiful Motivational Lines', 'http://www.kids2win.com/api/videosFile/jBTzy_most-beautiful-motivational-lines-hindi-animated.mp4', '-', 'Motivational', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ynGh0_most-beautiful-motivational-lines-hindi-animated.jpg?h=270&drp=1', NULL, 0, 2, 0, 0, 1, 0, 2850, 0),
(1019, 1, 21, '7', 'server_url', 'Khaayal Rakhya Kar', 'http://www.kids2win.com/api/videosFile/Axw8F_khaayal-rakhya-kar-l-f-p-guj-15.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5u18w_khaayal-rakhya-kar-l-f-p-guj-15.jpg?h=270&drp=1', NULL, 0, 93774, 0, 0, 1, 0, 7113, 0),
(1020, 1, 21, '7', 'server_url', 'Chaand Ne Kaho Aaje', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316717/ckplwt7ur2ln5rkyl7vf.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/HJtId_chaand-ne-kaho-aaje-l-f-guj.jpg?h=270&drp=1', NULL, 166, 702, 21, 0, 1, 1, 6656, 0),
(1021, 1, 21, '7', 'server_url', 'Vhalam Aavo Ne', 'http://www.kids2win.com/api/videosFile/DWio0_vhalam-aavo-ne-gujarati-l-ly.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NvfjT_vhalam-aavo-ne-gujarati-l-ly.jpg?h=270&drp=1', NULL, 0, 35168, 0, 0, 1, 0, 6267, 0),
(1022, 1, 21, '7', 'server_url', 'Mari Hare Tu Nathi Painvani - Gujarati', 'http://www.kids2win.com/api/videosFile/WdUEl_mari-hare-tu-nathi-painvani-gujarati-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KhkfG_mari-hare-tu-nathi-painvani-gujarati-love.jpg?h=270&drp=1', NULL, 0, 36822, 0, 0, 1, 0, 5072, 0),
(1023, 1, 21, '7', 'server_url', 'Awaara dil ki kore toke bolbo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609403374/poezdik0tetolxwcopqb.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609403379/seh2vzuqpuj24a4grcam.jpg', NULL, 143, 572, 59, 0, 1, 1, 843, 0),
(1024, 1, 21, '7', 'server_url', 'Laila Majnu Love', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313422/swlyjas5h4bigmq3pnhe.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313424/m6pelugeu5twwkvqqc1u.jpg', NULL, 109, 652, 87, 0, 1, 1, 1638, 0),
(1025, 1, 21, '7', 'server_url', 'Gujarati-lyrics-status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352657/q6vu845ytzvplrgtknqj.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352659/z4olinkjoxufoggnvoie.jpg', NULL, 141, 521, 69, 0, 1, 1, 1834, 0),
(1026, 1, 21, '7', 'server_url', 'Pyar Ki Galiyon Mein Mujhe', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309257/nduutfmlqssb0icldre9.mp4', '-', 'Love', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309260/xe7wrvltdes9g8zyektx.jpg', NULL, 105, 455, 69, 0, 1, 1, 2405, 0),
(1027, 1, 21, '7', 'server_url', 'Pan Prem To Adhuro Rahyo', 'http://www.kids2win.com/api/videosFile/sFRwG_pan-prem-to-adhuro-rahyo-love-gujrati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6lzgU_pan-prem-to-adhuro-rahyo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 13314, 0, 0, 1, 0, 2400, 0),
(1028, 1, 21, '7', 'server_url', 'Love Gujrati Video Status', 'http://www.kids2win.com/api/videosFile/xSlu6_love-gujrati-video-status-animated.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GWxaI_love-gujrati-video-status-animated.jpg?h=270&drp=1', NULL, 0, 13355, 0, 0, 1, 0, 2369, 0),
(1029, 1, 21, '7', 'server_url', 'Prem Jo Koi Kare To', 'http://www.kids2win.com/api/videosFile/mduOq_prem-jo-koi-kare-to-love-gujrati-fullscreen.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hWRTo_prem-jo-koi-kare-to-love-gujrati-fullscreen.jpg?h=270&drp=1', NULL, 0, 14531, 0, 0, 1, 0, 2214, 0),
(1030, 1, 21, '7', 'server_url', 'Chandne Kaho Aaje Aathme Nahi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609400920/t6cevcnwgx73zeeiruok.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609400922/akenimwttfvd4uncuqct.jpg', NULL, 177, 514, 86, 0, 1, 1, 2106, 0),
(1031, 1, 21, '7', 'server_url', 'New Gujarati Watsaap Status', 'http://www.kids2win.com/api/videosFile/LBw7M_new-gujarati-watsaap-status-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DwUB8_new-gujarati-watsaap-status-love.jpg?h=270&drp=1', NULL, 0, 10499, 0, 0, 1, 0, 2069, 0),
(1032, 1, 21, '7', 'server_url', 'Koi No Divas Aave Re', 'http://www.kids2win.com/api/videosFile/eo4IW_koi-no-divas-aave-re-love-gujrati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FJxZY_koi-no-divas-aave-re-love-gujrati.jpg?h=270&drp=1', NULL, 0, 10199, 0, 0, 1, 0, 2027, 0),
(1033, 1, 21, '7', 'server_url', 'Gujarati 30 Seconds Whatsapp Status', 'http://www.kids2win.com/api/videosFile/jJmCR_gujarati-30-seconds-whatsapp-status.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/836IR_gujarati-30-seconds-whatsapp-status.jpg?h=270&drp=1', NULL, 0, 11853, 0, 0, 1, 0, 1809, 0),
(1034, 1, 21, '7', 'server_url', 'Whatsapp Status Gujrati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609317248/uwyuiyqukdkofj0q34aj.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609317250/pla99xcae1exwacxjclo.jpg', NULL, 187, 581, 93, 0, 1, 1, 1782, 0),
(1035, 1, 21, '7', 'server_url', 'Khushbu Mehkaye Khub Cho Tame', 'http://www.kids2win.com/api/videosFile/FuDWr_khushbu-mehkaye-khub-cho-tame-fullscreen-love-gujarati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NYCpe_khushbu-mehkaye-khub-cho-tame-fullscreen-love-gujarati.jpg?h=270&drp=1', NULL, 0, 11246, 0, 0, 1, 0, 1742, 0),
(1036, 1, 21, '7', 'server_url', 'New Gujarati Love Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271008/axeso1outdktlrvxto6n.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271010/wngxqd4tb38sjluufprl.jpg', NULL, 101, 484, 50, 0, 1, 1, 0, 0),
(1037, 1, 21, '7', 'server_url', 'Baby Ne Bournvita Pivdavu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336238/sicd8bu7ogysjkfulmle.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609336241/yzj95xzqyhx38pvpy8sw.jpg', NULL, 156, 602, 40, 0, 1, 1, 1700, 0),
(1038, 1, 21, '7', 'server_url', 'New Gujarati Love Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609270044/dicgflo2lrqvp7jm4gwa.mp4', '-', 'Love', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609270046/hl3wqvnq378so3loovwn.jpg', NULL, 192, 659, 60, 0, 1, 1, 0, 0),
(1039, 1, 21, '7', 'server_url', 'Mann medo medo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609398969/zxyofyiqv625bximecsy.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609398971/jrkoyidzgm4lg4pylqh4.jpg', NULL, 172, 636, 53, 0, 1, 1, 1166, 0),
(1040, 1, 21, '7', 'server_url', 'Love gujrati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318135/ezbmwqhm8oczr37zs6jc.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609318137/dk4vavbhncbc4gomrpgq.jpg', NULL, 118, 671, 99, 0, 1, 1, 695, 0),
(1041, 1, 21, '7', 'server_url', 'Propose in gujarati', 'http://www.kids2win.com/api/videosFile/fcepA_propose-in-gujarati-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/JV7wz_propose-in-gujarati-love.jpg?h=270&drp=1', NULL, 0, 26823, 0, 0, 1, 0, 687, 0),
(1042, 1, 73, '7', 'server_url', 'Khaayal Rakhya Kar', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311914/ortfxfmhstmnaszq59ty.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609311917/rge7pjgvxypjuf4abd9o.jpg', NULL, 143, 776, 10, 0, 1, 1, 7113, 0),
(1043, 1, 73, '7', 'server_url', 'Chaand Ne Kaho Aaje', 'http://www.kids2win.com/api/videosFile/5807d_chaand-ne-kaho-aaje-l-f-guj.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HJtId_chaand-ne-kaho-aaje-l-f-guj.jpg?h=270&drp=1', NULL, 0, 52295, 0, 0, 1, 0, 6656, 0),
(1044, 1, 73, '7', 'server_url', 'Vhalam Aavo Ne', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313000/da1a3n8wtfwhagbntjmc.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313003/q2r5gxgyhklmirxrh8en.jpg', NULL, 169, 461, 99, 0, 1, 1, 6267, 0),
(1045, 1, 73, '7', 'server_url', 'Mari Hare Tu Nathi Painvani - Gujarati', 'http://www.kids2win.com/api/videosFile/WdUEl_mari-hare-tu-nathi-painvani-gujarati-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KhkfG_mari-hare-tu-nathi-painvani-gujarati-love.jpg?h=270&drp=1', NULL, 0, 36822, 0, 0, 1, 0, 5072, 0),
(1046, 1, 73, '7', 'server_url', 'Awaara dil ki kore toke bolbo', 'http://www.kids2win.com/api/videosFile/qEfIm_awaara-dil-ki-kore-toke-bolbo-love-gujrati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2Xfgv_awaara-dil-ki-kore-toke-bolbo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 4833, 0, 0, 1, 0, 843, 0),
(1047, 1, 73, '7', 'server_url', 'Laila Majnu Love', 'http://www.kids2win.com/api/videosFile/HWibp_laila-majnu-love-gujarati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ibgkE_laila-majnu-love-gujarati.jpg?h=270&drp=1', NULL, 0, 22033, 0, 0, 1, 0, 1638, 0),
(1048, 1, 73, '7', 'server_url', 'Gujarati-lyrics-status', 'http://www.kids2win.com/api/videosFile/3weDl_gujarati-lyrics-status.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OYzoW_gujarati-lyrics-status.jpg?h=270&drp=1', NULL, 0, 18767, 0, 0, 1, 0, 1834, 0),
(1049, 1, 73, '7', 'server_url', 'Pyar Ki Galiyon Mein Mujhe', 'http://www.kids2win.com/api/videosFile/Bj1FI_pyar-ki-galiyon-mein-mujhe-gujarati-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/MLByz_pyar-ki-galiyon-mein-mujhe-gujarati-love.jpg?h=270&drp=1', NULL, 0, 15344, 0, 0, 1, 0, 2405, 0),
(1050, 1, 73, '7', 'server_url', 'Pan Prem To Adhuro Rahyo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315822/veczy6vvoa27rxx5gmq9.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/6lzgU_pan-prem-to-adhuro-rahyo-love-gujrati.jpg?h=270&drp=1', NULL, 131, 766, 70, 0, 1, 1, 2400, 0),
(1051, 1, 73, '7', 'server_url', 'Love Gujrati Video Status', 'http://www.kids2win.com/api/videosFile/xSlu6_love-gujrati-video-status-animated.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GWxaI_love-gujrati-video-status-animated.jpg?h=270&drp=1', NULL, 0, 13355, 0, 0, 1, 0, 2369, 0),
(1052, 1, 73, '7', 'server_url', 'Prem Jo Koi Kare To', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271441/hxnnoyz5nbtmulr6qigd.mp4', '-', 'Love', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271444/fthje7qip42yvslv5qi6.jpg', NULL, 183, 466, 63, 0, 1, 1, 0, 0),
(1053, 1, 73, '7', 'server_url', 'Chandne Kaho Aaje Aathme Nahi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609313398/uq9pev8ds3yjg6yfdpft.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609313400/c4v0i69xi4389forohrd.jpg', NULL, 110, 614, 64, 0, 1, 1, 2106, 0),
(1054, 1, 73, '7', 'server_url', 'New Gujarati Watsaap Status', 'http://www.kids2win.com/api/videosFile/LBw7M_new-gujarati-watsaap-status-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DwUB8_new-gujarati-watsaap-status-love.jpg?h=270&drp=1', NULL, 0, 10499, 0, 0, 1, 0, 2069, 0),
(1055, 1, 73, '7', 'server_url', 'Koi No Divas Aave Re', 'http://www.kids2win.com/api/videosFile/eo4IW_koi-no-divas-aave-re-love-gujrati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FJxZY_koi-no-divas-aave-re-love-gujrati.jpg?h=270&drp=1', NULL, 0, 10199, 0, 0, 1, 0, 2027, 0),
(1056, 1, 73, '7', 'server_url', 'Gujarati 30 Seconds Whatsapp Status', 'http://www.kids2win.com/api/videosFile/jJmCR_gujarati-30-seconds-whatsapp-status.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/836IR_gujarati-30-seconds-whatsapp-status.jpg?h=270&drp=1', NULL, 0, 11853, 0, 0, 1, 0, 1809, 0),
(1057, 1, 73, '7', 'server_url', 'Whatsapp Status Gujrati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609318463/bysv7hqgmhkz6rqtn0k3.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609318465/lkxiotdzxek064ywom2u.jpg', NULL, 122, 717, 17, 0, 1, 1, 1782, 0),
(1058, 1, 73, '7', 'server_url', 'Khushbu Mehkaye Khub Cho Tame', 'http://www.kids2win.com/api/videosFile/FuDWr_khushbu-mehkaye-khub-cho-tame-fullscreen-love-gujarati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NYCpe_khushbu-mehkaye-khub-cho-tame-fullscreen-love-gujarati.jpg?h=270&drp=1', NULL, 0, 11246, 0, 0, 1, 0, 1742, 0),
(1059, 1, 73, '7', 'server_url', 'New Gujarati Love Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609402568/rejcbvue4epe4lnpgah5.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609402570/mdt6cg1pw53qp5rwlmc6.jpg', NULL, 161, 794, 50, 0, 1, 1, 1719, 0),
(1060, 1, 73, '7', 'server_url', 'Baby Ne Bournvita Pivdavu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609337323/ne0juf5r1xpco3uurdmx.mp4', '-', 'Love', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609337326/ma3j9lzb5umw2tyzu0yi.jpg', NULL, 198, 670, 100, 0, 1, 1, 1700, 0),
(1061, 1, 73, '7', 'server_url', 'New Gujarati Love Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609398917/q0jmebm94qifdkx1lwrn.mp4', '-', 'Love', 'Portrait', 'http://www.kids2win.com/api/thumbImage/dYWrv_new-gujarati-love-status.jpg?h=270&drp=1', NULL, 1, 896, 89, 0, 1, 1, 1686, 0),
(1062, 1, 73, '7', 'server_url', 'Mann medo medo', 'http://www.kids2win.com/api/videosFile/RYrEv_mann-medo-medo-love-gujrati.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KoMrz_mann-medo-medo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 14665, 0, 0, 1, 0, 1166, 0),
(1063, 1, 73, '7', 'server_url', 'Love gujrati', 'http://www.kids2win.com/api/videosFile/7rvjt_love-gujrati-animated.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/AIzb8_love-gujrati-animated.jpg?h=270&drp=1', NULL, 0, 8670, 0, 0, 1, 0, 695, 0),
(1064, 1, 73, '7', 'server_url', 'Propose in gujarati', 'http://www.kids2win.com/api/videosFile/fcepA_propose-in-gujarati-love.mp4', '-', 'Love', 'Landscape', 'http://www.kids2win.com/api/thumbImage/JV7wz_propose-in-gujarati-love.jpg?h=270&drp=1', NULL, 0, 26823, 0, 0, 1, 0, 687, 0),
(1065, 1, 80, '7', 'server_url', 'Navratri Special', 'http://www.kids2win.com/api/videosFile/HPLvd_navratri-special-sq-guj-fes.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fPxoB_navratri-special-sq-guj-fes.jpg?h=270&drp=1', NULL, 0, 32477, 0, 0, 1, 0, 8136, 0),
(1066, 1, 80, '7', 'server_url', 'Kadvu Chhe Satya Chhe', 'http://www.kids2win.com/api/videosFile/5o4v0_kadvu-chhe-satya-chhe-f-gu-dai.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ijxEY_kadvu-chhe-satya-chhe-f-gu-dai.jpg?h=270&drp=1', NULL, 0, 45681, 0, 0, 1, 0, 7959, 0),
(1067, 1, 80, '7', 'server_url', 'Khaayal Rakhya Kar', 'http://www.kids2win.com/api/videosFile/Axw8F_khaayal-rakhya-kar-l-f-p-guj-15.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/5u18w_khaayal-rakhya-kar-l-f-p-guj-15.jpg?h=270&drp=1', NULL, 0, 93774, 0, 0, 1, 0, 7113, 0),
(1068, 1, 80, '7', 'server_url', 'Chaand Ne Kaho Aaje', 'http://www.kids2win.com/api/videosFile/5807d_chaand-ne-kaho-aaje-l-f-guj.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/HJtId_chaand-ne-kaho-aaje-l-f-guj.jpg?h=270&drp=1', NULL, 0, 52295, 0, 0, 1, 0, 6656, 0),
(1069, 1, 80, '7', 'server_url', 'Happy Jagannath Rath Yatra', 'http://www.kids2win.com/api/videosFile/dGZvJ_happy-jagannath-rath-yatra-f-guj.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/nLUIG_happy-jagannath-rath-yatra-f-guj.jpg?h=270&drp=1', NULL, 0, 14616, 0, 0, 1, 0, 6591, 0),
(1070, 1, 80, '7', 'server_url', 'Vhalam Aavo Ne', 'http://www.kids2win.com/api/videosFile/DWio0_vhalam-aavo-ne-gujarati-l-ly.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/NvfjT_vhalam-aavo-ne-gujarati-l-ly.jpg?h=270&drp=1', NULL, 0, 35168, 0, 0, 1, 0, 6267, 0),
(1071, 1, 80, '7', 'server_url', 'Mari Hare Tu Nathi Painvani - Gujarati', 'http://www.kids2win.com/api/videosFile/WdUEl_mari-hare-tu-nathi-painvani-gujarati-love.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KhkfG_mari-hare-tu-nathi-painvani-gujarati-love.jpg?h=270&drp=1', NULL, 0, 36822, 0, 0, 1, 0, 5072, 0),
(1072, 1, 80, '7', 'server_url', 'Udi Patang - Uttarayan Special', 'http://www.kids2win.com/api/videosFile/5Kwv0_udi-patang-uttarayan-special-festival-gujrati-square.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/54wzA_udi-patang-uttarayan-special-festival-gujrati-square.jpg?h=270&drp=1', NULL, 0, 28896, 0, 0, 1, 0, 4201, 0),
(1073, 1, 80, '7', 'server_url', 'Happy New Year 2020', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609341578/vuu9kzlarfwzchhn7tha.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/IEYNK_happy-new-year-2020-fullscreen-gujrati.jpg?h=270&drp=1', NULL, 180, 573, 52, 0, 1, 1, 4063, 0),
(1074, 1, 80, '7', 'server_url', 'Choodiyan', 'http://www.kids2win.com/api/videosFile/nyRFz_choodiyan-navratri-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/IMyoW_choodiyan-navratri-gujrati.jpg?h=270&drp=1', NULL, 0, 65269, 0, 0, 1, 0, 3243, 0),
(1075, 1, 80, '7', 'server_url', 'Radha Ne Shyam Mali Jashe', 'http://www.kids2win.com/api/videosFile/JAzdj_radha-ne-shyam-mali-jashe-navaratri-festival-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/OPoRF_radha-ne-shyam-mali-jashe-navaratri-festival-gujrati.jpg?h=270&drp=1', NULL, 0, 55238, 0, 0, 1, 0, 3163, 0),
(1076, 1, 80, '7', 'server_url', 'Kumkum Na Pagla Padya - Navratri', 'http://www.kids2win.com/api/videosFile/NKmI6_kumkum-na-pagla-padya-navratri-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/PWYUk_kumkum-na-pagla-padya-navratri-gujrati.jpg?h=270&drp=1', NULL, 0, 33145, 0, 0, 1, 0, 3106, 0),
(1077, 1, 80, '7', 'server_url', 'Aavi Navratri - Festival - Lyrical', 'http://www.kids2win.com/api/videosFile/Cc85O_aavi-navratri-festival-gujrati-lyrical.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Y4Xad_aavi-navratri-festival-gujrati-lyrical.jpg?h=270&drp=1', NULL, 0, 35896, 0, 0, 1, 0, 3105, 0),
(1078, 1, 80, '7', 'server_url', 'Radha Ne Shyam Mali Jashe - Navaratri', 'http://www.kids2win.com/api/videosFile/qGsog_radha-ne-shyam-mali-jashe-navaratri-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/M1Nfd_radha-ne-shyam-mali-jashe-navaratri-gujrati.jpg?h=270&drp=1', NULL, 0, 24976, 0, 0, 1, 0, 3104, 0),
(1079, 1, 80, '7', 'server_url', 'Chogada - Navratri', 'http://www.kids2win.com/api/videosFile/I1U4G_chogada-navratri-animated.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/cQS5i_chogada-navratri-animated.jpg?h=270&drp=1', NULL, 0, 22894, 0, 0, 1, 0, 3101, 0),
(1080, 1, 80, '7', 'server_url', 'Awaara dil ki kore toke bolbo', 'http://www.kids2win.com/api/videosFile/qEfIm_awaara-dil-ki-kore-toke-bolbo-love-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/2Xfgv_awaara-dil-ki-kore-toke-bolbo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 4833, 0, 0, 1, 0, 843, 0),
(1081, 1, 80, '7', 'server_url', 'Laila Majnu Love', 'http://www.kids2win.com/api/videosFile/HWibp_laila-majnu-love-gujarati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/ibgkE_laila-majnu-love-gujarati.jpg?h=270&drp=1', NULL, 0, 22033, 0, 0, 1, 0, 1638, 0),
(1082, 1, 80, '7', 'server_url', 'Gujarati-lyrics-status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609308610/dt0pbmzbpqptfpftwulm.mp4', '-', 'Gujarati', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609308613/wewnbdpogihws1ebgtop.jpg', NULL, 169, 413, 81, 0, 1, 1, 1834, 0),
(1083, 1, 80, '7', 'server_url', 'Pyar Ki Galiyon Mein Mujhe', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609311262/ktacq2eklprubnacfcle.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/MLByz_pyar-ki-galiyon-mein-mujhe-gujarati-love.jpg?h=270&drp=1', NULL, 157, 814, 19, 0, 1, 1, 2405, 0),
(1084, 1, 80, '7', 'server_url', 'Pan Prem To Adhuro Rahyo', 'http://www.kids2win.com/api/videosFile/sFRwG_pan-prem-to-adhuro-rahyo-love-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6lzgU_pan-prem-to-adhuro-rahyo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 13315, 0, 0, 1, 0, 2400, 0),
(1085, 1, 80, '7', 'server_url', 'Love Gujrati Video Status', 'http://www.kids2win.com/api/videosFile/xSlu6_love-gujrati-video-status-animated.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GWxaI_love-gujrati-video-status-animated.jpg?h=270&drp=1', NULL, 0, 13355, 0, 0, 1, 0, 2369, 0),
(1086, 1, 80, '7', 'server_url', 'Prem Jo Koi Kare To', 'http://www.kids2win.com/api/videosFile/mduOq_prem-jo-koi-kare-to-love-gujrati-fullscreen.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/hWRTo_prem-jo-koi-kare-to-love-gujrati-fullscreen.jpg?h=270&drp=1', NULL, 0, 14531, 0, 0, 1, 0, 2214, 0),
(1087, 1, 80, '7', 'server_url', 'Chandne Kaho Aaje Aathme Nahi', 'http://www.kids2win.com/api/videosFile/1Hqgb_chandne-kaho-aaje-aathme-nahi-gujarati-love-animated.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YpwNe_chandne-kaho-aaje-aathme-nahi-gujarati-love-animated.jpg?h=270&drp=1', NULL, 0, 12651, 0, 0, 1, 0, 2106, 0),
(1088, 1, 80, '7', 'server_url', 'New Gujarati Watsaap Status', 'http://www.kids2win.com/api/videosFile/LBw7M_new-gujarati-watsaap-status-love.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/DwUB8_new-gujarati-watsaap-status-love.jpg?h=270&drp=1', NULL, 0, 10499, 0, 0, 1, 0, 2069, 0),
(1089, 1, 80, '7', 'server_url', 'Koi No Divas Aave Re', 'http://www.kids2win.com/api/videosFile/eo4IW_koi-no-divas-aave-re-love-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/FJxZY_koi-no-divas-aave-re-love-gujrati.jpg?h=270&drp=1', NULL, 0, 10200, 0, 0, 1, 0, 2027, 0),
(1090, 1, 80, '7', 'server_url', 'Gujarati 30 Seconds Whatsapp Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609272474/odlutmptpelu29jg2rfw.mp4', '-', 'Gujarati', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609272477/z9l8jqv2tu0aflyxkl4o.jpg', NULL, 190, 527, 100, 0, 1, 1, 1809, 0),
(1091, 1, 80, '7', 'server_url', 'Gujarati Whatsapp Status', 'http://www.kids2win.com/api/videosFile/C0TF8_gujarati-whatsapp-status-fullscreen.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TZz5S_gujarati-whatsapp-status-fullscreen.jpg?h=270&drp=1', NULL, 0, 10347, 0, 0, 1, 0, 1785, 0),
(1092, 1, 80, '7', 'server_url', 'Whatsapp Status Gujrati', 'http://www.kids2win.com/api/videosFile/t7yki_whatsapp-status-gujrati-video.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/S2rlF_whatsapp-status-gujrati-video.jpg?h=270&drp=1', NULL, 0, 15012, 0, 0, 1, 0, 1782, 0),
(1093, 1, 80, '7', 'server_url', 'Khushbu Mehkaye Khub Cho Tame', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609353212/hbcl3s1v44xj1plj6qtf.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/NYCpe_khushbu-mehkaye-khub-cho-tame-fullscreen-love-gujarati.jpg?h=270&drp=1', NULL, 195, 490, 13, 0, 1, 1, 1742, 0),
(1094, 1, 80, '7', 'server_url', 'New Gujarati Love Status', 'http://www.kids2win.com/api/videosFile/EzZdb_new-gujarati-love-status.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/6z0lo_new-gujarati-love-status.jpg?h=270&drp=1', NULL, 0, 23757, 1, 0, 1, 0, 1719, 0),
(1095, 1, 80, '7', 'server_url', 'Republic Day Special Status Gujrati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609271458/z7qwdrowsacgzrdtvffo.mp4', '-', 'Gujarati', 'Portrait', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609271460/nlkuhxcqtdwccfyg4iny.jpg', NULL, 142, 615, 72, 0, 1, 1, 0, 0),
(1096, 1, 80, '7', 'server_url', 'Baby Ne Bournvita Pivdavu', 'http://www.kids2win.com/api/videosFile/XFVvY_baby-ne-bournvita-pivdavu-gujarati-love.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/YLvQE_baby-ne-bournvita-pivdavu-gujarati-love.jpg?h=270&drp=1', NULL, 0, 9138, 0, 0, 1, 0, 1700, 0),
(1097, 1, 80, '7', 'server_url', 'New Gujarati Love Status', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609316749/may4pojalekj1w5ree6j.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/dYWrv_new-gujarati-love-status.jpg?h=270&drp=1', NULL, 179, 459, 49, 0, 1, 1, 1686, 0),
(1098, 1, 80, '7', 'server_url', 'Jay Shree Ram', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609336189/kxy0np7vh98xzv3gohfk.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/8p5Tr_jay-shree-ram-gujarati-god-fullscreen.jpg?h=270&drp=1', NULL, 186, 850, 90, 0, 1, 1, 1678, 0),
(1099, 1, 80, '7', 'server_url', 'Mann medo medo', 'http://www.kids2win.com/api/videosFile/RYrEv_mann-medo-medo-love-gujrati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/KoMrz_mann-medo-medo-love-gujrati.jpg?h=270&drp=1', NULL, 0, 14665, 0, 0, 1, 0, 1166, 0),
(1100, 1, 80, '7', 'server_url', 'Love gujrati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609352087/foxrriuk0wapmaumsm5j.mp4', '-', 'Gujarati', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609352089/y8bpvjcqfletocwozcmc.jpg', NULL, 189, 849, 56, 0, 1, 1, 695, 0),
(1101, 1, 80, '7', 'server_url', 'Paan lilu joyu ne tame Gujarati', 'http://www.kids2win.com/api/videosFile/FjpOG_pan-lilu-joyu-ne-tame-gujarati.mp4', '-', 'Gujarati', 'Landscape', 'http://www.kids2win.com/api/thumbImage/TzabU_pan-lilu-joyu-ne-tame-gujarati.jpg?h=270&drp=1', NULL, 0, 17184, 0, 0, 1, 0, 690, 0),
(1102, 1, 80, '7', 'server_url', 'Propose in gujarati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315395/nlxewtao8xyfuhjuui18.mp4', '-', 'Gujarati', 'Portrait', 'http://www.kids2win.com/api/thumbImage/JV7wz_propose-in-gujarati-love.jpg?h=270&drp=1', NULL, 1, 809, 96, 0, 1, 1, 687, 0),
(1103, 1, 84, '7', 'server_url', 'Navratri Special', 'http://www.kids2win.com/api/videosFile/HPLvd_navratri-special-sq-guj-fes.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/fPxoB_navratri-special-sq-guj-fes.jpg?h=270&drp=1', NULL, 0, 32477, 0, 0, 1, 0, 8136, 0),
(1104, 1, 84, '7', 'server_url', 'Happy Jagannath Rath Yatra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609315108/rq7gbcnegvfxbxoyh6r7.mp4', '-', 'Festivals', 'Portrait', 'http://www.kids2win.com/api/thumbImage/nLUIG_happy-jagannath-rath-yatra-f-guj.jpg?h=270&drp=1', NULL, 122, 481, 43, 0, 1, 1, 6591, 0),
(1105, 1, 84, '7', 'server_url', 'Udi Patang - Uttarayan Special', 'http://www.kids2win.com/api/videosFile/5Kwv0_udi-patang-uttarayan-special-festival-gujrati-square.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/54wzA_udi-patang-uttarayan-special-festival-gujrati-square.jpg?h=270&drp=1', NULL, 0, 28896, 0, 0, 1, 0, 4201, 0),
(1106, 1, 84, '7', 'server_url', 'Aavi Navratri - Festival - Lyrical', 'http://www.kids2win.com/api/videosFile/Cc85O_aavi-navratri-festival-gujrati-lyrical.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/Y4Xad_aavi-navratri-festival-gujrati-lyrical.jpg?h=270&drp=1', NULL, 1, 35896, 0, 0, 1, 0, 3105, 0),
(1107, 1, 84, '7', 'server_url', 'Radha Ne Shyam Mali Jashe - Navaratri', 'http://www.kids2win.com/api/videosFile/qGsog_radha-ne-shyam-mali-jashe-navaratri-gujrati.mp4', '-', 'Festivals', 'Landscape', 'http://www.kids2win.com/api/thumbImage/M1Nfd_radha-ne-shyam-mali-jashe-navaratri-gujrati.jpg?h=270&drp=1', NULL, 0, 24976, 0, 0, 1, 0, 3104, 0),
(1108, 1, 89, '7', 'server_url', 'Chogada - Navratri', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609270994/yfk36mquhdqufrdc1obe.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609270997/vvdq735lrrqph3uta2ez.jpg', NULL, 129, 526, 19, 0, 1, 1, 0, 0),
(1109, 1, 89, '7', 'server_url', 'Love Gujrati Video Status', 'http://www.kids2win.com/api/videosFile/xSlu6_love-gujrati-video-status-animated.mp4', '-', 'Animated', 'Landscape', 'http://www.kids2win.com/api/thumbImage/GWxaI_love-gujrati-video-status-animated.jpg?h=270&drp=1', NULL, 1, 13355, 0, 0, 1, 0, 2369, 0),
(1110, 1, 89, '7', 'server_url', 'Chandne Kaho Aaje Aathme Nahi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1609309510/nndsieklbnz3fixyeeyo.mp4', '-', 'Animated', 'Landscape', 'http://res.cloudinary.com/dhp7qpdwh/image/upload/v1609309512/h5srk5qfezmcmdaparnk.jpg', NULL, 179, 503, 62, 0, 1, 1, 2106, 0),
(1116, 17, 86, '7,5', 'server_url', 'Tataniya Dhara Vali Maa Khodal', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615036583/u/2021/03/06/32806_36e733c6b11c4778a94be1af26f27400.mp4.mp4', '', 'Khodal,maa', 'Portrait', '6208_video_thumb.jpg', NULL, 0, 2, 0, 0, 1, 0, NULL, 0),
(1120, 17, 86, '7,5', 'server_url', 'Hanuman Chalisa', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615038569/u/2021/03/06/12070_6a6a042f36c048c1a03e34e8aa27320d.mp4.mp4', '-', '', 'Portrait', '92638_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1121, 17, 82, '7,5', 'server_url', 'Shivaji Maharaja', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615039164/u/2021/03/06/44050_158599171d3d46048b367de3be393424.mp4.mp4', '', '', 'Portrait', '70095_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1122, 17, 73, '7,5', 'server_url', 'Milne Lage Hum Pahele Se', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615039395/u/2021/03/06/32235_c3f2b96c525a47b8ac043f82005e76ed.mp4.mp4', '', '', 'Portrait', '58092_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1123, 19, 86, '7', 'server_url', 'Jay Swaminarayan', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615042533/u/2021/03/06/52488_919558024370_status_615aabdb928b4d04a7f4507e4242ff41.mp4.mp4', '', '', 'Landscape', '46015_video_thumb.jpg', NULL, 0, 0, 1, 0, 1, 0, NULL, 0),
(1124, 17, 86, '7,5', 'server_url', 'Krishana', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615097339/u/2021/03/07/98433_359df981444946fea5f0d2ed3c018b38.mp4.mp4', '', '', 'Portrait', '61113_video_thumb.jpg', NULL, 2, 14, 0, 0, 1, 0, NULL, 0),
(1125, 16, 86, '7,5', 'server_url', 'krishna', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615097692/u/2021/03/07/24764_9fd10c4bbd9847b3ad3b3e3d9c08bd21.mp4.mp4', '', '', 'Portrait', '36655_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1126, 21, 85, '6', 'server_url', 'bado', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615201895/u/2021/03/08/84015_berenatiye.arb_20210308_CME1.mp4.mp4', '', '', 'Landscape', '50059_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1127, 21, 75, '6', 'server_url', 'Dezider Bado', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615201937/u/2021/03/08/97316_berenatiye.arb_20210308_CME1.mp4.mp4', '', '', 'Landscape', '91618_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1128, 15, 87, '5', 'server_url', 'Lakho Main Hai Tu Ek Nirala', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615211050/u/2021/03/08/39292_143971590_3659135797512707_2389679614129200885_n.mp4.mp4', '', 'ganpati,lakho main hai tu ek,God', 'Portrait', '12531_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1129, 15, 87, '5', 'server_url', 'Tum Hi Mere Jivan Ho', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615211143/u/2021/03/08/35436_145371591_142292641058811_2192533963237532648_n.mp4.mp4', '', 'tum hi mere jivan ho,ganpati bapa,ganpati,god', 'Portrait', '34904_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1130, 15, 87, '5', 'server_url', 'Bapa\'s Arati', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615211313/u/2021/03/08/81287_158624207_242424254217171_5416839851074631726_n.mp4.mp4', '', 'ganpati aarti,bapa aarati,God,ganapatin bappa', 'Portrait', '49847_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1131, 17, 86, '5,7', 'server_url', 'Ram Siya Ram Jai Jai Ram', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615265417/u/2021/03/09/26860_ced2bd27ec664700bfcc0f28895201c1.mp4.mp4', '', 'Ram,Siya,Ram Siya Ram,Hanuman ji,jaijairam', 'Landscape', '89779_video_thumb.jpg', NULL, 1, 4, 1, 0, 1, 0, NULL, 0),
(1132, 17, 89, '5', 'server_url', 'ye hasi vadiya', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615265482/u/2021/03/09/35414_6e821bf6b7bc4b8d8b8c24522b9f7e76.mp4.mp4', '', 'hasi vadiya,aa gye hum kaha', 'Landscape', '4735_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1133, 17, 103, '5', 'server_url', 'yeh hasi vadiya', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615265528/u/2021/03/09/55046_90ac7a91fc0e4b56854408246fb411b5.mp4.mp4', '', 'hasi vadiya,aa gye hum kaha', 'Landscape', '32392_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1134, 15, 71, '5', 'server_url', 'Jo tu mera humdard hai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615265754/u/2021/03/09/67388_147762735_943010099569045_3624066122354894759_n.mp4.mp4', '', 'humsard,jo tu mera humdard hai,teri dhadkane', 'Portrait', '39307_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1135, 15, 71, '5', 'server_url', 'Swiety', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615265842/u/2021/03/09/1077_149131336_243377810767238_3179673075205606037_n.mp4.mp4', '-', 'sweity', 'Portrait', '54809_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1136, 15, 86, '5', 'server_url', 'Ye jovan ab tere havale', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615265936/u/2021/03/09/28516_149870077_239023587766491_7408910516729906987_n.mp4.mp4', '', 'ye jivan ab tere havale,krishana', 'Portrait', '98756_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1137, 15, 73, '5', 'server_url', 'Kya Dil Ne Kaha', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266035/u/2021/03/09/88184_10000000_474303900243177_2820299181336204927_n.mp4.mp4', '', 'kya dil ne kaha,kya tum ne suna,love', 'Portrait', '29819_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1138, 15, 86, '5', 'server_url', 'Krishana More Saiya', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266131/u/2021/03/09/36369_150928157_467388837977307_2137849223397587487_n.mp4.mp4', '', 'more saiya,krishana,God', 'Portrait', '20978_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1139, 15, 71, '5,7', 'server_url', 'Asamano pe jo khuda Hai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266227/u/2021/03/09/21684_151375928_118479846871567_7921662852437109685_n.mp4.mp4', '', 'aasmano pe jo khuda hai,aaj kuthi mahobat ne angdai li,jubin', 'Portrait', '64116_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1140, 15, 71, '5', 'server_url', 'Ooh Humnava', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266297/u/2021/03/09/38702_151950836_1046818505813423_3071861846193746712_n.mp4.mp4', '', 'tum dena sath mera,humnava,ooh humnava', 'Portrait', '3261_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1141, 15, 72, '5', 'server_url', 'Tu hai Tera Ye Sansar Sa', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615266651/u/2021/03/09/82562_154134089_415874779514663_3618162164864119872_n.mp4.mp4', '', '', 'Portrait', '84159_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1142, 15, 71, '5', 'server_url', 'Naam Tere To Apani Zindagi Kardi', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266772/u/2021/03/09/38381_154797239_1162527477533823_1141064859786034288_n.mp4.mp4', '', 'naam tere to,apni jindgi kardi', 'Portrait', '71837_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1143, 15, 71, '5', 'server_url', 'Mana Anjan Huh Main Tere Vaste', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615266881/u/2021/03/09/70626_154585795_904884540279925_1340546595640912747_n.mp4.mp4', '', 'mana anjan hun me tere vaste,treanding', 'Portrait', '4162_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1144, 15, 73, '5', 'server_url', 'Ab Tumko Dekhake Sanse Chalti Hai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615266994/u/2021/03/09/53469_154819447_355304282251019_7959864663033446794_n.mp4.mp4', '', 'Ab tumko dekhke sanse chalti hai,love', 'Portrait', '33037_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1145, 15, 71, '5', 'server_url', 'shayari', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615267142/u/2021/03/09/32182_155001512_2896967423873496_9000335184683942048_n.mp4.mp4', '', 'shaayari', 'Portrait', '72564_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0);
INSERT INTO `tbl_video` (`id`, `user_id`, `cat_id`, `lang_ids`, `video_type`, `video_title`, `video_url`, `video_id`, `video_tags`, `video_layout`, `video_thumbnail`, `video_duration`, `total_likes`, `totel_viewer`, `total_download`, `featured`, `status`, `isUploaded`, `external_id`, `cloud_id`) VALUES
(1146, 15, 71, '5', 'server_url', 'Jane dil Main Kabse Hai Tu', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615267201/u/2021/03/09/15049_155819771_809982373261393_2193417498624124787_n.mp4.mp4', '', 'jane dil main kabse hai tu', 'Portrait', '88657_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1147, 15, 103, '5', 'server_url', 'Main Jis Din Bhula Du', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615267300/u/2021/03/09/80081_155839431_1076120439557404_1079231407464903886_n.mp4.mp4', '', 'main jis din bhula dun,jubin,love', 'Portrait', '96984_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1148, 15, 71, '5', 'server_url', 'Nahar Na Lag Jaye Jaanu', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615267376/u/2021/03/09/11356_157191427_443193806885546_7955213007446989827_n.mp4.mp4', '', 'najar na lag jaiye,nazar na lag jaye jaanu', 'Portrait', '43905_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1149, 15, 73, '5', 'server_url', 'Abhi Hame Aur Tume', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615267509/u/2021/03/09/47338_156023606_787381545517854_7966335497771349815_n.mp4.mp4', '', 'neha kakkar,guru,guru randhawa,abhi hame aur tumhe', 'Portrait', '30289_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1150, 15, 71, '5', 'server_url', 'Tu hi Hai Aarazu', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615267610/u/2021/03/09/32009_156227497_763153980992055_5157287312649034003_n.mp4.mp4', '-', '', 'Portrait', '72619_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1151, 15, 86, '5', 'server_url', 'Kusi ne Na Kiya Hai Ishq', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615267700/u/2021/03/09/68095_149408243_242707714134917_7743480421341494622_n.mp4.mp4', '', 'krishana,kano,radhe shatam,kisi na kiya hai', 'Portrait', '47960_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1152, 15, 86, '5', 'server_url', 'Tarasati Hai Nigaah Ye Meri', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615267789/u/2021/03/09/71506_149510752_2764079247255012_8819482683366123690_n.mp4.mp4', '', 'tarasti hai nigaah ye meri,krishana', 'Portrait', '75708_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1153, 15, 86, '5', 'server_url', 'Krishana Mitra', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615267848/u/2021/03/09/87139_150449218_749693692583848_6468943302775701029_n.mp4.mp4', '', 'krishana mitra,krishan,krishana vani', 'Portrait', '46387_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1154, 15, 86, '5,7', 'server_url', 'Bhaji Le Bhagvaan Sacha Sant Ne Mli', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615267929/u/2021/03/09/55963_149482400_802412797014214_2407733007056388700_n.mp4.mp4', '', 'Bhaji le bhagvaan sachha sant ne mli,krishana,gujrati', 'Portrait', '55043_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1155, 15, 86, '5', 'server_url', 'Tum mile Dil Khile', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615268046/u/2021/03/09/63822_151238899_1797287920449592_971751172978648413_n.mp4.mp4', '', 'tum mile,krishan,krishana', 'Landscape', '81760_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1156, 15, 86, '5', 'server_url', 'Muj Main Tuh', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615268132/u/2021/03/09/67973_149790426_247048513622460_710123283479422537_n.mp4.mp4', '', '', 'Portrait', '94370_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1157, 15, 86, '5', 'server_url', 'Bas Teri Dhum Dham Hai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615268225/u/2021/03/09/47922_152011826_468521600970862_925856924290716933_n.mp4.mp4', '', 'bas bas teri dhum dham hai,krishana', 'Portrait', '98680_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1158, 15, 86, '5', 'server_url', 'Mora Saiyaan', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615268306/u/2021/03/09/30009_153592524_110018871133326_6512910214208517538_n.mp4.mp4', '', 'mora saiyaan,krishana,radhe krishna', 'Landscape', '59220_video_thumb.jpg', NULL, 1, 0, 0, 0, 1, 0, NULL, 0),
(1159, 15, 86, '5', 'server_url', 'Tarasti hai nigaahe meri', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615268389/u/2021/03/09/85220_154031014_1341103996251644_3919372113109415513_n.mp4.mp4', '', 'tarasti hai nigaah ye meri,krishana,krishna', 'Portrait', '56239_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1160, 15, 86, '5', 'server_url', 'Tu Hai Tera Ye Sansar Sa', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615268532/u/2021/03/09/46817_155450200_190764342384605_3357084932548231705_n.mp4.mp4', '', 'Tu hai tera ye sansaar sa,krishan,krishna', 'Portrait', '38633_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1161, 15, 86, '5', 'server_url', 'Pake Tuje Main Khoya Khoya', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615268611/u/2021/03/09/23079_157518122_254628316252568_7811010886752739756_n.mp4.mp4', '', 'pake tuje main khoya khoya man,krishana', 'Portrait', '97049_video_thumb.jpg', NULL, 0, 2, 0, 0, 1, 0, NULL, 0),
(1162, 15, 86, '5', 'server_url', 'Radhe Radhe Hum Tere Bin Adhe', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615268711/u/2021/03/09/34707_156213991_913230876156020_1574933859447983920_n.mp4.mp4', '', 'radhe radhe hum tere bin aadhe,radhe krishna,krishana', 'Portrait', '57249_video_thumb.jpg', NULL, 0, 0, 1, 0, 1, 0, NULL, 0),
(1163, 29, 85, '7', 'server_url', 'exam', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615307089/u/2021/03/09/79307_VID-20210304-WA0004.mp4.mp4', '', '', 'Landscape', '53872_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1164, 29, 86, '7', 'server_url', 'kashtbhanjan Dev', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615307346/u/2021/03/09/661_VID-20210123-WA0002.mp4.mp4', '-', '', 'Portrait', '1782_video_thumb.jpg', NULL, 2, 11, 2, 0, 1, 0, NULL, 0),
(1165, 25, 80, '7', 'server_url', 'Shree KashtabhanjanDev', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615311918/u/2021/03/09/36260_1607792225094.mp4.mp4', '', '', 'Landscape', '34386_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1166, 17, 75, '5', 'server_url', 'Bewafa Tera Masoom Chehara', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615312732/u/2021/03/09/93021_154411482_742787489959495_7236674980490123285_n.mp4.mp4', '', 'bewafa Tera Masoom Chehara,bewafa,Masoom Chehara,jubin', 'Portrait', '79850_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1167, 17, 72, '5,7', 'server_url', 'Om Namah Shivai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615312846/u/2021/03/09/73193_157580866_3852469548152479_9123488472304314800_n.mp4.mp4', '', 'Om namh Shivai,mahadev,om namah', 'Portrait', '74534_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1168, 17, 72, '5', 'server_url', 'Har har mahadev', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615312925/u/2021/03/09/91837_144738951_849783812532682_1327525511467504096_n.mp4.mp4', '', 'har har mahadev,mahadev,Pooja mahadev', 'Portrait', '78284_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1169, 17, 72, '5,7', 'server_url', 'Raja Chela Ji Bhole Ki', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615313043/u/2021/03/09/42197_150401131_2817094715195426_5389195892291966513_n.mp4.mp4', '', 'bhole ki divani,gori shankar,mahadev', 'Landscape', '89712_video_thumb.jpg', NULL, 0, 0, 1, 0, 1, 0, NULL, 0),
(1170, 17, 72, '5', 'server_url', 'Tarasti Hai Nigaah Meri', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615313136/u/2021/03/09/30460_152733013_283557516522381_6896319835598864943_n.mp4.mp4', '', 'Taras ti hai nigaah meri,chahne walo ki,mahadev', 'Portrait', '91022_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1171, 17, 72, '5,7', 'server_url', 'Bhole Bam bam', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615313227/u/2021/03/09/73170_103475903_564909241122077_5989574513210880227_n.mp4.mp4', '', 'bolo bam bam,bhole bam bam,bhole nath', 'Portrait', '83392_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1172, 17, 72, '7,5', 'server_url', 'Fesla lele Hosla Dede', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615313356/u/2021/03/09/57334_109440330_749963505750354_744711434620046787_n.mp4.mp4', '', 'Hosla Dede,mahadev,shiv,shivratri', 'Portrait', '14506_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1173, 17, 72, '5,7', 'server_url', 'Shiv Tandav', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615313430/u/2021/03/09/75633_117225602_173407360890545_8444765742819032692_n.mp4.mp4', '', 'Shiv tandav,mahadev,bholenath', 'Portrait', '18574_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1174, 17, 72, '5,7', 'server_url', 'Mujhe Dar Nahi Kisi Aur Se', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615351125/u/2021/03/10/61569_155396222_183107786606517_8162015427096917437_n.mp4.mp4', '', 'mujhe dar nhi kisi aur se,mahadev,shiva,shivratri', 'Portrait', '14606_video_thumb.jpg', NULL, 0, 0, 1, 0, 1, 0, NULL, 0),
(1175, 17, 72, '5,7', 'server_url', 'Shivratri aa rhi hai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615358492/u/2021/03/10/4318_154681764_476673306849717_5139046984395189744_n.mp4.mp4', '', 'shivratri aa rahi hai,mahadev,shivratri special', 'Portrait', '64325_video_thumb.jpg', NULL, 0, 2, 0, 0, 1, 0, NULL, 0),
(1176, 17, 72, '5', 'server_url', 'Raja bhi hai Rank bhi gai', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615358559/u/2021/03/10/98748_125365583_1020248341807112_8011060531382433871_n.mp4.mp4', '', 'Raja bhi gai rank bhi gai,shivratri,mahadev,tandav', 'Portrait', '34953_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1177, 17, 72, '5,7', 'server_url', 'Kon Hai Tu Kon Kaha Se Tu Aya', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615358907/u/2021/03/10/25457_154585763_1078815185944455_1870844329703172807_n-1.mp4.mp4', '', 'kon hai tu kon kaha se tu aya,mahadev,shivratri', 'Portrait', '10214_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1178, 17, 72, '5', 'server_url', 'Shivaji Tandav', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615359271/u/2021/03/10/36780_158659623_829565377631657_3249761687196248898_n.mp4.mp4', '', 'Shiv,mahadev,tandav', 'Portrait', '50550_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1179, 36, 73, '5,7', 'server_url', 'miha\'s love story', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615394199/u/2021/03/10/17095_VID_42531109_082027_009.mp4.mp4', '', '', 'Portrait', '9109_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1180, 25, 80, '7,6', 'server_url', 'Shree KashtabhanjanDev Hanumanji Mandir Salangpur', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615397668/u/2021/03/10/26656_1605929114541.mp4.mp4', '', '', 'Landscape', '29204_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1181, 17, 72, '5,7', 'server_url', 'Om Namh Shivai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398020/u/2021/03/10/37564_119893972_3381183521941409_9161265843694632269_n.mp4.mp4', '', 'Om Namh Shivai,mahadev', 'Portrait', '92124_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1182, 17, 72, '5,7', 'server_url', 'Sar Pe Tere O ganga Maiya biraje', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398111/u/2021/03/10/96825_122782297_204936614409497_7890940112717296328_n.mp4.mp4', '', 'Om Namh Shivai', 'Portrait', '75019_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1183, 17, 72, '5', 'server_url', 'Namh Shivai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398282/u/2021/03/10/55540_123646041_784453912351696_4745626611232468159_n.mp4.mp4', '', 'Namh Shivai', 'Portrait', '96680_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1184, 17, 72, '5', 'server_url', 'Bhakat Nhi Vo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615398380/u/2021/03/10/44371_143713775_322769272430600_8342500272514101359_n.mp4.mp4', '', 'shiva', 'Portrait', '50273_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1185, 17, 72, '5,7', 'server_url', 'Namoh Namoh', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398490/u/2021/03/10/73138_149637875_425766078711290_1891273973143968313_n.mp4.mp4', '', 'namoh namoh', 'Portrait', '7946_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1186, 17, 72, '5,7', 'server_url', 'Shiva', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398576/u/2021/03/10/48324_153110592_164720178643511_5172330596602227639_n-1.mp4.mp4', '', 'shiva', 'Portrait', '67499_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1187, 17, 72, '5,7', 'server_url', 'Namoh Namoh', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398638/u/2021/03/10/93468_153633559_852837605271034_572159725393324453_n.mp4.mp4', '', 'namoh namo', 'Portrait', '2853_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1188, 17, 72, '5,7', 'server_url', 'Omkar hai Uski Vani', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615398766/u/2021/03/10/85848_154950077_183343033551661_2520206160766076284_n.mp4.mp4', '', 'Omkar,shiva', 'Portrait', '66295_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1189, 17, 72, '5', 'server_url', 'Tum jal dhan main', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615399108/u/2021/03/10/20027_109873864_928542097663021_39540003426958910_n.mp4.mp4', '', 'shiva', 'Portrait', '43155_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1190, 17, 72, '5', 'server_url', 'Tere Darbaar Main', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615399186/u/2021/03/10/25454_136966806_258721022316546_4464495302583599177_n.mp4.mp4', '', 'shiva,darbaar main', 'Portrait', '18920_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1191, 15, 72, '5', 'server_url', 'Shiv Parvati', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615399249/u/2021/03/10/52286_InShot_20210310_232721587.mp4.mp4', '', 'shiv', 'Landscape', '33402_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1192, 17, 72, '5', 'server_url', 'Bhole Babaji', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615399295/u/2021/03/10/2574_156321249_966034617306794_1525038469226272419_n-1.mp4.mp4', '', 'shiva', 'Portrait', '81730_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1193, 17, 72, '5,7', 'server_url', 'Adiyogi', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615399403/u/2021/03/10/33162_123100580_274579927207302_2264314040201592908_n.mp4.mp4', '', 'adiyogi', 'Portrait', '17530_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1195, 17, 72, '5', 'server_url', 'Bholenath Shankra', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615399542/u/2021/03/10/243_129961693_732279657642877_3563538061613222350_n.mp4.mp4', '', 'Namh Shivai', 'Portrait', '78349_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1196, 17, 72, '5,7', 'server_url', 'Shiv Tandav', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401566/u/2021/03/11/30563_87378145_123409052439561_1388035586086083525_n.mp4.mp4', '', 'Shiv tandav', 'Portrait', '92426_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1197, 17, 72, '5', 'server_url', 'Bhole Ki Ganga Main', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401620/u/2021/03/11/91702_86606417_1041996072826905_4531673282921246163_n-1.mp4.mp4', '', 'Bhole Ki Ganga main', 'Landscape', '95510_video_thumb.jpg', NULL, 1, 0, 0, 0, 1, 0, NULL, 0),
(1198, 17, 72, '5', 'server_url', 'Bhole Ki Talash Main', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401695/u/2021/03/11/82784_111067345_639087826705405_1215135823633314282_n-1.mp4.mp4', '', 'bhole ki talash main', 'Landscape', '98364_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1199, 17, 72, '5', 'server_url', 'Faisla Dede Hosla dede', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401841/u/2021/03/11/7641_107821006_590040641505522_3177269886127543300_n.mp4.mp4', '', 'Hosla dede,shiva', 'Landscape', '43912_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1200, 17, 72, '5,7', 'server_url', 'Shivratri', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401894/u/2021/03/11/47199_157741040_1568787386640784_5670890206333552767_n.mp4.mp4', '', 'Shivratri', 'Portrait', '78435_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1201, 17, 72, '5,7', 'server_url', 'Teri Kasturi Rain Jagai', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615401944/u/2021/03/11/6464_159115241_194788388649385_5541305764364928622_n.mp4.mp4', '', 'Teri Kasturi,shiv', 'Portrait', '69536_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1202, 17, 72, '5', 'server_url', 'Mahadeva', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402000/u/2021/03/11/73744_87052492_478820066329525_4069273040440783327_n.mp4.mp4', '', 'Mahadeva', 'Portrait', '15600_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1203, 17, 72, '5', 'server_url', 'Shambhu nath re', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402152/u/2021/03/11/91662_87391393_192708138604104_8514168353104040450_n%280%29.mp4.3gp', '', 'Mera bhola hai bhandari', 'Portrait', '1798_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1204, 17, 72, '5,7', 'server_url', 'Adiyogi', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402207/u/2021/03/11/84339_87044631_181255633176001_4602982773054464112_n.mp4.mp4', '', 'mahadev adiyogi', 'Landscape', '98363_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1205, 17, 72, '5', 'server_url', 'Jai Mahakal', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402254/u/2021/03/11/40812_158295571_267453308167468_5992316408677337484_n.mp4.mp4', '', 'jai mahakal', 'Portrait', '54908_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1206, 17, 72, '5', 'server_url', 'Namo Namo', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402309/u/2021/03/11/8926_158420037_2061504823990969_7285260557312545147_n.mp4.mp4', '', '', 'Portrait', '36228_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1207, 17, 72, '5', 'server_url', 'Mahadev Pray', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402356/u/2021/03/11/95808_136144365_394279838334957_1029148116479994837_n.mp4.mp4', '', 'mahadev tere bina', 'Portrait', '1261_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1208, 17, 72, '5', 'server_url', 'Tere Dar Se Me lti Shiva', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402417/u/2021/03/11/82353_143519306_922192711932025_1550136306658656897_n.mp4.mp4', '', 'tere dar se', 'Portrait', '79012_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1209, 17, 72, '5', 'server_url', 'Ek pal Ko Tu Nazar Jukah', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402482/u/2021/03/11/13017_155794272_732721584097754_2100679469535000269_n.mp4.mp4', '', 'ek pal to tu Nazar juka', 'Portrait', '38900_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1210, 17, 72, '5,7', 'server_url', 'Happy Shivratri', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402530/u/2021/03/11/13202_158470990_761893011119274_5763617121960731741_n.mp4.mp4', '', 'happy Shivratri', 'Portrait', '66058_video_thumb.jpg', NULL, 1, 2, 0, 0, 1, 0, NULL, 0),
(1211, 17, 72, '5,7', 'server_url', 'Devo ke Dev Mahadev', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402602/u/2021/03/11/16488_159273447_782142269062076_8465734020330387135_n.mp4.mp4', '', 'mahadev,shivratri', 'Landscape', '34315_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1212, 17, 72, '5', 'server_url', 'Shiv Kailasho Ke Vasi', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615402655/u/2021/03/11/14598_155878945_435719317486421_893226747333093238_n.mp4.mp4', '', 'Shiv Kailasho ke Vasi', 'Portrait', '48398_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1213, 17, 72, '5', 'server_url', 'Yaha Raja Bhi Gye', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615402724/u/2021/03/11/46937_158956942_432440144678161_4958230315054540892_n.mp4.mp4', '', 'yaha Raja bhi gye,shabhu', 'Portrait', '88212_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1214, 17, 72, '5', 'server_url', 'Jai Ho Jai Ho Shankra', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615440074/u/2021/03/11/61930_ca2991b56dc74d6e98c8ba6d5d5c7984.mp4.mp4', '', 'Adidev shankara', 'Portrait', '50549_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1215, 15, 72, '5', 'server_url', 'Mera Bhola', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615442521/u/2021/03/11/44412_a7acec4d3b004ff68366b66246f33bc3.mp4.mp4', '', 'bhola', 'Portrait', '40269_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1216, 15, 72, '5', 'server_url', 'Mahadeva', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615442607/u/2021/03/11/97558_36b5a546d02748a082ef4664a6878c1b.mp4.mp4', '', 'mahadev', 'Landscape', '4065_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1217, 17, 86, '5', 'server_url', 'Tune to Palbhar Main', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615452290/u/2021/03/11/7952_158224638_1091252811282273_3602727323535186757_n.mp4.mp4', '', 'krishna,morapiya', 'Portrait', '57996_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1218, 17, 72, '5', 'server_url', 'Namo Namo Shankra', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615452366/u/2021/03/11/95324_153098515_258749939070938_5770844812304870979_n.mp4.mp4', '', 'namo namo', 'Portrait', '196_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1219, 17, 89, '5,7,6', 'server_url', 'Nature', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615452439/u/2021/03/11/11295_152410063_1099254307222812_4314033870354963573_n.mp4.mp4', '', 'Nature', 'Portrait', '92348_video_thumb.jpg', NULL, 1, 0, 0, 0, 1, 0, NULL, 0),
(1220, 39, 86, '7', 'server_url', 'Jay thakar', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615459397/u/2021/03/11/11166_RoposoVideo67399524-4120-454b-b94a-2266ba660fb1.mp4.mp4', '', '', 'Portrait', '41530_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1221, 39, 86, '7', 'server_url', 'meldi maa', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615459519/u/2021/03/11/31167_RoposoVideodcc17a34-476f-430f-814b-fe63c7f829a0.mp4.mp4', '', '', 'Portrait', '33048_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1222, 39, 86, '7', 'server_url', 'Jay bhole', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615459763/u/2021/03/11/30090_RoposoVideo787c6817-886e-43d4-9f38-717eee340dac.mp4.mp4', '', '', 'Portrait', '16053_video_thumb.jpg', NULL, 0, 0, 0, 0, 0, 0, NULL, 0),
(1223, 17, 86, '5,7', 'server_url', 'Kashatbhajan Dev', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615549313/u/2021/03/12/32979_132903491_1014597382359883_8163736774488063794_n.mp4.mp4', '', 'kashatbhajan', 'Landscape', '64742_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1224, 17, 86, '7,5', 'server_url', 'Dada mahan che', 'http://res.cloudinary.com/dhp7qpdwh/video/upload/v1615549439/u/2021/03/12/42751_136480366_190806186066232_641896650099443976_n.mp4.mp4', '', '', 'Landscape', '75458_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1225, 17, 86, '7,5', 'server_url', 'Raghupati raghav', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615549554/u/2021/03/12/62191_118850273_1019233288542034_5832515697935179835_n.mp4.mp4', '', '', 'Portrait', '31397_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1226, 17, 86, '7,5', 'server_url', 'Sarangpur dham', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615549695/u/2021/03/12/96001_147103356_3926244064065899_5296065660217634951_n.mp4.mp4', '', '', 'Portrait', '42849_video_thumb.jpg', NULL, 0, 0, 0, 0, 1, 0, NULL, 0),
(1227, 17, 86, '7,5', 'server_url', 'jori shyamni radhani', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615549839/u/2021/03/12/6731_19387fee36fc4dd28fa19f3ea17df155.mp4.mp4', '', '', 'Portrait', '46313_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0),
(1228, 17, 86, '7,5', 'server_url', 'Mangla Charan', 'http://res.cloudinary.com/dsxarp1ws/video/upload/v1615549988/u/2021/03/12/24680_ffedfdcb539a41078e06b82cf9726a42.mp4.mp4', '', '', 'Portrait', '47354_video_thumb.jpg', NULL, 0, 1, 0, 0, 1, 0, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_list`
--
ALTER TABLE `tbl_contact_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_sub`
--
ALTER TABLE `tbl_contact_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_deleted_users`
--
ALTER TABLE `tbl_deleted_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_favourite`
--
ALTER TABLE `tbl_favourite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_follows`
--
ALTER TABLE `tbl_follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_img_status`
--
ALTER TABLE `tbl_img_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_language`
--
ALTER TABLE `tbl_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_like`
--
ALTER TABLE `tbl_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment_mode`
--
ALTER TABLE `tbl_payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quotes`
--
ALTER TABLE `tbl_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_smtp_settings`
--
ALTER TABLE `tbl_smtp_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_spinner`
--
ALTER TABLE `tbl_spinner`
  ADD PRIMARY KEY (`block_id`);

--
-- Indexes for table `tbl_suspend_account`
--
ALTER TABLE `tbl_suspend_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_redeem`
--
ALTER TABLE `tbl_users_redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_rewards_activity`
--
ALTER TABLE `tbl_users_rewards_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_verify_user`
--
ALTER TABLE `tbl_verify_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_contact_list`
--
ALTER TABLE `tbl_contact_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_contact_sub`
--
ALTER TABLE `tbl_contact_sub`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_deleted_users`
--
ALTER TABLE `tbl_deleted_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_favourite`
--
ALTER TABLE `tbl_favourite`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_follows`
--
ALTER TABLE `tbl_follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_img_status`
--
ALTER TABLE `tbl_img_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_language`
--
ALTER TABLE `tbl_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_like`
--
ALTER TABLE `tbl_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_payment_mode`
--
ALTER TABLE `tbl_payment_mode`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_quotes`
--
ALTER TABLE `tbl_quotes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_reports`
--
ALTER TABLE `tbl_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_smtp_settings`
--
ALTER TABLE `tbl_smtp_settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_spinner`
--
ALTER TABLE `tbl_spinner`
  MODIFY `block_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_suspend_account`
--
ALTER TABLE `tbl_suspend_account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tbl_users_redeem`
--
ALTER TABLE `tbl_users_redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users_rewards_activity`
--
ALTER TABLE `tbl_users_rewards_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `tbl_verify_user`
--
ALTER TABLE `tbl_verify_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1229;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
