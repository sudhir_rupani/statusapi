<?php 
    $action=$_GET['action'];
    $page_title=ucfirst($action).' Image Status';
    $active_page="status";
    
    include("includes/header.php");

    require("includes/function.php");
    require("language/language.php");

    if(isset($_GET['edit_id']))
    {
      $qry="SELECT * FROM tbl_img_status where id='".$_GET['edit_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

      $lang_ids=explode(',', $row['lang_ids']);
    }

    if(isset($_POST['btn_submit']) AND $action=='add')
    {
        $lang_ids=implode(',', $_POST['lang_id']);

        $image_tags=implode(',', $_POST['image_tags']);

        $file_size=round($_FILES['image_file']['size'] / 1024 / 1024, 2);

        if($file_size > $settings_details['image_file_size']) { 
            $_SESSION['class']='alert-danger';
            $_SESSION['msg']="Image file size must be less or equal to ".$settings_details['image_file_size']." MB";
            header( "Location:image_status.php?action=add");
            exit;
        }

        $ext = pathinfo($_FILES['image_file']['name'], PATHINFO_EXTENSION);

        $image_file=rand(0,99999)."_image_status.".$ext;

        //Main Image
        $tpath1='images/'.$image_file;   

        if($ext!='png')  {
          $pic1=compress_image($_FILES["image_file"]["tmp_name"], $tpath1, 80);
        }
        else{
          $tmp = $_FILES['image_file']['tmp_name'];
          move_uploaded_file($tmp, $tpath1);
        }

        $data = array( 
          'cat_id'  =>  $_POST['cat_id'],
          'lang_ids'  =>  $lang_ids,
          'image_title'  =>  addslashes($_POST['image_title']),
          'image_tags'  =>  $image_tags,
          'image_layout'  =>  $_POST['image_layout'],
          'image_file'  =>  $image_file,
          'status_type'  =>  'image'
        ); 

        $insert = Insert('tbl_img_status',$data); 

        $last_id = mysqli_insert_id($mysqli);
      
        if(isset($_POST['notify_user'])){

          $img_path=$file_path_img.'images/'.$image_file;

          $content = array(
            "en" => "New image is added by Admin",
          );
          
          $fields = array(
              'app_id' => ONESIGNAL_APP_ID,
              'included_segments' => array('All'),
              'data' => array("foo" => "bar","type" => "single_status","status_type" => "image","id" => $last_id,"external_link"=>false),
              'headings'=> array("en" => APP_NAME),
              'contents' => $content,
              'big_picture' =>$img_path
          );

          $fields = json_encode($fields);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                     'Authorization: Basic '.ONESIGNAL_REST_KEY));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $notify_res = curl_exec($ch);
          curl_close($ch);
        }

        $_SESSION['msg']="10";
        header( "Location:manage_image_status.php");
        exit; 
    }
    else if(isset($_POST['btn_submit']) AND ($action=='edit' OR isset($_GET['edit_id'])))
    {
        $lang_ids=implode(',', $_POST['lang_id']);

        $image_tags=implode(',', $_POST['image_tags']);

        if (!empty($_FILES['image_file']['name'])) {

            $file_size=round($_FILES['image_file']['size'] / 1024 / 1024, 2);

            if($file_size > $settings_details['image_file_size']) { 
                $_SESSION['class']='alert-danger';
                $_SESSION['msg']="Image file size must be less or equal to ".$settings_details['image_file_size']." MB";
                header( "Location:image_status.php?action=add");
                exit;
            }

            unlink('images/'.$row['image_file']);

            $ext = pathinfo($_FILES['image_file']['name'], PATHINFO_EXTENSION);

            $image_file=rand(0,99999)."_image_status.".$ext;

            //Main Image
            $tpath1='images/'.$image_file;   

            if($ext!='png')  {
              $pic1=compress_image($_FILES["image_file"]["tmp_name"], $tpath1, 80);
            }
            else{
              $tmp = $_FILES['image_file']['tmp_name'];
              move_uploaded_file($tmp, $tpath1);
            }
        }
        else{
          $image_file=$row['image_file'];
        }
        

        $data = array( 
          'cat_id'  =>  $_POST['cat_id'],
          'lang_ids'  =>  $lang_ids,
          'image_title'  =>  addslashes($_POST['image_title']),
          'image_tags'  =>  $image_tags,
          'image_layout'  =>  $_POST['image_layout'],
          'image_file'  =>  $image_file
        );  

        $update=Update('tbl_img_status', $data, "WHERE id = '".$_POST['edit_id']."'");

        $_SESSION['class']='alert-success';
        $_SESSION['msg']="11"; 
        header( "Location:image_status.php?edit_id=".$_POST['edit_id']."&action=edit");
        exit;

    }

?>
  
<!-- For Bootstrap Tags -->
<link rel="stylesheet" type="text/css" href="assets/bootstrap-tag/bootstrap-tagsinput.css">
<!-- End -->

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row mrg-top">
        <div class="col-md-12">
          <div class="col-md-12 col-sm-12">
            <?php if(isset($_SESSION['msg'])){?> 
             <div class="alert <?=$_SESSION['class']?> alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <?php if(!empty($client_lang[$_SESSION['msg']])){ echo $client_lang[$_SESSION['msg']]; }else{ echo $_SESSION['msg']; } ?></div>
            <?php unset($_SESSION['msg'], $_SESSION['class']);}?> 
          </div>
        </div>
      </div>
      <div class="card-body mrg_bottom"> 
        <form action="" name="addeditlanguage" method="post" class="form form-horizontal" enctype="multipart/form-data">
          <input  type="hidden" name="edit_id" value="<?php echo $_GET['edit_id'];?>" />

          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Category :-</label>
                <div class="col-md-6">
                  <select name="cat_id" id="cat_id" class="select2" required>
                    <option value="">--Select Category--</option>
                    <?php
                        $cat_qry="SELECT * FROM tbl_category WHERE `status`='1' ORDER BY `category_name`";
                        $cat_result=mysqli_query($mysqli,$cat_qry);
                        while($cat_row=mysqli_fetch_array($cat_result))
                        {
                    ?>                       
                      <option value="<?php echo $cat_row['cid'];?>" <?=($row['cat_id']==$cat_row['cid']) ? 'selected' : '';?>><?php echo $cat_row['category_name'];?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label" for="lang_id">Languages:-</label>
                <div class="col-md-6">

                  <select name="lang_id[]" id="lang_id" class="select2" multiple="" required>
                    <?php
                        

                        $sql="SELECT * FROM tbl_language WHERE `status`='1' ORDER BY `language_name`";
                        $res=mysqli_query($mysqli,$sql);
                        while($row_data=mysqli_fetch_assoc($res))
                        {
                    ?>                       
                      <option value="<?php echo $row_data['id'];?>" <?=(isset($_GET['edit_id']) && in_array($row_data['id'], $lang_ids)) ? 'selected' : ''; ?>><?php echo ucfirst(strtolower($row_data['language_name']));?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Title :-</label>
                <div class="col-md-6">
                  <input type="text" name="image_title" placeholder="Enter image title" id="image_title" value="<?php echo $row['image_title'];?>" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tags(Optional):-</label>
                <div class="col-md-6">
                  <input type="text" name="image_tags[]" id="image_tags" value="<?php  echo $row['image_tags'];?>" data-role="tagsinput" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Image Layout </label>
                <div class="col-md-6">
                  <select name="image_layout" id="image_layout" style="width:280px; height:25px;" class="select2" required="">
                    <option value="Landscape" <?=(isset($_GET['edit_id']) && $row['image_layout']=='Landscape') ? 'selected' : ''; ?>>Landscape</option>
                    <option value="Portrait" <?=(isset($_GET['edit_id']) && $row['image_layout']=='Portrait') ? 'selected' : ''; ?>>Portrait</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Select Image :-
                  <p class="control-label-help">(Recommended resolution: Landscape: 800x500,650x450<br/>Portrait: 720X1280, 640X1136, 350x800)<br/><strong>Note:</strong> Maximum <strong><?=$settings_details['image_file_size']?>MB</strong> file size)</p>
                </label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="image_file" value="fileupload" id="fileupload" accept=".png, .jpg, .jpeg" onchange="fileValidation()" <?=(!isset($_GET['edit_id'])) ? 'required' : ''?>>
                    <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="image alt" /></div>
                  </div>
                  <div id="uploadPreview">
                    <?php if(isset($_GET['edit_id'])){ ?>
                      <img width="100%" src="images/<?php echo $row['image_file']?>" style="margin-bottom: 20px"/>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <?php if(!isset($_GET['edit_id'])){ ?>
              <div class="form-group">
                <label class="col-md-3 control-label">Send notification:-</label>
                <div class="col-md-6" style="padding-top: 10px">
                  <input type="checkbox" id="ckbox_notify" class="cbx hidden" name="notify_user" value="true"/>
                  <label for="ckbox_notify" class="lbl"></label>
                </div>
              </div>
              <?php } ?>
              <br/>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="btn_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
        
<?php include("includes/footer.php");?>

<script type="text/javascript" src="assets/bootstrap-tag/bootstrap-tagsinput.js"></script>

<script type="text/javascript">

  $('#image_tags').tagsinput();
  
  function fileValidation(){
    var fileInput = document.getElementById('fileupload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/i;
    if(!allowedExtensions.exec(filePath)){
        
        if(filePath!=''){
          alert('Please upload file having extension .png, .jpg, .jpeg .PNG, .JPG, .JPEG only.');
          fileInput.value = '';
          return false;  
        }
        else{
          $("#uploadPreview img").hide();
          fileInput.setAttribute("required", "required");
        }
    }else{
        //image preview
        if (fileInput.files && fileInput.files[0]) {

            var file_size=parseFloat(((fileInput.files[0].size) / (1024 * 1024)).toFixed(2));
            var required_file_size=parseFloat('<?=$settings_details['image_file_size']?>');

            if(file_size <= required_file_size)
            {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('uploadPreview').innerHTML = '<img src="'+e.target.result+'" style="width:100%;height:100%;margin-bottom: 20px"/>';
              };
              reader.readAsDataURL(fileInput.files[0]);
            }
            else{
              fileInput.value = '';
              alert("Image file size must be less or equal to "+required_file_size+" MB");
              return false;
            }
        }
    }
  }


</script>