<?php 
  $page_title="Add Video";
  $active_page="status";
  
  include("includes/header.php");
	include("includes/connection.php");
  include("includes/function.php");
	include("language/language.php"); 

  $protocol = strtolower( substr( $_SERVER[ 'SERVER_PROTOCOL' ], 0, 5 ) ) == 'https' ? 'https' : 'http'; 

  $file_path_img = $protocol.'://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/';

	if(isset($_POST['submit']))
	{

      $lang_ids=implode(',', $_POST['lang_id']);
      $video_id='-';

      $video_tags=implode(',', $_POST['video_tags']);

      if ($_POST['video_type']=='server_url')
      {
          $video_url=addslashes(trim($_POST['video_url']));
          
      }
      else if ($_POST['video_type']=='local')
      {
          $path = "uploads/"; //set your folder path

          $file_size=round($_FILES['video_local']['size'] / 1024 / 1024, 2);

          if($file_size > $settings_details['video_file_size']) { 
              $_SESSION['class']='alert-danger';
              $_SESSION['msg']="Video file size must be less or equal to ".$settings_details['video_file_size']." MB";
              header( "Location:add_video.php");
              exit;
          }

          $video_local=rand(0,99999)."_".str_replace(" ", "-", $_FILES['video_local']['name']);

          $tmp = $_FILES['video_local']['tmp_name'];
          
          if (move_uploaded_file($tmp, $path.$video_local)) 
          {
              $video_url=$video_local;
          } else {
              echo "Error in uploading video file !!";
              exit;
          }
      }

      $ext = pathinfo($_FILES['video_thumbnail']['name'], PATHINFO_EXTENSION);

      $video_thumbnail=rand(0,99999)."_video_thumb.".$ext;

      //Main Image
      $tpath1='images/'.$video_thumbnail;   

      if($ext!='png')  {
        $pic1=compress_image($_FILES["video_thumbnail"]["tmp_name"], $tpath1, 80);
      }
      else{
        $tmp = $_FILES['video_thumbnail']['tmp_name'];
        move_uploaded_file($tmp, $tpath1);
      }

      $data = array( 
		    'cat_id'  =>  $_POST['cat_id'],
        'lang_ids'  =>  $lang_ids,
        'video_tags'  =>  $video_tags,
		    'video_type'  =>  $_POST['video_type'],
		    'video_title'  =>  addslashes($_POST['video_title']),
        'video_url'  =>  $video_url,
        'video_id'  =>  $video_id,
        'video_layout'  =>  $_POST['video_layout'],
        'video_thumbnail'  =>  $video_thumbnail
		    );		

	 		$qry = Insert('tbl_video',$data);	
      
      $last_id = mysqli_insert_id($mysqli);
      
      if(isset($_POST['notify_user'])){

          $img_path=$file_path_img.'images/'.$video_thumbnail;

          $content = array(
            "en" => "New video is added by Admin",
          );

          $fields = array(
              'app_id' => ONESIGNAL_APP_ID,
              'included_segments' => array('All'),
              'data' => array("foo" => "bar","type" => "single_status","status_type" => 'video',"id" => $last_id,"external_link"=>false),
              'headings'=> array("en" => APP_NAME),
              'contents' => $content,
              'big_picture' =>$img_path
          );

          $fields = json_encode($fields);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                     'Authorization: Basic '.ONESIGNAL_REST_KEY));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $notify_res = curl_exec($ch);
          curl_close($ch);
      }

      $_SESSION['class']='alert-success';
      $_SESSION['msg']="10";
		  header( "Location:add_video.php");
		  exit;
	}
	  
?>

<!-- For Bootstrap Tags -->
<link rel="stylesheet" type="text/css" href="assets/bootstrap-tag/bootstrap-tagsinput.css">
<!-- End -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(e) {
         $("#video_type").change(function(){
        
         var type=$("#video_type").val();
            
            if(type=="server_url")
            {
               
               $("#video_url_display").show();
               $("#thumbnail").show();
               $("#video_local_display").hide();
            }
            else
            {     
              $("#video_url_display").hide();               
              $("#video_local_display").show();
              $("#thumbnail").show();

            }    
            
       });
    });
</script>
<script type="text/javascript">
  $(document).ready(function(event){
      $('#video_local').change(function(e){

        var file_size=parseFloat(((this.files[0].size) / (1024 * 1024)).toFixed(2));
        var required_file_size=parseFloat('<?=$settings_details['video_file_size']?>');

        if(file_size <= required_file_size)
        {
          if(isVideo($(this).val())){
            $('.video-preview').attr('src', URL.createObjectURL(this.files[0]));
            $('#uploadPreview').show();
          }
          else
          {
            $('#video_local').val('');
            $('#uploadPreview').hide();
            if($(this).val()!='')
              alert("Only video files are allowed to upload.")
          }
        }
        else{
          $('#video_local').val('');
          $('#uploadPreview').hide();
          alert("Video file size must be less or equal to "+required_file_size+" MB");
        }
      });
  });
  function isVideo(filename) {
      var ext = getExtension(filename);
      switch (ext.toLowerCase()) {
      case 'm4v':
      case 'avi':
      case 'mp4':
      case 'mov':
      case 'mpg':
      case 'mpeg':
          // etc
          return true;
      }
      return false;
  }

  function getExtension(filename) {
      var parts = filename.split('.');
      return parts[parts.length - 1];
  }

</script>

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Add Video</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
                 <div class="alert <?=$_SESSION['class']?> alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <?php if(!empty($client_lang[$_SESSION['msg']])){ echo $client_lang[$_SESSION['msg']]; }else{ echo $_SESSION['msg']; } ?></div>
                <?php unset($_SESSION['msg'], $_SESSION['class']);}?> 
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="add_form" method="post" class="form form-horizontal" enctype="multipart/form-data">
 
              <div class="section">
                <div class="section-body">
                   <div class="form-group">
                    <label class="col-md-3 control-label">Category :-</label>
                    <div class="col-md-6">
                      <select name="cat_id" id="cat_id" class="select2" required>
                        <option value="">--Select Category--</option>
          							<?php
                            $cat_qry="SELECT * FROM tbl_category WHERE `status`='1' ORDER BY `category_name`";
                            $cat_result=mysqli_query($mysqli,$cat_qry);
          									while($cat_row=mysqli_fetch_array($cat_result))
          									{
          							?>          						 
          							<option value="<?php echo $cat_row['cid'];?>"><?php echo $cat_row['category_name'];?></option>	          							 
          							<?php
          								}
          							?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label" for="lang_id">Languages:-</label>
                    <div class="col-md-6">

                      <select name="lang_id[]" id="lang_id" class="select2" multiple="" required>
                        <?php
                            $sql="SELECT * FROM tbl_language WHERE `status`='1' ORDER BY `language_name`";
                            $res=mysqli_query($mysqli,$sql);
                            while($row=mysqli_fetch_assoc($res))
                            {
                        ?>                       
                          <option value="<?php echo $row['id'];?>"><?php echo ucfirst(strtolower($row['language_name']));?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Title :-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_title" id="video_title" value="" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Tags(Optional):-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_tags[]" id="video_tags" value="<?php  echo $row['video_tags'];?>" data-role="tagsinput" class="form-control" required>
                    </div>
                  </div>
                    
                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Upload Option :-</label>
                    <div class="col-md-6">                       
                      <select name="video_type" id="video_type" style="width:280px; height:25px;" class="select2" required>
                            <option value="">--Select Option--</option>                            
                            <option value="server_url">Server URL</option>
                            <option value="local">Browse From Computer</option>
                      </select>
                    </div>
                  </div>
                  <div id="video_url_display" class="form-group">
                    <label class="col-md-3 control-label">Video URL :-</label>
                    <div class="col-md-6">
                      <input type="text" name="video_url" id="video_url" value="" class="form-control">
                    </div>
                  </div>
                  <div id="video_local_display" class="form-group" style="display:none;">
                    <label class="col-md-3 control-label">Video Upload :-
                      <p class="control-label-help">(Note : Maximum <strong><?=$settings_details['video_file_size']?>MB</strong> file size)</p>
                    </label>
                    <div class="col-md-6">
                      <input type="file" name="video_local" id="video_local" value="" class="form-control">

                      <div id="uploadPreview" style="display: none;background: #eee;text-align: center;">
                        <video height="400" width="100%" class="video-preview" controls="controls"/>
                      </div>
                    </div>
                  </div><br>

                  

                  <div class="form-group">
                    <label class="col-md-3 control-label">Video Layout :-</label>
                    <div class="col-md-6">                       
                      <select name="video_layout" id="video_layout" style="width:280px; height:25px;" class="select2" required>
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                      </select>
                    </div>
                  </div>

                  <div id="thumbnail" class="form-group">
                    <label class="col-md-3 control-label">Thumbnail Image:-
                      <p class="control-label-help">(Recommended resolution: Landscape: 800x500,650x450  Portrait: 720X1280, 640X1136, 350x800)</p>
                    </label>

                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="video_thumbnail" value="" id="fileupload">
                       <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Send notification:-</label>
                    <div class="col-md-6" style="padding-top: 10px">
                      <input type="checkbox" id="ckbox_notify" class="cbx hidden" name="notify_user" value="true"/>
                      <label for="ckbox_notify" class="lbl"></label>
                    </div>
                  </div>
                  <br/>
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       

<script type="text/javascript" src="assets/bootstrap-tag/bootstrap-tagsinput.js"></script>

<script type="text/javascript">
  $('#video_tags').tagsinput();
</script>