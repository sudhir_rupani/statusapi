<?php 	
  
    $page_title="Settings";
    $active_page="settings";
    
    include("includes/connection.php");
		include("includes/header.php");
		require("includes/function.php");
		require("language/language.php");

    if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != ""){
      $url = $_SERVER['HTTP_REFERER'];
    }else{
      $url = "settings.php";
    }
	 
	
    $qry="SELECT * FROM tbl_settings where id='1'";
    $result=mysqli_query($mysqli,$qry);
    $settings_row=mysqli_fetch_assoc($result);

// exit;
    if(isset($_POST['submit']))
    {

      $img_res=mysqli_query($mysqli,"SELECT * FROM tbl_settings WHERE id='1'");
      $img_row=mysqli_fetch_assoc($img_res);
      

           if($_FILES['app_logo']['name']!="")
           {        

              unlink('images/'.$img_row['app_logo']);   

              $app_logo=$_FILES['app_logo']['name'];
              $pic1=$_FILES['app_logo']['tmp_name'];

              $tpath1='images/'.$app_logo;      
              copy($pic1,$tpath1);


                $data = array(
                // 'email_from'  =>  $_POST['email_from'],
                'app_name'  =>  $_POST['app_name'],
                'app_logo'  =>  $app_logo,  
                'app_description'  => addslashes($_POST['app_description']),
                'app_version'  =>  $_POST['app_version'],
                'app_author'  =>  $_POST['app_author'],
                'app_contact'  =>  $_POST['app_contact'],
                'app_email'  =>  $_POST['app_email'],   
                'app_website'  =>  $_POST['app_website'],
                'app_developed_by'  =>  $_POST['app_developed_by']                     

                );

      }
      else
      {
            $data = array(
            // 'email_from'  =>  $_POST['email_from'],
            'app_name'  =>  $_POST['app_name'],
            'app_description'  => addslashes($_POST['app_description']),
            'app_version'  =>  $_POST['app_version'],
            'app_author'  =>  $_POST['app_author'],
            'app_contact'  =>  $_POST['app_contact'],
            'app_email'  =>  $_POST['app_email'],   
            'app_website'  =>  $_POST['app_website'],
            'app_developed_by'  =>  $_POST['app_developed_by']
            );

      } 
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

          $_SESSION['msg']="11";
          header("Location: ".$url);
          exit;

    }

    if(isset($_POST['watermark_submit']))
    {
        if($_FILES['watermark_image']['name']!="")
        {         

            $watermark_image=$_FILES['watermark_image']['name'];
            $pic1=$_FILES['watermark_image']['tmp_name'];

            $tpath1='images/'.$watermark_image;      
            copy($pic1,$tpath1);

            $data = array
            (                 
              'watermark_on_off' => $_POST['watermark_on_off'],
              'watermark_image' => $watermark_image
            );
        }
        else
        {
            $data = array('watermark_on_off' => $_POST['watermark_on_off']);
        }


        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");


        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;

    }

    if(isset($_POST['admob_submit']))
    {

        if($_POST['banner_ad'])
        {
            $banner_ad="true";
        }
        else
        {
            $banner_ad="false";
        }

        if($_POST['interstital_ad'])
        {
            $interstital_ad="true";
        }
        else
        {
            $interstital_ad="false";
        }
       

        if($_POST['rewarded_video_ads'])
        {
            $rewarded_video_ads="true";
        }
        else
        {
            $rewarded_video_ads="false";
        }



        $data = array(
              'publisher_id'  =>  $_POST['publisher_id'],
              'interstital_ad'  =>  $interstital_ad,
              'interstital_ad_id'  =>  $_POST['interstital_ad_id'],
              'interstital_ad_click'  =>  $_POST['interstital_ad_click'],
              'banner_ad_type'  =>  $_POST['banner_ad_type'],
              'interstital_ad_type'  =>  $_POST['interstital_ad_type'],
              'banner_ad'  =>  $banner_ad,
              'banner_ad_id'  =>  $_POST['banner_ad_id'],
              'rewarded_video_ads'  => $rewarded_video_ads,
              'rewarded_video_ads_id'  =>  $_POST['rewarded_video_ads_id'],
              'rewarded_video_click'  =>  $_POST['rewarded_video_click'],
              'facebook_interstital_ad_id'  =>  $_POST['facebook_interstital_ad_id'],
              'facebook_banner_ad_id'  =>  $_POST['facebook_banner_ad_id']
        );


        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;

    }

    if(isset($_POST['api_submit']))
    {

        $data = array
        (
          'api_page_limit'  =>  trim($_POST['api_page_limit']),
          'api_latest_limit'  => '0',
          'api_cat_order_by'  =>  trim($_POST['api_cat_order_by']),
          'api_cat_post_order_by'  =>  trim($_POST['api_cat_post_order_by']),
          'api_all_order_by'  =>  trim($_POST['api_all_order_by']),
          'cat_show_home_limit'  =>  trim($_POST['cat_show_home_limit'])
        );
      
        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;   

    }


    if(isset($_POST['app_faq_submit']))
    {

        $data = array('app_faq'  =>  addslashes($_POST['app_faq']));

        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;
      
    }

    if(isset($_POST['app_submit']))
    {
        
        $data = array(
           'package_name'  =>  trim($_POST['package_name']),
           'otp_status' => $_POST['otp_status'] ? 'true' : 'false',
           'sms_otp_status' => $_POST['sms_otp_status'] ? 1 : 0,
           'default_youtube_url'  =>  trim($_POST['default_youtube_url']),
           'default_instagram_url'  =>  trim($_POST['default_instagram_url']),
           'auto_approve' => $_POST['auto_approve'] ? 'on' : 'off',
           'auto_approve_img' => $_POST['auto_approve_img'] ? 'on' : 'off',
           'auto_approve_gif' => $_POST['auto_approve_gif'] ? 'on' : 'off',
           'auto_approve_quote' => $_POST['auto_approve_quote'] ? 'on' : 'off',

           'user_video_upload_limit'  =>  trim($_POST['user_video_upload_limit']),
           'user_image_upload_limit'  =>  trim($_POST['user_image_upload_limit']),
           'user_gif_upload_limit'  =>  trim($_POST['user_gif_upload_limit']),
           'user_quotes_upload_limit'  =>  trim($_POST['user_quotes_upload_limit']),

           'video_upload_opt' => $_POST['video_upload_opt'] ? 'true' : 'false',
           'image_upload_opt' => $_POST['image_upload_opt'] ? 'true' : 'false',
           'gif_upload_opt' => $_POST['gif_upload_opt'] ? 'true' : 'false',
           'quotes_upload_opt' => $_POST['quotes_upload_opt'] ? 'true' : 'false',

           'video_file_size'  =>  trim($_POST['video_file_size']),
           'video_file_duration'  =>  trim($_POST['video_file_duration']),
           'image_file_size'  =>  trim($_POST['image_file_size']),
           'gif_file_size'  =>  trim($_POST['gif_file_size']),
           'delete_note'  =>  trim($_POST['delete_note']),
    		);

        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;
    }
 
 
    if(isset($_POST['app_pri_poly']))
    {

        $data = array('app_privacy_policy'  =>  addslashes($_POST['app_privacy_policy']));

        $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");

        $_SESSION['msg']="11";
        header("Location: ".$url);
        exit;
      
    }


    if(isset($_GET['payment_id'])){

      $id=trim($_GET['payment_id']);
      Delete('tbl_payment_mode','id='.$id);
      $_SESSION['msg']="12";
      header("Location: ".$url);
      exit;
    }

    require_once 'includes/lb_helper.php'; // Include LicenseBox external/client api helper file
    $api = new LicenseBoxAPI(); // Initialize a new LicenseBoxAPI object
    $verify_license=$api->verify_license(false);

?>

<?php if($verify_license['status']!=1){ 
  
  $sqlSettings="UPDATE tbl_settings SET `envato_purchased_status` = '0' WHERE `id`='1'";
  mysqli_query($mysqli, $sqlSettings);
?>
<div class="row">
  <div class="col-lg-12"><div class="alert alert-danger alert-dismissible fade in" role="alert">
    <h4 id="oh-snap!-you-got-an-error!">Something went to wrong!<a class="anchorjs-link" href="#oh-snap!-you-got-an-error!"><span class="anchorjs-icon"></span></a></h4>
        <p><?=$verify_license['message']?></p>    
    </div>
  </div>
</div>
<?php } ?>
 
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title">Settings</div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row mrg-top">
        <div class="col-md-12">
           
          <div class="col-md-12 col-sm-12">
            <?php if(isset($_SESSION['msg'])){?> 
             <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
            <?php unset($_SESSION['msg']);}?> 
          </div>
        </div>
      </div>
      <div class="card-body mrg_bottom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general_settings" name="General Settings" aria-controls="general_settings" role="tab" data-toggle="tab">General Settings</a></li>
            <li role="presentation"><a href="#app_settings" name="App Settings" aria-controls="app_settings" role="tab" data-toggle="tab">App Settings</a></li>
            <li role="presentation"><a href="#admob_settings" name="Admob Settings" aria-controls="admob_settings" role="tab" data-toggle="tab">Admob Settings</a></li> 
            <li role="presentation"><a href="#payment_settings" name="Payment Mode" aria-controls="payment_settings" role="tab" data-toggle="tab">Payment Mode</a></li> 
            <li role="presentation"><a href="#watermark_settings" name="Watermark" aria-controls="watermark_settings" role="tab" data-toggle="tab">Watermark </a></li>
            <li role="presentation"><a href="#api_settings" name="API Settings" aria-controls="api_settings" role="tab" data-toggle="tab">API</a></li>
            <li role="presentation"><a href="#api_faq" name="App FAQ" aria-controls="api_faq" role="tab" data-toggle="tab">App FAQ</a></li>
            
            <li role="presentation"><a href="#api_privacy_policy" name="Privacy Policy" aria-controls="api_privacy_policy" role="tab" data-toggle="tab"> Privacy Policy</a></li>
            <!-- <li role="presentation"><a href="#api_privacy_policy" name="Privacy Policy" aria-controls="api_privacy_policy" role="tab" data-toggle="tab"> Privacy Policy</a></li> -->
             
        </ul>
      
       <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="general_settings">	  
          <form action="" name="settings_from" method="post" class="form form-horizontal" enctype="multipart/form-data">
          
          <div class="section">
            <div class="section-body">
              <div class="form-group" style="">
                <label class="col-md-4 control-label">Email <span style="color: red">*</span>:-
                  <p class="control-label-help" style="color: red">(<strong>Note:</strong> This email is required when user want to contact you.)</p>
                </label>
                <div class="col-md-6">
                  <input type="text" name="app_email" id="app_email" value="<?php echo $settings_row['app_email'];?>" class="form-control">
                </div>
              </div>                   
              <div class="form-group">
                <label class="col-md-4 control-label">App Name :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_name" id="app_name" value="<?php echo $settings_row['app_name'];?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">App Logo :-</label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="app_logo" id="fileupload">
                     
                    	<?php if($settings_row['app_logo']!="") {?>
                    	  <div class="fileupload_img"><img type="image" src="images/<?php echo $settings_row['app_logo'];?>" alt="image" style="width: 100px;height: 100px;" /></div>
                    	<?php } else {?>
                    	  <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="image" /></div>
                    	<?php }?>
                    
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">App Description :-</label>
                <div class="col-md-6">
             
                  <textarea name="app_description" id="app_description" class="form-control"><?php echo $settings_row['app_description'];?></textarea>

                  <script>CKEDITOR.replace( 'app_description' );</script>
                </div>
              </div>
              <div class="form-group">&nbsp;</div>                 


              <div class="form-group">
                <label class="col-md-4 control-label">App Version :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_version" id="app_version" value="<?php echo $settings_row['app_version'];?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Author :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_author" id="app_author" value="<?php echo $settings_row['app_author'];?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Contact :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_contact" id="app_contact" value="<?php echo $settings_row['app_contact'];?>" class="form-control">
                </div>
              </div>     
                              
               <div class="form-group">
                <label class="col-md-4 control-label">Website :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_website" id="app_website" value="<?php echo $settings_row['app_website'];?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Developed By :-</label>
                <div class="col-md-6">
                  <input type="text" name="app_developed_by" id="app_developed_by" value="<?php echo $settings_row['app_developed_by'];?>" class="form-control">
                </div>
              </div> 
              <div class="form-group">
                <div class="col-md-9 col-md-offset-4">
                  <button type="submit" name="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
           </form>
          </div>
          <div role="tabpanel" class="tab-pane" id="admob_settings">
            <form action="" name="admob_settings" method="post" class="form form-horizontal" enctype="multipart/form-data">
             <div class="section">
              <div class="section-body">
                <div class="form-group">
                <label class="col-md-2 control-label">Publisher ID <a href="#target-content5"></a>:-</label>
                <div class="col-md-5">
                  <input type="text" name="publisher_id" id="publisher_id" value="<?php echo $settings_row['publisher_id'];?>" class="form-control">
                </div>
                </div>
                 <div class="col-md-8 col-md-offset-1">                
      						<div id="target-content5">  
      						  <div id="target-inner">
      						  <a href="#publisher_id" class="close">X</a>
      						  <img src="images/publisher_id.png" target="_blank" alt="publisher_id" />   
      						  </div>
      						</div>
    						  <div class="banner_ads_block">
    							  <div class="banner_ad_item">
    								<label class="control-label">Banner Ads :-</label>
    								 <div class="row toggle_btn">
    								  <input type="checkbox" id="checked1" name="banner_ad" value="true" class="cbx hidden" <?php if($settings_row['banner_ad']=='true'){ echo 'checked';}?> />
    								  <label for="checked1" class="lbl"></label>
    								</div>
    							  </div>
    								<div class="col-md-12">
    								<div class="form-group">
    								  <label class="col-md-4 control-label">Choose Banner Ad:-</label>
    								  <div class="col-md-8">
    								    <select name="banner_ad_type" id="banner_ad_type" class="select2">
    										<option value="facebook" <?php if($settings_row['banner_ad_type']=='facebook'){?>selected<?php }?>>Facebook</option>
    										<option value="admob" <?php if($settings_row['banner_ad_type']=='admob'){?>selected<?php }?>>Admob</option>								
    									</select>
    								  </div>
    								</div>
    								<div class="form-group">
    								  <label class="col-md-4 control-label">Admob banner id:-</label>
    								  <div class="col-md-8">
    								  <input type="text" name="banner_ad_id" id="banner_ad_id" value="<?php echo $settings_row['banner_ad_id'];?>" class="form-control">
    								  </div>
    								</div>   
    								<div class="form-group">
    								 <label class="col-md-4 control-label mr_bottom20">Facebook banner id :-</label>
    								  <div class="col-md-8">
    						        <input type="text" name="facebook_banner_ad_id" id="facebook_banner_ad_id" value="<?php echo $settings_row['facebook_banner_ad_id'];?>" class="form-control">
    								  </div>
    								</div>  
    							  </div>
    							</div> 				
      					  <div class="banner_ads_block">
    							  <div class="banner_ad_item">
    								<label class="control-label">Interstital Ads :-</label>
    								 <div class="row toggle_btn">
    								  <input type="checkbox" id="checked" name="interstital_ad" value="true" class="cbx hidden" <?php if($settings_row['interstital_ad']=='true'){ echo 'checked';}?> />
    								  <label for="checked" class="lbl"></label>
    								</div>
    							  </div>
    							  <div class="col-md-12">
    								<div class="form-group">
    								  <label class="col-md-4 control-label">Choose Interstital Ad:-</label>
    								  <div class="col-md-8">
    								    <select name="interstital_ad_type" id="interstital_ad_type" class="select2">
    										<option value="facebook" <?php if($settings_row['interstital_ad_type']=='facebook'){?>selected<?php }?>>Facebook</option>
    										<option value="admob" <?php if($settings_row['interstital_ad_type']=='admob'){?>selected<?php }?>>Admob</option>								
    									</select>
    								  </div>
    								</div>
    								
    								<div class="form-group">
    								  <label class="col-md-4 control-label">Admob Interstital id:-</label>
    								  <div class="col-md-8">
    						        <input type="text" name="interstital_ad_id" id="interstital_ad_id" value="<?php echo $settings_row['interstital_ad_id'];?>" class="form-control">
    								  </div>
    								</div>   
    								<div class="form-group">
    								 <label class="col-md-4 control-label">Facebook Interstital id :-</label>
    								  <div class="col-md-8">
    						        <input type="text" name="facebook_interstital_ad_id" id="facebook_interstital_ad_id" value="<?php echo $settings_row['facebook_interstital_ad_id'];?>" class="form-control">
    								  </div>
    								</div>  
    								<div class="form-group">
                      <label class="col-md-4 control-label">Interstital Clicks :-</label>
    								  <div class="col-md-8">
    				       			<input type="text" name="interstital_ad_click" id="interstital_ad_click" value="<?php echo $settings_row['interstital_ad_click'];?>" class="form-control">
    								  </div>
    								</div>  
    								</div> 
    							  </div>
    							</div>
                  <div class="col-md-8 col-md-offset-1">
                     <div class="banner_ads_block">
                      <div class="banner_ad_item">
                        <label class="control-label">Rewarded Video Ads:-</label>
                        <div class="row toggle_btn">
                          <input type="checkbox" id="checked3" name="rewarded_video_ads" value="true" class="cbx hidden" <?php if($settings_row['rewarded_video_ads']=='true'){ echo 'checked';}?>/>
                          <label for="checked3" class="lbl"></label>
                        </div>
                      </div>
                      <div class="col-md-12">             
                        <div class="form-group" id="rewarded_video_ad_id">                              
                            <p class="field_lable">Rewarded Video Ad ID :-
                            <span>
                            <a id="button" href="#target-content3" class="lable_tooltip">(?)
                              <span class="tooltip_text">Rewarded Video Ad ID</span>
                            </a>
                            <div id="target-content3">  
                              <div id="target-inner">
                              <a href="#rewarded_video_ad_id" class="close">X</a>
                              <img src="images/admob_banner_id.png" alt="admob_banner_id" />   
                              </div>
                            </div>
                            </span>
                          </p>
                          <div class="col-md-12">
                            <input type="text" name="rewarded_video_ads_id" id="rewarded_video_ads_id" value="<?php echo $settings_row['rewarded_video_ads_id'];?>" class="form-control">
                          </div>
                          <p class="field_lable">Rewarded Ad After Activity Clicks :-</p>
                          <div class="col-md-12">                                 
                              <input type="text" name="rewarded_video_click" id="rewarded_video_click" value="<?php echo $settings_row['rewarded_video_click'];?>" class="form-control ads_click">                                 
                          </div>
                        </div>                            
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8 col-md-offset-1">
    		            <button type="submit" name="admob_submit" class="btn btn-primary">Save</button>
                  </div>
              </div>
            </div>
          </form>
        </div>

        <div role="tabpanel" class="tab-pane" id="payment_settings">

            <div class="add_btn_primary"> 
              <a href="payment_mode.php?add" class="btn_edit">Add New</a>
            </div>
            <div class="clearfix"></div>
            <br/>
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th width="100">Sr No.</th>
                  <th>Payment Mode</th>
                  <th>Status</th>
                  <th class="cat_action_list" style="width:60px">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php 
                $sql="SELECT * FROM tbl_payment_mode ORDER BY `id` DESC";
                $res=mysqli_query($mysqli, $sql) or die(mysqli_error($mysqli));
                $no=1;
                while ($row=mysqli_fetch_assoc($res)) {
                ?>
                <tr>
                  <td><?=$no++?></td>
                  <td><?=$row['mode_title']?></td>
                  <td>
                    <?php if($row['status']!="0"){?>
                      <a title="Change Status" class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="deactive" data-column="status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Enable</span></span></a>

                    <?php }else{?>
                      <a title="Change Status" class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="active" data-column="status"><span class="badge badge-danger badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Disable </span></span></a>
                    <?php }?>
                  </td>
                  <td nowrap="">
                    <a href="payment_mode.php?edit_id=<?php echo $row['id'];?>" data-toggle="tooltip" data-tooltip="Edit" class="btn btn-primary btn_edit"><i class="fa fa-edit"></i></a>
                    <a href="?payment_id=<?php echo $row['id'];?>" data-toggle="tooltip" data-tooltip="Delete" class="btn btn-danger btn_edit" onclick="return confirm('Are you sure you want to delete this mode?');"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php
                }
              ?>
              </tbody>
          </table>
        </div> 
        <div role="tabpanel" class="tab-pane" id="watermark_settings">
          <form action="" name="settings_api" method="post" class="form form-horizontal" enctype="multipart/form-data" id="api_form">
            
            <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Watermark:-</label>
                <div class="col-md-6">
                    <select name="watermark_on_off" id="watermark_on_off" class="select2">
                      <option value="true" <?php if($settings_row['watermark_on_off']=='true'){?>selected<?php }?>>ON</option>
                      <option value="false" <?php if($settings_row['watermark_on_off']=='false'){?>selected<?php }?>>OFF</option>              
                    </select>                        
                </div>                   
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Watermark Image :-
                  <p class="control-label-help">(Recommended resolution: 100x100, 80x80)</p>
                </label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="watermark_image" id="fileupload">
                     
                      <?php if($settings_row['watermark_image']!="") {?>
                        <div class="fileupload_img"><img type="image" src="images/<?php echo $settings_row['watermark_image'];?>" alt="image" style="width: 100px;height: 100px;" /></div>
                      <?php } else {?>
                        <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="image" /></div>
                      <?php }?>
                    
                  </div>
                </div>
              </div>
              
              <div class="form-group">
              <div class="col-md-9 col-md-offset-3">
                <button type="submit" name="watermark_submit" class="btn btn-primary">Save</button>
              </div>
              </div>
            </div>
            </div>
          </form>
        </div>
         <div role="tabpanel" class="tab-pane" id="app_settings">
          <form action="" name="settings_api" method="post" class="form form-horizontal" enctype="multipart/form-data" id="api_form">
            
            <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Enable/Disable OTP Verification:-</label>
                <div class="col-md-6">
                  <div class="row toggle_btn" style="margin-top: 5px">
                      <input type="checkbox" id="chk_otp" name="otp_status" value="1" class="cbx hidden" <?php if($settings_row['otp_status']=='true'){ echo 'checked'; }?>/>
                      <label for="chk_otp" class="lbl"></label>
                  </div>
                </div>                   
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Enable/Disable MOBILE NUMBER OTP :-</label>
                <div class="col-md-6">
                  <div class="row toggle_btn" style="margin-top: 5px">
                      <input type="checkbox" id="sms_otp_status_id" name="sms_otp_status" value="1" class="cbx hidden" <?php if($settings_row['sms_otp_status']==1){ echo 'checked'; }?>/>
                      <label for="sms_otp_status_id" class="lbl"></label>
                  </div>
                </div>                   
              </div>
              <br/>
              <?php if(!file_exists('verification.php')){ ?>
              <div class="form-group">
                <label class="col-md-3 control-label">Android Package Name :-
                  <p class="control-label-help">(More info in Android Doc)</p>
                </label>
                <div class="col-md-6">
                  <input type="text" name="package_name" id="package_name" value="<?php echo $settings_row['package_name'];?>" class="form-control" placeholder="com.example.myapp">
                </div>
              </div>
              <?php }else{ echo '<input type="hidden" name="package_name" id="package_name" value="'.$settings_row['package_name'].'" class="form-control" placeholder="com.example.myapp">';} ?>
              <div class="form-group">
                <label class="col-md-3 control-label">Default Youtube URL:-</label>
                <div class="col-md-6">
                  <input type="text" name="default_youtube_url" id="default_youtube_url" value="<?php echo $settings_row['default_youtube_url'];?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Default Instagram URL:-</label>
                <div class="col-md-6">
                  <input type="text" name="default_instagram_url" id="default_instagram_url" value="<?php echo $settings_row['default_instagram_url'];?>" class="form-control">
                </div>
              </div>
               <div class="form-group">
                <label class="col-md-3 control-label">Delete User Note:-</label>
                <div class="col-md-6">
                  <textarea name="delete_note" rows="5" class="form-control" placeholder=""><?php echo $settings_row['delete_note'];?></textarea>
                </div>
              </div>
              <hr/>
              <p style="font-size: 16px;font-weight: 600">Upload Status Option</p>
              <p style="color: red"><strong>Note: </strong>If disable then its <strong>hidden</strong> from Upload Status Option in Application</p>
              <hr/>
              <div class="form-group">
                <label class="col-md-3 control-label">Video Status:-</label>
                <div class="col-md-9"> 
      						<div class="video_setting_item">
      						  <div style="padding-top: 15px">
      							<input type="checkbox" id="chk_video_upload" class="cbx hidden" name="video_upload_opt" value="true" <?php if($settings_details['video_upload_opt']=='true'){ echo 'checked';}?>/>
      							<label for="chk_video_upload" class="lbl"></label>
                    <span style="font-style: italic;color: #f00">(Now you are able to hide and show option of upload video status from app)</span>
      						  </div>
      						  <div class="row" style="padding: 0px;margin-top: 10px">
        							<label class="col-md-4 control-label" style="padding-top: 6px">Video File Upload Size:-
        							  <p style="color: red">Set file size in <strong>MB</strong></p>
        							</label>
        							<div class="col-md-2">
        							  <input type="number" name="video_file_size" min="1" value="<?php echo $settings_row['video_file_size'];?>" class="form-control limit_1">
        							</div>
        							<label class="col-md-4 control-label" style="padding-top: 6px">Video Duration:-
        							  <p style="color: red">Set video duration in <strong>SECOND</strong></p>
        							</label>
        							<div class="col-md-2">
        							  <input type="number" name="video_file_duration" min="1" value="<?php echo $settings_row['video_file_duration'];?>" class="form-control limit_1">
        							</div>
      						  </div>
      					  </div>
                </div>
              </div>

              <br/>
              <div class="form-group">
                <label class="col-md-3 control-label">Image Status:-</label>
                <div class="col-md-9"> 
      						<div class="video_setting_item">
      						  <div style="padding-top: 15px">
      							<input type="checkbox" id="chk_image_upload" class="cbx hidden" name="image_upload_opt" value="true" <?php if($settings_details['image_upload_opt']=='true'){ echo 'checked';}?>/>
      							<label for="chk_image_upload" class="lbl"></label>
                    <span style="font-style: italic;color: #f00">(Now you are able to hide and show option of upload image status from app)</span>
      						  </div>
      						  <div class="row" style="padding: 0px;margin-top: 10px">
        							<label class="col-md-4 control-label" style="padding-top: 6px">Image File Upload Size:-
        							  <p style="color: red">Set file size in <strong>MB</strong></p>
        							</label>
        							<div class="col-md-2">
        							  <input type="number" min="1" name="image_file_size" value="<?php echo $settings_row['image_file_size'];?>" class="form-control limit_1">
        							</div>
      						  </div>
      					  </div>
                </div>
              </div>
              <br/>
              <div class="form-group">
                <label class="col-md-3 control-label">GIF Status:-</label>
                <div class="col-md-9"> 
      						<div class="video_setting_item">
      						  <div style="padding-top: 15px">
      							<input type="checkbox" id="chk_gif_upload" class="cbx hidden" name="gif_upload_opt" value="true" <?php if($settings_details['gif_upload_opt']=='true'){ echo 'checked';}?>/>
      							<label for="chk_gif_upload" class="lbl"></label>
                    <span style="font-style: italic;color: #f00">(Now you are able to hide and show option of upload gif status from app)</span>
      						  </div>
      						  <div class="row" style="padding: 0px;margin-top: 10px">
        							<label class="col-md-4 control-label" style="padding-top: 6px">GIF File Upload Size:-
        							  <p style="color: red">Set file size in <strong>MB</strong></p>
        							</label>
        							<div class="col-md-2">
        							  <input type="number" min="1" name="gif_file_size" value="<?php echo $settings_row['gif_file_size'];?>" class="form-control limit_1">
        							</div>
      						  </div>
      					  </div>
                </div>
              </div>
              <br/>
              <div class="form-group">
                <label class="col-md-3 control-label">Quotes Status:-</label>
                <div class="col-md-9"> 
      					  <div class="video_setting_item">	
      						  <div style="padding-top: 7px">
      							<input type="checkbox" id="chk_quote_upload" class="cbx hidden" name="quotes_upload_opt" value="true" <?php if($settings_details['quotes_upload_opt']=='true'){ echo 'checked';}?>/>
      							<label for="chk_quote_upload" class="lbl"></label>
                    <span style="font-style: italic;color: #f00">(Now you are able to hide and show option of upload quotes status from app)</span>
      						  </div>
      					  </div>
                </div>
              </div>
              <hr/>
              <p style="font-size: 16px;font-weight: 600">Per day limit for status uploaded by users</p>
              <hr/>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Video Upload Limit:-</label>
                    <div class="col-md-12" style="margin-top: 15px">
                      <input type="number" name="user_video_upload_limit" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="user_video_upload_limit" value="<?php echo $settings_row['user_video_upload_limit'];?>" class="form-control"> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Image Upload Limit:-</label>
                    <div class="col-md-12" style="margin-top: 15px">
                      <input type="number" name="user_image_upload_limit" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="user_image_upload_limit" value="<?php echo $settings_row['user_image_upload_limit'];?>" class="form-control"> 
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">GIF Upload Limit:-</label>
                    <div class="col-md-12" style="margin-top: 15px">
                      <input type="number" name="user_gif_upload_limit" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="user_gif_upload_limit" value="<?php echo $settings_row['user_gif_upload_limit'];?>" class="form-control"> 
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Quotes Upload Limit:-</label>
                    <div class="col-md-12" style="margin-top: 15px">
                      <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="user_quotes_upload_limit" id="user_quotes_upload_limit" value="<?php echo $settings_row['user_quotes_upload_limit'];?>" class="form-control"> 
                    </div>
                  </div>
                </div>
              </div>
              
              <hr/>
              <p style="font-size: 16px;font-weight: 600">Auto approved status of verified users</p>
              <p style="color: red"><strong>Note: </strong>Enable to automatically approve uploaded <strong>status</strong> by verified users. If disabled then it will not be automatically approved. This feature is for only verified users.</p>
              <hr/>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Video Status:-</label>
                    <div class="col-md-12"> 
                      <div style="padding-top: 15px">
                        <input type="checkbox" id="chk_video" class="cbx hidden" name="auto_approve" value="on" <?php if($settings_details['auto_approve']=='on'){ echo 'checked';}?>/>
                        <label for="chk_video" class="lbl"></label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Image Status:-</label>
                    <div class="col-md-12"> 
                      <div style="padding-top: 15px">
                        <input type="checkbox" id="chk_image" class="cbx hidden" name="auto_approve_img" value="on" <?php if($settings_details['auto_approve_img']=='on'){ echo 'checked';}?>/>
                        <label for="chk_image" class="lbl"></label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">GIF Status:-</label>
                    <div class="col-md-12"> 
                      <div style="padding-top: 15px">
                        <input type="checkbox" id="chk_gif" class="cbx hidden" name="auto_approve_gif" value="on" <?php if($settings_details['auto_approve_gif']=='on'){ echo 'checked';}?>/>
                        <label for="chk_gif" class="lbl"></label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Quotes Status:-</label>
                    <div class="col-md-12"> 
                      <div style="padding-top: 15px">
                        <input type="checkbox" id="chk_quote" class="cbx hidden" name="auto_approve_quote" value="on" <?php if($settings_details['auto_approve_quote']=='on'){ echo 'checked';}?>/>
                        <label for="chk_quote" class="lbl"></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br/>
		          <hr/>
              <div class="form-group">
                <div class="col-md-7 col-md-offset-5">
                  <button type="submit" name="app_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
            </div>
          </form>
        </div> 

          <div role="tabpanel" class="tab-pane" id="api_settings">   
            <form action="" name="settings_api" method="post" class="form form-horizontal" enctype="multipart/form-data">
            
          <div class="section">
            <div class="section-body">
              
              <div class="form-group">
                <label class="col-md-3 control-label">Pagination Limit:-</label>
                <div class="col-md-6">
                  <input type="number" name="api_page_limit" id="api_page_limit" value="<?php echo $settings_row['api_page_limit'];?>" class="form-control"> 
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Category Show in Home Limit:-</label>
                <div class="col-md-6">
                  <input type="number" onkeypress="isNumberKey(this)" min="0" name="cat_show_home_limit" id="cat_show_home_limit" value="<?php echo $settings_row['cat_show_home_limit'];?>" class="form-control"> 
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-md-3 control-label">Category List Order By:-</label>
                <div class="col-md-6">
                    <select name="api_cat_order_by" id="api_cat_order_by" class="select2">
                      <option value="cid" <?php if($settings_row['api_cat_order_by']=='cid'){?>selected<?php }?>>ID</option>
                      <option value="category_name" <?php if($settings_row['api_cat_order_by']=='category_name'){?>selected<?php }?>>Name</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Category Status Order:-</label>
                <div class="col-md-6">
                   
                    
                    <select name="api_cat_post_order_by" id="api_cat_post_order_by" class="select2">
                      <option value="ASC" <?php if($settings_row['api_cat_post_order_by']=='ASC'){?>selected<?php }?>>ASC</option>
                      <option value="DESC" <?php if($settings_row['api_cat_post_order_by']=='DESC'){?>selected<?php }?>>DESC</option>
          
                    </select>
                    
                </div>
               
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">All Video Order:-</label>
                <div class="col-md-6">
                   
                    
                    <select name="api_all_order_by" id="api_all_order_by" class="select2">
                      <option value="ASC" <?php if($settings_row['api_all_order_by']=='ASC'){?>selected<?php }?>>ASC</option>
                      <option value="DESC" <?php if($settings_row['api_all_order_by']=='DESC'){?>selected<?php }?>>DESC</option>
          
                    </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="api_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
           </form>
          </div> 
          <div role="tabpanel" class="tab-pane" id="api_faq">   
          <form action="" name="api_faq" method="post" class="form form-horizontal" enctype="multipart/form-data">
            
          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">App FAQ :-</label>
                <div class="col-md-6">
             
                  <textarea name="app_faq" id="app_faq" class="form-control"><?php echo stripslashes($settings_row['app_faq']);?></textarea>

                  <script>CKEDITOR.replace( 'app_faq' );</script>
                </div>
              </div>
              
              <br>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="app_faq_submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
           </form>
          </div> 
          <div role="tabpanel" class="tab-pane" id="api_privacy_policy">   
            <form action="" name="api_privacy_policy" method="post" class="form form-horizontal" enctype="multipart/form-data">
          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">App Privacy Policy :-</label>
                <div class="col-md-6">
             
                  <textarea name="app_privacy_policy" id="privacy_policy" class="form-control"><?php echo stripslashes($settings_row['app_privacy_policy']);?></textarea>

                  <script>CKEDITOR.replace( 'privacy_policy' );</script>
                </div>
              </div>
              
              <br>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="app_pri_poly" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
           </form>
          </div>

        </div>   

      </div>
    </div>
  </div>
</div>

        
<?php include("includes/footer.php");?> 

<script type="text/javascript">

  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
    document.title = $(this).attr("name")+" | <?=APP_NAME?>";
  });

  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
  }

  $("#interstital_ad_click").blur(function(e){
    if($(this).val() == '')
      $(this).val("0");
  });
  $("#rewarded_video_click").blur(function(e){
    if($(this).val() == '')
      $(this).val("0");
  });

   $(".toggle_btn_a").on("click",function(e){
    e.preventDefault();
    
    var _for=$(this).data("action");
    var _id=$(this).data("id");
    var _column=$(this).data("column");
    var _table='tbl_payment_mode';

    $.ajax({
      type:'post',
      url:'processData.php',
      dataType:'json',
      data:{id:_id,for_action:_for,column:_column,table:_table,'action':'toggle_status','tbl_id':'id'},
      success:function(res){
          console.log(res);
          if(res.status=='1'){
            location.reload();
          }
        }
    });

  });

   $(".limit_1").blur(function(e){
    if($(this).val() < 1)
    {
      alert("Value must be >= 1");
      $(this).val("1");
    }
  });

  $("input[name='cat_show_home_limit']").blur(function(e){
    if($(this).val() == '')
    {
      $(this).val("0");
    }
  });

</script>